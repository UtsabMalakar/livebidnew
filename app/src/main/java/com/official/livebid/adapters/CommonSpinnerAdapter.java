package com.official.livebid.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.official.livebid.objects.EnquiryTypeModel;

import java.util.ArrayList;

/**
 * Created by Ics on 2/3/2016.
 */
public class CommonSpinnerAdapter implements SpinnerAdapter {
    private Context context;
    private ArrayList<EnquiryTypeModel> spinnerItems;
    int selectedCat=0;


    public CommonSpinnerAdapter(Context context, ArrayList<EnquiryTypeModel> spinnerItems) {
        this.context = context;
        this.spinnerItems = spinnerItems;
    }

    public int  getIndexById(String id){
        for(int i=1;i<spinnerItems.size();i++){
            if((spinnerItems.get(i).getId()).equals(id)){
                selectedCat=i;
            }
        }
        return selectedCat;
    }

    public String  getId(int i){
        return spinnerItems.get(i).getId();
    }

    public int getPosition(String string){
        return spinnerItems.indexOf(string);
    }

    // list of values the user can select after the user presses the spinner, where each value in the list can be adapted with the convertView parameter

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(android.R.layout.simple_spinner_item, null);

        }
        ((TextView) convertView).setText(spinnerItems.get(position).getValue());
        return convertView;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return spinnerItems.size();
    }

    @Override
    public Object getItem(int i) {
        return spinnerItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return spinnerItems.indexOf(getItem(i));
    }


    @Override
    public boolean hasStableIds() {
        return false;
    }

    // the value shown on the spinner before the user presses the spinner, where each value view can be adjusted with the convertView parameter

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) View.inflate(context, android.R.layout.simple_spinner_item, null);
        textView.setText(spinnerItems.get(position).getValue());
        return textView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}

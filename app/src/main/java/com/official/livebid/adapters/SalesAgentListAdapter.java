package com.official.livebid.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.activities.SalesAgent;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.objects.AgentInfo;
import com.official.livebid.staticClasses.ContactAgencyModal;
import com.official.livebid.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ics on 2/3/2016.
 */
public class SalesAgentListAdapter extends BaseAdapter {
    private Context context;
    ArrayList<AgentInfo> salesAgents;
    Activity activity;

    public SalesAgentListAdapter(Context context, ArrayList<AgentInfo> salesAgents) {
        this.context = context;
        activity=(Activity)context;
        this.salesAgents = salesAgents;
    }

    @Override
    public int getCount() {
        return salesAgents.size();
    }

    @Override
    public Object getItem(int i) {
        return salesAgents.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.sales_agents_list_item, null);
            holder = new ViewHolder();
            holder.ivAgentImg= (ImageView) view.findViewById(R.id.iv_agent_img);
            holder.tvName= (TextView) view.findViewById(R.id.tv_agent_name);
            holder.tvSalesAgentCode= (TextView) view.findViewById(R.id.tv_sales_agent_num);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final AgentInfo salesAgent=salesAgents.get(i);
        holder.tvName.setText(salesAgent.getName());
        holder.tvSalesAgentCode.setText(salesAgent.getUniqueNumber());
        if(!salesAgent.getProfileImage().trim().isEmpty()) {
            Picasso.with(context)
                    .load(salesAgent.getProfileImage())
                    .resize((int) CommonMethods.pxFromDp(activity, 150), (int) CommonMethods.pxFromDp(activity, 150))
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(holder.ivAgentImg);
        }else{
            Picasso.with(context)
                    .load(R.drawable.io_white_profile_default)
                    .transform(new CircleTransform())
                    .into(holder.ivAgentImg);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactAgencyModal.agentId=salesAgent.getUserId();
                ContactAgencyModal.agentName=salesAgent.getName();
                ((SalesAgent)context).finish();
                ((SalesAgent)context).overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);

            }
        });

        return view;
    }
    private  static  class ViewHolder{
        ImageView ivAgentImg;
        TextView tvName, tvSalesAgentCode;
    }
    public void addAll(ArrayList<AgentInfo> data) {
        for(AgentInfo ag:data){
            salesAgents.add(ag);
        }
    }
}

package com.official.livebid.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.activities.AgencyProfile;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.objects.PropertyInfo;
import com.official.livebid.objects.PropertyListInfo;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Ics on 2/1/2016.
 */
public class AgentProfilePropertiesListingsAdapter extends BaseAdapter {
    Context ctx;
    ArrayList<PropertyInfo> properties;
    Activity activity;

    public AgentProfilePropertiesListingsAdapter(Context ctx, ArrayList<PropertyInfo> propertyListInfos) {
        this.ctx = ctx;
        activity=(Activity)ctx;
        this.properties = propertyListInfos;
    }

    @Override
    public int getCount() {
        return properties.size();
    }

    @Override
    public Object getItem(int i) {
        return properties.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.agent_profile_property_list_item, null);
            holder = new ViewHolder();
            holder.ivPropertyImg= (ImageView) view.findViewById(R.id.iv_property_img);
            holder.tvPropertyStreet= (TextView) view.findViewById(R.id.tv_property_street);
            holder.tvPropertySuburbPostcode= (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
            holder.tvPrice= (TextView) view.findViewById(R.id.tv_property_price);
            holder.tvNosBathroom= (TextView) view.findViewById(R.id.tv_nosBathroom);
            holder.tvNosBedroom= (TextView) view.findViewById(R.id.tv_nosBedroom);
            holder.tvNosParking= (TextView) view.findViewById(R.id.tv_nosParking);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final PropertyInfo propertyInfo= properties.get(i);
        holder.tvPropertyStreet.setText(propertyInfo.getStreet());
        holder.tvPropertySuburbPostcode.setText(propertyInfo.getSuburb()+" " + propertyInfo.getState() + " "+propertyInfo.getPostcode());
//        holder.tvPrice.setText(propertyInfo.getPrice());
        holder.tvPrice.setText( "$" + NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(propertyInfo.getPrice())));
        holder.tvNosBathroom.setText(propertyInfo.getBathrooms());
        holder.tvNosBedroom.setText(propertyInfo.getBedrooms());
        holder.tvNosParking.setText(propertyInfo.getParking());

        if(!propertyInfo.getProperty_image().trim().isEmpty()) {
            Picasso.with(ctx)
                    .load(propertyInfo.getProperty_image())
                    .resize((int) CommonMethods.pxFromDp(activity, 150), (int) CommonMethods.pxFromDp(activity, 150))
                    .placeholder(R.mipmap.img_property_placeholder)
                    .centerCrop()
                    .into(holder.ivPropertyImg);
        }else{
            Picasso.with(ctx)
                    .load(R.drawable.img_coming_soon)
                    .resize((int) CommonMethods.pxFromDp(activity, 150), (int) CommonMethods.pxFromDp(activity, 150))
                    .centerCrop()
                    .placeholder(R.mipmap.img_property_placeholder)
                    .into(holder.ivPropertyImg);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Opener.PropertyDetails(activity,CommonMethods.mapPropInfoToPropListInfo(propertyInfo));

            }
        });

        return view;
    }
    private  static  class ViewHolder{
        ImageView ivPropertyImg;
        TextView tvPropertyStreet, tvPropertySuburbPostcode,
                 tvPrice, tvNosBedroom, tvNosBathroom,
                tvNosParking;
    }
    public void addAll(ArrayList<PropertyInfo> data) {
        for(PropertyInfo prop:data){
            properties.add(prop);
        }
    }

}

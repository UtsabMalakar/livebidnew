package com.official.livebid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.objects.SearchObject;

import java.util.ArrayList;

/**
 * Created by Robz on 1/8/2016.
 */
public class SearchAdapter extends BaseAdapter {

    Context context;
    ArrayList<SearchObject> alSearchObject;

    public SearchAdapter(Context context, ArrayList<SearchObject> alSearchObject) {
        this.context = context;
        this.alSearchObject = alSearchObject;
    }

    @Override
    public int getCount() {
        return alSearchObject.size();
    }

    @Override
    public Object getItem(int i) {
        return alSearchObject.get(i);
    }


    // Returns postcode;
    @Override
    public long getItemId(int i) {
        return Long.parseLong(alSearchObject.get(i).getPostcodeIds());
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.autosearch_list_item, viewGroup, false);
        }
        TextView tvSuburb = (TextView) view.findViewById(R.id.tv_suburb);
        TextView tvStatePostCodeCountry = (TextView) view.findViewById(R.id.tv_state_postcode_country);

        tvSuburb.setText(alSearchObject.get(i).getSuburb());
        String state_postCode_country = alSearchObject.get(i).getState() + " " + alSearchObject.get(i).getPostcode() + ", Australia";
        tvStatePostCodeCountry.setText(state_postCode_country);

        return view;
    }

    public void clearList() {
        if (alSearchObject.size() > 0) {
            alSearchObject.clear();
            notifyDataSetChanged();
        }

    }
}

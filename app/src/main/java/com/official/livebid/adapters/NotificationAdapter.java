package com.official.livebid.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.objects.NotificationModel;
import com.official.livebid.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ics on 2/18/2016.
 */
public class NotificationAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<NotificationModel> notifications;
    private AppMode appMode;

    public NotificationAdapter(Context context, ArrayList<NotificationModel> notifications, AppMode appMode) {
        this.context = context;
        this.notifications = notifications;
        this.appMode = appMode;
    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    @Override
    public NotificationModel getItem(int i) {
        return notifications.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.notification_list_item, null);
            holder = new ViewHolder();
            holder.ivNotificationImage = (ImageView) view.findViewById(R.id.iv_notification_img);
            holder.tvNotificationDesc = (TextView) view.findViewById(R.id.tv_notification);
            holder.tvNotifiedDate = (TextView) view.findViewById(R.id.tv_notification_time);
            holder.llNotification = (LinearLayout) view.findViewById(R.id.ll_notification);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        NotificationModel notification = notifications.get(i);

        //msg displayed according msg type
        Spanned msgInNotification;
        if (appMode == AppMode.agent)
            msgInNotification = notification.setNotificationMsgAgent();
        else
            msgInNotification = notification.setNotificationMsgUser(context);

        holder.tvNotificationDesc.setText(msgInNotification);
        holder.tvNotifiedDate.setText(DateTimeHelper.getLaggingTimeFromTimeStamp(notification.getDate_time_stamp()*1000));
        if (notification.getStatus().equalsIgnoreCase("1")) {
            holder.llNotification.setBackgroundColor(context.getResources().getColor(R.color.inactive));
        } else {
            holder.llNotification.setBackgroundColor(Color.WHITE);
        }
        if (appMode == AppMode.user) {
            if (!notification.getPropertyImage().trim().isEmpty())
            Picasso.with(context)
                    .load(notification.getPropertyImage().trim()).into(holder.ivNotificationImage);

        } else if (appMode == AppMode.agent) {
            if (!notification.getProfileImage().trim().isEmpty())
                Picasso.with(context)
                        .load(notification.getProfileImage())
                        .transform(new CircleTransform())
                        .into(holder.ivNotificationImage);
            else
                Picasso.with(context)
                        .load(R.drawable.profile_icon)
                        .transform(new CircleTransform())
                        .into(holder.ivNotificationImage);
        }
        return view;
    }

    private static class ViewHolder {
        ImageView ivNotificationImage;
        TextView tvNotificationDesc, tvNotifiedDate;
        LinearLayout llNotification;
    }


    public void deleteItem(int i) {
        notifications.remove(i);
        notifyDataSetChanged();
    }

    public void markRead(int i) {
        notifications.get(i).setStatus("1");
        notifyDataSetChanged();
    }

    public void addMoreData(ArrayList<NotificationModel> data) {
        notifications.addAll(data);
        notifyDataSetChanged();
    }

    public String getIdFromPosition(int pos) {
        String notificationId = notifications.get(pos).getId();
        return notificationId;
    }

    public String getPropertyId(int pos) {
        String propertyId = notifications.get(pos).getPropertyId();
        return propertyId;
    }

}

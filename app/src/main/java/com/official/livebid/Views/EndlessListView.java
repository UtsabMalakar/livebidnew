package com.official.livebid.Views;

/*
 * Copyright (C) 2012 Surviving with Android (http://www.survivingwithandroid.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.official.livebid.adapters.AgentProfilePropertiesListingsAdapter;
import com.official.livebid.adapters.SalesAgentListAdapter;
import com.official.livebid.objects.AgentInfo;
import com.official.livebid.objects.PropertyInfo;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.objects.SalesAgentModel;

import java.util.ArrayList;
import java.util.List;

public class EndlessListView<T> extends ListView implements OnScrollListener {

    private View footer;
    private boolean isLoading;
    private boolean isLoadingComplete = false;
    private EndlessListener listener;
    private BaseAdapter adapter;
    private int visibleThreshold = 0;
    private boolean isToolbarVisible = false;

    public EndlessListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setOnScrollListener(this);
    }

    public EndlessListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnScrollListener(this);
    }

    public EndlessListView(Context context) {
        super(context);
        this.setOnScrollListener(this);
    }

    public void setListener(EndlessListener listener) {
        this.listener = listener;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {

        if (getAdapter() == null)
            return;

        if (getAdapter().getCount() == 0)
            return;

//		int l = visibleItemCount + firstVisibleItem;
//		if (l >= totalItemCount && !isLoading) {
//			// It is time to add new data. We call the listener
//			if(!isLoadingComplete){
//                this.addFooterView(footer);
//                isLoading = true;
//                listener.loadData();
//            }
        Log.d("scroll", "scroll: " + view.getChildAt(0).getTop());
        int scrollPos = Math.abs(view.getChildAt(0).getTop());
        if (firstVisibleItem == 0) {
            if (scrollPos > 70) {
                if (!isToolbarVisible)
                    listener.showToolbar();
                isToolbarVisible = true;

            } else {
                if (isToolbarVisible)
                    listener.hideToolbar();
                isToolbarVisible = false;
            }
        }
        if (!isLoading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            // It is time to add new data. We call the listener
            if (!isLoadingComplete) {
                this.addFooterView(footer);
                isLoading = true;
                view.stopNestedScroll();
                listener.loadData();

            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    public void setLoadingView(int resId) {
        LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        footer = (View) inflater.inflate(resId, null, false);
//        this.addFooterView(footer);

    }


    public void setAdapter(BaseAdapter adapter) {
        super.setAdapter(adapter);
        this.adapter = adapter;
        this.removeFooterView(footer);
    }


    public void addNewData(ArrayList<T> data) {
        this.removeFooterView(footer);
        if (adapter instanceof AgentProfilePropertiesListingsAdapter)
            ((AgentProfilePropertiesListingsAdapter) adapter).addAll((ArrayList<PropertyInfo>) data);
        else if (adapter instanceof SalesAgentListAdapter)
            ((SalesAgentListAdapter) adapter).addAll((ArrayList<AgentInfo>) data);
        adapter.notifyDataSetChanged();
        isLoading = false;
    }


    public EndlessListener setListener() {
        return listener;
    }


    public static interface EndlessListener {
        public void loadData();

        public void showToolbar();

        public void hideToolbar();
    }

    public void finaliseLoading() {
        this.removeFooterView(footer);
        isLoading = false;
        isLoadingComplete = true;

    }


}

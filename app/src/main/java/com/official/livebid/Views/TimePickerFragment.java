package com.official.livebid.Views;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.EditText;

import java.util.Calendar;

public class
        TimePickerFragment extends DialogFragment {
    static String ARG_ET_FROM_ID = "etFromId";
    static String ARG_ET_TO_ID = "etToId";
    int etFromId;
    int etToId;
    private int hour;
    private int minute;

    public static TimePickerFragment setArguments(int etId) {
        TimePickerFragment fragment = new TimePickerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ET_FROM_ID, etId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), mTimeSetListener, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
        return timePickerDialog;
    }


    TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(android.widget.TimePicker timePicker, int i, int i2) {
            etFromId = getArguments().getInt(ARG_ET_FROM_ID);
            EditText etDate = (EditText) getActivity().findViewById(etFromId);
            if (timePicker.isShown()) {
                hour = i;
                minute = i2;
                setTime(etDate, hour, minute);

            }
        }


    };


    public void setTime(EditText et, int hours, int mins) {
        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        et.setText(aTime);
    }


}
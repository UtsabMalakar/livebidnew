package com.official.livebid.Views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;

/**
 * Created by Ics on 12/31/2015.
 */
public class CompoundEditText extends LinearLayout {
    private TextView tvLabel;
    private EditText etValue;
    private LinearLayout compoundView;
    private String txtLabel;
    private String txtValue;
    private int labelColor;
    private float textSizeLabel;
    private float textSizeValue;
    private String inputType;

    public CompoundEditText(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.compound_view, this);
    }

    public CompoundEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public CompoundEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }


    private void init(Context c, AttributeSet attrs) {
        TypedArray a = c.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);

        try {
            // get the text and colors specified using the names in attrs.xml
            txtLabel = a.getString(R.styleable.CustomEditText_label);
            txtValue = a.getString(R.styleable.CustomEditText_text);
            labelColor = a.getInteger(R.styleable.CustomEditText_labelColor, 0);
            textSizeLabel = a.getDimension(R.styleable.CustomEditText_textSizeLabel, 14);
            textSizeValue = a.getDimension(R.styleable.CustomEditText_textSizeValue, 14);
            inputType = a.getString(R.styleable.CustomEditText_inputType);

        } finally {
            a.recycle();
        }

        LayoutInflater.from(c).inflate(R.layout.compound_view, this);
        compoundView = (LinearLayout) findViewById(R.id.compoundText);
        tvLabel = (TextView) this.findViewById(R.id.textLabel);
        tvLabel.setText(txtLabel);
        tvLabel.setTextSize(textSizeLabel);
        tvLabel.setTextColor(labelColor);
        etValue = (EditText) this.findViewById(R.id.textEdit);
        etValue.setTextSize(textSizeValue);
        if (inputType.equalsIgnoreCase("name")) {
            etValue.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
            etValue.setFilters(new InputFilter[] {
                    new InputFilter() {
                        @Override
                        public CharSequence filter(CharSequence cs, int start,
                                                   int end, Spanned spanned, int dStart, int dEnd) {
                            // TODO Auto-generated method stub
                            if(cs.equals("")){ // for backspace
                                return cs;
                            }
                            if(cs.toString().matches("[a-zA-Z ]+")){
                                return cs;
                            }
                            return "";
                        }
                    }
            });
        } else if (inputType.equalsIgnoreCase("email")) {
            etValue.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        } else if (inputType.equalsIgnoreCase("password")) {
            etValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else if (inputType.equalsIgnoreCase("phone")) {
            etValue.setInputType(InputType.TYPE_CLASS_PHONE);
        } else {
            etValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        }
        etValue.clearFocus();
        //
        tvLabel.setText("");
        etValue.setHint(txtLabel);
        etValue.setFocusable(true);
        etValue.setClickable(true);
        etValue.setFocusableInTouchMode(true);
        etValue.setGravity(Gravity.LEFT);
        //


    }

    public String getText() {
        return etValue.getText().toString();
    }
    public void clear(){
        etValue.setText("");
    }
    public void setText(String str) {
      etValue.setText(str);
    }
    public void setFocusable(boolean b){
        etValue.setFocusable(b);
        etValue.setClickable(b);
    }
    public void setTextColor(int color) {
        tvLabel.setTextColor(color);
        etValue.setTextColor(color);
    }

    public void setDrawable(int drawableId) {
        etValue.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawableId, 0);
        etValue.setCompoundDrawablePadding(15);
    }

    public void reSetDrawable() {
        etValue.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    }

    public void setPreferred() {
        etValue.setHint("Email Address (Preferred)");
        etValue.clearFocus();

    }

    public void clearPreferred() {
        etValue.setHint("");
    }

    public void setInvalid() {
        setTextColor(getResources().getColor(R.color.theme_red));
        compoundView.setBackground(getResources().getDrawable(R.drawable.layout_bg_error));
    }

    public void setValid() {
        setTextColor(Color.BLACK);
        compoundView.setBackground(getResources().getDrawable(R.drawable.layout_bg));
    }
    public void addTextWatcher(TextWatcher tw){
        etValue.addTextChangedListener(tw);
    }
    public void enableEditMode(Boolean b){
        if(b){
            tvLabel.setText("");
            etValue.setHint(txtLabel);
            etValue.setFocusableInTouchMode(true);
            etValue.setFocusable(true);
            etValue.setClickable(true);
            etValue.setGravity(Gravity.LEFT);
        }
        else {
            tvLabel.setText(txtLabel);
            etValue.setFocusable(false);
            etValue.setClickable(false);
            etValue.setFocusableInTouchMode(false);
            etValue.setGravity(Gravity.RIGHT);
        }
    }
}


package com.official.livebid.demo;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.official.livebid.R;

import java.util.Calendar;
import java.util.Locale;

public class DateTimePicker extends AppCompatActivity {

    static String[] alWeekDays;
    static EditText et_date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time_picker);

        getSevenDays();
        et_date = (EditText) findViewById(R.id.et_date);
        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DateSelector().show(getSupportFragmentManager(), "title");
            }
        });

    }


    public static class DateSelector extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Date picker")
                    .setItems(alWeekDays, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            et_date.setText(alWeekDays[which]);
                        }
                    });
            return builder.create();
        }

    }

    private void getSevenDays() {
        alWeekDays = new String[5];
        Calendar cal = Calendar.getInstance();
        int weekIndex = 0;

        for (int i = 0; i < 7; i++) {
            int date = cal.get(Calendar.DATE);

            String dd = date > 9 ? date + "" : "0" + date;
            String mm = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
            String yyyy = cal.get(Calendar.YEAR) + "";
            String ee = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());

            String text = ee + "-" + dd + "-" + mm + "-" + yyyy;
            cal.add(Calendar.DAY_OF_MONTH, 1);
            Log.d("Date: ", text);

            if (!ee.equalsIgnoreCase("Sat") && !ee.equalsIgnoreCase("Sun")) {
                alWeekDays[weekIndex] = text;
                weekIndex++;
            }
        }

        System.out.println("Week days: " + alWeekDays.toString());
    }

}

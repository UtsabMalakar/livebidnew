package com.official.livebid.alerts;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.official.livebid.R;
import com.official.livebid.activities.HelpAndSupport;
import com.official.livebid.activities.MobileVerification;
import com.official.livebid.activities.ProblemSignIn;
import com.official.livebid.activities.PropertyDetails;
import com.official.livebid.activities.ResetPassword;
import com.official.livebid.activities.SubmitAuctionRegistration;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.activities.profile.AgentPersonalDetails;
import com.official.livebid.activities.profile.Profile;
import com.official.livebid.activities.profile.UserPersonalDetails;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.StringHelper;
import com.official.livebid.objects.AgencyInfo;
import com.official.livebid.objects.AgentInfo;
import com.official.livebid.objects.AvailabilityModel;
import com.official.livebid.objects.PropertyListInfo;

import java.util.ArrayList;


/**
 * Created by Rabin on 8/14/2015.
 */
public class Alerts extends AlertDialog {

    private final SharedPreference prefs;
    Context context;
    AlertDialog alertDialog;
    Activity activity;
    private SharedPreference pref;
    private int email_flag = 0;
    private AgencyInfo agentCall;

    public Alerts(Context context) {
        super(context);
        this.context = context;
        activity = ((Activity) context);
        prefs = new SharedPreference(context);
    }


    public void showOkMessage(String title, String msg) {
        Builder builder = initAlertDialog(title, msg);
        builder.setPositiveButton("Ok", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void showOkMessage(String msg) {
        Builder builder = initAlertDialog("", msg);
        builder.setPositiveButton(context.getResources().getString(R.string.ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void forgotPwdMsg(String msg) {
        Builder builder = initAlertDialog("", msg);
        builder.setPositiveButton(context.getResources().getString(R.string.ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
//                Opener.SignIn(activity);
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    private Builder initAlertDialog(String title, String msg) {
        Builder builder = new Builder(context);
        if (title.isEmpty()) {
            title = context.getResources().getString(R.string.app_name);
        }
        builder.setTitle(title);
        builder.setMessage(msg);
        return builder;
    }

    public void showThankYouMsg(String msg) {
        Builder builder = initAlertDialog("", msg);
        builder.setPositiveButton(context.getResources().getString(R.string.ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
//                opener.Submitted();
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void alertInvalidPin(String msg) {
        Builder builder = initAlertDialog("Invalid Code", msg);
        builder.setPositiveButton("Try Again", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //try again
                ((MobileVerification) activity).retryVerification();

            }
        });
        builder.setNegativeButton("Cancel", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //cancel
                ((MobileVerification) activity).cancelVerification();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void requestPaswordChange(String msg) {
        Builder builder = initAlertDialog("", msg);
        builder.setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //ok
                ((ProblemSignIn) activity).verifyChangePassword();

            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void successResetPassword(String title, String msg) {
        Builder builder = initAlertDialog(title, msg);
        builder.setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //ok
                ((ResetPassword) activity).okResetPassword();

            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void confirmLogout() {
        Builder builder = initAlertDialog("", "Are you sure you want to logout?");

        builder.setNegativeButton("Yes", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ((Profile) activity).performLogout();
            }
        }); builder.setPositiveButton("No", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //ok
                dismiss();

            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void rateApp() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rate_app_popup);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.layout_alert);
//        dialog.setCancelable(false);
        dialog.setCancelable(true);
        //there are a lot of settings, for dialog, check them all out!
        final TextView counter = (TextView) dialog.findViewById(R.id.tv_comment_count);
        final EditText etComment = (EditText) dialog.findViewById(R.id.et_comment);
        final LinearLayout llRatingBar = (LinearLayout) dialog.findViewById(R.id.ll_ratingbar);
        final LinearLayout llComment = (LinearLayout) dialog.findViewById(R.id.ll_comment);
        final String[] comments = new String[1];
        final String[] rating = new String[1];
        etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                llComment.setBackground(activity.getResources().getDrawable(R.drawable.layout_bg));
                comments[0] = etComment.getText().toString();
                counter.setText(comments[0].length() + "/500");
                if (comments[0].length() > 500) {
                    etComment.setText(comments[0].substring(0, 500));

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        final SeekBar rate = (SeekBar) dialog.findViewById(R.id.sb_raring_bar);
        rate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                llRatingBar.setBackground(activity.getResources().getDrawable(R.drawable.layout_bg));
                rating[0] = String.valueOf(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        //set up button
        Button btnOk = (Button) dialog.findViewById(R.id.btn_rate_app);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rating[0] == null || rating[0].equals("0") && (comments[0] == null || (comments[0] != null && comments[0].length() < 1))) {
//                    llRatingBar.setBackground(activity.getResources().getDrawable(R.drawable.layout_bg_error));
//                    llComment.setBackground(activity.getResources().getDrawable(R.drawable.layout_bg_error));
                    showOkMessage("Rating is required");
                } else if (rating[0] == null || rating[0].equals("0")) {
//                    llRatingBar.setBackground(activity.getResources().getDrawable(R.drawable.layout_bg_error));
                    showOkMessage("Rating is required");
                } else if (comments[0] == null || (comments[0] != null && comments[0].length() < 1)) {
//                    llComment.setBackground(activity.getResources().getDrawable(R.drawable.layout_bg_error));
                    showOkMessage("Comment is required");
                } else if (rating[0] != null && !rating[0].equals("0") && comments[0] != null && comments[0].length() > 0) {
                    dialog.dismiss();
                    ((HelpAndSupport) activity).rateApp(rating[0], comments[0]);
                }


            }
        });
        //now that the dialog is set up, it's time to show it
        dialog.show();
    }

    public void updateMobile(String msg) {
        Builder builder = initAlertDialog("", msg);
        builder.setPositiveButton(context.getResources().getString(R.string.ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
                com.official.livebid.staticClasses.MobileVerification.mode = 3;
//                Opener.MobileVerification(activity);
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }


    public void contactAgencyBottomSheet(final AgencyInfo agency) {

        View view = getLayoutInflater().inflate(R.layout.contact_agency_buttom_sheet, null);
        Button btnEmail = (Button) view.findViewById(R.id.btn_email);
        Button bntCall = (Button) view.findViewById(R.id.btn_call);
        Button bntVisitWeb = (Button) view.findViewById(R.id.btn_visit_website);
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        agentCall = agency;

        final Dialog mBottomSheetDialog = new Dialog(activity,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        bntCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                String mobile = agency.getMobile();
                if(mobile!=null && !mobile.isEmpty()) {
                    if (CommonMethods.isCallAvailable(agency.getAvailabilities())) {
                        permissionCheckCall(agency.getMobile());
                    } else {
                        showOkMessage("LiveBid",
                                "You can not call " + agency.getName() + " at this time.\n Please check his availability."
                        );
                    }
                }
                else {
                    showOkMessage("LiveBid",
                            "\nMobile number not available."
                    );
                }
            }
        });

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
//                CommonMethods.sendEmail(AgencyProfile.this, "shiv.anga@yahoo.com");
                Opener.contactAgency(activity, agency.getId());
            }
        });
        bntVisitWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if(agency.getWebsite()==null ) {
                    showOkMessage("Url is not available.");
                    return;
                }
                if(agency.getWebsite()=="" ) {
                    showOkMessage("Url is not available.");
                    return;
                }
                CommonMethods.visitUrl(activity, agency.getWebsite());
            }
        });


    }

    private void permissionCheckCall(String mobile) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && activity.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions(
                    new String[]{Manifest.permission.CALL_PHONE},
                    1300);

            return;
        } else {
            CommonMethods.makeCall(activity, mobile);
        }
    }



    public void MyBids() {

        View view = getLayoutInflater().inflate(R.layout.my_bids_bottom_sheet, null);
        Button btnOffers = (Button) view.findViewById(R.id.btn_myoffers);
        Button btnMyBids = (Button) view.findViewById(R.id.btn_mybids);
        Button btnProperties = (Button) view.findViewById(R.id.btn_reg_prop);
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);

        final Dialog mBottomSheetDialog = new Dialog(activity,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

            }
        });
        btnMyBids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Opener.myRegisteredProperties(activity, CommonDef.MY_BIDS_BIDS);
//
            }
        });

        btnOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
//                CommonMethods.sendEmail(AgencyProfile.this, "shiv.anga@yahoo.com");
                Opener.myRegisteredProperties(activity, CommonDef.MY_BIDS_OFFERS);

            }
        });
        btnProperties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Opener.myRegisteredProperties(activity, CommonDef.MY_BIDS_REGISTERED_PROPERTY);

            }
        });


    }

    public void contactAgentBottomSheet(final AgentInfo agent) {

        View view = getLayoutInflater().inflate(R.layout.contact_agent_bottom_sheet, null);
        Button btnEmail = (Button) view.findViewById(R.id.btn_email);
//        btnEmail.setText("Via Email");
        Button bntCall = (Button) view.findViewById(R.id.btn_call);
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);

        final Dialog mBottomSheetDialog = new Dialog(activity,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        bntCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                String mobile = agent.getMobile();
                if(mobile!=null && !mobile.isEmpty()) {
                    ArrayList<AvailabilityModel> availabilityModels = agent.getAvailabilities();
                    if (CommonMethods.isCallAvailable(agent.getAvailabilities())) {
                        permissionCheckCall(agent.getMobile());
                    } else {
                        showOkMessage("LiveBid",
                                "You can not call " + StringHelper.capitalizeInitial(agent.getName()) + " at this time.\n Please check his availability."
                        );
                    }
                }
                else {
                    showOkMessage("LiveBid",
                            "\nMobile number not available."
                    );
                }
            }
        });
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
////                CommonMethods.sendEmail(AgentPublicProfile.this, "shiv.anga@yahoo.com");
                Opener.contactAgent(activity, agent.getUserId());
            }
        });


    }

    public void confirmOK() {

        View view = getLayoutInflater().inflate(R.layout.layout_confirm_alert, null);
        Button btnOk = (Button) view.findViewById(R.id.btn_ok);
        final Dialog mBottomSheetDialog = new Dialog(activity,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Fragment fragment = activity.getFragmentManager().findFragmentById(R.id.profile_container);
                if (fragment instanceof UserPersonalDetails)
                    ((UserPersonalDetails) fragment).resetUserPersonalDetails();
                else if (fragment instanceof AgentPersonalDetails)
                    ((AgentPersonalDetails) fragment).resetAgentPersonalDetails();
            }
        });


    }

    public void OkSendEmail(String msg) {
        Builder builder = initAlertDialog("", msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //ok
                activity.finish();
                activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);

            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void saleOfContract() {

        View view = getLayoutInflater().inflate(R.layout.sale_of_contract, null);
        Button btnPreviewFile = (Button) view.findViewById(R.id.btn_preview_file);
        Button btnDownloadFile = (Button) view.findViewById(R.id.btn_download_file);
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);

        final Dialog mBottomSheetDialog = new Dialog(activity,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        btnPreviewFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Toast.makeText(activity,"Preview File",Toast.LENGTH_SHORT).show();

            }
        });

        btnDownloadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Toast.makeText(activity,"DownLoad File",Toast.LENGTH_SHORT).show();
            }
        });


    }
    public void confirmAuctionRegistration(String msg) {

        View view = getLayoutInflater().inflate(R.layout.layout_confirm_alert, null);
        Button btnOk = (Button) view.findViewById(R.id.btn_ok);
        TextView tvMsg= (TextView) view.findViewById(R.id.tv_msg);
        tvMsg.setText(msg);
        final Dialog mBottomSheetDialog = new Dialog(activity,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if(activity instanceof SubmitAuctionRegistration){
                    activity.finish();
                }
            }
        });


    }

    public void SaleOfContract(final Activity activity, String code, String msg, String pdfUrl, final PropertyListInfo propertyListInfo){

        final android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(activity);

        email_flag = 0;
        Rect displayRectangle = new Rect();
        Window window = activity.getWindow();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.activity_sale_of_contract, null);

        final Button btn_continue = (Button) dialogView.findViewById(R.id.btn_Continue);
        btn_continue.setEnabled(false);
        WebView webView = (WebView) dialogView.findViewById(R.id.wv_privacy_policy);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);

        CheckBox checkbox_agree = (CheckBox) dialogView.findViewById(R.id.cbox_agree_to_tnc);
        CheckBox checkBox_amend = (CheckBox) dialogView.findViewById(R.id.cbox_request_for_amend);
        final CheckBox checkBox_email = (CheckBox) dialogView.findViewById(R.id.cbox_send_email);




        checkBox_email.setEnabled(false);

        checkbox_agree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked){
                    btn_continue.setEnabled(false);
                }
                else{
                    btn_continue.setEnabled(true);
                }
            }
        });



        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialogView.setMinimumWidth((int)(displayRectangle.width() * 0.9f));
        dialogView.setMinimumHeight((int)(displayRectangle.height() * 0.9f));

        dialogBuilder.setView(dialogView);

        final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        if (code.equalsIgnoreCase("0001")) {
            checkBox_email.setEnabled(true);
            webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdfUrl);
        }
        else{
            showOkMessage(msg);
            checkBox_email.setEnabled(false);
        }

        checkBox_email.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    email_flag = 1;
                }
                else{
                    email_flag = 0;
                }
            }
        });

        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (prefs.getStringValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS).equalsIgnoreCase("0"))
                    Opener.AuctionRegistration(activity, propertyListInfo);
                else
                    ((PropertyDetails)activity).registerAuction();
//                ((PropertyDetails)activity).joinLiveAuction();
                if (checkBox_email.isChecked()) {
                    ((PropertyDetails)activity).loadContractWithEmail();
                }
                alertDialog.dismiss();
            }
        });
    }
    public void cancelAuctionReg() {

        View view = getLayoutInflater().inflate(R.layout.cancel_auction_reg_popup, null);
        Button btnYes = (Button) view.findViewById(R.id.btn_yes);
        Button btnNo = (Button) view.findViewById(R.id.btn_no);
        final Dialog mBottomSheetDialog = new Dialog(activity);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                ((PropertyDetails)activity).cancelRegisterAuction();

            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

            }
        });


    }
    public void signIn() {
        Builder builder = initAlertDialog("", "You are not logged in. Do you want to sign in now?");
        builder.setPositiveButton("YES", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //ok
                Opener.newRegister((Activity) context, UserType.EXISTING);

            }
        });
        builder.setNegativeButton("NO", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }


}

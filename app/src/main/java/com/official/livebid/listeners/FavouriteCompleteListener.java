package com.official.livebid.listeners;

/**
 * Created by Ics on 3/16/2016.
 */
public interface FavouriteCompleteListener {
    void onSuccess(boolean b);
    void onFailure();
    void onNotLoggedIn();
}

package com.official.livebid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.official.livebid.activities.AgencyProfile;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.objects.AgentInfo;
import com.official.livebid.staticClasses.StaticVars;
import com.official.livebid.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class SalesTeamActivity extends AppCompatActivity implements View.OnClickListener {

    private ListView listView;
    private ArrayList<AgentInfo> agentInfos;
    private AgentInfo salesAgent;
    private LinearLayout llSalesAgentContainer;
    private ArrayList<AgentInfo> salesTeam;
    private AgencyProfile agencyProfile;
    Alerts alerts;
    private String agencyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_team);
        getSupportActionBar().hide();

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            agencyId = bundle.getString("agencyIdGlobal");
        }
        ImageButton ivBack = (ImageButton) findViewById(R.id.ibtn_left);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView tvCenter = (TextView) findViewById(R.id.tv_center);
        tvCenter.setText(getString(R.string.agent_list));
        llSalesAgentContainer = (LinearLayout) findViewById(R.id.ll_agency_sales_team);
        salesTeam = new ArrayList<>();
        alerts = new Alerts(this);



//        Bundle bundle = getIntent().getExtras();
        agentInfos = StaticVars.agentInfo;
//        StaticVars.resetAgetnInfo();
        loadList();
//        agencyProfile = new AgencyProfile();
//        getArraylist();



    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Opener.AgencyProfile(this,agencyId);
        finish();
    }

    public void getArraylist() {
//        this.agentInfo = agencyProfile.getSalesTeamArray();
//
//        loadList();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_contact:
//                Toast.makeText(getApplicationContext(), "contact  " + view.getTag(), Toast.LENGTH_SHORT).show();
                alerts.contactAgentBottomSheet(agentInfos.get((Integer) v.getTag()));
                break;
            case R.id.rl_agent_details:
                Opener.AgentPublicProfile(SalesTeamActivity.this, agentInfos.get((Integer) v.getTag()).getUserId());
                break;
        }
    }

    private static class ViewHolder {
        RelativeLayout rlAgentDetails;
        ImageView ivAgentImg;
        TextView tvAgentName,
                tvSaleAgentNo;
        Button btnContact;


    }

    public void loadList(){
        ViewHolder holder;
        HashMap<String,String> name=null;
            for (int i = 0; i < agentInfos.size(); i++) {
                salesAgent = agentInfos.get(i);
                holder = new ViewHolder();
                View view = getLayoutInflater().inflate(R.layout.sales_team_item, null, false);
                holder.rlAgentDetails = (RelativeLayout) view.findViewById(R.id.rl_agent_details);
//            holder.rlAgentDetails.setTag(salesAgent.getUserId());
                holder.rlAgentDetails.setTag(i);
                holder.ivAgentImg = (ImageView) view.findViewById(R.id.iv_agent_img);
                holder.tvAgentName = (TextView) view.findViewById(R.id.tv_agent_name);
                holder.tvSaleAgentNo = (TextView) view.findViewById(R.id.tv_agent_unique_no);
                holder.btnContact = (Button) view.findViewById(R.id.btn_contact);
//            holder.btnContact.setTag(salesAgent.getUserId());
                holder.btnContact.setTag(i);
                name = CommonMethods.getName(salesAgent.getName().split(" "));
                holder.tvAgentName.setText(Html.fromHtml("<b>" + name.get("fName") + "</b>" + " " + name.get("lName")));
                holder.tvSaleAgentNo.setText("Sales Agent- " + salesAgent.getUniqueNumber());
                if (!salesAgent.getProfileImage().trim().isEmpty()) {
                    Picasso.with(SalesTeamActivity.this)
                            .load(salesAgent.getProfileImage())
                            .resize((int) CommonMethods.pxFromDp(SalesTeamActivity.this, 60), (int) CommonMethods.pxFromDp(SalesTeamActivity.this, 60))
                            .centerCrop()
                            .transform(new CircleTransform())
                            .into(holder.ivAgentImg);
                }else {
                    Picasso.with(SalesTeamActivity.this)
                            .load(R.drawable.io_white_profile_default)
                            .transform(new CircleTransform())
                            .into(holder.ivAgentImg);
                }
                if (llSalesAgentContainer.getChildCount() > 0) {
                    llSalesAgentContainer.addView(addDivider());
                }

                llSalesAgentContainer.addView(view);
                expand(view);

                salesTeam.add(salesAgent);

//
                holder.rlAgentDetails.setOnClickListener(this);
                holder.btnContact.setOnClickListener(this);
            }

    }
    private View addDivider() {
        FrameLayout v = new FrameLayout(this);
        v.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 1));
        v.setBackgroundColor(getResources().getColor(R.color.form_divider));
        return v;
    }
    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();
//        Log.d("height", String.valueOf(targetHeight));

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? RelativeLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(2 * (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}

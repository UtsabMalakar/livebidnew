package com.official.livebid.asyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.official.livebid.helpers.AppController;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.R;
import com.official.livebid.errorHandler.VolleyErrorHandler;
import com.official.livebid.helpers.ConnectionMngr;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.utils.HttpClient;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rabin on 8/13/2015.
 */
public class VolleyRequest {

    int method, requestCode;
    Activity activity;
    HashMap<String, String> params = new HashMap<>();
    HashMap<String, String> headers = new HashMap<>();
    boolean postImage = false;
    String url;
    public Bitmap profileImg;
    public AsyncInterface asyncInterface = null;
    VolleyErrorHandler volleyErrorHandler;

    ProgressDialog pd;

    File mImageFile;

    public boolean showPd = true;
    public Object tag = null;

    /**
     * @param activity
     * @param method      :: Post or Get
     * @param params      :: params to post
     * @param url         :: url to webservice
     * @param headers     :: headers to post
     * @param postImage   :: boolean value if to post the image or not
     * @param requestCode :: request type for return object
     */
    public VolleyRequest(Activity activity, int method,
                         HashMap<String, String> params, String url,
                         HashMap<String, String> headers, boolean postImage,
                         int requestCode) {
        this.activity = activity;
        this.method = method;
        this.params = params;
        this.url = url;
        this.headers = headers;
        this.postImage = postImage;
        this.requestCode = requestCode;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public VolleyRequest(Activity activity, int method, String url,
                         int requestCode) {
        this.activity = activity;
        this.method = method;
        this.url = url;
        this.requestCode = requestCode;
    }

    public VolleyRequest(Activity activity, int method,
                         HashMap<String, String> params, String url,
                         boolean postImage,
                         int requestCode) {
        this.activity = activity;
        this.method = method;
        this.params = params;
        this.url = url;
//        this.headers = headers;
        this.postImage = postImage;
        this.requestCode = requestCode;

//        initProgressDialog();
    }

    void initProgressDialog() {
        if (showPd) {
            pd = new ProgressDialog(activity);
            pd.setMessage(activity.getResources().getString(R.string.msg_loading));
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }
    }

    public void request() {
//        ConnectionMngr connectionMngr=new ConnectionMngr(activity);
//        if (!connectionMngr.hasConnection()) //no internet connection
//            return;
        initProgressDialog();
        if (method == Request.Method.POST) {
            if (postImage) {
//                mImageFile = new File(params.get("img_url").toString());
//                doPostRequestWithImage();
                if (profileImg == null) {
                    doPostRequest();
                } else {
                    new UpdateProfileAsyncTask().execute(url);
                }
            } else
                doPostRequest();
        } else {
            doGetRequest();
        }
    }

    public void request(boolean hidePd) {
        initProgressDialog();
        if (hidePd)
//            pd.hide();
            pd.dismiss();
        if (method == Request.Method.POST) {
            if (postImage) {
//                mImageFile = new File(params.get("img_url").toString());
//                doPostRequestWithImage();
                if (profileImg == null) {
                    doPostRequest();
                } else {
                    new UpdateProfileAsyncTask().execute(url);
                }
            } else
                doPostRequest();
        } else {
            doGetRequest();
        }
    }


    private void doPostRequest() {

//        System.out.print("Rabin is testing " + params.toString());
        StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                asyncInterface.processFinish(s, requestCode);
                if (showPd && pd.isShowing()) {
//                    pd.hide();
                    pd.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyErrorHandler = new VolleyErrorHandler(volleyError, activity);
                volleyErrorHandler.errorHandler();
                if (showPd && pd.isShowing()) {
//                    pd.hide();
                    pd.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("Params: " + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                System.out.println("Headers: " + headers);
                return headers;
            }
        };
        if (tag != null) {
            stringRequest.setTag(tag);
        }
        RetryPolicy policy = new DefaultRetryPolicy(CommonDef.SOCKET_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    void doGetRequest() {
        StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                asyncInterface.processFinish(s, requestCode);
                if (showPd && pd.isShowing())
//                    pd.hide();
                    pd.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyErrorHandler = new VolleyErrorHandler(volleyError, activity);
                volleyErrorHandler.errorHandler();
                if (showPd && pd.isShowing()) {
//                    pd.hide();
                    pd.dismiss();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                System.out.println("Headers: " + headers);
                return headers;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(CommonDef.SOCKET_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    void doPostRequestWithImage() {
        new PostImageWithData().execute();
    }

    class PostImageWithData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage(activity.getResources().getString(R.string.msg_loading));
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }

        @Override
        protected void onPostExecute(String string) {
            super.onPostExecute(string);
            pd.dismiss();
            asyncInterface.processFinish(string, requestCode);
//            System.out.println(CommonDef.TAG+string);
        }

        @Override
        protected String doInBackground(Void... paramss) {
            String result = "";
            return result;
        }
    }

    public int uploadFile(final String sourceFileUri) {
        int serverResponseCode = 0;
        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {


            // file not found or source file not exists:


            return 0;

        } else {

            try {
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL("");
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + lineEnd);

                for (String key : params.keySet()) {
                    String value = params.get(key);
                    dos.writeBytes("Content-Disposition: form-data; name=\"value\"" + lineEnd);
                    //dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + lineEnd);
                    //dos.writeBytes("Content-Length: " + name.length() + lineEnd);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(value); // mobile_no is String variable
                    dos.writeBytes(lineEnd);

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                }


//Adding Parameter media file(audio,video and image)

                dos.writeBytes(twoHyphens + boundary + lineEnd);

                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);
                // create a buffer of maximum size
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {
                    System.out.println("Response Msg: " + conn.getResponseMessage());
                    // file upload complete.
                }

                // close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {

                ex.printStackTrace();
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (final Exception e) {

                Log.e("Upload file Exception",
                        "Exception : " + e.getMessage(), e);
            }
            return serverResponseCode;
        }
    }

    private class UpdateProfileAsyncTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... param) {
            String url = param[0];
            String data = "";
            Bitmap b = null;
            b = profileImg;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.PNG, 100, baos);

            try {
                HttpClient client = new HttpClient(url);
                client.connectForMultipart(headers.get("user_id"), headers.get("hash_code"), headers.get("device_id"), headers.get("device_type"), headers.get("user_type"));
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    client.addFormPart(key, value);
                    if (key.equalsIgnoreCase(value)) {
                        client.addFilePart(key, "image.png", baos.toByteArray());
                    }
                }
//                client.addFormPart("first_name",params.get("first_name"));
//                client.addFormPart("last_name", params.get("last_name"));
//                client.addFormPart("address", params.get("address"));
//                client.addFormPart("lat", params.get("lat"));
//                client.addFormPart("lng", params.get("lng"));
//                client.addFilePart("profile_image", "image.jpg", baos.toByteArray());
                client.finishMultipart();
                data = client.getResponse();
            } catch (Throwable t) {
                t.printStackTrace();
            }

            return data;
        }

        @Override
        protected void onPostExecute(String data) {
            pd.dismiss();
            System.out.println("ResponseData=>" + data);
            asyncInterface.processFinish(data, requestCode);
        }


    }


}

package com.official.livebid.staticClasses;

/**
 * Created by Ics on 2/4/2016.
 */
public class AgentDetailsModel {
    public static String fromDay;
    public static String toDay;
    public static String toTime;
    public static String fromTime;
    public static String desc;

    public static void clearAgentDetails(){
        fromDay=null;
        toDay=null;
        fromTime=null;
        toTime=null;
        desc=null;
    }
}

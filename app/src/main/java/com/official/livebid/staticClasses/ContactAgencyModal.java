package com.official.livebid.staticClasses;

/**
 * Created by Ics on 2/11/2016.
 */
public class ContactAgencyModal {
    public static int    enquiryTypeId;
    public static String agentId="0";
    public static String agentName;
    public static String message;
    public static String fullName;
    public static String mobileNumber;
    public static String emailAddress;
    public static int    contactType=1;

    public static void clearContactAgencyModal(){
        enquiryTypeId=0;
        agentId="";
        agentName="";
        message="";
        fullName="";
        mobileNumber="";
        emailAddress="";
        contactType=1;
    }


}

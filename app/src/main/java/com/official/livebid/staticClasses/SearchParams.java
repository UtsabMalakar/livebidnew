package com.official.livebid.staticClasses;

/**
 * Created by Robz on 1/14/2016.
 */
public class SearchParams {

    public static String postCode = "";
    public static String postCodeId = "";
    public static String keywordSearch = "";
    public static String autoKeyword = ""; // for the search bar; if user doesn't selects from the list provided.
    public static String propertyType = "";
    public static String listingType = "";
    public static String priceRangeMin = "", priceRangeMax = "";
    public static String landRangeMin = "", landRangeMax = "";
    public static String nosBathrooms = "", nosBedrooms = "", nosParking = "";
    public static String lat = "0", lng = "0";

    public static String suburbNames="";

    public static void resetPostCode() {
        postCode = "";
    }

    public static void reset() {
        keywordSearch = "";
        propertyType = "";
        listingType = "";
        priceRangeMin = "";
        priceRangeMax = "";
        landRangeMin = "";
        landRangeMax = "";
        nosBathrooms = "";
        nosBedrooms = "";
        nosParking = "";
//        lat = "";
//        lng = "";
    }

    public static void initTypes() {
        SearchParams.listingType = "1";
        SearchParams.propertyType = "1";
    }

}

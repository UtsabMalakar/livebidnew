package com.official.livebid.staticClasses;

import com.official.livebid.objects.AgentInfo;

import java.util.ArrayList;

/**
 * Created by Test on 11/25/2016.
 */

public class StaticVars {

    public static ArrayList<AgentInfo> agentInfo = new ArrayList<>();

    public static void resetAgetnInfo(){
        agentInfo.clear();
    }

}

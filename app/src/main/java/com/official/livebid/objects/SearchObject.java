package com.official.livebid.objects;

/**
 * Created by Robz on 1/8/2016.
 */
public class SearchObject {

    private String id, postcode, suburb, state, latitude, longitude;

    public void setPostcodeId(String id) {
        this.id = id;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPostcodeIds() {
        return id;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getSuburb() {
        return suburb;
    }

    public String getState() {
        return state;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}

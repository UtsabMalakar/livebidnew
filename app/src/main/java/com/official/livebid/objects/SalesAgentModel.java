package com.official.livebid.objects;

/**
 * Created by Ics on 2/3/2016.
 */
public class SalesAgentModel {
    String name;
    String agentCode;
    String imageUrl;

    public SalesAgentModel(String name, String agentCode, String imageUrl) {
        this.name = name;
        this.agentCode = agentCode;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.official.livebid.objects;

/**
 * Created by Ics on 1/25/2016.
 */
public class GoogleAddress {
    private String description;
    private String placeId;

    public GoogleAddress(String description, String placeId) {
        this.description = description;
        this.placeId = placeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }
}

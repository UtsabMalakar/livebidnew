package com.official.livebid.objects;

import java.util.ArrayList;


public class AgentInfo {

    private String id;
    private String userId;
    private String agencyId;
    private String uniqueNumber;
    private String name;
    private String email;
    private String mobile;
    private String profileImage;


    private String address;
    private String agentStreet;
    private String agentSuburb;
    private String agentState;
    private String agentPostcode;
    private String postcodeId;
    private String description;
    private String lat;
    private String lng;
    private String fromDay;
    private String toDay;
    private String fromTime;
    private String toTime;
    private String agencyName;
    private String agencyStreet;
    private String agencySuburb;
    private String agencyState;
    private String agencyPostcode;
    private String agencyLogoUrl;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private ArrayList<AvailabilityModel> availabilities;

    public ArrayList<AvailabilityModel> getAvailabilities() {
        return availabilities;
    }

    public void setAvailabilities(ArrayList<AvailabilityModel> availabilities) {
        this.availabilities = availabilities;
    }

    public String getAgentStreet() {
        if (agentStreet == null || agentStreet.equalsIgnoreCase("")) {
            return agentStreet;
        } else
            return agentStreet.substring(0, 1).toUpperCase() + agentStreet.substring(1);
    }

    public void setAgentStreet(String agentStreet) {
        this.agentStreet = agentStreet;
    }

    public String getAgentSuburb() {
        return agentSuburb;
    }

    public void setAgentSuburb(String agentSuburb) {
        this.agentSuburb = agentSuburb;
    }

    public String getAgentState() {
        return agentState;
    }

    public void setAgentState(String agentState) {
        this.agentState = agentState;
    }

    public String getAgentPostcode() {
        return agentPostcode;
    }

    public void setAgentPostcode(String agentPostcode) {
        this.agentPostcode = agentPostcode;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The agencyId
     */
    public String getAgencyId() {
        return agencyId;
    }

    /**
     * @param agencyId The agency_id
     */
    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId;
    }

    /**
     * @return The uniqueNumber
     */
    public String getUniqueNumber() {
        return uniqueNumber;
    }

    /**
     * @param uniqueNumber The unique_number
     */
    public void setUniqueNumber(String uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile The mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return The profileImage
     */
    public String getProfileImage() {
        return profileImage;
    }

    /**
     * @param profileImage The profile_image
     */
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The postcodeId
     */
    public String getPostcodeId() {
        return postcodeId;
    }

    /**
     * @param postcodeId The postcode_id
     */
    public void setPostcodeId(String postcodeId) {
        this.postcodeId = postcodeId;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * @return The lng
     */
    public String getLng() {
        return lng;
    }

    /**
     * @param lng The lng
     */
    public void setLng(String lng) {
        this.lng = lng;
    }

    /**
     * @return The fromDay
     */
    public String getFromDay() {
        return fromDay;
    }

    /**
     * @param fromDay The from_day
     */
    public void setFromDay(String fromDay) {
        this.fromDay = fromDay;
    }

    /**
     * @return The toDay
     */
    public String getToDay() {
        return toDay;
    }

    /**
     * @param toDay The to_day
     */
    public void setToDay(String toDay) {
        this.toDay = toDay;
    }

    /**
     * @return The fromTime
     */
    public String getFromTime() {
        return fromTime;
    }

    /**
     * @param fromTime The from_time
     */
    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    /**
     * @return The toTime
     */
    public String getToTime() {
        return toTime;
    }

    /**
     * @param toTime The to_time
     */
    public void setToTime(String toTime) {
        this.toTime = toTime;
    }


    /**
     * @return The agencyName
     */
    public String getAgencyName() {
        return agencyName == null ? "-" : agencyName;
    }

    /**
     * @param agencyName The agency_name
     */
    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    /**
     * @return The agencyStreet
     */
    public String getAgencyStreet() {
        return agencyStreet;
    }

    /**
     * @param agencyStreet The agency_street
     */
    public void setAgencyStreet(String agencyStreet) {
        this.agencyStreet = agencyStreet;
    }

    /**
     * @return The agencySuburb
     */
    public String getAgencySuburb() {
        return agencySuburb == null ? "-" : agencySuburb;
    }

    /**
     * @param agencySuburb The agency_suburb
     */
    public void setAgencySuburb(String agencySuburb) {
        this.agencySuburb = agencySuburb;
    }

    /**
     * @return The agencyState
     */
    public String getAgencyState() {
        return agencyState;
    }

    /**
     * @param agencyState The agency_state
     */
    public void setAgencyState(String agencyState) {
        this.agencyState = agencyState;
    }

    /**
     * @return The agencyPostcode
     */
    public String getAgencyPostcode() {
        return agencyPostcode == null ? "-" : agencyPostcode;
    }

    /**
     * @param agencyPostcode The agency_postcode
     */
    public void setAgencyPostcode(String agencyPostcode) {
        this.agencyPostcode = agencyPostcode;
    }

    /**
     * @return The agencyLogoUrl
     */
    public String getAgencyLogoUrl() {
        return agencyLogoUrl == null ? "" : agencyLogoUrl;
    }

    /**
     * @param agencyLogoUrl The agency_logo_url
     */
    public void setAgencyLogoUrl(String agencyLogoUrl) {
        this.agencyLogoUrl = agencyLogoUrl;
    }


}
package com.official.livebid.objects;

import java.io.Serializable;

/**
 * Created by Ics on 3/11/2016.
 */
public class AuctionApplicationModel implements Serializable{
    public String userId;
    public String uniqueId;
    public String profileImage;
    public int applicationStatus;
    public String propId;

    public AuctionApplicationModel(String userId, String uniqueId, String profileImage, int applicationStatus,String propId) {
        this.userId = userId;
        this.uniqueId = uniqueId;
        this.profileImage = profileImage;
        this.applicationStatus = applicationStatus;
        this.propId=propId;
    }

    public AuctionApplicationModel(String userId, String uniqueId, String profileImage) {
        this.userId = userId;
        this.uniqueId = uniqueId;
        this.profileImage = profileImage;
    }
}

package com.official.livebid.objects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Robz on 1/6/2016.
 */
public class PropertyListInfo implements Serializable {

    //    public String
//            id,
//            agentId,
//            propertyTitle,
//            propertyDesc,
//            propertyCode,
//            propertyType,
//            landArea,
//            cat,
//            postCode,
//            suburb,
//            lat,
//            lon,
//            views,
//            agentName,
//            agentTradingName,
//            agentImportId,
//            distance,
//            street,
    public String id,
            auctionDate,
            agencyId,
            agentId, propertyTitle = "", propertyDesc,propertyOtherFeatures,
            propertyCode, propertyType,
            landArea, cat, postCode = "", suburb = "Suburb Name", lat = "35.3080", lng = "149.1245", views, agentName,
            agentTradingName, agentImportId, distance, street = "00/00 Street Name",
            propertyImgUrl,
            statusTag,
            dateTag,
            propertyState,
            propertyStatusIdentifier,
            price,
            price_text,
            nosOfBedroom,
            nosOfBathroom,
            nosOfParking,
            agentProfileImg, inspectionTimeString,
            agentFName,
            agentLName,
            agentAgencyLogoUrl,
            registeredForProperty;
    public long auctionDateTimeStamp;
    public int propertyStatus2, propertyStatus;

    public String averagePrice;
    private boolean sold;
    private boolean fav;
    public ArrayList<String> property_images_more = new ArrayList<>();

    public boolean isSold() {
        return sold;
    }

    public void setSold(boolean sold) {
        this.sold = sold;
    }

    public void setSold(String sold) {
        this.sold = sold.equals("1") ? true : false;
    }

    public void setFav(String fav) {
        this.fav = fav.equals("1") ? true : false;
    }

    public boolean getFav() {
        return this.fav;
    }

    public void setFav(boolean fav) {
        this.fav = fav;
    }

    public boolean isFav() {
        return fav;
    }

    public PropertyListInfo getPropertyInfo(JSONObject joProp) throws JSONException {
        PropertyListInfo propertyListInfo = new PropertyListInfo();
        propertyListInfo.id = joProp.getString("id");
        propertyListInfo.agentId = joProp.getString("agent_id");
        propertyListInfo.agencyId = joProp.getString("agency_id");
        propertyListInfo.auctionDate = joProp.getString("auction_date");
        propertyListInfo.agentAgencyLogoUrl = joProp.getString("agency_logo_url");
        propertyListInfo.agentFName = joProp.getString("agent_name");
        propertyListInfo.agentLName = joProp.getString("agent_name");
        propertyListInfo.propertyStatus = joProp.getInt("property_status");
        propertyListInfo.statusTag = "";
        propertyListInfo.dateTag = "Auction due to 40 days";
        propertyListInfo.setFav(joProp.getString("favorite"));
        propertyListInfo.nosOfBathroom = joProp.getString("bathrooms");
        propertyListInfo.nosOfBedroom = joProp.getString("bedrooms");
        propertyListInfo.nosOfParking = joProp.getString("parking");
        propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(joProp.getString("price")));
        propertyListInfo.propertyStatusIdentifier = "from";
        propertyListInfo.suburb = joProp.getString("suburb");
        propertyListInfo.postCode = joProp.getString("postcode");
        propertyListInfo.propertyTitle = joProp.getString("property_title");
        propertyListInfo.propertyDesc = joProp.getString("property_description");
        propertyListInfo.street = joProp.getString("street");
        propertyListInfo.propertyState = joProp.getString("state");
        propertyListInfo.propertyImgUrl = joProp.getString("property_image");
        propertyListInfo.agentProfileImg = joProp.getString("agent_profile_image");
        propertyListInfo.distance = joProp.getString("distance");
        propertyListInfo.lat = joProp.getString("lat");
        propertyListInfo.lng = joProp.getString("lng");
        propertyListInfo.inspectionTimeString = joProp.getString("inspection_times");
        propertyListInfo.registeredForProperty = joProp.getString("registered_for_property");
        propertyListInfo.auctionDateTimeStamp = joProp.getLong("auction_date_timestamp");
        propertyListInfo.propertyStatus2 = joProp.getInt("property_status_2");
        propertyListInfo.price_text = joProp.getString("price_text");
        return propertyListInfo;
    }


}

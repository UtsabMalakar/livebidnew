package com.official.livebid.objects;

import java.util.ArrayList;

/**
 * Created by Ics on 2/9/2016.
 */
public class PropertyInfo
{
    private String agent_profile_image;
    private String property_description;

    public String getProperty_other_geatures() {
        return property_other_geatures;
    }

    public void setProperty_other_geatures(String property_other_geatures) {
        this.property_other_geatures = property_other_geatures;
    }

    private String property_other_geatures;
    private String parking;
    private String street;
    private String state;
    private String bathrooms;
    private String lng;
//    private ArrayList<Property_images_more> property_images_more;
    private ArrayList<OHI> inspectionTimes;
    private String id;
    private String auction_date;
    private String authority;
    private String distance;
    private String suburb;
    private String lat;
    private String property_code;
    private String agent_id;

    public String getAgency_id() {
        return agency_id;
    }

    public ArrayList<String> property_images_more = new ArrayList<>();

    public void setAgency_id(String agency_id) {
        this.agency_id = agency_id;
    }

    private String agency_id;
    private String bedrooms;
    private String favorite;
    private String postcode;
    private String agency_logo_url;
    private String property_title;
    private String price;



    private int property_status;
    private String property_image;
    private String agent_name;
    private String unique_number;
    private String inspectionTimeString;
    private String registeredForProperty;
    private long auctionDateTimeStamp;
    private int propertyStatus2;

    public int getPropertyStatus2() {
        return propertyStatus2;
    }

    public void setPropertyStatus2(int propertyStatus2) {
        this.propertyStatus2 = propertyStatus2;
    }

    public long getAuctionDateTimeStamp() {
        return auctionDateTimeStamp;
    }

    public void setAuctionDateTimeStamp(long auctionDateTimeStamp) {
        this.auctionDateTimeStamp = auctionDateTimeStamp;
    }

    public String getRegisteredForProperty() {
        return registeredForProperty;
    }

    public void setRegisteredForProperty(String registeredForProperty) {
        this.registeredForProperty = registeredForProperty;
    }
    public int getProperty_status() {
        return property_status;
    }

    public void setProperty_status(int property_status) {
        this.property_status = property_status;
    }
    public String getInspectionTimeString() {
        return inspectionTimeString;
    }

    public void setInspectionTimeString(String inspectionTimeString) {
        this.inspectionTimeString = inspectionTimeString;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }



    public ArrayList<OHI> getInspectionTimes() {
        return inspectionTimes;
    }

    public void setInspectionTimes(ArrayList<OHI> inspectionTimes) {
        this.inspectionTimes = inspectionTimes;
    }

    public String getAgent_profile_image ()
    {
        return agent_profile_image;
    }

    public void setAgent_profile_image (String agent_profile_image)
    {
        this.agent_profile_image = agent_profile_image;
    }

    public String getProperty_description ()
    {
        return property_description;
    }

    public void setProperty_description (String property_description)
    {
        this.property_description = property_description;
    }

    public String getParking ()
    {
        return parking;
    }

    public void setParking (String parking)
    {
        this.parking = parking;
    }

    public String getStreet ()
    {
        return street;
    }

    public void setStreet (String street)
    {
        this.street = street;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getBathrooms ()
    {
        return bathrooms;
    }

    public void setBathrooms (String bathrooms)
    {
        this.bathrooms = bathrooms;
    }

    public String getLng ()
    {
        return lng;
    }

    public void setLng (String lng)
    {
        this.lng = lng;
    }

//    public ArrayList<Property_images_more> getProperty_images_more() {
//        return property_images_more;
//    }
//
//    public void setProperty_images_more(ArrayList<Property_images_more> property_images_more) {
//        this.property_images_more = property_images_more;
//    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAuction_date ()
    {
        return auction_date;
    }

    public void setAuction_date (String auction_date)
    {
        this.auction_date = auction_date;
    }

    public String getAuthority ()
    {
        return authority;
    }

    public void setAuthority (String authority)
    {
        this.authority = authority;
    }

    public String getDistance ()
    {
        return distance;
    }

    public void setDistance (String distance)
    {
        this.distance = distance;
    }

    public String getSuburb ()
    {
        return suburb;
    }

    public void setSuburb (String suburb)
    {
        this.suburb = suburb;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }

    public String getProperty_code ()
    {
        return property_code;
    }

    public void setProperty_code (String property_code)
    {
        this.property_code = property_code;
    }

    public String getAgent_id ()
    {
        return agent_id;
    }

    public void setAgent_id (String agent_id)
    {
        this.agent_id = agent_id;
    }

    public String getBedrooms ()
    {
        return bedrooms;
    }

    public void setBedrooms (String bedrooms)
    {
        this.bedrooms = bedrooms;
    }


    public String getPostcode ()
    {
        return postcode;
    }

    public void setPostcode (String postcode)
    {
        this.postcode = postcode;
    }

    public String getAgency_logo_url ()
    {
        return agency_logo_url;
    }

    public void setAgency_logo_url (String agency_logo_url)
    {
        this.agency_logo_url = agency_logo_url;
    }

    public String getProperty_title ()
    {
        return property_title;
    }

    public void setProperty_title (String property_title)
    {
        this.property_title = property_title;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }


    public String getProperty_image ()
    {
        return property_image;
    }

    public void setProperty_image (String property_image)
    {
        this.property_image = property_image;
    }



    public String getAgent_name ()
    {
        return agent_name;
    }

    public void setAgent_name (String agent_name)
    {
        this.agent_name = agent_name;
    }

    public String getUnique_number ()
    {
        return unique_number;
    }

    public void setUnique_number (String unique_number)
    {
        this.unique_number = unique_number;
    }

    public static class Property_images_more {
        String imageId;
        String ImageUrl;

        public Property_images_more(String imageUrl) {
            ImageUrl = imageUrl;
        }

        public String getImageId() {
            return imageId;
        }

        public void setImageId(String imageId) {
            this.imageId = imageId;
        }

        public String getImageUrl() {
            return ImageUrl;
        }

        public void setImageUrl(String imageUrl) {
            ImageUrl = imageUrl;
        }
    }

    public static class OHI{
        String ohiDay;
        String ohtFromTime;
        String ohtToTime;

        public OHI(String ohiDay, String ohtFromTime, String ohtToTime) {
            this.ohiDay = ohiDay;
            this.ohtFromTime = ohtFromTime;
            this.ohtToTime = ohtToTime;
        }

        public String getOhiDay() {
            return ohiDay;
        }

        public void setOhiDay(String ohiDay) {
            this.ohiDay = ohiDay;
        }

        public String getOhtFromTime() {
            return ohtFromTime;
        }

        public void setOhtFromTime(String ohtFromTime) {
            this.ohtFromTime = ohtFromTime;
        }

        public String getOhtToTime() {
            return ohtToTime;
        }

        public void setOhtToTime(String ohtToTime) {
            this.ohtToTime = ohtToTime;
        }
    }


}


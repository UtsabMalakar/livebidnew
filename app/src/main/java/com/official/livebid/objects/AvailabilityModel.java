package com.official.livebid.objects;

public class AvailabilityModel {
    int fromDay;
    int toDay;
    String toTime;
    String fromTime;

    public AvailabilityModel(String fromTime, String toTime, int toDay, int fromDay) {
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.toDay = toDay;
        this.fromDay = fromDay;
    }

    public int getFromDay() {
        return fromDay;
    }

    public void setFromDay(int fromDay) {
        this.fromDay = fromDay;
    }

    public int getToDay() {
        return toDay;
    }

    public void setToDay(int toDay) {
        this.toDay = toDay;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }
}
package com.official.livebid.objects;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.View;

import com.official.livebid.adapters.NotificationAdapter;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ics on 2/18/2016.
 * "status": "0",
 * "date_time_stamp": 1462523816
 * <p/>
 * <p/>
 * "notification_id": "4",
 * "date_time": "2016-05-06 18:36:56",
 * "notification_type": "1",
 * "notification_message": "",
 * "propertyId": "3044",
 * "property_image": "http://www.bllprwick.com.au/lt-14-14P1264-0715037725-rsd.jpg",
 * "profile_image": "http://livebid.draftserver.com/livebid/assets/uploads/profile/b567ec6b4a0915cc24d1c310d03588cf.jpg",
 * "status": "0",
 * "date_time_stamp": 1462523816
 */
public class NotificationModel implements Serializable {
    private String id;
    private String date;
    private String description;//notification_messafe
    private int notification_type;
    private String propertyId;
    private String propertyImage;
    private String profileImage;
    private String status;
    private String propertyCode;
    private long date_time_stamp;
    private String userName;
    private String propertyStatus;
    private String soldToUserid;

    public String getSoldToUserid() {
        return soldToUserid;
    }

    public void setSoldToUserid(String soldToUserid) {
        this.soldToUserid = soldToUserid;
    }

    public String getPropertyStatus() {
        return propertyStatus;
    }

    public void setPropertyStatus(String propertyStatus) {
        propertyStatus = propertyStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String userId;


    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getPropertyImage() {
        return propertyImage;
    }

    public void setPropertyImage(String propertyImage) {
        this.propertyImage = propertyImage;
    }

    public int getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(int notification_type) {
        this.notification_type = notification_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public long getDate_time_stamp() {
        return date_time_stamp;
    }

    public void setDate_time_stamp(long date_time_stamp) {
        this.date_time_stamp = date_time_stamp;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public ArrayList<NotificationModel> getUserNotifications(JSONArray jArrList) throws JSONException {
        ArrayList<NotificationModel> notifications = new ArrayList<>();
        JSONObject jObjNInfo = null;
        NotificationModel notificationModel;
        for (int i = 0; i < jArrList.length(); i++) {
            jObjNInfo = jArrList.getJSONObject(i);
            notificationModel = new NotificationModel();
            notificationModel.id = jObjNInfo.getString("notification_id");
            notificationModel.date = (jObjNInfo.getString("date_time"));
            notificationModel.notification_type = (jObjNInfo.getInt("notification_type"));
            notificationModel.description = (jObjNInfo.getString("notification_message"));
            notificationModel.propertyId = (jObjNInfo.getString("property_id"));
            notificationModel.propertyCode = (jObjNInfo.getString("property_code"));
            notificationModel.userName = (jObjNInfo.getString("users_name"));
            notificationModel.propertyImage = (jObjNInfo.getString("property_image"));
            notificationModel.profileImage = (jObjNInfo.getString("profile_image"));
            notificationModel.status = (jObjNInfo.getString("status"));
            notificationModel.propertyStatus = (jObjNInfo.getString("property_status"));
            notificationModel.date_time_stamp = (jObjNInfo.getLong("date_time_stamp"));
            notificationModel.soldToUserid = (jObjNInfo.getString("sold_to_user_id"));
            notifications.add(notificationModel);
        }
        return notifications;
    }

    public ArrayList<NotificationModel> getAgentNotifications(JSONArray jArrList) throws JSONException {
        ArrayList<NotificationModel> notifications = new ArrayList<>();
        JSONObject jObjNInfo = null;
        NotificationModel notificationModel;
        for (int i = 0; i < jArrList.length(); i++) {
            jObjNInfo = jArrList.getJSONObject(i);
            notificationModel = new NotificationModel();
            notificationModel.id = jObjNInfo.getString("notification_id");
            notificationModel.date = (jObjNInfo.getString("date_time"));
            notificationModel.notification_type = (jObjNInfo.getInt("notification_type"));
            notificationModel.propertyId = (jObjNInfo.getString("property_id"));
            notificationModel.propertyCode = (jObjNInfo.getString("property_code"));
            notificationModel.userName = (jObjNInfo.getString("users_name"));
            notificationModel.propertyImage = (jObjNInfo.getString("property_image"));
            notificationModel.profileImage = (jObjNInfo.getString("profile_image"));
            notificationModel.status = (jObjNInfo.getString("status"));
            notificationModel.propertyStatus = (jObjNInfo.getString("property_status"));
            notificationModel.date_time_stamp = (jObjNInfo.getLong("date_time_stamp"));
            notificationModel.userId = (jObjNInfo.getString("user_id"));
            notifications.add(notificationModel);
        }
        return notifications;
    }

    public NotificationModel getNotificationUser(String result) {
        NotificationModel notificationModel = null;
        try {
            JSONObject joNotification = new JSONObject(result);
            notificationModel = new NotificationModel();
            notificationModel.id = joNotification.getString("notification_id");
            notificationModel.date = (joNotification.getString("date_time"));
            notificationModel.notification_type = (joNotification.getInt("notification_type"));
            notificationModel.description = (joNotification.getString("notification_message"));
            notificationModel.propertyId = (joNotification.getString("property_id"));
            notificationModel.propertyCode = (joNotification.getString("property_code"));
            notificationModel.userName = (joNotification.getString("users_name"));
            notificationModel.propertyImage = (joNotification.getString("property_image"));
            notificationModel.profileImage = (joNotification.getString("profile_image"));
            notificationModel.status = (joNotification.getString("status"));
            notificationModel.propertyStatus = (joNotification.getString("property_status"));
            notificationModel.date_time_stamp = (joNotification.getLong("date_time_stamp"));


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return notificationModel;
    }
    public NotificationModel getNotificationAgent(String result) {
        NotificationModel notificationModel = null;
        try {
            JSONObject joNotification = new JSONObject(result);
            notificationModel = new NotificationModel();
            notificationModel.id = joNotification.getString("notification_id");
            notificationModel.date = (joNotification.getString("date_time"));
            notificationModel.notification_type = (joNotification.getInt("notification_type"));
            notificationModel.propertyId = (joNotification.getString("property_id"));
            notificationModel.propertyCode = (joNotification.getString("property_code"));
            notificationModel.userName = (joNotification.getString("users_name"));
            notificationModel.propertyImage = (joNotification.getString("property_image"));
            notificationModel.profileImage = (joNotification.getString("profile_image"));
            notificationModel.status = (joNotification.getString("status"));
            notificationModel.propertyStatus = (joNotification.getString("property_status"));
            notificationModel.date_time_stamp = (joNotification.getLong("date_time_stamp"));
            notificationModel.userId = (joNotification.getString("user_id"));


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return notificationModel;
    }

    public Spanned setNotificationMsgUser(Context context) {
        Spanned msg = (Html.fromHtml(""));
        switch (this.notification_type) {
            case 1:
                msg = (Html.fromHtml("<b>Property #" + this.getPropertyCode() + "</b>" + " is about to start auction - join now"));
                return msg;
            case 2:
                msg = (Html.fromHtml("Bidding has been started for <b>Property #" + this.getPropertyCode() + "</b> - join now"));
                return msg;
            case 3:
                msg = (Html.fromHtml(this.description+ " - sent by agent before auction start for  <b>Property #" + this.getPropertyCode() + "</b> "));
                return msg;
            case 4:
                msg = (Html.fromHtml("First call notification for <b>Property #" + this.getPropertyCode() + "</b>"));
                return msg;
            case 5:
                msg = (Html.fromHtml("Second call notification for <b>Property #" + this.getPropertyCode() + "</b>"));
                return msg;
            case 6:
                msg = (Html.fromHtml("Third call notification for <b>Property #" + this.getPropertyCode() + "</b>"));
                return msg;
            case 7:
                msg = (Html.fromHtml("<b>Property #" + this.getPropertyCode() + "</b> has been passed in - "+this.description));
                return msg;
            case 8:
               String winner= (CommonMethods.getLoggedInUserId(context).equalsIgnoreCase(this.userId))?"You":this.userName;
                msg = (Html.fromHtml("<b>Property #" + this.getPropertyCode() + "</b> has been sold to <b>"+ winner.toUpperCase()+" </b>"));
                return msg;
        }
        return msg;
    }

    public Spanned setNotificationMsgAgent() {
        Spanned msg = (Html.fromHtml(""));
        switch (this.notification_type) {
            case 1:
                msg = (Html.fromHtml("Pre-offer notifications for <b>property #" + this.getPropertyCode() + "</b> by user <b>#" + this.getUserName() + "</b>"));
                return msg;
            case 3:
                msg = (Html.fromHtml("<b>#" + this.getUserName() + "</b> has registered for property <b>#" + this.getPropertyCode()+"</b>"));
                return msg;
            case 2:
                msg = (Html.fromHtml("Post-offer notifications for <b>property #" + this.getPropertyCode() + "</b> by user <b>#" + this.getUserName() + "</b>"));
                return msg;
        }
        return msg;
    }

}

package com.official.livebid.objects;

import android.content.Context;

import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ics on 6/13/2016.
 */
public class LiveBidSettings implements Serializable {
    Context context;
    String id;
    int type;
    String title;
    int status;

    public LiveBidSettings(Context context) {
        this.context = context;
    }

    public LiveBidSettings() {
    }

    public void setLiveBidSettings(JSONArray jASettings) {
        JSONObject jObjSetting = null;
        LiveBidSettings ls = new LiveBidSettings();
        for (int i = 0; i < jASettings.length(); i++) {
            try {
                jObjSetting = jASettings.getJSONObject(i);
//                ls.id = jObjSetting.getString("id");
                ls.type = jObjSetting.getInt("setting_type");
                ls.title = jObjSetting.getString("title");
                ls.status = jObjSetting.getInt("status");
                setInPreferences(ls.type, ls.status);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    private void setInPreferences(int type, int status) {
        SharedPreference prefs = new SharedPreference(context);
        switch (type) {
            case 1:
                prefs.setKeyValues(CommonDef.SharedPrefKeys.PUSH_NOTIFICATION, status);
                break;
            case 2:
                prefs.setKeyValues(CommonDef.SharedPrefKeys.SHOW_SURROUNDINGS, status);
                break;
        }

    }

    public int getLiveBidSetting(String key) {
        SharedPreference prefs = new SharedPreference(context);
        return (prefs.getIntValues(CommonDef.SharedPrefKeys.PUSH_NOTIFICATION));

    }

}

package com.official.livebid.objects;

/**
 * Created by Ics on 2/3/2016.
 */
public class EnquiryTypeModel {
    String id;
    String value;

    public EnquiryTypeModel(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

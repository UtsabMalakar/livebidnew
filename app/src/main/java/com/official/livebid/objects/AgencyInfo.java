package com.official.livebid.objects;

import java.util.ArrayList;

public class AgencyInfo {

    private String id;
    private String name;
    private String street;
    private String suburb;
    private String state;
    private String postcode;
    private String website;
    private String logoUrl;
    private String currentlyOpen;
    private String lat;
    private String lng;
    private String mobile;
    private String phone;
    private String description;
    private ArrayList<AvailabilityModel> availabilities;

    public ArrayList<AvailabilityModel> getAvailabilities() {
        return availabilities;
    }

    public String getCurrentlyOpen() {
        return currentlyOpen;
    }

    public void setCurrentlyOpen(String currentlyOpen) {
        this.currentlyOpen = currentlyOpen;
    }

    public void setAvailabilities(ArrayList<AvailabilityModel> availabilities) {
        this.availabilities = availabilities;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
//        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The street
     */
    public String getStreet() {
        return street.substring(0, 1).toUpperCase() + street.substring(1);
    }

    /**
     * @param street The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return The suburb
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     * @param suburb The suburb
     */
    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode The postcode
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return The logoUrl
     */
    public String getLogoUrl() {
        return logoUrl;
    }

    /**
     * @param logoUrl The logo_url
     */
    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    /**
     * @return The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * @return The lng
     */
    public String getLng() {
        return lng;
    }

    /**
     * @param lng The lng
     */
    public void setLng(String lng) {
        this.lng = lng;
    }

    /**
     * @return The mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile The mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }



}


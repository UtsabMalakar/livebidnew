package com.official.livebid.objects;

/**
 * Created by Ics on 3/10/2016.
 */
public class PreAuctionOfferModel {
    public String userId;
    public String uniqueId;
    public String profileImage;
    public String offerAmount;
    public String offerTime;
    public String settlementTime;
    public String emailAddress;
    public String mobileNumber;


    public PreAuctionOfferModel(String userId, String uniqueId, String offerAmount, String offerTime, String settlementTime, String emailAddress, String mobileNumber,String profileImage) {
        this.userId = userId;
        this.uniqueId = uniqueId;
        this.offerAmount = offerAmount;
        this.offerTime = offerTime;
        this.settlementTime = settlementTime;
        this.emailAddress = emailAddress;
        this.mobileNumber = mobileNumber;
        this.profileImage=profileImage;
    }
}

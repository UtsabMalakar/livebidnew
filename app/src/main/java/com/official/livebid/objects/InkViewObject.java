package com.official.livebid.objects;

/**
 * Created by Rabin on 6/15/2015.
 */
public class InkViewObject {

    float x, y;
    long eventTime;
    int action;

    public InkViewObject(float x, float y, long eventTime, int action) {
        this.x = x;
        this.y = y;
        this.eventTime = eventTime;
        this.action = action;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public long getEventTime() {
        return eventTime;
    }

    public int getAction() {
        return action;
    }
}

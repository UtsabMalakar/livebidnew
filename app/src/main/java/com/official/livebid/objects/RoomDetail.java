package com.official.livebid.objects;

import java.io.Serializable;

/**
 * Created by Ics on 4/28/2016.
 */
public class RoomDetail implements Serializable {
    public String auctionId;
    public String propertyId;
    public String roomName;
    public int callCount;
    public String  soldTo;
    public String  soldAmount;
    public String  status;
    public String  startDateTime;

}

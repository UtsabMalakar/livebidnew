package com.official.livebid.enums;

import java.io.Serializable;

/**
 * Created by Ics on 5/11/2016.
 */
public enum OfferType implements Serializable {
    PRE_OFFER(1),
    POST_OFFER(2);
    private int offerType;
    OfferType(int i) {
        this.offerType=i;
    }

    public int getOfferType() {
        return offerType;
    }
}

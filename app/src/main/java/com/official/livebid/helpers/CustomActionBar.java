package com.official.livebid.helpers;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.activities.LandSelector;
import com.official.livebid.activities.MapsActivity;
import com.official.livebid.activities.PriceSelector;
import com.official.livebid.activities.liveAuction.PropertyAuction;
import com.official.livebid.activities.myListings.MyListings;
import com.official.livebid.activities.newRegistration.NewRegistration;
import com.official.livebid.activities.viewListingsAgent.ViewListingAgent;
import com.official.livebid.activities.watchList.WatchList;
import com.official.livebid.activities.whatsOn.AuctionToday;
import com.official.livebid.activities.whatsOn.OpenHouseToday;
import com.official.livebid.enums.AppMode;

/**
 * Created by Rabin on 12/31/2015.
 */
public class CustomActionBar {


    public class DiscoverTopMenu {

        public void leftAction() {

        }

    }

    public static class TitleBarWithBackButtonOnly implements View.OnClickListener {
        private final TextView tvTitle;
        Context context;
        Activity activity;
        ImageButton ibtBack;
//        MenuActivity menuActivity;

        public TitleBarWithBackButtonOnly(Context context) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.GONE);
            ibtBack.setVisibility(View.VISIBLE);
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.finish();
                    activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    break;
            }
        }
    }

    public static class MobileVerificationTitleBar implements View.OnClickListener {
        Context context;
        Activity activity;
        TextView tvLeft;

        public MobileVerificationTitleBar(Context context) {
            this.context = context;
            activity = ((Activity) context);
            tvLeft = (TextView) activity.findViewById(R.id.tv_left);
            tvLeft.setVisibility(View.VISIBLE);
            tvLeft.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tv_left:
                    activity.finish();
                    activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    break;
            }
        }
    }

    public static class PropertyListingMenu implements View.OnClickListener {
        private final TextView tvTitle;
        ImageButton ibtnLeft, ibtnRight;
        Activity activity;
        String callingActivity;

        public PropertyListingMenu(Activity activity,String callingActivity) {
            this.activity = activity;
            this.callingActivity = callingActivity;
            ibtnLeft = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtnRight = (ImageButton) activity.findViewById(R.id.ibtn_right);
            ibtnRight.setImageResource(R.drawable.location_map_icon);

            ibtnLeft.setVisibility(View.VISIBLE);
            ibtnRight.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Search Results");

            ibtnLeft.setOnClickListener(this);
            ibtnRight.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
//                    activity.finish();
                    if(callingActivity.equalsIgnoreCase(CommonDef.CallingActivity.Discover_Activity))
                    {
                        Opener.Discover(activity);
                    }
                    else if(callingActivity.equalsIgnoreCase(CommonDef.CallingActivity.Whats_On))
                    {
                        Opener.WhatsOn(activity);
                    }
                    else if(callingActivity.equalsIgnoreCase(CommonDef.CallingActivity.Open_House))
                    {
                       Opener.OpenHouseToday(activity);
                    }
                    else if(callingActivity.equalsIgnoreCase(CommonDef.CallingActivity.Auctions))
                    {
                        Opener.AuctionToday(activity);
                    }
                    else
                    {
                        activity.finish();
                    }
                    activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    break;
                case R.id.ibtn_right:
                    Opener.PropertyMapListing(activity);
//                    activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    break;
            }
        }
    }

    public static class TitleBarWithBackAndTitle implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public TitleBarWithBackAndTitle(Context context, String title) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.finish();
                    activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    break;
            }
        }
    }

    public static class UserProfileTitleBar implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;
        TextView tvEdit;
        public boolean editMode = true;

        public UserProfileTitleBar(Context context, String title) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
            tvEdit = (TextView) activity.findViewById(R.id.tv_right);
            tvEdit.setText("EDIT");
            tvEdit.setVisibility(View.GONE);
            ibtBack.setOnClickListener(this);
//            tvEdit.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
                case R.id.tv_right:
//                    editMode = true;
//                    tvEdit.setVisibility(View.GONE);
//                    Fragment fragment = activity.getFragmentManager().findFragmentById(R.id.profile_container);
//                    if (fragment instanceof UserPersonalDetails)
//                        ((UserPersonalDetails) fragment).enableEditMode(true);
//                    else if (fragment instanceof AgentPersonalDetails)
//                        ((AgentPersonalDetails) fragment).enableEditMode(true);
//                    break;
            }
        }

        public void enableEdit(boolean b) {
//            if (b) {
//                tvEdit.setVisibility(View.VISIBLE);
//            } else {
//                tvEdit.setVisibility(View.GONE);

            tvEdit.setVisibility(View.GONE);
        }
    }

    public static class KnowledgeBaseTitleBar implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public KnowledgeBaseTitleBar(Context context, String title) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
//                    if (activity.getFragmentManager().getBackStackEntryCount() == 1) {
//                        activity.finish();
//                        activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
//                        return;
//                    }
//                    activity.getFragmentManager().popBackStackImmediate();
//                    ((UserProfile)activity).showTabBar();
                    activity.onBackPressed();
                    break;
            }
        }
    }

    public static class AgentPublicProfileTitleBar implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public AgentPublicProfileTitleBar(Context context, String title) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
//                    if (activity.getFragmentManager().getBackStackEntryCount() == 1) {
//                        activity.finish();
//                        activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
//                        return;
//                    }
//                    activity.getFragmentManager().popBackStackImmediate();
//                    ((UserProfile)activity).showTabBar();
                    activity.onBackPressed();
                    break;
            }
        }
    }


    public static class MapsActivityTitleBar implements View.OnClickListener {
        private final ImageButton ibtnRight;
        MapsActivity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public MapsActivityTitleBar(MapsActivity activity, String title) {
            this.activity = activity;
            ibtBack = (ImageButton) this.activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            ibtnRight = (ImageButton) activity.findViewById(R.id.ibtn_right);
            ibtnRight.setVisibility(View.VISIBLE);
            ibtnRight.setImageResource(R.drawable.right_drak_blue_icon);
            tvTitle = (TextView) this.activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
            ibtnRight.setOnClickListener(this);
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            activity.onBackPressed();
        }
    }

    public static class SelectPriceTitleBar implements View.OnClickListener {
        PriceSelector activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public SelectPriceTitleBar(PriceSelector activity, String title) {
            this.activity = activity;
            ibtBack = (ImageButton) this.activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) this.activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            activity.onBackPressed();
        }
    }

    public static class SelectLandTitleBar implements View.OnClickListener {
        LandSelector activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public SelectLandTitleBar(LandSelector activity, String title) {
            this.activity = activity;
            ibtBack = (ImageButton) this.activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) this.activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            activity.onBackPressed();
        }
    }


    public static class AuctionRegistrationMenu implements View.OnClickListener {
        Activity activity;
        TextView tvTitle;
        TextView tvLeft;
        TextView tvRight;

        public AuctionRegistrationMenu(Activity activity, int pageNo) {
            this.activity = activity;
            tvLeft = (TextView) this.activity.findViewById(R.id.tv_left);
            tvTitle = (TextView) this.activity.findViewById(R.id.tv_center);
            tvRight = (TextView) this.activity.findViewById(R.id.tv_right);
            tvLeft.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.VISIBLE);
            tvRight.setVisibility(View.VISIBLE);
            tvLeft.setText("Cancel");
            tvTitle.setText("Auction Registration");
            tvRight.setText(pageNo + "/3");
            tvLeft.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_left:
                    activity.onBackPressed();
                    break;

            }
        }
    }

    public static class NotificationTitleBar implements View.OnClickListener {
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public NotificationTitleBar(Activity activity) {
            this.activity = activity;
            ibtBack = (ImageButton) this.activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) this.activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Notifications");
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            activity.onBackPressed();
        }
    }

    public static class PreAuctionOffer implements View.OnClickListener {
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public PreAuctionOffer(Activity activity,String title) {
            this.activity = activity;
            ibtBack = (ImageButton) this.activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) this.activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            activity.onBackPressed();
        }
    }

    public static class WatchListCAB implements View.OnClickListener {
        ImageButton ibtnLeft, ibtnRight;
        WatchList activity;
        TextView tvTitle;

        public WatchListCAB(WatchList activity) {
            this.activity = activity;
            ibtnLeft = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtnRight = (ImageButton) activity.findViewById(R.id.ibtn_right);
            ibtnRight.setImageResource(R.drawable.location_map_icon);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Watchlist");
            ibtnLeft.setVisibility(View.VISIBLE);
            ibtnRight.setVisibility(View.VISIBLE);
            ibtnRight.setTag(0);
            ibtnLeft.setOnClickListener(this);
            ibtnRight.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
                case R.id.ibtn_right:
                    if ((int) ibtnRight.getTag() == 0) {
                        ibtnRight.setImageResource(R.drawable.right_drak_blue_icon);
                        activity.openMapView();
                        ibtnRight.setTag(1);
                    } else {
                        ibtnRight.setImageResource(R.drawable.location_map_icon);
                        activity.openListView();
                        ibtnRight.setTag(0);

                    }
//                    Opener.PropertyMapListing(activity);
//                    activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    break;
            }
        }
    }

    public static class ViewListingAgentCAB {
        ImageButton ibtnLeft;
        ImageButton ibtnRight;
        ViewListingAgent activity;
        ImageView ivLogo;
        LinearLayout llCab;

        public ViewListingAgentCAB(ViewListingAgent viewListingAgent) {
            this.activity = viewListingAgent;
            ibtnLeft = (ImageButton) this.activity.findViewById(R.id.ibtn_left);
            ibtnRight = (ImageButton) this.activity.findViewById(R.id.ibtn_right);
            ibtnLeft.setVisibility(View.VISIBLE);
            ibtnRight.setVisibility(View.GONE);
            ibtnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.onBackPressed();
                }
            });
        }
    }

    public static class PreAuctionOfferAgentCAB implements View.OnClickListener {
        private final ImageButton ibtRight;
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public PreAuctionOfferAgentCAB(Context context, String title) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            ibtRight = (ImageButton) activity.findViewById(R.id.ibtn_right);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(Html.fromHtml("Property " + "<b>#" + title + "</b>"));
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
//                    if (activity.getFragmentManager().getBackStackEntryCount() == 1) {
//                        activity.finish();
//                        activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
//                        return;
//                    }
//                    activity.getFragmentManager().popBackStackImmediate();
//                    ((UserProfile)activity).showTabBar();
                    activity.onBackPressed();
                    break;
            }
        }
    }

    public static class ApplicationDetailsCAB implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public ApplicationDetailsCAB(Context context, String title) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(Html.fromHtml("Application " + "<b>#" + title + "</b>"));
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
//                    if (activity.getFragmentManager().getBackStackEntryCount() == 1) {
//                        activity.finish();
//                        activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
//                        return;
//                    }
//                    activity.getFragmentManager().popBackStackImmediate();
//                    ((UserProfile)activity).showTabBar();
                    activity.onBackPressed();
                    break;
            }
        }
    }

    public static class MyListing implements View.OnClickListener {
        ImageButton ibtnLeft, ibtnRight;
        MyListings activity;
        TextView tvTitle;

        public MyListing(MyListings activity) {
            this.activity = activity;
            ibtnLeft = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtnRight = (ImageButton) activity.findViewById(R.id.ibtn_right);
            ibtnRight.setImageResource(R.drawable.location_map_icon);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("My Listings");
//            ibtnLeft.setVisibility(View.VISIBLE);
            ibtnRight.setVisibility(View.VISIBLE);
            ibtnRight.setTag(0);
            ibtnLeft.setOnClickListener(this);
            ibtnRight.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
                case R.id.ibtn_right:
                    if ((int) ibtnRight.getTag() == 0) {
                        ibtnRight.setImageResource(R.drawable.right_drak_blue_icon);
                        activity.openMapView();
                        ibtnRight.setTag(1);
                    } else {
                        ibtnRight.setImageResource(R.drawable.location_map_icon);
                        activity.openListView();
                        ibtnRight.setTag(0);

                    }
//                    Opener.PropertyMapListing(activity);
//                    activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    break;
            }
        }
    }

    public static class WhatsOnCAB implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public WhatsOnCAB(Context context) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("What's On");
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
            }
        }
    }

    public static class DiscoverCAB implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;
        ImageView ivLogo;

        public DiscoverCAB(Context context) {
            this.context = context;
            activity = ((Activity) context);
            ivLogo= (ImageView) activity.findViewById(R.id.iv_center);
            ivLogo.setVisibility(View.VISIBLE);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.GONE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Discover");
            tvTitle.setVisibility(View.GONE);
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
            }
        }
    }

    public static class AuctionTodayCAB implements View.OnClickListener {
        ImageButton ibtnLeft, ibtnRight;
        AuctionToday activity;
        TextView tvTitle;

        public AuctionTodayCAB(AuctionToday activity) {
            this.activity = activity;
            ibtnLeft = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtnRight = (ImageButton) activity.findViewById(R.id.ibtn_right);
            ibtnRight.setImageResource(R.drawable.location_map_icon);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Auctions - This week");
            ibtnLeft.setVisibility(View.VISIBLE);
            ibtnRight.setVisibility(View.VISIBLE);
            ibtnRight.setTag(0);
            ibtnLeft.setOnClickListener(this);
            ibtnRight.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
                case R.id.ibtn_right:
                    if ((int) ibtnRight.getTag() == 0) {
                        ibtnRight.setImageResource(R.drawable.right_drak_blue_icon);
                        activity.openMapView();
                        ibtnRight.setTag(1);
                    } else {
                        ibtnRight.setImageResource(R.drawable.location_map_icon);
                        activity.openListView();
                        ibtnRight.setTag(0);

                    }
//                    Opener.PropertyMapListing(activity);
//                    activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    break;
            }
        }
    }

    public static class OpenHouseTodayCAB implements View.OnClickListener {
        ImageButton ibtnLeft, ibtnRight;
        OpenHouseToday activity;
        TextView tvTitle;


        public OpenHouseTodayCAB(OpenHouseToday activity) {
            this.activity = activity;
            ibtnLeft = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtnRight = (ImageButton) activity.findViewById(R.id.ibtn_right);
            ibtnRight.setImageResource(R.drawable.location_map_icon);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Open House - This Week");
            ibtnLeft.setVisibility(View.VISIBLE);
            ibtnRight.setVisibility(View.VISIBLE);
            ibtnRight.setTag(0);
            ibtnLeft.setOnClickListener(this);
            ibtnRight.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
                case R.id.ibtn_right:
                    if ((int) ibtnRight.getTag() == 0) {
                        ibtnRight.setImageResource(R.drawable.right_drak_blue_icon);
                        activity.openMapView();
                        ibtnRight.setTag(1);
                    } else {
                        ibtnRight.setImageResource(R.drawable.location_map_icon);
                        activity.openListView();
                        ibtnRight.setTag(0);

                    }
//                    Opener.PropertyMapListing(activity);
//                    activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    break;
            }
        }
    }

    public static class MyBidsCAB implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public MyBidsCAB(Context context) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("My Bids");
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
            }
        }
    }


    public static class SoldPropertySummaryCAB implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public SoldPropertySummaryCAB(Context context) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setImageResource(R.drawable.left_arrow);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Summary Report");
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
            }
        }
    }

    public static class PlaceBidCAB implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public PlaceBidCAB(Context context) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Place Bid");
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
            }

        }
    }

    public static class PropertyAuctionCAB implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack, ibtnRight;
        TextView tvTitle;

        public PropertyAuctionCAB(Context context, com.official.livebid.enums.AppMode appMode) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText("Property Auction");
            ibtnRight = (ImageButton) activity.findViewById(R.id.ibtn_right);
            ibtnRight.setVisibility(View.VISIBLE);
            if (appMode == AppMode.agent) {
                ibtnRight.setImageResource(R.drawable.ic_passed_icon);
                ibtnRight.setEnabled(true);
            } else {
                ibtnRight.setImageResource(R.drawable.ic_bell);
                ibtnRight.setVisibility(View.GONE);
                ibtnRight.setEnabled(false);
            }
            ibtBack.setOnClickListener(this);
            ibtnRight.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
                case R.id.ibtn_right:
                    ((PropertyAuction) activity).showPassPropertyPopup();
                    break;
            }

        }

        public void desablePassProperty() {
            ibtnRight.setEnabled(false);
        }
    }

    public static class MyRegisteredPropertyCAB implements View.OnClickListener {
        Context context;
        Activity activity;
        ImageButton ibtBack;
        TextView tvTitle;

        public MyRegisteredPropertyCAB(Context context, int selector) {
            this.context = context;
            activity = ((Activity) context);
            ibtBack = (ImageButton) activity.findViewById(R.id.ibtn_left);
            ibtBack.setVisibility(View.VISIBLE);
            tvTitle = (TextView) activity.findViewById(R.id.tv_center);
            tvTitle.setVisibility(View.VISIBLE);
            if (selector == CommonDef.MY_BIDS_BIDS)
                tvTitle.setText("My Bids");
            else if (selector == CommonDef.MY_BIDS_OFFERS)
                tvTitle.setText("My Offers");
            else if (selector == CommonDef.MY_BIDS_REGISTERED_PROPERTY)
                tvTitle.setText("Registered Properties");
            else tvTitle.setText("My Bids");
            ibtBack.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_left:
                    activity.onBackPressed();
                    break;
            }

        }
    }

    public static class NewRegistrationCAB {
        ImageButton ibtnLeft;
        ImageButton ibtnRight;
        NewRegistration activity;
        ImageView ivLogo;
        LinearLayout llCab;

        public NewRegistrationCAB(NewRegistration viewListingAgent) {
            this.activity = viewListingAgent;
            ibtnLeft = (ImageButton) this.activity.findViewById(R.id.ibtn_left);
            ibtnRight = (ImageButton) this.activity.findViewById(R.id.ibtn_right);
            ibtnLeft.setVisibility(View.VISIBLE);
            ibtnRight.setVisibility(View.GONE);
            ibtnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.onBackPressed();
                }
            });
        }
    }
}

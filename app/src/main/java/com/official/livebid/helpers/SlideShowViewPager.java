package com.official.livebid.helpers;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by user on 2/5/2016.
 */
public class SlideShowViewPager extends ViewPager {

    private OnClickListener mOnClickListner;

    public SlideShowViewPager(Context context) {
        super(context);
        setup();
    }

    public SlideShowViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    private void setup() {
        final GestureDetector tapGestureDetector = new GestureDetector(getContext(), new TapGestureListener());

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                tapGestureDetector.onTouchEvent(event);
                return false;
            }
        });
    }

    public void setOnViewPagerClickListener(OnClickListener onClickListener) {
        mOnClickListner = onClickListener;
    }


    private class TapGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (mOnClickListner != null) {
                mOnClickListner.onClick(SlideShowViewPager.this);
            }
            return true;
        }
    }

}


package com.official.livebid.helpers;

/**
 * Created by Robz Stha on 12/26/2014.
 */
public final class CommonDef {
    public static final String AUCTION_APPLICATION = "auction_application";
    public static final String SOLD_PROPERTY = "sold_property";
    public static final String PROP_AUCTION = "auction_property";
    public static final String LATEST_BID = "latest_bid";
    public static final int RC_PLACE_BID = 400;
    public static final String ROOM_DETAIL = "room_detail";
    public static final String OFFER_TYPE = "offer_type";
    public static final String MY_BID_PROP = "my_bid_prop";
    public static final String MY_BID_SELECTOR ="my_bid_selector" ;
    public static final boolean canPlaceBid = true;

    public static final int MY_BIDS_BIDS=1;
    public static final int MY_BIDS_OFFERS=2;
    public static final int MY_BIDS_REGISTERED_PROPERTY=3;
    public static final String REG_TYPE = "reg_type";
    public static final String USER_TYPE = "user_type";
    public static final String USER_DATA = "user_data";
    public static final String GUEST_TITLE = "guest_title";
    public static final int RC_REG_DEVICE = 5000;
    public static final String PUSH_NOTIFICATION = "push_notification";
    public static final int RC_UPDATE_SETTINGS = 50000;//5000 only for settings
    public static final int REQUEST_LOCATION = 6000;
    public static final String LIVEBID_URL = "feedback@livebid.com.au";


//    public static final String DIRECTORY = Environment.getExternalStorageDirectory()+"/"+"SMP/";

    public static String TAG = "Nabin is testing ";
    public final static int SOCKET_TIME_OUT = 10000;
    public final static int REQUEST_REGISTER_USER = 101;
    public final static int REQUEST_ACTIVATE_USER = 102;
    public final static int REQUEST_SIGN_IN = 103;
    public final static int REQUEST_NEW_CODE = 104;
    public final static int REQUEST_TOS = 105;
    public final static int REQUEST_PRIVACY_POLICY = 106;
    public final static int REQUEST_FORGOT_PASSWORD = 107;
    public final static int REQUEST_FORGOT_PASSWORD_VERIFICATION = 108;
    public final static int REQUEST_RESET_PASSWORD = 109;
    public final static int REQUEST_AUTOCOMPLETE_SEARCH = 110;
    public final static int REQUEST_DISCOVER_PROPERTYLIST = 111;
    public final static int REQUEST_SEARCH = 112;
    public final static int REQUEST_NEW_CODE_RESET_PWD=110;
    public final static int REQUEST_LOGOUT=111;
    public final static int REQUEST_USER_PROFILE =112;
    public final static int REQUEST_UPDATE_USER_PROFILE =113;
    public final static int REQUEST_KNOWLEDGE_BASE =114;
    public final static int REQUEST_RATE_APP =115;
    public final static int REQUEST_VERIFY_MOBILE =116;
    public final static int REQUEST_LAT_LNG =117;
    public final static int REQUEST_ADDRESS_FROM_LAT_LNG =118;
    public final static int REQUEST_NEW_CODE_MOBILE_CHANGE =119;
    public final static int REQUEST_AGENT_DETAILS =120;
    public final static int REQUEST_UPDATE_AGENT_DETAILS =121;
    public final static int REQUEST_AGENCY_PROFILE =122;
    public final static int REQUEST_AGENT_PUBLIC_PROFILE =123;
    public final static int REQUEST_PROPERTY_LISTINGS =124;
    public final static int REQUEST_AGENT_LISTINGS =125;
    public final static int REQUEST_ENQUIRY_TYPE =126;
    public final static int REQUEST_SEND_ENQUIRY =127;
    public final static int REQUEST_PROPERTY_AGENT_CONTACT =128;
    public final static int REQUEST_UPLOAD_IMAGE =130;

    public static String PROPERTY_LIST_INFO = "propertyListInfo";
    public final static int CAPTURE_SIGN = 12345;
    public final static int REQUEST_REGISTER_AUCTION = 201;
    public final static int REQUEST_REGISTER_AUCTION_SECOND = 305;
    public final static int REQUEST_REGISTER_AUCTION_THIRD = 306;
    public static final int REQUEST_CANCEL_AUCTION_REGISTER =202 ;
    public static final int REQUEST_MAKE_PRE_OFFER =203 ;
    public static final int REQUEST_MY_OFFER =204 ;
    public static final int REQ_AGENTS_LISTING =204 ;
    public static final int REQ_PRE_OFFERS_AGENT =205 ;
    public static final int REQ_PRE_AUCTION_APPLICATIONS =206 ;
    public static final int REQ_AUCTION_APPLICATION =207 ;
    public static final int REQ_VERIFY_AUCTION_APPLICATION =208 ;
    public static final int REQ_REG_BIDDERS =208 ;
    public static final int REQ_SALE_OF_CONTRACT =209 ;
    public static final int REQ_SALE_OF_CONTRACT_EMAIL =304 ;
    public static final int RC_WATCH_LIST =210 ;
    public static final int RC_MAKE_FAVOURITE =211 ;
    public static final int RC_WHATS_ON =212 ;
    public static final int RC_SUMMARY_REPORT_SOLD =213 ;
    public static final int RC_GET_BID_HISTORY =214 ;
    public static final int RC_JOIN_LIVE_AUCTION =215 ;
    public static final int RC_FETCH_BIDS =216 ;
    public static final int RC_SEND_GENERAL_BID =217 ;
    public static final int RC_SEND_VENDOR_BID = 218;
    public static final int RC_SEND_FIRST_CALL = 219;
    public static final int RC_SEND_SECOND_CALL = 220;
    public static final int RC_SEND_THIRD_CALL = 221;
    public static final int RC_PASS_PROPERTY = 222;
    public static final int RC_SOLD_PROPERTY = 223;
    public static final int RC_SEND_ON_SITE_BID = 224;
    public static final int RC_START_AUCTION = 225;
    public static final int RC_NOTIFY_BIDDERS = 226;
    public static final int RC_MY_REGISTERED_PROPERTIES = 227;
    public static final int RC_MY_BIDS_DETAIL = 228;
    public static final int RC_CONNECT_LIVE_AUCTION =229 ;
    public static final int RC_REG_AGENT=230 ;
    public static final int RC_GET_PROPERTY_INFO =231 ;


    public static final int RC_FETCH_NOTIFICATION =300 ;
    public static final int RC_DELETE_NOTIFICATION =301 ;
    public static final int RC_MARK_NOTIFICATION_SEEN =302 ;
    public static final int RC_UPDATE_DEVICE_INFO =303 ;
    public static final int REQ_PROPERTY_BID_DLT = 303;




    public static class SharedPrefKeys {

        public static final String UNIQUE_NUMBER = "unique_number";
        public static final String LAT = "lat";
        public static final String LNG = "lng";
        public static final String PUSH_NOTIFICATION = "push_notification";
        public static final String SHOW_SURROUNDINGS = "surroundings";
        public static String IS_LOGGED_IN = "isLoggedIn";
        public static String USER_ID = "userId";
        public static String OLD_USER_ID = "Old_userId";
        public static String USER_TYPE = "userType";
        public static String FIRST_NAME = "firstName";
        public static String LAST_NAME = "lastName";
        public static String USER_EMAIL = "userEmail";  // this is the original username
        public static String MOBILE_NUMBER = "mobileNumber";
        public static String ADDRESS = "address";
        public static String HASH_CODE = "hashCode";
        public static String DEVICE_ID = "deviceId";
        public static String CATEGORY_ID = "categoryId";
        public static String REG_ATTEMP = "attemp";
        public static String RESET_PWD_ATTEMP = "attemp";
        public static String PROFILE_IMG = "profile_img";
        public static String DENSITY_MATRIX = "density_matrix";
        public static String AUCTION_REGISTRATION_STATUS = "auction_register_status";
        public static String AGENT_ID = "agent_id";

        public static String Search_PostCodeIds="searchPostCodeIds";
        public static String Search_SuburbNames = "searchSuburbNames";
    }

    public static class CallingActivity{
        public static String Discover_Activity="1";
        public static String Whats_On="2";
        public static String Open_House="3";
        public static String Auctions="4";

        public static String callingActivity = "callingactivity";
        public static String searchedSuburbs="searchedSuburb";
    }
}

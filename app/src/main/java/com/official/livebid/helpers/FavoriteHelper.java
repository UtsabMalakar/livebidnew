package com.official.livebid.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.AppMode;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.listeners.FavouriteCompleteListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Ics on 3/16/2016.
 */
public class FavoriteHelper implements AsyncInterface {
    Context ctx;
    private Alerts alerts;
    FavouriteCompleteListener favouriteCompleteListener;
    AlertDialog alertDialog;

    public FavoriteHelper(Context ctx) {
        this.ctx = ctx;
        alerts=new Alerts(ctx);

    }
    public void makeFavorite(final String propertyId,boolean b, final FavouriteCompleteListener favouriteCompleteListener){
        this.favouriteCompleteListener=favouriteCompleteListener;
        if(new com.official.livebid.helpers.AppMode(ctx).getAppmode()== AppMode.guest){
            favouriteCompleteListener.onNotLoggedIn();
            return;
        }
        if(b){
            new AlertDialog.Builder(ctx)
                    .setTitle("LiveBid")
                    .setMessage("Do you really want to remove from watchlist?")
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            favouriteCompleteListener.onFailure();
                        }
                    })
                    .setPositiveButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            favouriteCompleteListener.onFailure();
                        }})
                    .setNegativeButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestFavourite(propertyId);
                        }
                    }).show();
            return;
        }

        requestFavourite(propertyId);
    }

    private void requestFavourite(String propertyId) {
        HashMap<String, String> headers = CommonMethods.getHeaders((Activity) ctx);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyId);
        System.out.println("Auction Applications params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest((Activity)ctx, Request.Method.POST, params, UrlHelper.MAKE_FAVOURITE, headers, false, CommonDef.RC_MAKE_FAVOURITE);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("Auction Application Response:" + result);
        switch (requestCode) {
            case CommonDef.RC_MAKE_FAVOURITE:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        favouriteCompleteListener.onSuccess(joResult.getBoolean("favorite"));

                    } else {
                        favouriteCompleteListener.onFailure();
//                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }
    }
}

package com.official.livebid.helpers;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.official.livebid.R;
import com.official.livebid.SalesTeamActivity;
import com.official.livebid.activities.ActivityGuestMode;
import com.official.livebid.activities.AgencyProfile;
import com.official.livebid.activities.AgentPublicProfile;
import com.official.livebid.activities.AuctionRegistration;
import com.official.livebid.activities.ContactAgency;
import com.official.livebid.activities.ContactAgent;
import com.official.livebid.activities.DocVerification.DocumentVerification;
import com.official.livebid.activities.GuestAuctionRegistration;
import com.official.livebid.activities.GuestNotifications;
import com.official.livebid.activities.HelpAndSupport;
import com.official.livebid.activities.KnowledgeBase;
import com.official.livebid.activities.LandSelector;
import com.official.livebid.activities.LandingScreen;
import com.official.livebid.activities.MapsActivity;
import com.official.livebid.activities.MobileVerification;
import com.official.livebid.activities.Notifications;
import com.official.livebid.activities.PreAuctionOfferUser;
import com.official.livebid.activities.PriceSelector;
import com.official.livebid.activities.PrivacyPolicy;
import com.official.livebid.activities.ProblemSignIn;
import com.official.livebid.activities.PropertyDetails;
import com.official.livebid.activities.PropertyListing;
import com.official.livebid.activities.Refine;
import com.official.livebid.activities.ResetPassword;
import com.official.livebid.activities.SalesAgent;
import com.official.livebid.activities.Search;
import com.official.livebid.activities.Settings;
import com.official.livebid.activities.SettingsLoremIpsum;
import com.official.livebid.activities.Signature;
import com.official.livebid.activities.SubmitAuctionRegistration;
import com.official.livebid.activities.TermsOfServices;
import com.official.livebid.activities.discover.Discover;
import com.official.livebid.activities.liveAuction.PassedPropertySummaryReport;
import com.official.livebid.activities.liveAuction.PropertyAuction;
import com.official.livebid.activities.liveAuction.PropertyAuctionModel;
import com.official.livebid.activities.liveAuction.SoldPropertyModel;
import com.official.livebid.activities.liveAuction.SoldPropertySummaryReport;
import com.official.livebid.activities.myBids.MyBids;
import com.official.livebid.activities.myBids.MyBidsGuest;
import com.official.livebid.activities.myBids.MyBidsHome;
import com.official.livebid.activities.myBids.MyRegisteredProperty;
import com.official.livebid.activities.myBids.MyRegisteredPropertyDetail;
import com.official.livebid.activities.myBids.RegisteredPropertyModel;
import com.official.livebid.activities.myListings.MyListings;
import com.official.livebid.activities.newRegistration.NewRegistration;
import com.official.livebid.activities.newRegistration.RegType;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.activities.profile.Profile;
import com.official.livebid.activities.viewListingsAgent.AuctionApplicationDetail;
import com.official.livebid.activities.viewListingsAgent.AuctionApplications;
import com.official.livebid.activities.viewListingsAgent.PreAuctionOffersAgent;
import com.official.livebid.activities.viewListingsAgent.RegisteredBidders;
import com.official.livebid.activities.viewListingsAgent.SaleOfContract;
import com.official.livebid.activities.viewListingsAgent.ViewListingAgent;
import com.official.livebid.activities.watchList.WatchList;
import com.official.livebid.activities.whatsOn.AuctionToday;
import com.official.livebid.activities.whatsOn.OpenHouseToday;
import com.official.livebid.activities.whatsOn.WhatsOn;
import com.official.livebid.enums.OfferType;
import com.official.livebid.objects.AgentInfo;
import com.official.livebid.objects.AuctionApplicationModel;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.objects.RoomDetail;
import com.official.livebid.staticClasses.StaticVars;

import java.util.ArrayList;


/**
 * Created by Rabin
 * <p/>
 * This class is the activity opener
 */
public class Opener {
    private static SharedPreference pref;

    public static void LandingScreen(Activity activity) {
        pref = new SharedPreference(activity);
        if(!pref.getBoolValues(CommonDef.SharedPrefKeys.IS_LOGGED_IN)){
          Intent  intent = new Intent(activity, LandingScreen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }else{
            Discover(activity);
        }
        activity.finish();
    }

//    public static void Register(Activity activity) {
//        Intent intent = new Intent(activity, Registration.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//    }
    public static void newRegister(Activity activity,UserType userType) {
        Intent intent = new Intent(activity, NewRegistration.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.USER_TYPE,userType);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }



    public static void PropertyListing(Activity activity, String result, String callingActivity) {
        Intent intent = new Intent(activity, PropertyListing.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("result", result);
        intent.putExtra(CommonDef.CallingActivity.callingActivity,callingActivity);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    public static void MobileVerification(Activity activity,RegType regType) {
        Intent intent = new Intent(activity, MobileVerification.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.REG_TYPE,regType);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

//    public static void SignIn(Activity activity) {
//        Intent intent = new Intent(activity, SignIn.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//
//    }

    public static void ProblemSignIn(Activity activity, RegType regType) {
        Intent intent = new Intent(activity, ProblemSignIn.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.REG_TYPE,regType);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public static void ResetPassword(Activity activity, RegType regType) {
        Intent intent = new Intent(activity, ResetPassword.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.REG_TYPE,regType);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public static void TOS(Activity activity) {
        Intent intent = new Intent(activity, TermsOfServices.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public static void PrivacyPolicy(Activity activity) {
        Intent intent = new Intent(activity, PrivacyPolicy.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public static void PropertyMapListing(Activity activity) {
        Intent intent = new Intent(activity, MapsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void Search(Activity activity, String callingActivity) {
        Intent intent = new Intent(activity, Search.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.CallingActivity.callingActivity, callingActivity);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void Refine(Activity activity, String callingActivity, String searchedSuburbs) {
        Intent intent = new Intent(activity, Refine.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.CallingActivity.callingActivity,callingActivity);
        intent.putExtra(CommonDef.CallingActivity.searchedSuburbs,searchedSuburbs);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void Discover(Activity activity) {
        Intent intent = new Intent(activity, Discover.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);


    }
    public static void Profile(Activity activity) {
        Intent intent = new Intent(activity, Profile.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//        Intent intent = new Intent(activity, UserProfile.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    public static void HelpNSupport(Activity activity) {
        Intent intent = new Intent(activity, HelpAndSupport.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    public static void KnowledgeBase(Activity activity) {
        Intent intent = new Intent(activity, KnowledgeBase.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    public static void Settings(Activity activity) {
        Intent intent = new Intent(activity, Settings.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    public static void SettingsLoremIpsum(Activity activity) {
        Intent intent = new Intent(activity, SettingsLoremIpsum.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    public static void AgentPublicProfile(Activity activity,String agentID) {
        Intent intent = new Intent(activity, AgentPublicProfile.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("agent_id",agentID);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    public static void AgencyProfile(Activity activity,String agencyId) {
        Intent intent = new Intent(activity, AgencyProfile.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("agency_id",agencyId);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    public static void contactAgency(Activity activity,String agencyId) {
        Intent intent = new Intent(activity, ContactAgency.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("agency_id",agencyId);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    public static void contactAgent(Activity activity,String agentId) {
        Intent intent = new Intent(activity, ContactAgent.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("agent_id",agentId);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    public static void SelectAgent(Activity activity,String agencyId) {
        Intent intent = new Intent(activity, SalesAgent.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("agency_id",agencyId);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }


    public static void PropertyDetails(Activity activity, PropertyListInfo propertyListInfo) {
        Intent intent = new Intent(activity, PropertyDetails.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROPERTY_LIST_INFO, propertyListInfo);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void Notification(Activity activity) {
        Intent intent = new Intent(activity, Notifications.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void GuestNotification(Activity activity) {
        Intent intent = new Intent(activity, GuestNotifications.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void GuestAuctionRegistration(Activity activity) {
        Intent intent = new Intent(activity, GuestAuctionRegistration.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void PriceSelector(Activity activity) {
        Intent intent = new Intent(activity, PriceSelector.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void LandSelector(Activity activity) {
        Intent intent = new Intent(activity, LandSelector.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void Signature(Activity activity){
        Intent intent = new Intent(activity, Signature.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void AuctionRegistration(Activity activity, PropertyListInfo prop){
        Intent intent = new Intent(activity, AuctionRegistration.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROPERTY_LIST_INFO,prop);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void SubmitAuctionRegistration(Activity activity, Uri doc, Uri self, int item){
        Intent intent = new Intent(activity, SubmitAuctionRegistration.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("docUri",doc);
        intent.putExtra("self",self);
        intent.putExtra("selectedId",item);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void MakeAnOffer(Activity activity, PropertyListInfo prop, OfferType offerType, long longPriveValue, String settlementDate){
        Intent intent = new Intent(activity, PreAuctionOfferUser.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROPERTY_LIST_INFO,prop);
        intent.putExtra(CommonDef.OFFER_TYPE,offerType);
        intent.putExtra("PriceValue",longPriveValue);
        intent.putExtra("settlementDate",settlementDate);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void ViewListingsAgent(Activity activity,PropertyListInfo prop){
        Intent intent = new Intent(activity, ViewListingAgent.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROPERTY_LIST_INFO,prop);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void MyListings(Activity activity){
        Intent intent = new Intent(activity, MyListings.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void PreAuctionOfferAgent(Activity activity,PropertyListInfo prop){
        Intent intent = new Intent(activity, PreAuctionOffersAgent.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROPERTY_LIST_INFO,prop);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void AuctionApplications(Activity activity,PropertyListInfo prop){
        Intent intent = new Intent(activity, AuctionApplications.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROPERTY_LIST_INFO,prop);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void RegisteredBidders(Activity activity,PropertyListInfo prop){
        Intent intent = new Intent(activity, RegisteredBidders.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROPERTY_LIST_INFO,prop);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void AuctionApplicationDetail(Activity activity,AuctionApplicationModel app){
        Intent intent = new Intent(activity, AuctionApplicationDetail.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.AUCTION_APPLICATION,app);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void SaleOfContract(Activity activity){
        Intent intent = new Intent(activity, SaleOfContract.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void WatchList(Activity activity){
        Intent intent = new Intent(activity, WatchList.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void WhatsOn(Activity activity){
        Intent intent = new Intent(activity, WhatsOn.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void AuctionToday(Activity activity) {
        Intent intent = new Intent(activity, AuctionToday.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void OpenHouseToday(Activity activity){
        Intent intent = new Intent(activity, OpenHouseToday.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void MyBds(Activity activity){
        Intent intent = new Intent(activity, MyBids.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void MyBidsHome(Activity activity){
        Intent intent = new Intent(activity, MyBidsHome.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    public static void MyBidsGuest(Activity activity) {
        Intent intent = new Intent(activity, MyBidsGuest.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void SalesTeam(Activity activity, ArrayList<AgentInfo> agentInfo,String agencyId) {

        StaticVars.agentInfo = agentInfo;

        Intent intent = new Intent(activity, SalesTeamActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("agencyIdGlobal",agencyId);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void SoldPropertySummary(Activity activity, SoldPropertyModel property) {
        Intent intent = new Intent(activity, SoldPropertySummaryReport.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.SOLD_PROPERTY,property);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void PassedPropertySummary(Activity activity, SoldPropertyModel property) {
        Intent intent = new Intent(activity, PassedPropertySummaryReport.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.SOLD_PROPERTY,property);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void PropertyAuction(Activity activity, PropertyAuctionModel property, RoomDetail roomDetail) {
        Intent intent = new Intent(activity, PropertyAuction.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROP_AUCTION,property);
        intent.putExtra(CommonDef.ROOM_DETAIL,roomDetail);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void myRegisteredProperties(Activity activity,int selector) {
        Intent intent = new Intent(activity, MyRegisteredProperty.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.MY_BID_SELECTOR,selector);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void registeredPropertyDetail(Activity activity, RegisteredPropertyModel property,int selector) {
        Intent intent = new Intent(activity, MyRegisteredPropertyDetail.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.MY_BID_PROP,property);
        intent.putExtra(CommonDef.MY_BID_SELECTOR,selector);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public static void guestMode(Activity activity, String title) {
        Intent intent = new Intent(activity, ActivityGuestMode.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.GUEST_TITLE,title);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void docVerification(Activity activity){
        Intent intent = new Intent(activity, DocumentVerification.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}



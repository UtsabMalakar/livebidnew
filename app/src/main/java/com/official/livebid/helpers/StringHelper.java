package com.official.livebid.helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ics on 6/24/2016.
 */
public class StringHelper {
    public static String capitalizeInitial(String str) {
        StringBuffer stringbf = new StringBuffer();
        Matcher m = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(str);
        while (m.find()) {
            m.appendReplacement(stringbf, m.group(1).toUpperCase() + m.group(2).toLowerCase());
        }
        return m.appendTail(stringbf).toString();
    }
}

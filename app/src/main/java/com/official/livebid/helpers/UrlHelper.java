package com.official.livebid.helpers;

/**
 * Created by Rabin on 8/13/2015.
 */
public class
UrlHelper {

//    public final static String BASE_URL = "http://192.168.0.54/livebid/webservice/";
    public final static String BASE_URL = "http://livebid.draftserver.com/livebid/webservice/";
//    public final static String BASE_URL = "http://app.livebid.com.au/webservice/";
//    public final static String BASE_URL = "http://ci.draftserver.com/livebid/webservice/";

    public final static String REGISTER_USER = BASE_URL+"register";
    public final static String VERIFY_USER = BASE_URL+"mobile_verification";
    public final static String RESEND_CODE = BASE_URL+"resend_sms";
    public final static String SIGN_IN = BASE_URL+"login";
    public final static String GET_TOS = BASE_URL+"get_terms";
    public final static String GET_PRIVACY_POLICY = BASE_URL+"get_privacy";
    public final static String FORGET_PASSWORD = BASE_URL+"forgot_password";
    public final static String FORGET_PASSWORD_VERIFICATION = BASE_URL+"mobile_verification_reset";
    public final static String RESET_PASSWORD = BASE_URL+"reset_password_code";
    public final static String AUTOCOMPLETE_SEARCH = BASE_URL+"discover/autocompletion_search";
    public final static String DISCOVER_PROPERTY_LIST = BASE_URL+"discover/dashboard";
    public final static String SEARCH = BASE_URL+"discover/refine_search";
    public final static String RESEND_CODE_RESET_PWD = BASE_URL+"resend_sms_reset";
    public final static String LOG_OUT= BASE_URL+"logout";
    public final static String USER_VIEW_PROFILE= BASE_URL+"view_profile";
    public final static String UPDATE_USER_PROFILE = BASE_URL+"edit_user_profile";
    public final static String KNOWLEDGE_BASE = BASE_URL+"knowledge_base";
    public final static String RATE_APP = BASE_URL+"add_rating";
    public final static String VERIFY_MOBILE = BASE_URL+"verify_mobile_number";
    public final static String RESEND_CODE_MOBILE_CHANGE = BASE_URL+"resend_sms_temp";
    public final static String GET_AGENT_DETAILS = BASE_URL+"agents/view_profile";
    public final static String UPDATE_AGENT_DETAILS = BASE_URL+"agents/edit_agent_profile";
    public final static String GET_AGENCY_PROFILE = BASE_URL+"agents/agency_profile";
    public final static String GET_AGENT_PUBLIC_PROFILE = BASE_URL+"agents/agent_public_profile";
    public final static String GET_AGENT_LISTINGS = BASE_URL+"agents/agency_agents_list";
    public final static String GET_ENQUIRY_TYPE = BASE_URL+"get_enquiry_types";
    public final static String SEND_ENQUIRY = BASE_URL+"agents/contact_agent";
    public final static String GET_PROPERTY_LISTINGS = BASE_URL+"agents/property_listing";

    public final static String GET_PROPERTY_AGENTS = BASE_URL+"/discover/property_detail_agents";
    public final static String UPLOAD_IMAGE = BASE_URL+"upload_image";
    public final static String REGISTER_TO_AUCTION = BASE_URL+"auctions/register_to_auction";
    public final static String CANCEL_AUCTION_REG = BASE_URL+"auctions/cancel_auction_registration";
    public final static String MAKE_PRE_OFFER = BASE_URL+"auctions/make_pre_offer";
    public final static String MY_OFFER_DETAILS = BASE_URL+"discover/my_offer_detail";
    public final static String GET_AGENTS_MY_LISTING = BASE_URL+"agents/agents_listing";
    public final static String GET_PRE_OFFERS_AGENT = BASE_URL+"agents/pre_auction_offers";
    public final static String GET_PRE_AUCTION_APPLICATIONS = BASE_URL+"agents/total_applications";
    public final static String GET_AUCTION_APPLICATION = BASE_URL+"agents/get_user_detail";
    public final static String VERIFY_AUCTION_APPLICATION = BASE_URL+"auctions/verify_auction_registration";
    public final static String GET_REG_BIDDERS = BASE_URL+"agents/registered_bidders";
    public final static String GET_SALE_OF_CONTRACT = BASE_URL+"agents/process_contract_of_sale";
    public final static String GET_WATCH_LIST = BASE_URL+"discover/watch_list";
    public final static String MAKE_FAVOURITE = BASE_URL+"discover/make_favourite";
    public final static String GET_WHATS_ON = BASE_URL+"discover/dashboard";
    public final static String GET_SUMMARY_REPORT_SOLD = BASE_URL+"livebid/report_summary";
    public final static String GET_BID_HISTORY = BASE_URL+"livebid/bid_history";
    public final static String JOIN_LIVE_AUCTION = BASE_URL+"livebid/join_auction";
    public final static String FETCH_BIDS = BASE_URL+"livebid/fetch_previous_bids";
    public final static String SEND_BID = BASE_URL+"livebid/send_message";
    public final static String START_AUCTION = BASE_URL+"livebid/start_auction";
    public static final String NOTIFY_BIDDERS =BASE_URL+"notifications/notify_bidders";
    public static final String MY_REGISTERED_PROPERTIES =BASE_URL+"discover/my_registered_properties";
    public static final String MY_BIDS_DETAIL = BASE_URL+"discover/my_bid_detail";
    public static final String MY_BID_BIDS = BASE_URL + "discover/my_bids";
    public static final String MY_BID_OFFERS = BASE_URL+"discover/my_offers";
    public static final String REG_AGENT = BASE_URL+"register_agent";
    public static final String REG_DEVICE = BASE_URL + "update_device_info";
    public final static String FETCH_NOTIFICATION = BASE_URL+"notifications/fetch_app_notifications";
    public final static String DELETE_NOTIFICATION = BASE_URL+"notifications/delete_notification";
    public final static String MARK_NOTIFICATION_SEEN = BASE_URL+"notifications/make_notification_seen";
    public final static String UPDATE_DEVICE_INFO = BASE_URL+"update_device_info";
    public static final String GET_PROPERTY_INFO = BASE_URL +"discover/property_detail";
    public static final String UPDATE_SETTINGS = BASE_URL+"setting/on_off_settings";
    public static final String GET_PROPERTY_BID_DTL = BASE_URL + "agents/get_property_bid_dtl";
}

package com.official.livebid.helpers;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.official.livebid.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Prajeet on 2/1/2016.
 */
public class ImageSlideAdapter extends PagerAdapter {

   ArrayList<String> mImages;
    Context mContext;
    LayoutInflater mLayoutInflater;
    int propertyStatus;


    public ImageSlideAdapter(Context context, ArrayList<String> images,int propertyStatus) {
        this.mContext = context;
        this.mImages = images;
        this.propertyStatus = propertyStatus;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
//        if(this.propertyStatus==2)
//        {
//            if(mImages.size()>0)
//                return 1;
//            else
//                return 0;
//        }
//        else
//        {
            return mImages.size();
//        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.slide_show_image, container, false);
        final ImageView slideImageView = (ImageView) itemView.findViewById(R.id.slideshow_single_img_iv);
        final ProgressBar pbLoading = (ProgressBar) itemView.findViewById(R.id.pbLoading);
//        pbLoading.setVisibility(View.GONE);
        if(!mImages.get(position).trim().isEmpty()) {
            pbLoading.setVisibility(View.GONE);
            if (propertyStatus == 2) {
                Picasso.with(mContext).
                        load(mImages.get(position)).
                        into(slideImageView, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
            }
            else
            {
                Picasso.with(mContext).
                        load(mImages.get(position)).
                        into(slideImageView, new Callback() {
                            @Override
                            public void onSuccess() {
//                                pbLoading.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {

                            }
                        });
            }
        }
        else
        {
            Picasso.with(mContext).
                    load(R.drawable.img_coming_soon).
                    into(slideImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            pbLoading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
        container.addView(itemView);
        return itemView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View) object);
    }
}

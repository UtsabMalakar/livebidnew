package com.official.livebid.helpers;

import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.android.volley.Request;
import com.official.livebid.adapters.PropertyListingAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.SearchParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Robz on 1/14/2016.
 */
public class SearchHelper implements AsyncInterface {

    Activity activity;
    private Alerts alerts;
    private OnSearchComplete listener;

    public SearchHelper(Activity activity) {


        this.activity = activity;
        this.listener=(OnSearchComplete)activity;
        alerts = new Alerts(activity);
    }

    public void search() {
        HashMap<String, String> params = new HashMap<>();

        params.put("postcode_ids", SearchParams.postCodeId);
        params.put("keywords", SearchParams.keywordSearch);
        params.put("property_types", SearchParams.propertyType);
        params.put("listing_types", SearchParams.listingType);
        params.put("price1", SearchParams.priceRangeMin);
        params.put("price2", SearchParams.priceRangeMax);
        params.put("land_size1", SearchParams.landRangeMin);
        params.put("land_size2", SearchParams.landRangeMax);
        params.put("bedrooms", SearchParams.nosBedrooms);
        params.put("bathrooms", SearchParams.nosBathrooms);
        params.put("parking", SearchParams.nosParking);
        params.put("auto_keyword", SearchParams.autoKeyword);
        params.put("lat", SearchParams.lat);
        params.put("lng", SearchParams.lng);

        HashMap<String, String> header = CommonMethods.getHeaders(activity);
        VolleyRequest volleyRequest = new VolleyRequest(activity, Request.Method.POST, params, UrlHelper.SEARCH, header, false, CommonDef.REQUEST_SEARCH);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("Search result: " + result);
        System.out.print("Testing search==" + SearchParams.postCodeId + "----------" + SearchParams.suburbNames);
        SharedPreference sharedPreference;
        sharedPreference = new SharedPreference(activity);
        sharedPreference.setKeyValues(CommonDef.SharedPrefKeys.Search_PostCodeIds, SearchParams.postCodeId);
        sharedPreference.setKeyValues(CommonDef.SharedPrefKeys.Search_SuburbNames, SearchParams.suburbNames);

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            if (code.equals("0001")) {
                JSONArray jaList = joResult.getJSONArray("list");
                if (jaList.length() > 0) {
                  listener.onRresultFound(result);
//                    Opener.PropertyListing(activity, result);
                }
            } else {
                listener.onError(msg);
//                alerts.showOkMessage(msg);
            }
        } catch (JSONException e) {
            alerts.showOkMessage(e.toString());
            e.printStackTrace();
        }

//        Toast.makeText(activity, "Search result: "+result, Toast.LENGTH_LONG).show();

    }
    public static interface OnSearchComplete{
        void onRresultFound(String result);
        void onResultNotFound(String msg);
        void onError(String msg);
    }
}

package com.official.livebid.helpers;

import android.content.Context;

/**
 * Created by Ics on 3/30/2016.
 */
public class AppMode {
    private static com.official.livebid.enums.AppMode mode;
    private static SharedPreference prefs;

    public AppMode(Context context) {
        prefs = new SharedPreference(context);

    }
    public com.official.livebid.enums.AppMode getAppmode(){
        if (prefs != null && prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).equals("2")) {
            mode = com.official.livebid.enums.AppMode.user;
        } else if (prefs != null && prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).equals("3")) {
            mode = com.official.livebid.enums.AppMode.agent;
        } else {
            mode = com.official.livebid.enums.AppMode.guest;
        }
        return mode;
    }

    public  static  String getAuctionRegStatus(){
        return prefs.getStringValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS);
    }


}

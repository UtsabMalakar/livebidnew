package com.official.livebid.helpers;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.graphics.Point;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.official.livebid.enums.AppMode;
import com.official.livebid.objects.AvailabilityModel;
import com.official.livebid.objects.PropertyInfo;
import com.official.livebid.objects.PropertyListInfo;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Rabin on 9/7/2015.
 */
public class CommonMethods {

    public static Intent getPickImageChooserIntent(Activity activity) {

// Determine Uri of camera image to  save.
        Uri outputFileUri = getCaptureImageOutputUri(activity);

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = activity.getPackageManager();

// collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

// collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

// the main intent is the last in the  list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main  intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture  by camera.
     */
    private static Uri getCaptureImageOutputUri(Activity activity) {
        Uri outputFileUri = null;
        File getImage = activity.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from  {@link #//getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera  and gallery image.
     *
     * @param data the returned data of the  activity result
     */
    public static Uri getPickImageResultUri(Intent data, Activity activity) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri(activity) : data.getData();
    }

    /**
     * Method for removing the keyboard if touched outside the editview.
     *
     * @param view
     * @param baseActivity
     */
    public static void setupUI(View view, final Activity baseActivity) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
//                    baseActivity.closeKeyboard();
                    hideSoftKeyboard(baseActivity);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView, baseActivity);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {

        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    /**
     * @return : file name with jpt extension
     */
    public static String randomFileName(String ext) {
        String fileName;
        fileName = String.format("%s.%s", System.currentTimeMillis(), ext);
        return fileName;
    }

    public static Point getDisplaySize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static HashMap<String, String> getHeaders(Activity activity) {
        SharedPreference prefs = new SharedPreference(activity);
        String userType;
        if (prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).isEmpty()) {
            userType = "1";
        } else {
            userType = prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", prefs.getStringValues(CommonDef.SharedPrefKeys.USER_ID));
        params.put("hash_code", prefs.getStringValues(CommonDef.SharedPrefKeys.HASH_CODE));
        params.put("device_id", prefs.getStringValues(CommonDef.SharedPrefKeys.DEVICE_ID));
        params.put("device_type", "2");
        params.put("user_type", userType);
        return params;
    }
    public static HashMap<String, String> getHeaders(Context activity) {
        SharedPreference prefs = new SharedPreference(activity);
        String userType;
        if (prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).isEmpty()) {
            userType = "1";
        } else {
            userType = prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", prefs.getStringValues(CommonDef.SharedPrefKeys.USER_ID));
        params.put("hash_code", prefs.getStringValues(CommonDef.SharedPrefKeys.HASH_CODE));
        params.put("device_id", prefs.getStringValues(CommonDef.SharedPrefKeys.DEVICE_ID));
        params.put("device_type", "2");
        params.put("user_type", userType);
        return params;
    }

    public static void setImageviewRatio(Context ctx, ImageView iv, int padding) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels - 2 * padding;
        int screenHeight = screenWidth * 3 / 4;
        System.out.println("Width=>" + screenWidth);
        System.out.println("Height=>" + screenHeight);
        iv.getLayoutParams().width = screenWidth;
        iv.getLayoutParams().height = screenWidth * 3 / 4;
        iv.requestLayout();
    }

    public static float dpFromPx(final Activity activity, final int px) {
        return px / activity.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Activity activity, final int dp) {
        return dp * activity.getResources().getDisplayMetrics().density;
    }
    public static float pxFromDp(final Context activity, final int dp) {
        return dp * activity.getResources().getDisplayMetrics().density;
    }

    public static void makeCall(Context context, String mobileNumber) {

        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
        if ((tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT)) {
            Toast.makeText(context, "Please insert SIM", Toast.LENGTH_SHORT).show();
            return;
        }
        if ((tm.getSimState() != TelephonyManager.SIM_STATE_READY)) {
            Toast.makeText(context, "Sim is not ready.", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mobileNumber));
        try {
            context.startActivity(callIntent);

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "yourActivity is not founded", Toast.LENGTH_SHORT).show();
        }
    }

    public static void visitUrl(Context context, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);

    }

    public static void sendEmail(Context context, String EmailId) {
        /* Create the Intent */
        final Intent emailIntent = new Intent(Intent.ACTION_SEND);

/* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{EmailId});
//        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");

/* Send it off to the Activity-Chooser */
        context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }
    /**
     * method ro return day string from dayindex
     *
     * @param from
     * @param to
     * @return
     */

    public static String getAvailableDay(int from, int to) {
        if (from != to)
            return getDay(from) + " - " + getDay(to);
        else
            return getDay(from);


    }

    private static String getDay(int n) {
        String day = "";
        switch (n) {
            case 1:
                day = "Monday";
                break;
            case 2:
                day = "Tuesday";
                break;
            case 3:
                day = "Wednesday";
                break;
            case 4:
                day = "Thursday";
                break;
            case 5:
                day = "Friday";
                break;
            case 6:
                day = "Saturday";
                break;
            case 7:
                day = "Sunday";
                break;
        }
        return day;
    }

    /**
     * method to check call availability
     */
    public static boolean isCallAvailable(ArrayList<AvailabilityModel> availabilities) {
        boolean result = false;
        for (int i = 0; i < availabilities.size(); i++) {
            if (isInDayRange(availabilities.get(i).getFromDay(), availabilities.get(i).getToDay())
                    && isInTimeRange(availabilities.get(i).getFromTime(), availabilities.get(i).getToTime())) {
                result = true;
                break;
            }
        }
        return result;
    }


    private static boolean isInTimeRange(String timeFrom, String timeTo) {
        boolean result;
        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR);
        int minute = now.get(Calendar.MINUTE);
        int am_pm = now.get(Calendar.AM_PM);
        Date date;
        Date sTime;
        Date tTime;
        sTime = parseTime(timeFrom);
        tTime = parseTime(timeTo);
        date = parseTime(hour + ":" + minute + ((am_pm == 0) ? " AM" : " PM"));
        System.out.println("now:" + date.toString());
        System.out.println("stime:" + sTime.toString());
        System.out.println("ttime:" + tTime.toString());
        if (date.after(sTime) && date.before(tTime)) {
            result = true;
        } else {
            result = false;
        }
        System.out.println("time:" + result);
        return result;
    }

    private static Date parseTime(String time) {
        SimpleDateFormat inputParser = new SimpleDateFormat("hh:mm aa",Locale.US);
        try {
            return inputParser.parse(time);

        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    private static boolean isInDayRange(int dayFrom, int dayTo) {
        boolean result = false;
        Calendar now = Calendar.getInstance();
        int day = now.get(Calendar.DAY_OF_WEEK);
        System.out.println("day:" + day);

        if (day >= dayFrom && day <= dayTo) {
            result = true;
        } else {
            result = false;
        }
        System.out.println("day:" + result);
        return result;
    }

    /**
     * map propertyInfo model to propertyListInfo
     */
    public static PropertyListInfo mapPropInfoToPropListInfo(PropertyInfo info) {
        PropertyListInfo prop = new PropertyListInfo();
        prop.id = info.getId();
        prop.agentId = info.getAgent_id();
        prop.agencyId = info.getAgency_id();
        prop.propertyTitle = info.getProperty_title();
        prop.propertyDesc = info.getProperty_description();
        prop.propertyOtherFeatures = info.getProperty_other_geatures();
        prop.propertyCode = info.getProperty_code();
        prop.propertyType = "";
        prop.landArea = "";
        prop.cat = "";
        prop.postCode = info.getPostcode();
        prop.suburb = info.getSuburb();
        prop.lat = info.getLat();
        prop.lng = info.getLng();
        prop.views = "";
        prop.agentName = info.getAgent_name();
        prop.agentTradingName = "";
        prop.agentImportId = "";
        prop.distance = info.getDistance();
        prop.street = info.getStreet();
        prop.propertyImgUrl = info.getProperty_image();
        prop.propertyStatus = info.getProperty_status();
        prop.propertyStatus2 = info.getPropertyStatus2();
        prop.dateTag = info.getAuction_date();
        prop.suburb = info.getSuburb();
        prop.propertyState = info.getState();
        prop.propertyStatusIdentifier = "from";
        prop.price = "$" + NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(info.getPrice()));
        prop.nosOfBedroom = info.getBedrooms();
        prop.nosOfBathroom = info.getBathrooms();
        prop.nosOfParking = info.getParking();
        prop.agentProfileImg = info.getAgent_profile_image();
        HashMap<String, String> agentName = getName(info.getAgent_name().trim().split(" "));
        prop.agentFName = agentName.get("fName");
        prop.agentLName = agentName.get("lName");
        prop.agentAgencyLogoUrl = info.getAgency_logo_url();
        prop.inspectionTimeString = info.getInspectionTimeString();
        prop.registeredForProperty = info.getRegisteredForProperty();
        prop.auctionDate = info.getAuction_date();
        prop.setFav(info.getFavorite());
        prop.auctionDateTimeStamp=info.getAuctionDateTimeStamp();
        prop.property_images_more = info.property_images_more;

        return prop;
    }

    public static HashMap<String, String> getName(String[] name) {
        HashMap<String, String> temp = new HashMap<>();
        temp.put("fName", name[0]);
        String lastName = "";
        for (int i = 0; i < name.length; i++) {
            if (i != 0) {
                lastName += name[i] + " ";
            }
        }
        temp.put("lName", lastName.isEmpty() ? name[0]:lastName);
        return temp;
    }


 
    public static AppMode getAppMode(Context ctx){
        SharedPreference prefs=new SharedPreference(ctx);
        if (prefs != null && prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).equals("2")) {
            return AppMode.user;
        } else if (prefs != null && prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).equals("3")) {
            return AppMode.agent;
        } else {
            return AppMode.guest;
        }
    }


    public static String getPropertyStatus2(int status){
        if(status==1){
            return "NEW";
        }
        else if(status==2){
            return "TRENDING";
        }
        else if(status==3){
            return "REGISTERED";
        }
        else if(status==4||status==0){
            return "AUCTION DUE TO START";
        }
        else return "";
    }
    public static String getPropertyStatus(int status){
        if(status==1){
            return "AUCTION LIVE NOW";
        }
        else if(status==2){
            return "PROPERTY SOLD";
        }
        else if(status==3){
            return "PROPERTY PASSED IN";
        }
        else if(status==4){
            return "ON THE MARKET";
        }
        else return "";
    }
    public static String getLoggedInUserId(Context context){
        return new SharedPreference(context).getStringValues(CommonDef.SharedPrefKeys.USER_ID);
    }
    public static String getLoggedInUserUniqueId(Context context){
        return new SharedPreference(context).getStringValues(CommonDef.SharedPrefKeys.UNIQUE_NUMBER);
    }
    public static boolean isMobileValid(String mobile) {
//        isMibileOk = mobile.length() >= 9 ? true : false;
//        final Pattern phonePattern = Pattern.compile("^(?:\\+?61|0)4 ?(?:(?:[01] ?[0-9]|2 ?[0-57-9]|3 ?[1-9]|4 ?[7-9]|5 ?[018]) ?[0-9]|3 ?0 ?[0-5])(?: ?[0-9]){5}$");
        final Pattern phonePattern = Pattern.compile("^(?:\\(?(?:\\+?61|0)4\\)?(?:[ -]?[0-9]){7}[0-9]$)");
        return phonePattern.matcher(mobile).matches();

    }


    public static String getFormattedPrice(long bidAmount) {
        return "$" + NumberFormat.getNumberInstance(Locale.US).format(bidAmount);
    }

}

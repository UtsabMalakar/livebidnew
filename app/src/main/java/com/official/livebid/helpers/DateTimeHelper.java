package com.official.livebid.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Ics on 5/13/2016.
 */
public class DateTimeHelper {
    public static String getLaggingTime(String datetime) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date notifiedDate = null;
        Date notifiedDateLocal = null;
        try {
            notifiedDate = dateFormat.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = Calendar.getInstance().getTime();
        dateFormat.setTimeZone(TimeZone.getDefault());
        try {
            notifiedDateLocal = dateFormat.parse(dateFormat.format(notifiedDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long different = now.getTime() - notifiedDateLocal.getTime();

        System.out.println("notified : " + notifiedDate);
        System.out.println("now : " + now);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weeksInMilli = daysInMilli * 7;
        long monthsInMilli = daysInMilli * 30;
        long yearsInMilli = monthsInMilli * 12;

        long count = 0;
        String lagging = "";
        if (different / yearsInMilli >= 1) {
            count = different / yearsInMilli;
            lagging = count > 1 ? "years" : "year";
        } else if (different / monthsInMilli >= 1) {
            count = different / monthsInMilli;
            lagging = count > 1 ? "months" : "month";
        } else if (different / weeksInMilli >= 1) {
            count = different / weeksInMilli;
            lagging = count > 1 ? "weeks" : "week";
        } else if (different / daysInMilli >= 1) {
            count = different / daysInMilli;
            lagging = count > 1 ? "days" : "day";
        } else if (different / hoursInMilli >= 1) {
            count = different / hoursInMilli;
            lagging = count > 1 ? "hours" : "hour";
        } else if (different / minutesInMilli >= 1) {
            count = different / minutesInMilli;
            lagging = "mins";
        } else if (different / secondsInMilli >= 1) {
            count = different / secondsInMilli;
            lagging = "secs";
        } else {

            return "Just Now";
        }

        return count + " " + lagging + " ago";
    }

    public static String getLaggingTimeFormated(String datetime) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date notifiedDate = null;
        try {
            notifiedDate = dateFormat.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = new Date();
        long different = now.getTime() - notifiedDate.getTime();
        System.out.println("notified : " + notifiedDate);
        System.out.println("now : " + now);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weeksInMilli = daysInMilli * 7;
        long monthsInMilli = daysInMilli * 30;
        long yearsInMilli = monthsInMilli * 12;

        long count = 0;
        String lagging = "";

        if (different / yearsInMilli >= 1) {
            count = different / yearsInMilli;
            lagging = count > 1 ? "years" : "year";
        } else if (different / monthsInMilli >= 1) {
            count = different / monthsInMilli;
            lagging = count > 1 ? "months" : "month";
        } else if (different / weeksInMilli >= 1) {
            count = different / weeksInMilli;
            lagging = count > 1 ? "weeks" : "week";
        } else if (different / daysInMilli >= 1) {
            count = different / daysInMilli;
            lagging = count > 1 ? "days" : "day";
        } else if (different / hoursInMilli >= 1) {
            count = different / hoursInMilli;
            lagging = count > 1 ? "hours" : "hour";
        } else if (different / minutesInMilli >= 1) {
            count = different / minutesInMilli;
            lagging = "mins";
        } else if (different / secondsInMilli >= 1) {
            count = different / secondsInMilli;
            lagging = "secs";
        } else {
            return "Just Now";
        }

        return count + " " + lagging + " ago";
    }

    public static String getAuctionDueDate(String datetime) {
        final long SECOND = 1000;
        final long MINUTE = 60 * SECOND;
        final long HOUR = 60 * MINUTE;
        final long DAY = 24 * HOUR;
        final long MONTH = 30 * DAY;

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat outDateFormat = new SimpleDateFormat("EEE d MMM 'at' hh:mm aa");
        Date notifiedDate = null;
        try {
            notifiedDate = dateFormat.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = Calendar.getInstance().getTime();

        long ms = Math.abs(notifiedDate.getTime() - now.getTime());
        if (ms >= MONTH) {
//            System.out.println("diff  ms="+ms +"month="+MONTH);
            return "<b>auction</b> on " + outDateFormat.format(notifiedDate);
        } else {
            int length = 0;//for displaying two time component
            StringBuffer text = new StringBuffer("");
            if (ms > DAY) {
                if (length < 2) {
                    text.append(ms / DAY).append(" days ");
                    ms %= DAY;
                    length++;
                }
            }
            if (ms > HOUR) {
                if (length < 2) {
                    text.append(ms / HOUR).append(" hours ");
                    ms %= HOUR;
                    length++;
                }
            }
            if (ms > MINUTE) {
                if (length < 2) {
                    text.append(ms / MINUTE).append(" mins ");
                    ms %= MINUTE;
                    length++;
                }
            }
            if (ms > SECOND) {
                if (length < 2) {
                    text.append(ms / SECOND).append(" seconds ");
                    ms %= SECOND;
                    length++;
                }
            }
            return "<b>Auction</b> in " + (text.toString());
        }


    }

    public static String getAuctionDueDateFromTimeStamp(long timestamp) {
        final long SECOND = 1000;
        final long MINUTE = 60 * SECOND;
        final long HOUR = 60 * MINUTE;
        final long DAY = 24 * HOUR;
        final long MONTH = 30 * DAY;

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        DateFormat outDateFormat = new SimpleDateFormat("EEE d MMM 'at' hh:mm a");
//        Date notifiedDate = null;
//        try {
//            notifiedDate = dateFormat.parse(timestamp);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        Date now = Calendar.getInstance().getTime();

        long ms = Math.abs(timestamp * 1000 - now.getTime());
        if (ms >= MONTH) {
//            System.out.println("diff  ms="+ms +"month="+MONTH);
            return "<b>Auction</b> on " + outDateFormat.format(new Date(timestamp));
        } else {
            int length = 0;//for displaying two time component
            StringBuffer text = new StringBuffer("");
            if (ms > DAY) {
                if (length < 2) {
                    text.append(ms / DAY).append(" days ");
                    ms %= DAY;
                    length++;
                }
            }
            if (ms > HOUR) {
                if (length < 2) {
                    text.append(ms / HOUR).append(" hours ");
                    ms %= HOUR;
                    length++;
                }
            }
            if (ms > MINUTE) {
                if (length < 2) {
                    text.append(ms / MINUTE).append(" mins ");
                    ms %= MINUTE;
                    length++;
                }
            }
            if (ms > SECOND) {
                if (length < 2) {
                    text.append(ms / SECOND).append(" seconds ");
                    ms %= SECOND;
                    length++;
                }
            }
            return "<b>Auction</b> in " + (text.toString());
        }


    }

    public static String getdateEEEdMMM(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE d MMM");
        Date d = null;
        try {
            d = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat.format(d);
    }

    public static String[] getFormatedDatedTimeFromDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date1 = null;
        try {
            date1 = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE d MMM");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
//        Date d = new Date(Long.parseLong(dateTimeStamp)*1000);
        return new String[]{dateFormat.format(date1), timeFormat.format(date1)};
    }

    public static String[] getFormatedDatedTime(String dateTimeStamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE d MMM");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        Date d = new Date(Long.parseLong(dateTimeStamp) * 1000);
        return new String[]{dateFormat.format(d), timeFormat.format(d)};
    }

    public static long getAuctionDueTimeInMillis(String auctionDateTime) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date auctionDate = null;
        Date local = null;
        try {
            auctionDate = dateFormat.parse(auctionDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dateFormat.setTimeZone(TimeZone.getDefault());
        try {
            local = dateFormat.parse(dateFormat.format(auctionDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = Calendar.getInstance().getTime();
        return local.getTime() - now.getTime();


    }

    public static boolean isAuctionDueToStart(Long auctionDateTime) {
        long now = System.currentTimeMillis();
        return ((now - auctionDateTime * 1000) >= 0) ? true : false;


    }

    public static String getLaggingTimeFormated(long timestamp) {

        long different = timestamp;
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weeksInMilli = daysInMilli * 7;
        long monthsInMilli = daysInMilli * 30;
        long yearsInMilli = monthsInMilli * 12;

        long count = 0;
        String lagging = "";

        if (different / yearsInMilli >= 1) {
            count = different / yearsInMilli;
            lagging = count > 1 ? "years" : "year";
        } else if (different / monthsInMilli >= 1) {
            count = different / monthsInMilli;
            lagging = count > 1 ? "months" : "month";
        } else if (different / weeksInMilli >= 1) {
            count = different / weeksInMilli;
            lagging = count > 1 ? "weeks" : "week";
        } else if (different / daysInMilli >= 1) {
            count = different / daysInMilli;
            lagging = count > 1 ? "days" : "day";
        } else if (different / hoursInMilli >= 1) {
            count = different / hoursInMilli;
            lagging = count > 1 ? "hours" : "hour";
        } else if (different / minutesInMilli >= 1) {
            count = different / minutesInMilli;
            lagging = "mins";
        } else if (different / secondsInMilli >= 1) {
            count = different / secondsInMilli;
            lagging = "secs";
        } else {
            return "Just Now";
        }

        return count + " " + lagging + " ago";
    }

    public static String getLaggingTimeFromTimeStamp(long timestamp) {

        long different = System.currentTimeMillis() - timestamp;
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weeksInMilli = daysInMilli * 7;
        long monthsInMilli = daysInMilli * 30;
        long yearsInMilli = monthsInMilli * 12;

        long count = 0;
        String lagging = "";

        if (different / yearsInMilli >= 1) {
            count = different / yearsInMilli;
            lagging = count > 1 ? "years" : "year";
        } else if (different / monthsInMilli >= 1) {
            count = different / monthsInMilli;
            lagging = count > 1 ? "months" : "month";
        } else if (different / weeksInMilli >= 1) {
            count = different / weeksInMilli;
            lagging = count > 1 ? "weeks" : "week";
        } else if (different / daysInMilli >= 1) {
            count = different / daysInMilli;
            lagging = count > 1 ? "days" : "day";
        } else if (different / hoursInMilli >= 1) {
            count = different / hoursInMilli;
            lagging = count > 1 ? "hours" : "hour";
        } else if (different / minutesInMilli >= 1) {
            count = different / minutesInMilli;
            lagging = "mins";
        } else if (different / secondsInMilli >= 1) {
            count = different / secondsInMilli;
            lagging = "secs";
        } else {
            return "Just Now";
        }

        return count + " " + lagging + " ago";
    }
}

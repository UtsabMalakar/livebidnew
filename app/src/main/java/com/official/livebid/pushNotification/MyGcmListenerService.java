package com.official.livebid.pushNotification; /**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.official.livebid.R;
import com.official.livebid.activities.SplashScreen;
import com.official.livebid.activities.discover.Discover;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.objects.NotificationModel;

import java.util.List;
import java.util.Random;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    String greetMsg = "";
    String notificationId;
    String notificationType;
    String propertyId;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("m");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        NotificationModel notificationModel=null;
        Spanned msg = (Html.fromHtml(""));
        AppMode appMode= CommonMethods.getAppMode(getApplicationContext());
        if (appMode==AppMode.user) {
            notificationModel = new NotificationModel().getNotificationUser(message);
            msg=notificationModel.setNotificationMsgUser(getApplicationContext());
        }
        else {
            notificationModel = new NotificationModel().getNotificationAgent(message);
            msg=notificationModel.setNotificationMsgAgent();
        }

        sendNotification(notificationModel,msg);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(NotificationModel notification, Spanned message) {



        Intent openingIntent1 = new Intent(this, SplashScreen.class);
        openingIntent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Intent openingIntent2 = new Intent(this, Discover.class);
        openingIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Intent myIntent = new Intent(getApplicationContext(), MyReciever.class);
        myIntent.putExtra(CommonDef.PUSH_NOTIFICATION,notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_of_live_bid)
                .setContentTitle("LiveBid")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Random rand=new Random();
        int nid=rand.nextInt()+1;
        notificationManager.notify(nid/* ID of notification */, notificationBuilder.build());
    }

    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equals(myPackage);
    }
}

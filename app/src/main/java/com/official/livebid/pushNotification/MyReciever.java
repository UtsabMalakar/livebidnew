package com.official.livebid.pushNotification;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.official.livebid.R;
import com.official.livebid.activities.PropertyDetails;
import com.official.livebid.activities.SplashScreen;
import com.official.livebid.activities.profile.Profile;
import com.official.livebid.adapters.NotificationAdapter;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.AppController;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.NotificationModel;
import com.official.livebid.objects.PropertyListInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.measite.minidns.record.A;

public class MyReciever extends BroadcastReceiver implements AsyncInterface {
    NotificationModel notification = null;
    AppMode appMode;
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        appMode = CommonMethods.getAppMode(context);
        this.context = context;
        Bundle b = intent.getExtras();
        if (b != null) {
            notification = (NotificationModel) b.getSerializable(CommonDef.PUSH_NOTIFICATION);
            if (isAppForground(context)) {
                Log.i("liveBid", "foreGround");
                getNotifiedPropertyInfo(context, notification.getPropertyId());
            } else {
                Log.i("liveBid", "NotforeGround");
                Intent splach = new Intent(context, SplashScreen.class);
                splach.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(splach);
            }


        }


    }

    public boolean isAppForground(Context mContext) {

        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(mContext.getPackageName())) {
                return false;
            }
        }

        return true;
    }

    private void getNotifiedPropertyInfo(final Context context, final String propertyId) {
        StringRequest sr = new StringRequest(Request.Method.POST, UrlHelper.GET_PROPERTY_INFO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("UpdateDeviceInfo", response.toString());

                try {
                    JSONObject jObjResult = new JSONObject(response);
                    String msg, code;
                    code = jObjResult.getString("code");
                    msg = jObjResult.getString("msg");

                    if (code.equalsIgnoreCase("0001")) {
                        PropertyListInfo property = new PropertyListInfo().getPropertyInfo(jObjResult.getJSONObject("detail"));
                        if (appMode == AppMode.user)
                            openPropertyDetais(property);
                        else
                           openViewListingAgent(property);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                System.out.println();

                params.put("property_id", propertyId);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = CommonMethods.getHeaders(context);

                System.out.println("Headers: " + params);
                return params;
            }
        };

        DefaultRetryPolicy policy = new DefaultRetryPolicy(CommonDef.SOCKET_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(sr, "UpdateDevice");
    }


    private void openPropertyDetais(PropertyListInfo property) {
        Intent intent = new Intent(context, PropertyDetails.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROPERTY_LIST_INFO, property);
        context.startActivity(intent);
    }
    private void openViewListingAgent(PropertyListInfo property) {
        Intent intent = new Intent(context, PropertyDetails.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CommonDef.PROPERTY_LIST_INFO, property);
        context.startActivity(intent);
    }


    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.RC_GET_PROPERTY_INFO:
                System.out.println("Property Details =>" + result);
                try {
                    JSONObject jObjResult = new JSONObject(result);
                    String msg, code;
                    code = jObjResult.getString("code");
                    msg = jObjResult.getString("msg");

                    if (code.equalsIgnoreCase("0001")) {
                        PropertyListInfo property = new PropertyListInfo().getPropertyInfo(jObjResult.getJSONObject("detail"));
                        if (appMode == AppMode.user)
                            Opener.PropertyDetails((Activity) context, property);
                        else
                            Opener.ViewListingsAgent((Activity) context, property);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;

        }

    }
}
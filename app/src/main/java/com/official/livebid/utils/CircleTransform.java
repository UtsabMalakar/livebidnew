package com.official.livebid.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.official.livebid.helpers.CommonMethods;
import com.squareup.picasso.Transformation;

public class CircleTransform implements Transformation {

    Activity activity;

    public CircleTransform(Activity activity) {
        this.activity = activity;
    }
    public CircleTransform() {}

    @Override
    public Bitmap transform(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());
//        int size = source.getWidth();

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        Paint paint1 = new Paint();
        paint1.setColor(Color.parseColor("#ffffff"));
        BitmapShader shader = new BitmapShader(squaredBitmap,
                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;


        if(activity!=null){
            int pxValue = (int)CommonMethods.pxFromDp(activity, 2);
            canvas.drawCircle(r, r, r, paint1);
            canvas.drawCircle(r, r, (r-pxValue), paint);
        }else{
            canvas.drawCircle(r, r, (r), paint);
        }

        squaredBitmap.recycle();
        return bitmap;
    }

    @Override
    public String key() {
        return "circle";
    }
}
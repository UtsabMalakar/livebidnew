package com.official.livebid.activities.watchList;


import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.helpers.Opener;
import com.official.livebid.listeners.FavouriteCompleteListener;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.PropertyLists;
import com.official.livebid.utils.GPSTracker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.official.livebid.R.id.map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragPropertyMapView extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "";
    private FragmentActivity myContext;
    private Activity activity;
    private GoogleMap mMap;
    GPSTracker gpsTracker;
    String lat = "", lng = "";
    Marker clickedMarker;
    private boolean isLocationSet = false;
    RelativeLayout rlMapDetails;
    ImageView ivPropertyImg;
    TextView tvPropertyStreet, tvPropertySuburbPostcode,
            tvPrice, tvNosBedroom, tvNosBathroom,
            tvNosParking;
    private CheckBox chkFav;
    private MapFragment mapFragment;

    public FragPropertyMapView() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        this.activity = activity;
        super.onAttach(activity);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_frag_property_map_view, container, false);
        rlMapDetails = (RelativeLayout) view.findViewById(R.id.rl_map_details);
        mapFragment = getMapFragment();
        mapFragment.getMapAsync(this);
        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        ArrayList<Marker> markers = new ArrayList<>();

        if (PropertyLists.alPropertyListInfo != null && !PropertyLists.alPropertyListInfo.isEmpty()) {
            for (PropertyListInfo propertyListsInfo : PropertyLists.alPropertyListInfo) {
                if (propertyListsInfo.lat != null && !propertyListsInfo.lat.isEmpty()) {
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(propertyListsInfo.lat), Double.parseDouble(propertyListsInfo.lng)))
                            .anchor(0.5f, 0.5f)
                            .snippet(PropertyLists.alPropertyListInfo.indexOf(propertyListsInfo) + "")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_watchlist_marker_icon)));
                    markers.add(marker);
                }
            }
            manageMapView(markers);
        }else {

            permissionCheckLocation();


        }
    }

    private void permissionCheckLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && activity.checkSelfPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && activity.checkSelfPermission(
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    1200);
            return;
        } else {
            setMyLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case 1200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    setMyLocation();
                break;
        }
    }

    private void setMyLocation() {
        if(isConnectingToInternet()) {
            GPSTracker gps;
//            final ProgressDialog pd = ProgressDialog.show(myContext, "", "Loading location...", true, true);

            gps = new GPSTracker(activity);
            double lat, lng;
            String latlng = null;

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                lat = gps.getLatitude();
                lng = gps.getLongitude();
                LatLng coordinate = new LatLng(lat, lng);
                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 5);
                mMap.animateCamera(yourLocation);


                // \n is for new line
//           Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + this.MyLat + "\nLong: " + this.MyLang, Toast.LENGTH_LONG).show();
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }else{
            LatLng coordinate = new LatLng(0, 0);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 10);
            mMap.animateCamera(yourLocation);

        }


    }

    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }

    private void manageMapView(ArrayList<Marker> markers) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        // Then obtain a movement description object by using the factory: CameraUpdateFactory:
        int padding = (int) CommonMethods.pxFromDp(getActivity(), 50); // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.animateCamera(cu);


        rlMapDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickedMarker != null) {
                    PropertyListInfo propertyListInfo = PropertyLists.alPropertyListInfo.get(Integer.parseInt(clickedMarker.getSnippet()));
                    Opener.PropertyDetails(getActivity(), propertyListInfo);
                }
            }
        });
        initMapDetailsView();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (clickedMarker != null)
                    clickedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_watchlist_marker_icon));

                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_star_icon));
                clickedMarker = marker;

                PropertyListInfo propertyListInfo = PropertyLists.alPropertyListInfo.get(Integer.parseInt(marker.getSnippet()));
                setMarkerDetails(propertyListInfo);
                if (rlMapDetails.getVisibility() != View.VISIBLE) {
                    Animation bottomUp = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.bottom_up);
                    rlMapDetails.startAnimation(bottomUp);
                    rlMapDetails.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (clickedMarker != null) {
                    Animation slideDown = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.slide_down);
                    rlMapDetails.startAnimation(slideDown);
                    rlMapDetails.setVisibility(View.INVISIBLE);
                    clickedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_watchlist_marker_icon));
                    clickedMarker = null;
                }
            }
        });
    }

    private void setMarkerDetails(final PropertyListInfo propertyInfo) {
        tvPropertyStreet.setText(propertyInfo.street);
        tvPropertySuburbPostcode.setText(propertyInfo.suburb + ", " + propertyInfo.postCode);
        tvPrice.setText(propertyInfo.price);
        tvNosBathroom.setText(propertyInfo.nosOfBathroom);
        tvNosBedroom.setText(propertyInfo.nosOfBedroom);
        tvNosParking.setText(propertyInfo.nosOfParking);
        if (!propertyInfo.propertyImgUrl.trim().isEmpty()) {
            Picasso.with(getActivity())
                    .load(propertyInfo.propertyImgUrl)
                    .resize((int) CommonMethods.pxFromDp(getActivity(), 150), (int) CommonMethods.pxFromDp(getActivity(), 150))
                    .centerCrop()
                    .into(ivPropertyImg);
        } else {
            Picasso.with(getActivity())
                    .load(R.drawable.img_coming_soon)
                    .resize((int) CommonMethods.pxFromDp(getActivity(), 150), (int) CommonMethods.pxFromDp(getActivity(), 150))
                    .centerCrop()
                    .into(ivPropertyImg);
        }
        chkFav.setChecked(propertyInfo.getFav());
        chkFav.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final CheckBox cboxTemp = (CheckBox) view;
                        new FavoriteHelper(getActivity()).makeFavorite(propertyInfo.id, propertyInfo.getFav(), new FavouriteCompleteListener() {
                            @Override
                            public void onSuccess(boolean b) {
                                int position = 0;
                                for (int i = 0; i < PropertyLists.alPropertyListInfo.size(); i++) {
                                    if (propertyInfo.id.equalsIgnoreCase(PropertyLists.alPropertyListInfo.get(i).id)) {
                                        position = i;
                                    }
                                }
                                PropertyLists.alPropertyListInfo.get(position).setFav(b);
                                PropertyLists.alPropertyListInfo.remove(position);
                                if (clickedMarker != null) {
                                    Animation slideDown = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.slide_down);
                                    rlMapDetails.startAnimation(slideDown);
                                    rlMapDetails.setVisibility(View.INVISIBLE);
                                    clickedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_watchlist_marker_icon));
                                    clickedMarker.remove();
//                                    clickedMarker = null;
                                }

                            }

                            @Override
                            public void onFailure() {
                                cboxTemp.setChecked(propertyInfo.getFav());
                            }

                            @Override
                            public void onNotLoggedIn() {
                                cboxTemp.setChecked(false);
                                new Alerts(getActivity()).signIn();
                            }
                        });


                        // Webservice call for change.
                    }
                });
    }

    private void initMapDetailsView() {
        ivPropertyImg = (ImageView) rlMapDetails.findViewById(R.id.iv_property_img);
        tvPropertyStreet = (TextView) rlMapDetails.findViewById(R.id.tv_property_street);
        tvPropertySuburbPostcode = (TextView) rlMapDetails.findViewById(R.id.tv_property_suburb_postcode);
        tvPrice = (TextView) rlMapDetails.findViewById(R.id.tv_property_price);
        tvNosBathroom = (TextView) rlMapDetails.findViewById(R.id.tv_nosBathroom);
        tvNosBedroom = (TextView) rlMapDetails.findViewById(R.id.tv_nosBedroom);
        tvNosParking = (TextView) rlMapDetails.findViewById(R.id.tv_nosParking);
        chkFav = (CheckBox) rlMapDetails.findViewById(R.id.cbox_fav);

    }

    private MapFragment getMapFragment() {
        FragmentManager fm = null;

        Log.d(TAG, "sdk: " + Build.VERSION.SDK_INT);
        Log.d(TAG, "release: " + Build.VERSION.RELEASE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Log.d(TAG, "using getFragmentManager");
            fm = getFragmentManager();
        } else {
            Log.d(TAG, "using getChildFragmentManager");
            fm = getChildFragmentManager();
        }

        return (MapFragment) fm.findFragmentById(map);
    }
}

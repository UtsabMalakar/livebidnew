package com.official.livebid.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class HelpAndSupport extends ActionBarActivity implements View.OnClickListener,AsyncInterface {

    private CustomActionBar.TitleBarWithBackAndTitle helpAndSupportActionBar;
    TextView btnRateApp;
    TextView btnKnowledgeBase;
    TextView contactUs;
    private Alerts alerts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_and_support);
        getSupportActionBar().hide();
        alerts=new Alerts(this);
        helpAndSupportActionBar=new CustomActionBar.TitleBarWithBackAndTitle(this,"Help & Support");
        btnRateApp= (TextView) findViewById(R.id.btn_rate_app);
        btnKnowledgeBase= (TextView) findViewById(R.id.btn_knowledge_base);
        contactUs= (TextView) findViewById(R.id.btn_contact_us);
        contactUs.setOnClickListener(this);
        btnRateApp.setOnClickListener(this);
        btnKnowledgeBase.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_help_and_support, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_rate_app:
                alerts.rateApp();
                break;
            case R.id.btn_knowledge_base:
                Opener.KnowledgeBase(this);
                break;
            case R.id.btn_contact_us:
                CommonMethods.sendEmail(HelpAndSupport.this,CommonDef.LIVEBID_URL);
                break;
        }
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_RATE_APP:
                System.out.println("rate app=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        alerts.showOkMessage(msg);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
        }
    }
    public void rateApp(String rate,String comment){
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("comments", comment);
        params.put("rating", rate);

        System.out.println("logout header=>" + headers.toString());
        VolleyRequest volleyRequest = new VolleyRequest(HelpAndSupport.this, Request.Method.POST, params, UrlHelper.RATE_APP, headers, false, CommonDef.REQUEST_RATE_APP);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }
}

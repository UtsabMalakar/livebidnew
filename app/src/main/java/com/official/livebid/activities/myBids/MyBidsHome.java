package com.official.livebid.activities.myBids;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyBidsHome extends MenuActivity {
    private View view;
    private TextView tvTitle;
    private Button btnBrowse;
    private LinearLayout myBidsHomeContainer;
    String[] myBidsHomeItems = {"My Bids", "My Offers", "Registered Properties"};
    String[] myBidsHomeImages = {
            "http://livebid.draftserver.com/livebid/assets/uploads/images/77ab3b1062254d688cfbf41148d83b40.png",
            "http://livebid.draftserver.com/livebid/assets/uploads/images/0e7f7b63c88bcaa4bf066885805192bc.png",
            "http://livebid.draftserver.com/livebid/assets/uploads/images/77ab3b1062254d688cfbf41148d83b40.png"};
    private ImageView ivBgImage;
    private ArrayList<RegisteredPropertyModel> registeredProperties;


    @Override
    public int getLayoutId() {
        return R.layout.activity_my_bids_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_my_bids_home);
        getSupportActionBar().hide();
        new CustomActionBar.MyBidsCAB(this);
        myBidsHomeContainer = (LinearLayout) findViewById(R.id.mybids_home_container);
        myBidsHomeContainer.removeAllViews();
        for (int i=0;i<3;i++) {
            view = getLayoutInflater().inflate(R.layout.mybids_home_item, null);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            ivBgImage = (ImageView) view.findViewById(R.id.iv_bgImg);
            Picasso.with(this).load(myBidsHomeImages[i]).resize(1080, 1080).into(ivBgImage);
//            tvTitle.setText(myBidsHomeItems[i]);
            btnBrowse = (Button) view.findViewById(R.id.btn_browse);
            btnBrowse.setTag(myBidsHomeItems[i]);
            btnBrowse.setText(myBidsHomeItems[i]);
            btnBrowse.setOnClickListener(btnBrowseClickListener);
            myBidsHomeContainer.addView(view);


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_bids_home, menu);
        return true;
    }

    private View.OnClickListener btnBrowseClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String action = (String) view.getTag();
            if (action.equalsIgnoreCase("my bids")) {
                Opener.myRegisteredProperties(MyBidsHome.this, CommonDef.MY_BIDS_BIDS);
            } else if (action.equalsIgnoreCase("My Offers")) {
                Opener.myRegisteredProperties(MyBidsHome.this, CommonDef.MY_BIDS_OFFERS);
            } else if (action.equalsIgnoreCase("Registered Properties")) {
                Opener.myRegisteredProperties(MyBidsHome.this, CommonDef.MY_BIDS_REGISTERED_PROPERTY);

            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MenuActivity.activeTab = MenuActivity.NOT_SELECTED;
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    @Override
    protected void onResume() {
        super.onResume();
        imgMyBids = (ImageButton) findViewById(R.id.imgLeftMenu);
        imgMyBids.setImageResource(R.drawable.ic_my_bids_icon);
    }
}
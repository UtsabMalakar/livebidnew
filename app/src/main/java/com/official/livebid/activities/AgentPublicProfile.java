package com.official.livebid.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.official.livebid.R;
import com.official.livebid.Views.EndlessListView;
import com.official.livebid.Views.ExpandableTextView;
import com.official.livebid.adapters.AgentProfilePropertiesListingsAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.StringHelper;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.AgentInfo;
import com.official.livebid.objects.AvailabilityModel;
import com.official.livebid.objects.PropertyInfo;
import com.official.livebid.utils.CircleTransform;
import com.official.livebid.utils.GPSTracker;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AgentPublicProfile extends AppCompatActivity implements AsyncInterface, OnMapReadyCallback, EndlessListView.EndlessListener {
    EndlessListView lvAgentProfilePropertyListings;
    Button btnContactAgent;
    Button btnEmail, btnCall;
    //    RelativeLayout rl_contactAgent;
//    LinearLayout ll_ContactAgent;
    TextView tvViewMore, tvAgentDesc;
    boolean isViewMore = false;
    ExpandableTextView etvAgentDesc;
    private CustomActionBar.AgentPublicProfileTitleBar titleBar;
    private ArrayList<AvailabilityModel> agentAvailabilities;
    private Alerts alerts;
    String agentID = "94";
    TextView tvAgentName, tvAgencyAddress, tvSalesAgentNum;
    ImageView ivAgentImg, ivAgencyLogo;
    TextView tvAvailableDay, tvAvailableTime;
    TextView tvAgentAbout;
    private AgentInfo agentInfo;
    private ArrayList<AvailabilityModel> availabilities;
    private AgentProfilePropertiesListingsAdapter propertiesListingsAdapter;
    ArrayList<PropertyInfo> properties;

    double lat, lng;
    GPSTracker gps;
    private boolean isLocationSet = false;
    int offset = 0;//property
    boolean isLastPage = false;//property
    private TextView tvAgencySuburb;

    private void showAgentPublicProfile(AgentInfo agentInfo) {
        titleBar = new CustomActionBar.AgentPublicProfileTitleBar(this, agentInfo.getName());
        tvAgentName.setText(agentInfo.getName());
        tvAgencyAddress.setText(agentInfo.getAgentStreet());
        tvAgencySuburb.setText(agentInfo.getAgentSuburb() +" "+ agentInfo.getAgentState() + " " + agentInfo.getAgentPostcode());
        tvSalesAgentNum.setText(agentInfo.getUniqueNumber());
        tvAvailableDay.setText(CommonMethods.getAvailableDay(agentInfo.getAvailabilities().get(0).getFromDay(), agentInfo.getAvailabilities().get(0).getToDay()));
        tvAvailableTime.setText(agentInfo.getAvailabilities().get(0).getFromTime() + " - " + agentInfo.getAvailabilities().get(0).getToTime());
        tvAgentAbout.setText("About " + agentInfo.getName().split(" ")[0]);
        etvAgentDesc.setText(agentInfo.getDescription());
        initialiseMap();
        if (!agentInfo.getProfileImage().isEmpty()) {
            Picasso.with(AgentPublicProfile.this)
                    .load(agentInfo.getProfileImage())
                    .resize((int) CommonMethods.pxFromDp(AgentPublicProfile.this, 120), (int) CommonMethods.pxFromDp(AgentPublicProfile.this, 120))
                    .centerCrop()
//                        .placeholder( R.drawable.img_loading)
//                        .memoryPolicy(MemoryPolicy.NO_CACHE)
//                        .networkPolicy(NetworkPolicy.NO_CACHE)
                    .transform(new CircleTransform())
                    .into(ivAgentImg);
        }else {
            Picasso.with(AgentPublicProfile.this)
                    .load(R.drawable.io_white_profile_default)
                    .transform(new CircleTransform())
                    .into(ivAgentImg);
        }
        if (!agentInfo.getAgencyLogoUrl().isEmpty()) {
            ivAgencyLogo.setVisibility(View.VISIBLE);
            Picasso.with(AgentPublicProfile.this)
                    .load(agentInfo.getAgencyLogoUrl())
                    .resize((int) CommonMethods.pxFromDp(AgentPublicProfile.this, 60), (int) CommonMethods.pxFromDp(AgentPublicProfile.this, 60))
                    .centerCrop()
//                        .placeholder( R.drawable.img_loading)
//                        .memoryPolicy(MemoryPolicy.NO_CACHE)
//                        .networkPolicy(NetworkPolicy.NO_CACHE)
                    .transform(new CircleTransform())
                    .into(ivAgencyLogo);
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_public_profile);
        getSupportActionBar().hide();
//        rl_contactAgent = (RelativeLayout) findViewById(R.id.rl_contact_agent);
//        ll_ContactAgent = (LinearLayout) findViewById(R.id.ll_contact_agent);
//        btnCancel = (Button) findViewById(R.id.btn_cancel);
        gps = new GPSTracker(this);
        alerts = new Alerts(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            agentID = b.getString("agent_id");
        }
        View header = getLayoutInflater().inflate(R.layout.agent_public_profile_header, null, false);
        tvAgentName = (TextView) header.findViewById(R.id.tv_agent_name);
        tvAgencyAddress = (TextView) header.findViewById(R.id.tv_agency_address);
        tvAgencySuburb = (TextView) header.findViewById(R.id.tv_agentSuburb);
        tvSalesAgentNum = (TextView) header.findViewById(R.id.tv_sales_agent_num);
        tvAvailableDay = (TextView) header.findViewById(R.id.tv_agent_available_day);
        tvAvailableTime = (TextView) header.findViewById(R.id.tv_agent_available_time);
        tvAgentAbout = (TextView) header.findViewById(R.id.tv_agent_about);
        ivAgentImg = (ImageView) header.findViewById(R.id.iv_agent_img);
        ivAgencyLogo = (ImageView) header.findViewById(R.id.iv_agency_img);

        btnEmail = (Button) header.findViewById(R.id.btn_email);
        btnCall = (Button) header.findViewById(R.id.btn_call);

//        tvAgentDesc = (TextView) header.findViewById(R.id.tv_agent_about_desc);
        etvAgentDesc = (ExpandableTextView) header.findViewById(R.id.expand_text_view);
//        etvAgentDesc.setText("Sets the right-hand compound drawable of the TextView to the specified icon and sets an error message that will be displayed in a popup when the TextView has focus. The icon and error message will be reset to null when any key events cause changes to the TextView's text. The drawable must already have had setBounds(Rect) set on it. If the error is null, the error message will be cleared (and you should provide a null icon as well).Sets the right-hand compound drawable of the TextView to the specified icon and sets an error message that will be displayed in a popup when the TextView has focus. The icon and error message will be reset to null when any key events cause changes to the TextView's text. The drawable must already have had setBounds(Rect) set on it. If the error is null, the error message will be cleared (and you should provide a null icon as well).");
        btnContactAgent = (Button) findViewById(R.id.btn_contact_agent);
        lvAgentProfilePropertyListings = (EndlessListView) findViewById(R.id.lv_agent_profile_property_listings);
        lvAgentProfilePropertyListings.setLoadingView(R.layout.layout_endless_loading);
        lvAgentProfilePropertyListings.addHeaderView(header);
//         propertiesListingsAdapter = new AgentProfilePropertiesListingsAdapter(this, initFakePropertyList());
//        lvAgentProfilePropertyListings.setAdapter(propertiesListingsAdapter);
//        lvAgentProfilePropertyListings.setListener(this);


        btnContactAgent.setVisibility(View.VISIBLE);
        btnContactAgent.setOnClickListener(clickListener);
        btnEmail.setOnClickListener(clickListener);
        btnCall.setOnClickListener(clickListener);
        agentAvailabilities = new ArrayList<>();
        agentAvailabilities.add(new AvailabilityModel("10:30 AM", "4:30 PM", 5, 1));
        System.out.println("date and time range:" + agentAvailabilities.get(0).toString());
        getAgentProfile();
        getPropertyList();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_agent_public_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_contact_agent:
//                    rl_contactAgent.setVisibility(View.VISIBLE);
                    alerts.contactAgentBottomSheet(agentInfo);
                    break;
                case R.id.btn_email:

//                    CommonMethods.sendEmail(AgentPublicProfile.this, "shiv.anga@yahoo.com");
                    Opener.contactAgent(AgentPublicProfile.this, agentInfo.getId());
                    break;
                case R.id.btn_call:
//                    rl_contactAgent.setVisibility(View.VISIBLE);
                    agentInfo.getAvailabilities().get(0).getFromDay();
                    if (CommonMethods.isCallAvailable(agentInfo.getAvailabilities())){
                        permissionCheckCall();
                    }
//                        CommonMethods.makeCall(AgentPublicProfile.this, agentInfo.getMobile());
                    else
                        alerts.showOkMessage("LiveBid",
                                "You can not call " + StringHelper.capitalizeInitial(agentInfo.getName()) + " at this time.\n Please check his availability."
                        );
                    break;
            }
        }
    };


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    private void permissionCheckCall() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && this.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.CALL_PHONE},
                    1300);

            return;
        } else {
            CommonMethods.makeCall(AgentPublicProfile.this, agentInfo.getMobile());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    CommonMethods.makeCall(AgentPublicProfile.this, agentInfo.getMobile());
                break;

        }
    }



    private void getAgentProfile() {
        HashMap<String, String> headers = CommonMethods.getHeaders(AgentPublicProfile.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("agent_id", agentID);
        System.out.println("agency details params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(AgentPublicProfile.this, Request.Method.POST, params, UrlHelper.GET_AGENT_PUBLIC_PROFILE, headers, false, CommonDef.REQUEST_AGENT_PUBLIC_PROFILE);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_AGENT_PUBLIC_PROFILE:
                System.out.println("agent details=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        agentInfo = new AgentInfo();
                        availabilities = new ArrayList<AvailabilityModel>();
                        JSONObject jObjDetails = joResult.getJSONObject("detail");
                        agentInfo.setId(jObjDetails.getString("id"));
                        agentInfo.setUserId(jObjDetails.getString("user_id"));
                        agentInfo.setAgencyId(jObjDetails.getString("agency_id"));
                        agentInfo.setUniqueNumber(jObjDetails.getString("unique_number"));
                        agentInfo.setName(jObjDetails.getString("name"));
                        agentInfo.setEmail(jObjDetails.getString("email"));
                        agentInfo.setMobile(jObjDetails.getString("mobile"));
                        agentInfo.setProfileImage(jObjDetails.getString("profile_image"));
                        agentInfo.setAddress(jObjDetails.getString("street"));
                        agentInfo.setAgentStreet(jObjDetails.getString("street"));
                        agentInfo.setAgentSuburb(jObjDetails.getString("suburb"));
                        agentInfo.setAgentState(jObjDetails.getString("state"));
                        agentInfo.setAgentPostcode(jObjDetails.getString("postcode"));
                        agentInfo.setLat(jObjDetails.getString("lat"));
                        agentInfo.setLng(jObjDetails.getString("lng"));
                        agentInfo.setDescription(jObjDetails.getString("description"));
                        JSONArray jaAvailabilities = jObjDetails.getJSONArray("availabilities");
                        JSONObject joAvailability;
                        for (int j = 0; j < jaAvailabilities.length(); j++) {
                            joAvailability = jaAvailabilities.getJSONObject(j);
                            availabilities.add(new AvailabilityModel(
                                    joAvailability.getString("from_time"),
                                    joAvailability.getString("to_time"),
                                    joAvailability.getInt("to_day"),
                                    joAvailability.getInt("from_day")


                            ));
                        }
                        agentInfo.setAvailabilities(availabilities);
                        if (jObjDetails.has("agency_logo_url"))
                            agentInfo.setAgencyLogoUrl(jObjDetails.getString("agency_logo_url"));
                        if (jObjDetails.has("agency_name"))
                            agentInfo.setAgencyName(jObjDetails.getString("agency_name"));
                        if (jObjDetails.has("agency_street"))
                            agentInfo.setAgencyStreet(jObjDetails.getString("agency_street"));
                        if (jObjDetails.has("agency_suburb"))
                            agentInfo.setAgencySuburb(jObjDetails.getString("agency_suburb"));
                        if (jObjDetails.has("agency_state"))
                            agentInfo.setAgencyState(jObjDetails.getString("agency_state"));
                        if (jObjDetails.has("agency_postcode"))
                            agentInfo.setAgencyPostcode(jObjDetails.getString("agency_postcode"));
                        showAgentPublicProfile(agentInfo);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_PROPERTY_LISTINGS:
                System.out.println("property Listings =>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        JSONArray propertyList = joResult.getJSONArray("list");
                        JSONObject prop = null;
                        PropertyInfo mProp = null;
                        PropertyInfo.OHI mOhi = null;
                        properties = new ArrayList<>();
                        PropertyInfo.Property_images_more moreImage = null;
                        ArrayList<PropertyInfo.OHI> inspectionTimes = null;
                        ArrayList<PropertyInfo.Property_images_more> propertyMoreImages = null;
                        for (int i = 0; i < propertyList.length(); i++) {
                            inspectionTimes = new ArrayList<PropertyInfo.OHI>();
                            propertyMoreImages = new ArrayList<PropertyInfo.Property_images_more>();
                            prop = propertyList.getJSONObject(i);
                            mProp = new PropertyInfo();
                            mProp.setId(prop.getString("id"));
                            mProp.setAgent_id(prop.getString("agent_id"));
                            mProp.setAgency_id(prop.getString("agency_id"));
                            mProp.setAuction_date(prop.getString("auction_date"));
                            mProp.setProperty_image(prop.getString("property_image"));
                            mProp.setProperty_title(prop.getString("property_title"));
                            mProp.setProperty_description(prop.getString("property_description"));
                            mProp.setPrice(prop.getString("price"));
                            mProp.setStreet(prop.getString("street"));
                            mProp.setPostcode(prop.getString("postcode"));
                            mProp.setSuburb(prop.getString("suburb"));
                            mProp.setState(prop.getString("state"));
                            mProp.setParking(prop.getString("parking"));
                            mProp.setBathrooms(prop.getString("bathrooms"));
                            mProp.setBedrooms(prop.getString("bedrooms"));
                            mProp.setLat(prop.getString("lat"));
                            mProp.setLng(prop.getString("lng"));
                            mProp.setProperty_status(prop.getInt("property_status"));
                            mProp.setProperty_code(prop.getString("property_code"));
                            mProp.setAuthority(prop.getString("authority"));
                            mProp.setAgent_name(prop.getString("agent_name"));
                            mProp.setUnique_number(prop.getString("unique_number"));
                            mProp.setAgent_profile_image(prop.getString("agent_profile_image"));
                            mProp.setAgency_logo_url(prop.getString("agency_logo_url"));
                            mProp.setFavorite(prop.getString("favorite"));
                            mProp.setDistance(prop.getString("distance"));
                            mProp.setRegisteredForProperty(prop.getString("registered_for_property"));
                            mProp.setAuctionDateTimeStamp(prop.getLong("auction_date_timestamp"));
                            mProp.setPropertyStatus2(prop.getInt("property_status_2"));
                            JSONArray jOhi = prop.getJSONArray("inspection_times");
                            if (jOhi.length() > 0) {
                                for (int j = 0; j < jOhi.length(); j++) {
                                    mOhi = new PropertyInfo.OHI(
                                            jOhi.getJSONObject(j).getString("inspection_date"),
                                            jOhi.getJSONObject(j).getString("inspection_time_from"),
                                            jOhi.getJSONObject(j).getString("inspection_time_to")
                                    );
                                    inspectionTimes.add(mOhi);
                                }
                            }
                            mProp.setInspectionTimeString(jOhi.toString());
                            JSONArray JPropertyImageMore = prop.getJSONArray("property_images_more");
                            if (JPropertyImageMore.length() > 0) {
                                for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                   /* moreImage = new PropertyInfo.Property_images_more(
                                            JPropertyImageMore.getJSONObject(j).getString("image_url")
                                    );*/

                                    mProp.property_images_more.add(JPropertyImageMore.getString(j));
                                }
                            }
                            mProp.setInspectionTimes(inspectionTimes);
//                            mProp.setProperty_images_more(propertyMoreImages);
                            properties.add(mProp);


                        }
                        isLastPage = joResult.getBoolean("last_page");

                        if (offset == 0) {
                            propertiesListingsAdapter = new AgentProfilePropertiesListingsAdapter(this, properties);
                            lvAgentProfilePropertyListings.setAdapter(propertiesListingsAdapter);
                            lvAgentProfilePropertyListings.setLoadingView(R.layout.layout_endless_loading);
                            lvAgentProfilePropertyListings.setListener(this);

                        } else {
                            lvAgentProfilePropertyListings.addNewData(properties);
                        }
                        offset = joResult.getInt("offset");


                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
        }
    }

    private void initialiseMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
//        MapFragment mapFragment = getMapFragment();
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng myPlace = new LatLng(Double.parseDouble(agentInfo.getLat()), Double.parseDouble(agentInfo.getLng()));
        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    CommonDef.REQUEST_LOCATION);
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 15));
        googleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon))
                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                .position(myPlace));
    }


    @Override
    public void loadData() {
        if (!isLastPage)
            getPropertyList();
        else
            lvAgentProfilePropertyListings.finaliseLoading();
    }

    @Override
    public void showToolbar() {

    }

    @Override
    public void hideToolbar() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
            isLocationSet = true;
        } else {
            gps.showSettingsAlert();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gps.stopUsingGPS();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gps.stopUsingGPS();
    }

    private void getPropertyList() {
        HashMap<String, String> headers = CommonMethods.getHeaders(AgentPublicProfile.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", agentID);
        params.put("listing_type", "1");
        params.put("offset", offset + "");
        params.put("lat", lat + "");
        params.put("lng", lng + "");
        System.out.println("peopertyListings params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(AgentPublicProfile.this, Request.Method.POST, params, UrlHelper.GET_PROPERTY_LISTINGS, headers, false, CommonDef.REQUEST_PROPERTY_LISTINGS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(true);
    }
}

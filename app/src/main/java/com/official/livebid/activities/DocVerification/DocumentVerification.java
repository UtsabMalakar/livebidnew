package com.official.livebid.activities.DocVerification;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.official.livebid.R;
import com.official.livebid.adapters.CommonSpinnerAdapter;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.objects.EnquiryTypeModel;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;

public class DocumentVerification extends AppCompatActivity implements View.OnClickListener, CropImageView.OnSetImageUriCompleteListener, CropImageView.OnGetCroppedImageCompleteListener, AdapterView.OnItemSelectedListener {
    private static final int IMG_DOC = 1237;
    private static final int IMG_FACE = 1238;
    private TextView tvImgDoc, tvImgSelfie;
    private ImageView ivImgDoc, ivImgSelfie;
    private CropImageView cropImageView;
    private Button btnCrop, btnSaveChanges;
    private RelativeLayout rlCropView;
    private LinearLayout rlMainView;
    private Spinner spDocType;
    private static int whichImg = 0; //0->none,1->DOC,2->SELPHI
    CommonSpinnerAdapter spinnerAdapter;
    ArrayList<EnquiryTypeModel> alDocTypes;
    private Uri imageUriDoc;
    private Uri imageUriSelf;
    private int selectedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.layout_doc_verification);
        new CustomActionBar.AuctionRegistrationMenu(DocumentVerification.this, 2);
        rlCropView = (RelativeLayout) findViewById(R.id.rl_crop_view);
        rlMainView = (LinearLayout) findViewById(R.id.ll_main_view);
        cropImageView = (CropImageView) findViewById(R.id.CropImageView);
        cropImageView.setCropShape(CropImageView.CropShape.RECTANGLE);
        cropImageView.setFixedAspectRatio(false);
        cropImageView.setOnGetCroppedImageCompleteListener(this);
        cropImageView.setOnSetImageUriCompleteListener(this);
        btnCrop = (Button) findViewById(R.id.btn_crop);
        btnSaveChanges = (Button) findViewById(R.id.btn_bottom);
        spDocType = (Spinner) findViewById(R.id.sp_doc_type);
        spDocType.setOnItemSelectedListener(this);
        btnSaveChanges.setOnClickListener(this);
        btnCrop.setOnClickListener(this);
        tvImgDoc = (TextView) findViewById(R.id.tv_img_doc);
        tvImgSelfie = (TextView) findViewById(R.id.tv_img_selfie);
        ivImgDoc = (ImageView) findViewById(R.id.iv_img_doc);
        ivImgSelfie = (ImageView) findViewById(R.id.iv_img_selfie);
        tvImgDoc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (tvImgDoc.getRight() - tvImgDoc.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        Toast.makeText(getApplicationContext(), "take doc image", Toast.LENGTH_SHORT).show();
                        startActivityForResult(CommonMethods.getPickImageChooserIntent(DocumentVerification.this), IMG_DOC);
                        return true;
                    }
                }
                return false;
            }
        });
        tvImgSelfie.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (tvImgSelfie.getRight() - tvImgSelfie.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        Toast.makeText(getApplicationContext(), "take selfie", Toast.LENGTH_SHORT).show();
                        startActivityForResult(CommonMethods.getPickImageChooserIntent(DocumentVerification.this), IMG_FACE);
                        return true;
                    }
                }
                return false;
            }
        });
        alDocTypes = new ArrayList<>();
        alDocTypes.add(new EnquiryTypeModel("0", "Select"));
        alDocTypes.add(new EnquiryTypeModel("1", "Driving License"));
        alDocTypes.add(new EnquiryTypeModel("2", "Passport"));
//        alDocTypes.add(new EnquiryTypeModel("1", "ID Card"));

        spinnerAdapter = new CommonSpinnerAdapter(this, alDocTypes);
        spDocType.setAdapter(spinnerAdapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IMG_DOC:
                if (resultCode == Activity.RESULT_OK) {

                    whichImg = 1;//doc
                    rlCropView.setVisibility(View.VISIBLE);
                    btnCrop.setVisibility(View.VISIBLE);
                    rlMainView.setVisibility(View.GONE);
                    btnSaveChanges.setVisibility(View.GONE);
                    imageUriDoc = CommonMethods.getPickImageResultUri(data, DocumentVerification.this);
                    cropImageView.setImageUriAsync(imageUriDoc);

                }
                break;
            case IMG_FACE:
                if (resultCode == Activity.RESULT_OK) {
                    whichImg = 2;//selphi
                    rlCropView.setVisibility(View.VISIBLE);
                    btnCrop.setVisibility(View.VISIBLE);
                    rlMainView.setVisibility(View.GONE);
                    btnSaveChanges.setVisibility(View.GONE);
                    imageUriSelf = CommonMethods.getPickImageResultUri(data, DocumentVerification.this);
                    cropImageView.setImageUriAsync(imageUriSelf);

                }
                break;
        }

    }

    @Override
    public void onGetCroppedImageComplete(CropImageView view, Bitmap bitmap, Exception error) {
        if (error == null) {
            if (whichImg == 1)
                ivImgDoc.setImageBitmap(bitmap);
            else if (whichImg == 2)
                ivImgSelfie.setImageBitmap(bitmap);


        } else {
            Toast.makeText(cropImageView.getContext(), "Image crop failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            if (imageUriDoc!=null&&imageUriSelf!=null){
                btnSaveChanges.setEnabled(true);
            }
//            Toast.makeText(cropImageView.getContext(), "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(cropImageView.getContext(), "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_crop:
                cropImageView.getCroppedImageAsync(cropImageView.getCropShape(), 0, 0);
                rlCropView.setVisibility(View.GONE);
                btnCrop.setVisibility(View.GONE);
                rlMainView.setVisibility(View.VISIBLE);
                btnSaveChanges.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_bottom:
                if (selectedItem!=0) {
                    finish();
                    Opener.SubmitAuctionRegistration(DocumentVerification.this, imageUriDoc, imageUriSelf, selectedItem);
                }
                else{
                    Toast.makeText(this,"Please select a doctype",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        selectedItem = i;

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

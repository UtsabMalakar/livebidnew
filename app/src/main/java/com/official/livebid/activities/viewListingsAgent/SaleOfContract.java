package com.official.livebid.activities.viewListingsAgent;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;

import java.util.HashMap;

import static com.official.livebid.activities.PropertyDetails.propertyListInfo;

public class SaleOfContract extends ActionBarActivity implements AsyncInterface,View.OnClickListener {
    WebView wv;
    private com.official.livebid.alerts.Alerts alerts;
    private CustomActionBar.TitleBarWithBackAndTitle privacyPolicyTitleBar;
    private Button btn_continue;
    private SharedPreference pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_of_contract);
        getSupportActionBar().hide();
        init();
        VolleyRequest volleyRequest = new VolleyRequest(SaleOfContract.this, Request.Method.GET, UrlHelper.GET_SALE_OF_CONTRACT, CommonDef.REQ_SALE_OF_CONTRACT);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }
    private void init() {
        privacyPolicyTitleBar=new CustomActionBar.TitleBarWithBackAndTitle(this,"Sale of Contract");
        wv = (WebView) findViewById(R.id.wv_privacy_policy);
        btn_continue = (Button) findViewById(R.id.btn_Continue);
        alerts = new Alerts(this);
        pref = new SharedPreference(this);
        btn_continue.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sale_of_contract, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        if (requestCode == CommonDef.REQ_SALE_OF_CONTRACT) {
            System.out.println("Privacy policy=>" + result);
            wv.loadData(result, "text/html", null);

        }
    }

    @Override
    public void onClick(View v) {
        pref.setKeyValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS, "0");
        switch (v.getId()) {
            case R.id.btn_Continue:
            if (pref.getStringValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS).equalsIgnoreCase("0"))
                Opener.AuctionRegistration(SaleOfContract.this, propertyListInfo);
            else
                registerAuction();

                break;
        }
    }

    private void registerAuction() {
        HashMap<String, String> headers = CommonMethods.getHeaders(SaleOfContract.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("register Auction params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(SaleOfContract.this, Request.Method.POST, params, UrlHelper.REGISTER_TO_AUCTION, headers, false, CommonDef.REQUEST_REGISTER_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }
}

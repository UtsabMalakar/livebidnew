package com.official.livebid.activities.liveAuction;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;

import java.util.ArrayList;

/**
 * Created by Ics on 5/2/2016.
 */
public class PropertyAuctionListAdapter extends BaseAdapter {
    private ArrayList<CurrentBidModel> auctionList;
    private Context context;

    public PropertyAuctionListAdapter(Context context, ArrayList<CurrentBidModel> auctionList) {
        this.context = context;
        this.auctionList = auctionList;
    }

    @Override
    public int getCount() {
        return auctionList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        CurrentBidsItemHolder cbh =null;
        if(view==null){
            view= LayoutInflater.from(context).inflate(R.layout.layout_current_bid_item,null, true);
            cbh=new CurrentBidsItemHolder(view);
            view.setTag(cbh);
        }
        else {
            cbh=(CurrentBidsItemHolder)view.getTag();
        }
        CurrentBidModel currentBidModel=auctionList.get(i);
        setCurrentBidModel(i,currentBidModel, cbh);

        return view;
    }

    private void setCurrentBidModel(int pos,CurrentBidModel currentBid, CurrentBidsItemHolder cbh) {
        if (currentBid.bidType == CurrentBidModel.MessageType.auctionStart) {
            cbh.messageContainer.setPadding(
                    (int) CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,5),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,5));
            cbh.seperator.setVisibility(View.GONE);
            cbh.tvAmount.setText(Html.fromHtml("<strong>Auction Start</strong>"));
            cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            cbh.tvTimeEllapsed.setText(DateTimeHelper.getFormatedDatedTime(currentBid.startDateTime)[0]);
            cbh.tvBidder.setText(DateTimeHelper.getFormatedDatedTime(currentBid.startDateTime)[1]);
        }

        else if (currentBid.bidType == CurrentBidModel.MessageType.firstCAll) {
            cbh.messageContainer.setPadding(
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15));
            cbh.seperator.setVisibility(View.VISIBLE);
            cbh.tvAmount.setText(Html.fromHtml("<strong>" + getCallReminderMessage(1,currentBid.bidAmount) + "</strong>"));
            cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_bell), null, null, null);
            cbh.tvTimeEllapsed.setText(currentBid.bidTime);
            cbh.tvBidder.setText("-");
        }
        else if (currentBid.bidType == CurrentBidModel.MessageType.secondCall) {
            cbh.messageContainer.setPadding(
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15));
            cbh.seperator.setVisibility(View.VISIBLE);
            cbh.tvAmount.setText(Html.fromHtml("<strong>" + getCallReminderMessage(2,currentBid.bidAmount) + "</strong>"));
            cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_bell), null, null, null);
            cbh.tvTimeEllapsed.setText(currentBid.bidTime);
            cbh.tvBidder.setText("-");
        }
        else if (currentBid.bidType == CurrentBidModel.MessageType.thirdCall) {
            cbh.messageContainer.setPadding(
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15));
            cbh.seperator.setVisibility(View.VISIBLE);
            cbh.tvAmount.setText(Html.fromHtml("<strong>" + getCallReminderMessage(3,currentBid.bidAmount) + "</strong>"));
            cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_bell), null, null, null);
            cbh.tvTimeEllapsed.setText(currentBid.bidTime);
            cbh.tvBidder.setText("-");
        }
        else if(currentBid.bidType == CurrentBidModel.MessageType.onSiteBid) {
            cbh.messageContainer.setPadding(
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15));
            cbh.seperator.setVisibility(View.VISIBLE);
            cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            cbh.tvAmount.setText(currentBid.bidAmount);
            cbh.tvTimeEllapsed.setText(currentBid.bidTime);
            cbh.tvBidder.setText("On site Bid");
        }
        else if (currentBid.bidType == CurrentBidModel.MessageType.vendorBid) {
            cbh.messageContainer.setPadding(
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15));
            cbh.seperator.setVisibility(View.VISIBLE);
            cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            cbh.tvAmount.setText(currentBid.bidAmount);
            cbh.tvTimeEllapsed.setText(currentBid.bidTime);
            cbh.tvBidder.setText("Vendor Bid");
        }
        else {
            cbh.messageContainer.setPadding(
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15),
                    (int)CommonMethods.pxFromDp((Activity)context,15));
            cbh.seperator.setVisibility(View.VISIBLE);
            cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            cbh.tvAmount.setText(currentBid.bidAmount);
            cbh.tvTimeEllapsed.setText(currentBid.bidTime);
            cbh.tvBidder.setText(currentBid.userUniqueId);
        }
    }

    public static class CurrentBidsItemHolder{
        protected TextView tvAmount, tvTimeEllapsed, tvBidder;
        protected LinearLayout messageContainer;
        protected View seperator;
        protected View rootView;

        public CurrentBidsItemHolder(View itemView) {
            rootView = itemView;
            tvAmount = (TextView) itemView.findViewById(R.id.tv_current_bid_amt);
            tvTimeEllapsed = (TextView) itemView.findViewById(R.id.tv_ellasped_time);
            tvBidder = (TextView) itemView.findViewById(R.id.tv_bidder);
            messageContainer= (LinearLayout) itemView.findViewById(R.id.ll_message_container);
            seperator=itemView.findViewById(R.id.sep);
        }
    }

    private String getCallReminderMessage(int count,String price){
        return getCountMessage(count)+" Call for"+price;
    }

    private String getCountMessage(int n){
        if(n==1)
            return "1st";
        if (n==2)
            return "2nd";
        return "Last";

    }
    public void addMessage(CurrentBidModel messages) {
        this.auctionList.add(0,messages);
        notifyDataSetChanged();
    }
}

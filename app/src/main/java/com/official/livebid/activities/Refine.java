package com.official.livebid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SearchHelper;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.staticClasses.SearchParams;
import com.official.livebid.utils.RangeSeekBar;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Refine extends ActionBarActivity implements SearchHelper.OnSearchComplete{
    TextView tvPriceRange;
    TextView tvLandRange;
    TextView tvSearchBar;
    int priceMin = 50000, priceMax = 10000000;
    int landMin = 50, landMax = 900000;
    Button btnSearch, btnClear;
    EditText etKeywordSearch;
    static Integer minP=0,maxP=0;
    static Integer minL=0,maxL=0;
    boolean flagRefineChanged = false;
    private String callingActivity;

    private String sqMtr;
    ImageButton ibtnBack;
    CheckBox cboxPropertyAll, cboxPropertyHouse,
            cboxApartment, cboxTownhouse,
            cboxLand, cboxVill;

    CheckBox cboxListAll, cboxListNew,
            cboxListTrending, cboxListSold,
            cboxListPassedIn, cboxListLiveNow;

    RadioGroup rgBathroom, rgBedroom, rgParking;

    ArrayList<String> alpropertyCheckbox;
    ArrayList<String> allistingCheckbox;

    private static final String PROP_ALL = "1";
    private static final String PROP_HOUSE = "2";
    private static final String PROP_APARTMENT = "3";
    private static final String PROP_TOWNHOUSE = "4";
    private static final String PROP_LAND = "5";
    private static final String PROP_VILLA = "6";

    private static final String LIST_ALL = "1";
    private static final String LIST_NEW = "2";
    private static final String LIST_TRENDING = "3";
    private static final String LIST_SOLD = "4";
    private static final String LIST_PASSED_IN = "5";
    private static final String LIST_LIVE = "6";

    private TextView tvPrice;
    private TextView tvLand;
    RelativeLayout rlSelectPrice;
    RelativeLayout rlSelectLand;
    private com.official.livebid.alerts.Alerts alerts;
    private SharedPreference pref;
    private String searchedSuburbs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_refine);
        init();
        checkIfClearIsActive();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setPrice();
        setLand();

    }

    private void setPrice() {
        if (minP.intValue() == 0 && maxP.intValue() == 0) {
            tvPrice.setText(formatPrice(minP));
            return;
        }
        if (minP.intValue() == maxP.intValue())
            tvPrice.setText(Html.fromHtml("<font color=\"#E01E32\">" + formatPrice(minP) + "</font>"));
        else
            tvPrice.setText(Html.fromHtml("<font color=\"#E01E32\">" + formatPrice(minP) + "</font>" + " to " + "<font color=\"#E01E32\">" + formatPrice(maxP) + "</font>"));

        btnClear.setEnabled(true);
    }
    private void setLand() {
        if (minL.intValue() == 0 && maxL.intValue() == 0) {
            tvLand.setText(formatLand(maxL));
            return;
        }
        if (minL.intValue() == maxL.intValue())
            tvLand.setText(Html.fromHtml("<font color=\"#E01E32\">" + formatLand(minL) + "</font>"));
        else
            tvLand.setText(Html.fromHtml("<font color=\"#E01E32\">" + formatLand(minL) + "</font>" + " to " + "<font color=\"#E01E32\">" + formatLand(maxL) + "</font>"));

        btnClear.setEnabled(true);
    }

    private void checkIfClearIsActive() {
        Thread clearActive = new Thread(new Runnable() {
            @Override
            public void run() {
//                Looper.prepare();
                System.out.print("Value changed1");
                if (flagRefineChanged) {
                    System.out.print("Value changed");
                } else {
                    System.out.print("Value not changed");
                }
//                while (flagRefineChanged){
//                    System.out.print("Value changed");
//                }
//                Looper.loop();
            }
        });
        clearActive.start();
    }

    /**
     * Manages the property listing check boxes;
     */
    private void initPropertyListingCBox() {
        alpropertyCheckbox = new ArrayList<>();
        cboxPropertyAll = (CheckBox) findViewById(R.id.cbox_propertyTypeAll);
        cboxPropertyHouse = (CheckBox) findViewById(R.id.cbox_propertyTypeHouse);
        cboxApartment = (CheckBox) findViewById(R.id.cbox_propertyTypeApartment);
        cboxTownhouse = (CheckBox) findViewById(R.id.cbox_propertyTypeTownHouse);
        cboxLand = (CheckBox) findViewById(R.id.cbox_propertyTypeLand);
        cboxVill = (CheckBox) findViewById(R.id.cbox_propertyTypeVilla);
        tvSearchBar = (TextView) findViewById(R.id.tv_acAddressSearch);

        cboxPropertyAll.setOnCheckedChangeListener(checkedChangeListener);
        cboxPropertyHouse.setOnCheckedChangeListener(checkedChangeListener);
        cboxApartment.setOnCheckedChangeListener(checkedChangeListener);
        cboxTownhouse.setOnCheckedChangeListener(checkedChangeListener);
        cboxLand.setOnCheckedChangeListener(checkedChangeListener);
        cboxVill.setOnCheckedChangeListener(checkedChangeListener);
    }

    CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
            flagRefineChanged = true;
            btnClear.setEnabled(true);

            switch (compoundButton.getId()) {
                case R.id.cbox_propertyTypeAll:
                    if (checked) {
                        cboxPropertyHouse.setChecked(false);
                        cboxApartment.setChecked(false);
                        cboxTownhouse.setChecked(false);
                        cboxLand.setChecked(false);
                        cboxVill.setChecked(false);
                        SearchParams.propertyType = PROP_ALL;
                    } else {
                        SearchParams.propertyType = "";
                    }
                    break;
                case R.id.cbox_propertyTypeApartment:
                    setPropertyChecked(checked, PROP_APARTMENT);
                    break;
                case R.id.cbox_propertyTypeHouse:
                    setPropertyChecked(checked, PROP_HOUSE);
                    break;
                case R.id.cbox_propertyTypeLand:
                    setPropertyChecked(checked, PROP_LAND);
                    break;
                case R.id.cbox_propertyTypeTownHouse:
                    setPropertyChecked(checked, PROP_TOWNHOUSE);
                    break;
                case R.id.cbox_propertyTypeVilla:
                    setPropertyChecked(checked, PROP_VILLA);
                    break;
                case R.id.cbox_listingTypeAll:
                    if (checked) {
                        cboxListNew.setChecked(false);
                        cboxListTrending.setChecked(false);
                        cboxListSold.setChecked(false);
                        cboxListPassedIn.setChecked(false);
                        cboxListLiveNow.setChecked(false);
                        SearchParams.listingType = LIST_ALL;
                    } else {
                        SearchParams.listingType = "";
                    }
                    break;
                case R.id.cbox_listingTypeNew:
                    setListingChecked(checked, LIST_NEW);
                    break;
                case R.id.cbox_listingTypeSold:
                    setListingChecked(checked, LIST_SOLD);
                    break;
                case R.id.cbox_listingTypeTrending:
                    setListingChecked(checked, LIST_TRENDING);
                    break;
                case R.id.cbox_listingPassedIn:
                    setListingChecked(checked, LIST_PASSED_IN);
                    break;
                case R.id.cbox_listingLiveNow:
                    setListingChecked(checked, LIST_LIVE);
                    break;
            }
        }
    };

    /**
     * Set the state of checkbox of property listing.
     *
     * @param checked      : if the checkbox is checked or not
     * @param propertyType : if is all, Villa, house, land ...
     */
    private void setPropertyChecked(boolean checked, String propertyType) {
        if (checked) {
            cboxPropertyAll.setChecked(false);
            if (SearchParams.propertyType.equals(PROP_ALL)) {
                SearchParams.propertyType = propertyType;
            } else {
                alpropertyCheckbox.add(propertyType);
            }
        } else {
            alpropertyCheckbox.remove(propertyType);
        }


        SearchParams.propertyType = "";
        for (String temp : alpropertyCheckbox) {
            SearchParams.propertyType += temp + ",";
        }
    }

    private TextWatcher twKewordsSearch = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            flagRefineChanged = true;
            btnClear.setEnabled(true);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void setListingChecked(boolean checked, String listingType) {
        if (checked) {
            cboxListAll.setChecked(false);
            if (SearchParams.listingType.equals(LIST_ALL)) {
                SearchParams.listingType = listingType;
            } else {
                allistingCheckbox.add(listingType);
            }
        } else {
            allistingCheckbox.remove(listingType);
        }


        SearchParams.listingType = "";
        for (String temp : allistingCheckbox) {
            SearchParams.listingType += temp + ",";
        }
    }


    private RadioGroup.OnCheckedChangeListener rgCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            flagRefineChanged = true;
            btnClear.setEnabled(true);
            setRadio(i);
            System.out.println("Radio id: -> i: " + i);
        }
    };

    private void setRadio(int id) {
        switch (id) {
            case R.id.rb_bathroom_any:
                SearchParams.nosBathrooms = "0";
                break;
            case R.id.rb_bathroom_one:
                SearchParams.nosBathrooms = "1";
                break;
            case R.id.rb_bathroom_two:
                SearchParams.nosBathrooms = "2";
                break;
            case R.id.rb_bathroom_three:
                SearchParams.nosBathrooms = "3";
                break;
            case R.id.rb_bathroom_four:
                SearchParams.nosBathrooms = "4";
                break;
            case R.id.rb_bathroom_five_plus:
                SearchParams.nosBathrooms = "5";
                break;
            case R.id.rb_bedroom_any:
                SearchParams.nosBedrooms = "0";
                break;
            case R.id.rb_bedroom_one:
                SearchParams.nosBedrooms = "1";
                break;
            case R.id.rb_bedroom_two:
                SearchParams.nosBedrooms = "2";
                break;
            case R.id.rb_bedroom_three:
                SearchParams.nosBedrooms = "3";
                break;
            case R.id.rb_bedroom_four:
                SearchParams.nosBedrooms = "4";
                break;
            case R.id.rb_bedroom_five_plus:
                SearchParams.nosBedrooms = "5";
                break;
            case R.id.rb_parking_any:
                SearchParams.nosParking = "0";
                break;
            case R.id.rb_parking_one:
                SearchParams.nosParking = "1";
                break;
            case R.id.rb_parking_two:
                SearchParams.nosParking = "2";
                break;
            case R.id.rb_parking_three:
                SearchParams.nosParking = "3";
                break;
            case R.id.rb_parking_four:
                SearchParams.nosParking = "4";
                break;
            case R.id.rb_parking_five_plus:
                SearchParams.nosParking = "5";
                break;
        }
    }

    private void init() {
        alerts=new Alerts(this);
        pref= new SharedPreference(this);
        sqMtr = " m²";
        SearchParams.initTypes();
        tvPrice= (TextView) findViewById(R.id.tv_price);
        tvLand= (TextView) findViewById(R.id.tv_land);
        rlSelectPrice= (RelativeLayout) findViewById(R.id.rl_select_price);
        rlSelectLand= (RelativeLayout) findViewById(R.id.rl_select_land);
        rlSelectPrice.setOnClickListener(clickListener);
        rlSelectLand.setOnClickListener(clickListener);
        ibtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        ibtnBack.setOnClickListener(clickListener);

        btnSearch = (Button) findViewById(R.id.btn_search);
        btnClear = (Button) findViewById(R.id.btn_clear);
        etKeywordSearch = (EditText) findViewById(R.id.et_keywordSearch);
        etKeywordSearch.addTextChangedListener(twKewordsSearch);
        btnSearch.setOnClickListener(clickListener);
        btnClear.setOnClickListener(clickListener);

        initPropertyListingCBox();
        initListingTypeCBox();
        rgBathroom = (RadioGroup) findViewById(R.id.rg_bathrooms);
        rgBedroom = (RadioGroup) findViewById(R.id.rg_bedrooms);
        rgParking = (RadioGroup) findViewById(R.id.rg_parkings);

        rgBathroom.setOnCheckedChangeListener(rgCheckedChangeListener);
        rgBedroom.setOnCheckedChangeListener(rgCheckedChangeListener);
        rgParking.setOnCheckedChangeListener(rgCheckedChangeListener);

        Bundle b = getIntent().getExtras();
        if(b!=null)
        {
            callingActivity = b.getString(CommonDef.CallingActivity.callingActivity);
            searchedSuburbs = b.getString(CommonDef.CallingActivity.searchedSuburbs);
        }
        setSearchHistory();

    }


    private void setSearchHistory() {
        pref = new SharedPreference(this);
        String searched_suburb = pref.getStringValues(CommonDef.SharedPrefKeys.Search_SuburbNames);
        System.out.print("Searched suburb" + searched_suburb);
        List<String> list = new ArrayList<String>(Arrays.asList(searchedSuburbs.split(",")));
        tvSearchBar.setText(getSearchedNames(list));
    }

    private String getSearchedNames(List<String> list)
    {
        int no_of_search=0;
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<list.size();i++)
        {
            if(i==0)
            {
                sb.append(list.get(i));
            }
            else
            {
                no_of_search++;
            }
        }
        if(list.size()==1)
        {
            return sb.toString();
        }
        else {
            sb.append(" and " + no_of_search + " more");
            return sb.toString();
        }
    }


    private void initListingTypeCBox() {
        allistingCheckbox = new ArrayList<>();
        cboxListAll = (CheckBox) findViewById(R.id.cbox_listingTypeAll);
        cboxListNew = (CheckBox) findViewById(R.id.cbox_listingTypeNew);
        cboxListTrending = (CheckBox) findViewById(R.id.cbox_listingTypeTrending);
        cboxListSold = (CheckBox) findViewById(R.id.cbox_listingTypeSold);
        cboxListPassedIn = (CheckBox) findViewById(R.id.cbox_listingPassedIn);
        cboxListLiveNow = (CheckBox) findViewById(R.id.cbox_listingLiveNow);

        cboxListAll.setOnCheckedChangeListener(checkedChangeListener);
        cboxListNew.setOnCheckedChangeListener(checkedChangeListener);
        cboxListTrending.setOnCheckedChangeListener(checkedChangeListener);
        cboxListSold.setOnCheckedChangeListener(checkedChangeListener);
        cboxListPassedIn.setOnCheckedChangeListener(checkedChangeListener);
        cboxListLiveNow.setOnCheckedChangeListener(checkedChangeListener);

    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_back:
                    onBackPressed();
                    break;
                case R.id.btn_search:
                    if (!etKeywordSearch.getText().toString().isEmpty()) {
                        SearchParams.keywordSearch = etKeywordSearch.getText() + "";
                    }
                    System.out.println("Search Params: " + SearchParams.keywordSearch + "\n" +
                            SearchParams.nosParking + "\n" +
                            SearchParams.nosBedrooms + "\n" +
                            SearchParams.nosBathrooms + "\n" +
                            SearchParams.propertyType + "\n" +
                            SearchParams.listingType + "\n" +
                            SearchParams.postCode + "\n" +
                            SearchParams.landRangeMax + "\n" +
                            SearchParams.landRangeMin + "\n" +
                            SearchParams.priceRangeMin + "\n" +
                            SearchParams.priceRangeMax + "\n"
                    );

                    SearchHelper searchHelper = new SearchHelper(Refine.this);
                    searchHelper.search();

                    break;
                case R.id.btn_clear:
                    reset();
                    break;
                case  R.id.rl_select_price:
                    Opener.PriceSelector(Refine.this);
                    break;
                case  R.id.rl_select_land:
                    Opener.LandSelector(Refine.this);
                    break;
            }
        }
    };

    private String getStringPrice(int price) {
        String value;
        if (price > 999999) {
            value = price / 1000000 + "M";
        } else {
            value = price / 1000 + "K";
        }
        return "$" + value;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        maxP=0;
        minP=0;
        minL=0;
        maxL=0;
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    private void reset() {
        SearchParams.reset();
        SearchParams.initTypes();
        etKeywordSearch.setText("");
        cboxListAll.setChecked(true);
        cboxPropertyAll.setChecked(true);
        rgBathroom.check(R.id.rb_bathroom_any);
        rgBedroom.check(R.id.rb_bedroom_any);
        rgParking.check(R.id.rb_parking_any);
        maxP=0;minP=0;
        setPrice();
        maxL=0;minL=0;
        setLand();
        btnClear.setEnabled(false);
    }
    private String formatPrice(long price) {

        if (price > 999999)
            if (price % 1000000 == 0)
                return "$" + price / 1000000 + "M";
            else
                return "$" + (double) price / 1000000 + "M";
        else if (price > 999)
            if (price % 1000 == 0)
                return "$" + price / 1000 + "K";
            else
                return "$" + (double) price / 1000 + "K";
        else if(price>0)
            return "$" + price;
        else
            return "Any";
    }
    private String formatLand(int i) {
        if (i == 0)
            return "Any";
        else
            return i + " m²";

    }

    @Override
    public void onRresultFound(String result) {
        Opener.PropertyListing(this, result,callingActivity);
    }

    @Override
    public void onResultNotFound(String msg) {
        alerts.showOkMessage(msg);
    }

    @Override
    public void onError(String msg) {
        alerts.showOkMessage(msg);
    }
}

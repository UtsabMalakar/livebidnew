package com.official.livebid.activities.whatsOn;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SlideShowViewPager;
import com.official.livebid.listeners.FavouriteCompleteListener;
import com.official.livebid.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ics on 3/17/2016.
 */
public class WhatsOnAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_AUCTION = 0;
    private static final int TYPE_OPEN_HOUSE = 1;
    private static final int TYPE_ITEM = 2;
    private ArrayList<OpenHouseProperty> allProperties;
    private Context context;
    private String auctionImgUrl;
    private String openHouseImageUrl;

    public WhatsOnAdapter(Context context, ArrayList<OpenHouseProperty> allProperties, String auctionImgUrl, String openHouseImageUrl) {
        this.context = context;
        this.allProperties = allProperties;
        this.auctionImgUrl = auctionImgUrl;
        this.openHouseImageUrl = openHouseImageUrl;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_list_item, parent, false);
            return new PropertyViewHolder(view);
        } else if (viewType == TYPE_AUCTION) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.auctions_trending_list_item, parent, false);
            return new AuctionViewHolder(view);
        } else if (viewType == TYPE_OPEN_HOUSE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.auctions_trending_list_item, parent, false);
            return new OpenHouseViewHolder(view);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PropertyViewHolder) {
            final int finalPos = position - 2;
            final OpenHouseProperty propertyListInfo = allProperties.get(finalPos);
            final PropertyViewHolder propertyViewHolder = (PropertyViewHolder) holder;
            propertyViewHolder.tvPrice.setText(propertyListInfo.price);
            if (!propertyListInfo.isOHP()) {

                if (propertyListInfo.propertyStatus == 0) {
                    if (propertyListInfo.propertyStatus2 == 4) {
                        propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                        propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                        propertyViewHolder.tvDateTag.setVisibility(View.GONE);

                    } else {
                        if (propertyListInfo.propertyStatus2 != 0)
                        propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                        propertyViewHolder.tvDateTag.setVisibility(View.VISIBLE);
                        propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                        propertyViewHolder.tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
                    }

                } else {
                    propertyViewHolder.tvDateTag.setVisibility(View.GONE);
                    propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                    propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
                }
            } else {
                if (propertyListInfo.getOhpStatus() == 0 || propertyListInfo.getOhpStatus()==2) {
                    propertyViewHolder.tvStatusTag.setText(propertyListInfo.getStatusTag());
                    propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                } else {
                    propertyViewHolder.tvStatusTag.setText(propertyListInfo.getStatusTag());
                    propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                }
                propertyViewHolder.tvDateTag.setBackgroundColor(context.getResources().getColor(R.color.theme_blue));
                propertyViewHolder.tvDateTag.setText(Html.fromHtml(propertyListInfo.getOHTimeTag()));
            }
            propertyViewHolder.tvAgentFName.setText(propertyListInfo.agentFName);
            propertyViewHolder.tvAgentLName.setText(propertyListInfo.agentLName);
            propertyViewHolder.cboxFav.setChecked(propertyListInfo.isFav());
            propertyViewHolder.tvPropertyStreet.setText(propertyListInfo.street);
            propertyViewHolder.tvPropertySuburbPostcode.setText(propertyListInfo.suburb + " "+ propertyListInfo.propertyState+ " " + propertyListInfo.postCode);
            if (!propertyListInfo.agentAgencyLogoUrl.trim().isEmpty())
                Picasso.with(context)
                        .load(propertyListInfo.agentAgencyLogoUrl)
                        .resize((int)CommonMethods.pxFromDp(context,96),(int)CommonMethods.pxFromDp(context,60))
                        .into(propertyViewHolder.ivAgentAgencyIcon);
            else {
                Picasso.with(context)
                        .load(R.drawable.black_livebid_logo)
                        .into(propertyViewHolder.ivAgentAgencyIcon);
            }
            if (!propertyListInfo.agentProfileImg.trim().isEmpty()) {
                Picasso.with(context)
                        .load(propertyListInfo.agentProfileImg)
                        .placeholder(R.drawable.profile_icon)
                        .transform(new CircleTransform((Activity) context))
                        .into(propertyViewHolder.ivAgentImg);
            } else {
                Picasso.with(context)
                        .load(R.drawable.profile_icon)
//                        .transform(new CircleTransform((Activity) context))
                        .into(propertyViewHolder.ivAgentImg);
            }
            //view pager adapter
            ImageSlideAdapter mImageSlideAdapter;
            mImageSlideAdapter = new ImageSlideAdapter(context, propertyListInfo.property_images_more,propertyListInfo.propertyStatus);
            propertyViewHolder.mViewPager.setAdapter(mImageSlideAdapter);
            propertyViewHolder.mViewPager.setOnViewPagerClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Opener.PropertyDetails((Activity) context, propertyListInfo);

                }
            });
            if (propertyListInfo.propertyStatus == 2) {
                propertyViewHolder.tvStatusTag.setVisibility(View.GONE);
                propertyViewHolder.ivSoldPropertyTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvPropertyImageIndicator.setVisibility(View.GONE);
//                propertyViewHolder.mViewPager.beginFakeDrag();

            }else {
                propertyViewHolder.ivSoldPropertyTag.setVisibility(View.GONE);
                if (propertyListInfo.propertyStatus==5){
                    propertyViewHolder.tvStatusTag.setVisibility(View.INVISIBLE);
                }
                else {
                    propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                }
                propertyViewHolder.tvPropertyImageIndicator.setVisibility(View.GONE);
                propertyViewHolder.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        propertyViewHolder.tvPropertyImageIndicator.setText(position+1 + "/" + propertyListInfo.property_images_more.size());
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });

            }

            propertyViewHolder.tvNosBathroom.setText(propertyListInfo.nosOfBathroom);
            propertyViewHolder.tvNosBedroom.setText(propertyListInfo.nosOfBedroom);
            propertyViewHolder.tvNosParking.setText(propertyListInfo.nosOfParking);
            propertyViewHolder.tvPriceText.setVisibility(View.VISIBLE);
            propertyViewHolder.tvPriceText.setText(propertyListInfo.price_text);

            propertyViewHolder.cboxFav.setOnClickListener(

                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final CheckBox cboxTemp = (CheckBox) view;
                            new FavoriteHelper(context).makeFavorite(allProperties.get(finalPos).id, allProperties.get(finalPos).getFav(), new FavouriteCompleteListener() {
                                @Override
                                public void onSuccess(boolean b) {
                                    allProperties.get(finalPos).setFav(b);
                                    cboxTemp.setChecked(b);
                                }

                                @Override
                                public void onFailure() {
                                    cboxTemp.setChecked(allProperties.get(finalPos).getFav());
                                }

                                @Override
                                public void onNotLoggedIn() {
                                    cboxTemp.setChecked(false);
                                    new Alerts((Activity) context).signIn();
                                }
                            });

                            // Webservice call for change.

                        }
                    });
            propertyViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if(details) {
//                    System.out.println("id: main: " + (int) getItemId(position) + " " + getItemId(position) + " " + position);
                    Opener.PropertyDetails((Activity) context, allProperties.get(finalPos));
//                    }
                }
            });

        } else if (holder instanceof AuctionViewHolder) {
            AuctionViewHolder auctionViewHolder = (AuctionViewHolder) holder;
            auctionViewHolder.tvTitle.setText("Auctions");
            auctionViewHolder.btnBrowse.setText("Auctions");
            Picasso.with(context)
                    .load(auctionImgUrl)
                    .resize(1080, 1080)
                    .centerCrop()
                    .placeholder(R.mipmap.img_property_placeholder)
                    .into(auctionViewHolder.ivBgImage);
            auctionViewHolder.btnBrowse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Opener.AuctionToday((Activity) context);
//                    Toast.makeText(context, "in progress", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (holder instanceof OpenHouseViewHolder) {
            OpenHouseViewHolder openHouseViewHolder = (OpenHouseViewHolder) holder;
            openHouseViewHolder.tvTitle.setText("Open House");
            openHouseViewHolder.btnBrowse.setText("Open House");
            Picasso.with(context)
                    .load(openHouseImageUrl)
                    .resize(1080, 1080)
                    .centerCrop()
                    .placeholder(R.mipmap.img_property_placeholder)
                    .into(openHouseViewHolder.ivBgImage);
            openHouseViewHolder.btnBrowse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Opener.OpenHouseToday((Activity) context);
//                    Toast.makeText(context, "in progress", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_AUCTION;
        if (position == 1)
            return TYPE_OPEN_HOUSE;
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        if (allProperties.size() == 0)
            return 2;
        return allProperties.size() + 2;
    }

    public void addMoreData(ArrayList<OpenHouseProperty> data) {
        int startPosition = allProperties.size();
        this.allProperties.addAll(data);
        this.notifyItemRangeInserted(startPosition, data.size());
    }

    public class PropertyViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvPropertyImageIndicator;
        private final SlideShowViewPager mViewPager;
        private final TextView tvPriceText;
        ImageView ivPropertyImg, ivAgentImg, ivAgentAgencyIcon, ivSoldPropertyTag;
        ;
        CheckBox cboxFav;
        TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
                tvPropertyStatusIdentifier, tvPrice, tvNosBedroom, tvNosBathroom,
                tvNosParking, tvAgentFName, tvAgentLName;
        View rootView;

        public PropertyViewHolder(View view) {
            super(view);
            rootView = view;
            mViewPager = (SlideShowViewPager) view.findViewById(R.id.vp_property_img);
            tvPropertyImageIndicator = (TextView) view.findViewById(R.id.tvPropertyImageIndicator);
            ivAgentImg = (ImageView) view.findViewById(R.id.iv_agent_img);
            ivSoldPropertyTag = (ImageView) view.findViewById(R.id.iv_property_sold_tag);
            ivAgentAgencyIcon = (ImageView) view.findViewById(R.id.iv_agent_agency_icon);
            cboxFav = (CheckBox) view.findViewById(R.id.cbox_fav);

            com.official.livebid.enums.AppMode appMode = CommonMethods.getAppMode(context);
            if (appMode == AppMode.guest){
                cboxFav.setVisibility(View.GONE);
            }

            tvStatusTag = (TextView) view.findViewById(R.id.tv_property_status_tag);
            tvPriceText = (TextView) view.findViewById(R.id.tvPriceText);
            tvDateTag = (TextView) view.findViewById(R.id.tv_property_date_tag);
            tvPropertySuburbPostcode = (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
            tvPropertyStreet = (TextView) view.findViewById(R.id.tv_property_street);
            tvPropertyStatusIdentifier = (TextView) view.findViewById(R.id.tv_property_status_identifier);
            tvAgentFName = (TextView) view.findViewById(R.id.tv_agent_fname);
            tvAgentLName = (TextView) view.findViewById(R.id.tv_agent_lname);
            tvPrice = (TextView) view.findViewById(R.id.tv_property_price);
            tvNosBedroom = (TextView) view.findViewById(R.id.tv_nosBedroom);
            tvNosBathroom = (TextView) view.findViewById(R.id.tv_nosBathroom);
            tvNosParking = (TextView) view.findViewById(R.id.tv_nosParking);
        }
    }

    public class AuctionViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        Button btnBrowse;
        ImageView ivBgImage;

        public AuctionViewHolder(View view) {
            super(view);

            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            btnBrowse = (Button) view.findViewById(R.id.btn_browse);
            ivBgImage = (ImageView) view.findViewById(R.id.iv_bgImg);
        }


    }

    public class OpenHouseViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        Button btnBrowse;
        ImageView ivBgImage;

        public OpenHouseViewHolder(View view) {
            super(view);

            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            btnBrowse = (Button) view.findViewById(R.id.btn_browse);
            ivBgImage = (ImageView) view.findViewById(R.id.iv_bgImg);
        }


    }
}

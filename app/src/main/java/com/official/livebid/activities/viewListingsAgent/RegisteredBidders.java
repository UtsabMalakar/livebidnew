package com.official.livebid.activities.viewListingsAgent;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.AuctionApplicationModel;
import com.official.livebid.objects.PropertyListInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RegisteredBidders extends AppCompatActivity implements AsyncInterface, View.OnClickListener {
    LinearLayoutManager linearLayoutManager;
    private PropertyListInfo propertyListInfo;
    private RecyclerView rvRegisteredBidders;
    private Alerts alerts;
    private RegisteredBiddersAdapter regBiddersAdapter;
    private Button btnNotifyBidders;
    private boolean isToolbarShown=false;
    private LinearLayout cabContainer;
    private ImageView ibtnBack;
    private TextView tvCenter;
    private ImageView ibtnRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_registered_bidders);
        init();
        getRegisteredBidders();
    }

    private void getRegisteredBidders() {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        params.put("registered_from_cms", "0");//0->default 1-> onsiteb bidders
        System.out.println("registered bidders params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(RegisteredBidders.this, Request.Method.POST, params, UrlHelper.GET_REG_BIDDERS, headers, false, CommonDef.REQ_REG_BIDDERS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void init() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            propertyListInfo = (PropertyListInfo) bundle.getSerializable(CommonDef.PROPERTY_LIST_INFO);
        }
        alerts = new Alerts(this);
        new CustomActionBar.PreAuctionOfferAgentCAB(this, propertyListInfo.propertyCode);
        // auction Applications
        rvRegisteredBidders = (RecyclerView) findViewById(R.id.rv_reg_bidders);
        linearLayoutManager = new LinearLayoutManager(this);
        rvRegisteredBidders.setLayoutManager(linearLayoutManager);
        btnNotifyBidders = (Button) findViewById(R.id.btn_bottom);
        btnNotifyBidders.setEnabled(true);
        btnNotifyBidders.setOnClickListener(this);
        cabContainer = (LinearLayout) findViewById(R.id.cab);
        ibtnBack = (ImageView) findViewById(R.id.ibtn_left);
        ibtnBack.setImageResource(R.drawable.back_icon);
        tvCenter = (TextView) findViewById(R.id.tv_center);
        tvCenter.setVisibility(View.INVISIBLE);

        tvCenter.setTextColor(Color.WHITE);
        ibtnRight = (ImageView) findViewById(R.id.ibtn_right);
        cabContainer.setBackgroundColor(Color.TRANSPARENT);
        rvRegisteredBidders.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.computeVerticalScrollOffset() > 100) {
                    showToolbar();
                    isToolbarShown = true;
                } else {
                    hideToolbar();
                    isToolbarShown = false;
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registered_bidders, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("registered Bidders Response:" + result);
        switch (requestCode) {
            case CommonDef.REQ_REG_BIDDERS:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        JSONArray jaPreOffers = joResult.getJSONArray("list");
                        if (jaPreOffers.length() > 0) {
                            JSONObject joOffer = null;
                            ArrayList<AuctionApplicationModel> auctionApplications = new ArrayList<>();
                            AuctionApplicationModel auctionApplication = null;
                            for (int i = 0; i < jaPreOffers.length(); i++) {
                                joOffer = jaPreOffers.getJSONObject(i);
                                auctionApplication = new AuctionApplicationModel(
                                        joOffer.getString("user_id"),
                                        joOffer.getString("unique_id"),
                                        joOffer.getString("profile_image")
                                );
                                auctionApplications.add(auctionApplication);
                            }
                            regBiddersAdapter = new RegisteredBiddersAdapter(RegisteredBidders.this, auctionApplications, propertyListInfo);
                            rvRegisteredBidders.setAdapter(regBiddersAdapter);
                        }


                    } else {
                        regBiddersAdapter = new RegisteredBiddersAdapter(RegisteredBidders.this, new ArrayList<AuctionApplicationModel>(), propertyListInfo);
                        rvRegisteredBidders.setAdapter(regBiddersAdapter);
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_NOTIFY_BIDDERS:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        alerts.showOkMessage(msg);

                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    public void showNotifyBiddersPopUp() {
        View view = getLayoutInflater().inflate(R.layout.layout_vendor_bid_popup, null);
        final Button btnConfirm = (Button) view.findViewById(R.id.btn_confirm);
        btnConfirm.setText("Send");
        final Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        final EditText etOfferPrice = (EditText) view.findViewById(R.id.et_offer_price);
        final TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        final View sep = view.findViewById(R.id.view_sep);
        sep.setVisibility(View.GONE);
        tvTitle.setText("Notify Bidders");
        etOfferPrice.setHint("Write Message");
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(100);
        etOfferPrice.setFilters(fArray);
        etOfferPrice.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etOfferPrice.getText().toString().isEmpty()) {
                    etOfferPrice.setError("Message should not be empty");
                    return;
                }
                mBottomSheetDialog.dismiss();
                notifyBidders(etOfferPrice.getText().toString());
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();

            }
        });
    }

    private void notifyBidders(String message) {
        HashMap<String, String> headers = CommonMethods.getHeaders(RegisteredBidders.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        params.put("notification_message", message);
        System.out.println("notify bidders param: " + params.toString());
        VolleyRequest vrs = new VolleyRequest(RegisteredBidders.this, Request.Method.POST, params, UrlHelper.NOTIFY_BIDDERS, headers, false, CommonDef.RC_NOTIFY_BIDDERS);
        vrs.asyncInterface = this;
        vrs.request();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_bottom:
                showNotifyBiddersPopUp();
                break;
        }

    }
    public void showToolbar() {
        if (isToolbarShown)
            return;
        ibtnBack.setImageResource(R.drawable.left_arrow);
        tvCenter.setVisibility(View.VISIBLE);
        tvCenter.setTextColor(Color.BLACK);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(cabContainer, "backgroundColor", new ArgbEvaluator(), Color.argb(0, 255, 255, 255), Color.argb(255, 255, 255, 255));//getResources().getColor(R.color.theme_yellow)
        colorFade.setDuration(1000);
        colorFade.start();
    }


    public void hideToolbar() {
        if (!isToolbarShown)
            return;
        ibtnBack.setImageResource(R.drawable.back_icon);
        tvCenter.setVisibility(View.INVISIBLE);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(cabContainer, "backgroundColor", new ArgbEvaluator(), Color.argb(255, 255, 255, 255), Color.argb(0, 255, 255, 255));
        colorFade.setDuration(1000);
        colorFade.start();
    }
}

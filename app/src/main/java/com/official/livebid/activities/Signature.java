package com.official.livebid.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.graphics.BitmapCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.official.livebid.R;
import com.official.livebid.Views.InkView;

import java.io.ByteArrayOutputStream;

public class Signature extends AppCompatActivity implements View.OnClickListener{

    private InkView inkView;
    private TextView tvSignHint;
    ImageButton ibtnBack, ibtnDone, ibtnReset;
    boolean isSigned=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_signature);
        init();
    }

    private void init() {
        tvSignHint = (TextView) findViewById(R.id.tv_sign_hint);
        inkView = (InkView) findViewById(R.id.ink);
        ibtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        ibtnDone = (ImageButton) findViewById(R.id.ibtn_done);
        ibtnReset = (ImageButton) findViewById(R.id.ibtn_reset);
        ibtnBack.setOnClickListener(this);
        ibtnDone.setOnClickListener(this);
        ibtnReset.setOnClickListener(this);

        inkView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    tvSignHint.setVisibility(View.GONE);
                }
                return false;
            }
        });
        inkView.addInkListener(new InkView.InkListener() {
            @Override
            public void onInkClear() {
                isSigned=false;
            }

            @Override
            public void onInkDraw() {
                isSigned=true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ibtn_back:
                Intent intent=new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
                break;
            case R.id.ibtn_done:
                saveSignAndProceed();
                break;
            case R.id.ibtn_reset:
                inkView.clear();
                tvSignHint.setVisibility(View.VISIBLE);
                break;
        }


    }

    private void saveSignAndProceed() {

        //Convert to byte array
        if(!isSigned){
            Toast.makeText(Signature.this, "Signature is empty", Toast.LENGTH_SHORT).show();
            return;
        }
        Bitmap signature = inkView.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        signature.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();


        Intent intent=new Intent();
        intent.putExtra("image",byteArray);
        setResult(RESULT_OK, intent);
        finish();

    }
}

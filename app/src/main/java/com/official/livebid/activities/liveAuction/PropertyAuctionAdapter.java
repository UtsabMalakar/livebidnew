package com.official.livebid.activities.liveAuction;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.listeners.FavouriteCompleteListener;

import java.util.ArrayList;

/**
 * Created by Ics on 4/19/2016.
 */
public class PropertyAuctionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<CurrentBidModel> auctionList;
    private Context context;
    private PropertyAuctionModel property;
    private boolean isWon = false;
    final static int ITEM_PROPERTY = 0;
    final static int ITEM_BID_TITLE = 1;
    final static int ITEM_BID = 2;
    final static int ITEM_BID_WON = 5;
    private boolean animateView = false;
    AppMode appMode;
    private long onSiteBidAmount = 0;
    private PlaceOnSiteBidListener listener;
    private String wonHtml = "<b>congratulations!</b><br /><br />" +
            "Great job on winning the auction!<br />" +
            "<br />" +
            "please arrange a suitable time to meet up with the property agent within today to finalise and" +
            "sign off the contract agreement." +
            "As per agreed(unless stated otherwise via an amended Contract of Sale), you will have to:" +
            "<br /><br />" +
            "-pay a 10% Deposit upfront<br />" +
            "-pay the rest of the balance within 12 weeks";

    public PropertyAuctionAdapter(Context context, ArrayList<CurrentBidModel> auctionList, PropertyAuctionModel property, AppMode mode, boolean isWon, PlaceOnSiteBidListener listener) {
        this.context = context;
        this.auctionList = auctionList;
        this.property = property;
        this.appMode = mode;
        this.listener = listener;
        this.isWon = isWon;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return ITEM_PROPERTY;
        else if (position == 1) {
            if (appMode == AppMode.user && isWon)
                return ITEM_BID_WON;
            else
                return ITEM_BID_TITLE;
        } else
            return ITEM_BID;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == ITEM_PROPERTY) {
            view = LayoutInflater.from(context).inflate(R.layout.property_header, parent, false);
            return new PropertyHolder(view);
        } else if (viewType == ITEM_BID_TITLE) {
            view = LayoutInflater.from(context).inflate(R.layout.layout_current_bid_desc, parent, false);
            return new CurrentBisDescHolder(view);
        } else if (viewType == ITEM_BID_WON) {
            view = LayoutInflater.from(context).inflate(R.layout.layout_bid_won, parent, false);
            return new BidWonViewHolder(view);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.layout_current_bid_item, parent, false);
            return new CurrentBidsItemHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PropertyHolder) {
            setProperty(holder);
            return;
        }
        if (holder instanceof CurrentBisDescHolder) {
            setTitle(holder);
            return;
        }
        if (holder instanceof CurrentBidsItemHolder) {
            setCurrentBids(holder, position - 2);
            return;
        }
        if (holder instanceof BidWonViewHolder) {
            setWinMessage(holder);
        }
    }

    private void setWinMessage(RecyclerView.ViewHolder holder) {
        BidWonViewHolder bwh = (BidWonViewHolder) holder;
        bwh.tvWonText.setText(Html.fromHtml(wonHtml));
    }

    private void setCurrentBids(RecyclerView.ViewHolder holder, int pos) {
        CurrentBidsItemHolder cbh = (CurrentBidsItemHolder) holder;
        CurrentBidModel currentBid = auctionList.get(pos);
        switch (currentBid.bidType) {
            case auctionStart:
                cbh.messageContainer.setPadding(
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 5),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 5));
                cbh.seperator.setVisibility(View.GONE);
                cbh.tvAmount.setText(Html.fromHtml("<strong>Auction Start</strong>"));
                cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                cbh.tvTimeEllapsed.setText(DateTimeHelper.getFormatedDatedTime(currentBid.startDateTime)[0]);
                cbh.tvBidder.setText(DateTimeHelper.getFormatedDatedTime(currentBid.startDateTime)[1]);
                cbh.messageContainer.setBackgroundColor(Color.WHITE);
                break;
            case firstCAll:
                cbh.messageContainer.setPadding(
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15));
                cbh.seperator.setVisibility(View.VISIBLE);
                cbh.tvAmount.setText(Html.fromHtml("<strong>" + getCallReminderMessage(1, currentBid.bidAmount) + "</strong>"));
                cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_bell), null, null, null);
                cbh.tvTimeEllapsed.setText(currentBid.bidTime);
                cbh.tvBidder.setText("-");
                if (currentBid.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(context)))
                    cbh.messageContainer.setBackgroundColor(context.getResources().getColor(R.color.self_highlight));
                else
                    cbh.messageContainer.setBackgroundColor(Color.WHITE);
                break;
            case secondCall:
                cbh.messageContainer.setPadding(
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15));
                cbh.seperator.setVisibility(View.VISIBLE);
                cbh.tvAmount.setText(Html.fromHtml("<strong>" + getCallReminderMessage(2, currentBid.bidAmount) + "</strong>"));
                cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_bell), null, null, null);
                cbh.tvTimeEllapsed.setText(currentBid.bidTime);
                cbh.tvBidder.setText("-");
                if (currentBid.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(context)))
                    cbh.messageContainer.setBackgroundColor(context.getResources().getColor(R.color.self_highlight));
                else
                    cbh.messageContainer.setBackgroundColor(Color.WHITE);
                break;
            case thirdCall:
                cbh.messageContainer.setPadding(
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15));
                cbh.seperator.setVisibility(View.VISIBLE);
                cbh.tvAmount.setText(Html.fromHtml("<strong>" + getCallReminderMessage(3, currentBid.bidAmount) + "</strong>"));
                cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_bell), null, null, null);
                cbh.tvTimeEllapsed.setText(currentBid.bidTime);
                cbh.tvBidder.setText("-");
                if (currentBid.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(context)))
                    cbh.messageContainer.setBackgroundColor(context.getResources().getColor(R.color.self_highlight));
                else
                    cbh.messageContainer.setBackgroundColor(Color.WHITE);
                break;
            case onSiteBid:
                cbh.messageContainer.setPadding(
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15));
                cbh.seperator.setVisibility(View.VISIBLE);
                cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                cbh.tvAmount.setText(currentBid.bidAmount);
                cbh.tvTimeEllapsed.setText(currentBid.bidTime);
//                cbh.tvBidder.setText("On site Bid");
                cbh.tvBidder.setText(currentBid.userUniqueId+"(On-Site)");
                cbh.messageContainer.setBackgroundColor(Color.WHITE);
                break;
            case vendorBid:
                cbh.messageContainer.setPadding(
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15));
                cbh.seperator.setVisibility(View.VISIBLE);
                cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                cbh.tvTimeEllapsed.setText(currentBid.bidTime);
                if (currentBid.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(context))) {
                    cbh.messageContainer.setBackgroundColor(context.getResources().getColor(R.color.self_highlight));
                    cbh.tvBidder.setText(Html.fromHtml("<b>Vendor Bid</b>"));
                    cbh.tvAmount.setText(Html.fromHtml("<b>" + currentBid.bidAmount + "</b>"));

                } else {
                    cbh.messageContainer.setBackgroundColor(Color.WHITE);
                    cbh.tvBidder.setText("Vendor Bid");
                    cbh.tvAmount.setText(currentBid.bidAmount);
                }
                break;
            case generalBid:
                cbh.messageContainer.setPadding(
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 15));
                cbh.seperator.setVisibility(View.VISIBLE);
                cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                cbh.tvTimeEllapsed.setText(currentBid.bidTime);
                if (currentBid.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(context))) {
                    cbh.messageContainer.setBackgroundColor(context.getResources().getColor(R.color.self_highlight));
                    cbh.tvBidder.setText(Html.fromHtml("<b>You</b>"));
                    cbh.tvAmount.setText(Html.fromHtml("<b>" + currentBid.bidAmount + "</b>"));

                } else {
                    cbh.messageContainer.setBackgroundColor(Color.WHITE);
                    cbh.tvBidder.setText(currentBid.userUniqueId);
                    cbh.tvAmount.setText(currentBid.bidAmount);
                }

                break;

            case propertySold:
                cbh.messageContainer.setPadding(
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 5),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 5));
                cbh.tvAmount.setText(Html.fromHtml("<strong>Auction Closed</strong>"));
                cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                cbh.tvTimeEllapsed.setText(DateTimeHelper.getFormatedDatedTime(currentBid.startDateTime)[0]);
                cbh.tvBidder.setText(DateTimeHelper.getFormatedDatedTime(currentBid.startDateTime)[1]);
                cbh.messageContainer.setBackgroundColor(Color.WHITE);
                break;
            case propertyPassedIn:
                cbh.messageContainer.setPadding(
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 5),
                        (int) CommonMethods.pxFromDp((Activity) context, 15),
                        (int) CommonMethods.pxFromDp((Activity) context, 5));
                cbh.tvAmount.setText(Html.fromHtml("<strong>Auction closed</strong>"));
                cbh.tvAmount.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                cbh.tvTimeEllapsed.setText(DateTimeHelper.getFormatedDatedTime(currentBid.startDateTime)[0]);
                cbh.tvBidder.setText(DateTimeHelper.getFormatedDatedTime(currentBid.startDateTime)[1]);
                cbh.messageContainer.setBackgroundColor(Color.WHITE);
                break;
        }
        if (animateView)
            setAnimation(cbh.rootView, pos);
        animateView = false;

    }

    private void setTitle(RecyclerView.ViewHolder holder) {
        final CurrentBisDescHolder cbd = (CurrentBisDescHolder) holder;
        if (this.appMode == AppMode.agent) {
            cbd.llVendorBidHolder.setVisibility(View.VISIBLE);
            cbd.etOnSiteBidder.setFocusable(false);
            cbd.etOnSiteBidder.setFocusableInTouchMode(false);
            cbd.etOnSiteBidder.setClickable(true);
            if (onSiteBidAmount > 0)
                cbd.etOnSiteBidder.setText(onSiteBidAmount + "");
            cbd.etOnSiteBidder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onPlaceOnSiteBid();
                }

            });

        } else
            cbd.llVendorBidHolder.setVisibility(View.GONE);

    }

    private void setProperty(RecyclerView.ViewHolder holder) {
        final PropertyHolder header = (PropertyHolder) holder;
        header.tvHighestBidTitle.setText("Highest Bid");
        header.tvHighestBidAmount.setText(property.highestBidAmount);
        header.tvHighestBidderTitle.setText("Highest Bidder");
        header.tvHighestBidder.setText(property.highestBidder);
        header.tvPropertyStreet.setText(property.street);
        header.tvPropertySuburbPostcode.setText(property.regionPostcode);
        //Set adapter viewpager
        ImageSlideAdapter mImageSlideAdapter;
        mImageSlideAdapter = new ImageSlideAdapter(context,property.moreImages,1);
        header.mViewPager.setAdapter(mImageSlideAdapter);

            header.tvStatusTag.setVisibility(View.VISIBLE);
            header.tvPropertyImageIndicator.setVisibility(View.GONE);
        header.tvPropertyImageIndicator.setText(1 + "/" + property.moreImages.size());
            header.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    header.tvPropertyImageIndicator.setText(position+1 + "/" + property.moreImages.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        header.cboxFav.setChecked(property.isFav());
        header.cboxFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CheckBox cboxTemp = (CheckBox) view;
                new FavoriteHelper(context).makeFavorite(property.propertyId, property.isFav(), new FavouriteCompleteListener() {
                    @Override
                    public void onSuccess(boolean b) {
                        property.setFav(b);
                        cboxTemp.setChecked(b);
                    }

                    @Override
                    public void onFailure() {
                        cboxTemp.setChecked(property.isFav());
                    }

                    @Override
                    public void onNotLoggedIn() {
                        cboxTemp.setChecked(false);
                        new Alerts(context).signIn();
                    }
                });

                // Webservice call for change.

            }
        });
//        header.tvDateTag.setBackgroundColor(context.getResources().getColor(R.color.theme_red));
        header.tvStatusTag.setVisibility(View.VISIBLE);
        header.tvDateTag.setVisibility(View.GONE);
        header.tvStatusTag.setText(auctionList.size() > 1 ? "ON THE MARKET" : "AUCTION LIVE NOW");
    }

    @Override
    public int getItemCount() {
        if (isWon)
            return 2;
        return auctionList.size() + 2;
    }


    public static class PropertyHolder extends RecyclerView.ViewHolder {
        private final TextView tvPropertyImageIndicator;
        private final ViewPager mViewPager;
        private final CheckBox cboxFav;
        protected ImageView ivPropertyImg;
        protected TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
                tvPropertyStatusIdentifier, tvHighestBidAmount, tvHighestBidder, tvHighestBidderTitle, tvHighestBidTitle;
        protected View rootView;

        public PropertyHolder(View view) {
            super(view);
            rootView = view;
            cboxFav = (CheckBox) view.findViewById(R.id.cbox_fav);
            mViewPager = (ViewPager) view.findViewById(R.id.vp_property_img);
            tvPropertyImageIndicator = (TextView) view.findViewById(R.id.tvPropertyImageIndicator);
            tvStatusTag = (TextView) view.findViewById(R.id.tv_property_status_tag);
            tvDateTag = (TextView) view.findViewById(R.id.tv_property_date_tag);
            tvPropertySuburbPostcode = (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
            tvPropertyStreet = (TextView) view.findViewById(R.id.tv_property_street);
            tvPropertyStatusIdentifier = (TextView) view.findViewById(R.id.tv_property_status_identifier);
            //change acc to need
            tvHighestBidTitle = (TextView) view.findViewById(R.id.tv_reserve_price_title);
            tvHighestBidAmount = (TextView) view.findViewById(R.id.tv_reserve_price);
            tvHighestBidder = (TextView) view.findViewById(R.id.tv_property_no);
            tvHighestBidderTitle = (TextView) view.findViewById(R.id.tv_property_no_title);
        }
    }

    public static class CurrentBisDescHolder extends RecyclerView.ViewHolder {
        protected View rootView;
        protected LinearLayout llVendorBidHolder;
        protected EditText etOnSiteBidder;

        public CurrentBisDescHolder(View view) {
            super(view);
            rootView = view;
            llVendorBidHolder = (LinearLayout) view.findViewById(R.id.ll_vendor_bid_holder);
            etOnSiteBidder = (EditText) view.findViewById(R.id.et_onsite_bid_amount);
        }
    }

    public static class CurrentBidsItemHolder extends RecyclerView.ViewHolder {
        protected TextView tvAmount, tvTimeEllapsed, tvBidder;
        protected LinearLayout messageContainer;
        protected View seperator;
        protected View rootView;

        public CurrentBidsItemHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tvAmount = (TextView) itemView.findViewById(R.id.tv_current_bid_amt);
            tvTimeEllapsed = (TextView) itemView.findViewById(R.id.tv_ellasped_time);
            tvBidder = (TextView) itemView.findViewById(R.id.tv_bidder);
            messageContainer = (LinearLayout) itemView.findViewById(R.id.ll_message_container);
            seperator = itemView.findViewById(R.id.sep);
        }
    }


    public static class BidWonViewHolder extends RecyclerView.ViewHolder {
        protected TextView tvWonText;
        protected View rootView;

        public BidWonViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tvWonText = (TextView) itemView.findViewById(R.id.tv_bid_won);// change text to start bid
        }
    }

    public void addMessage(CurrentBidModel messages, PropertyAuctionModel property, boolean isWon) {
        this.isWon = isWon;
        this.auctionList.add(0, messages);
        this.property = property;
        animateView = true;
        notifyDataSetChanged();
    }

    public void refresh() {
        notifyDataSetChanged();

    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position == 0) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
        }


    }

    private String getCallReminderMessage(int count, String price) {
        return getCountMessage(count) + " Call for " + price;
    }

    private String getCountMessage(int n) {
        if (n == 1)
            return "1st";
        if (n == 2)
            return "2nd";
        return "Last";

    }



    public interface PlaceOnSiteBidListener {
        void onPlaceOnSiteBid();
        void onCancelOnSiteBid();
    }

    public boolean isDuplicate(String id) {
        for (CurrentBidModel cbm : auctionList) {
            if (cbm.msgId.equalsIgnoreCase(id))
                return true;
        }
        return false;
    }
}

package com.official.livebid.activities.discover;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.utils.EndlessRecyclerViewScrollListener;
import com.official.livebid.utils.GPSTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Discover extends MenuActivity implements AsyncInterface {

    RecyclerView rcDiscover;
    ImageButton ibtnSearch;
    TextView tvSearchSuburb;
    RelativeLayout rlSearchBar;
    ArrayList<PropertyListInfo> alPropertyListInfo;
    //    double lat, lng;
    String lat, lng;
    GPSTracker gps;
    Alerts alerts;
    boolean isLocationSet = false;
    private boolean isLastPage = false;
    private int offset = 0;
    private DiscoverAdapter discoverAdapter;
    private LinearLayoutManager linearLayoutManager;
    private PropertyListInfo propertyListInfo;
    private EndlessRecyclerViewScrollListener endlessScrollListener;
    private SharedPreference pref;
    private String searched_suburb = "";

    @Override
    public int getLayoutId() {
        return R.layout.activity_discover;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getSupportActionBar().hide();
//        setContentView(R.layout.activity_discover);
        init();
//        MenuActivity.tabbar.setVisibility(View.VISIBLE);
    }

    private void getProperty(boolean hidePd) {
        if (isLocationSet) {
            HashMap<String, String> params = new HashMap<>();
            params.put("lat", lat + "");
            params.put("lng", lng + "");
            params.put("offset", offset + "");
            params.put("is_trending", "0");
            params.put("discover_home", "1");
            params.put("this_week_auction", "0");
            params.put("this_week_open_house", "0");
            params.put("post_code_ids", pref.getStringValues("searchPostCodeIds"));
            System.out.println("discover params " + params);
            HashMap headers = CommonMethods.getHeaders(this);
            VolleyRequest volleyRequest = new VolleyRequest(Discover.this, Request.Method.POST, params, UrlHelper.DISCOVER_PROPERTY_LIST, headers, false, CommonDef.REQUEST_DISCOVER_PROPERTYLIST);
            volleyRequest.asyncInterface = this;
            volleyRequest.request(hidePd);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gps.stopUsingGPS();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gps.stopUsingGPS();
    }

    private void initFakePropertyList() {
        alPropertyListInfo = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            PropertyListInfo propertyListInfo = new PropertyListInfo();
            propertyListInfo.agentAgencyLogoUrl = "";
            propertyListInfo.agentFName = "Rabin";
            propertyListInfo.agentLName = "Shrestha";
//            propertyListInfo.statusTag = "New";
            propertyListInfo.dateTag = "Auction start due date 40 days.";
            propertyListInfo.setFav("1");
            propertyListInfo.nosOfBathroom = "4";
            propertyListInfo.nosOfBedroom = "4";
            propertyListInfo.nosOfParking = "2";
            propertyListInfo.price = "$142,345,678";
            propertyListInfo.propertyStatusIdentifier = "from";
            propertyListInfo.suburb = "Kingsford";
            propertyListInfo.propertyState = "North Sydney";
            propertyListInfo.propertyImgUrl = "";
            propertyListInfo.distance = "";
            alPropertyListInfo.add(propertyListInfo);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        isLastPage = false;
//        isLocationSet = false;
//        offset = 0;
//        discoverAdapter = null;
//        init();
//        getProperty(true);
//        MenuActivity.tabbar.setVisibility(View.VISIBLE);
            /*  menuAdapter.refreshMenu();
        setSearchHistory();*/
//        isLocationSet = true;
//
//        setSearchHistory();
//        rcDiscover.addOnScrollListener(endlessScrollListener);
////
//        getProperty(false);
//        MenuActivity.selectedPos = 1;
//        MenuActivity.activeTab = NOT_SELECTED;
//        imgWhatsOn.setImageResource(R.drawable.ic_whats_on);
//        if (userType == AppMode.agent) {
//            imgMyBids.setImageResource(R.drawable.ic_my_listing_off_icon);
//        }
//        else {
//            imgMyBids.setImageResource(R.drawable.ic_my_bids);
//        }*/


//        endlessScrollListener.reset(0, true);
//        if (gps.canGetLocation()) {
//            lat = gps.getLatitude()+"";
//            lng = gps.getLongitude()+"";
//            System.out.println("lat=" + lat);
//            System.out.println("lng=" + lng);
//            isLocationSet = true;
//            isLastPage = false;
//            offset = 0;
//            discoverAdapter = null;
//            getProperty();
//        } else {
//            gps.showSettingsAlert();
//        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if(requestCode==1234){
//            if (grantResults[0]== PackageManager.PERMISSION_GRANTED){
//                gps.getLocation();
//            }
//            else finish();
//        }
    }

    private void init() {
        gps = new GPSTracker(Discover.this);
        alerts = new Alerts(Discover.this);
        rcDiscover = (RecyclerView) findViewById(R.id.rc_discover);
        rlSearchBar = (RelativeLayout) findViewById(R.id.rlSearchBar);
        new CustomActionBar.DiscoverCAB(this);
        linearLayoutManager = new LinearLayoutManager(this);
        rcDiscover.setLayoutManager(linearLayoutManager);
        endlessScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (!isLastPage) {

                    getProperty(true);
                }
            }

        };

        rcDiscover.addOnScrollListener(endlessScrollListener);

        ibtnSearch = (ImageButton) findViewById(R.id.ibtn_search);
        tvSearchSuburb = (TextView) findViewById(R.id.tvSearchSuburb);
        rlSearchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Opener.Search(Discover.this, CommonDef.CallingActivity.Discover_Activity);
            }
        });
        ibtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Opener.Search(Discover.this, CommonDef.CallingActivity.Discover_Activity);
            }
        });
        setSearchHistory();
        if (!pref.getStringValues(CommonDef.SharedPrefKeys.LAT).isEmpty() && !pref.getStringValues(CommonDef.SharedPrefKeys.LNG).isEmpty()) {
            lat = pref.getStringValues(CommonDef.SharedPrefKeys.LAT);
            lng = pref.getStringValues(CommonDef.SharedPrefKeys.LNG);
            isLocationSet = true;

        } else if (gps.canGetLocation()) {
            lat = gps.getLatitude() + "";
            lng = gps.getLongitude() + "";
            System.out.println("lat=" + lat);
            System.out.println("lng=" + lng);
            isLocationSet = true;

        } else {
            gps.showSettingsAlert();
        }
        setSearchHistory();
        getProperty(false);

    }

    private void setSearchHistory() {
        pref = new SharedPreference(this);
        searched_suburb = pref.getStringValues(CommonDef.SharedPrefKeys.Search_SuburbNames);
        System.out.print("Searched suburb" + searched_suburb);
        List<String> list = new ArrayList<String>(Arrays.asList(searched_suburb.split(",")));
        tvSearchSuburb.setText(getSearchedNames(list));
    }

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (mDrawerlayout.isDrawerOpen(GravityCompat.END)) {
            mDrawerlayout.closeDrawers();
        } else if (!prefs.getBoolValues(CommonDef.SharedPrefKeys.IS_LOGGED_IN)) {
            super.onBackPressed();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }


    private void initPropertyList(String result) {

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            String[] images = new String[2];
            images[0] = joResult.getString("discover_main_image");
            images[1] = joResult.getString("discover_trending_image");
            if (code.equals("0001")) {
                JSONArray jaList = joResult.getJSONArray("list");
                if (jaList.length() > 0) {
                    alPropertyListInfo = new ArrayList<>();
                    for (int i = 0; i < jaList.length(); i++) {
                        JSONObject joList = jaList.getJSONObject(i);
                        propertyListInfo = new PropertyListInfo();
                        propertyListInfo.id = joList.getString("id");
                        propertyListInfo.agentId = joList.getString("agent_id");
                        propertyListInfo.agencyId = joList.getString("agency_id");
                        propertyListInfo.auctionDate = joList.getString("auction_date");
                        propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
                        HashMap<String, String> agentName = CommonMethods.getName(joList.getString("agent_name").split(" "));
                        propertyListInfo.agentFName = agentName.get("fName");
                        propertyListInfo.agentLName = agentName.get("lName");
                        propertyListInfo.propertyStatus = joList.getInt("property_status");
                        propertyListInfo.dateTag = "Auction due to 40 days";
                        propertyListInfo.setFav(joList.getString("favorite"));
                        propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
                        propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
                        propertyListInfo.nosOfParking = joList.getString("parking");
                        propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((long) Double.parseDouble(joList.getString("price")));
                        propertyListInfo.price_text = joList.getString("price_text");
                        propertyListInfo.propertyStatusIdentifier = "from";
                        propertyListInfo.price_text = joList.getString("price_text");
                        propertyListInfo.suburb = joList.getString("suburb");
                        propertyListInfo.postCode = joList.getString("postcode");
                        propertyListInfo.propertyTitle = joList.getString("property_title");
                        propertyListInfo.propertyDesc = joList.getString("property_description");
                        propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
                        propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
                        propertyListInfo.street = joList.getString("street");
                        propertyListInfo.propertyState = joList.getString("state");
                        propertyListInfo.propertyImgUrl = joList.getString("property_image");
                        propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
                        propertyListInfo.distance = joList.getString("distance");
                        propertyListInfo.lat = joList.getString("lat");
                        propertyListInfo.lng = joList.getString("lng");
                        propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
                        propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
                        propertyListInfo.auctionDateTimeStamp = joList.getLong("auction_date_timestamp");
                        propertyListInfo.propertyStatus2 = joList.getInt("property_status_2");

                        propertyListInfo.property_images_more = new ArrayList<>();
//                        propertyListInfo.property_images_more.add(joList.getString("property_image"));

                        JSONArray JPropertyImageMore = joList.getJSONArray("property_images_more");
                        if (JPropertyImageMore.length() > 0) {
                            for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                   /* moreImage = new PropertyInfo.Property_images_more(
                                            JPropertyImageMore.getJSONObject(j).getString("image_url")
                                    );*/

                                propertyListInfo.property_images_more.add(JPropertyImageMore.getString(j));
                            }
                        }

                        alPropertyListInfo.add(propertyListInfo);
                    }
//                    rcDiscover.setAdapter(new DiscoverAdapter(this, alPropertyListInfo,images));
                    isLastPage = joResult.getBoolean("last_page");
                    if (discoverAdapter == null) {
                        discoverAdapter = new DiscoverAdapter(this, alPropertyListInfo);
                        rcDiscover.setAdapter(discoverAdapter);
                    } else {
                        discoverAdapter.addMoreData(alPropertyListInfo);

                    }
                    offset = joResult.getInt("offset");
                    if (joResult.has("auction_register_status"))
                        pref.setKeyValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS, joResult.getString("auction_register_status"));
                } else {
                    discoverAdapter = new DiscoverAdapter(this, new ArrayList<PropertyListInfo>());
                    rcDiscover.setAdapter(discoverAdapter);
                    findViewById(R.id.no_property).setVisibility(View.VISIBLE);
                }
            } else {
                alerts.showOkMessage(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_DISCOVER_PROPERTYLIST:
                System.out.println("Result " + result);
                initPropertyList(result);

                break;
        }
    }

    private String getSearchedNames(List<String> list) {
        int no_of_search = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                sb.append(list.get(i));
            } else {
                no_of_search++;
            }
        }
        if (list.size() == 1) {
            return sb.toString();
        } else {
            sb.append(" and " + no_of_search + " more");
            return sb.toString();
        }
    }

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }


}

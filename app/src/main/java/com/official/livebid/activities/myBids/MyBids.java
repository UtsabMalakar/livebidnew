package com.official.livebid.activities.myBids;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import com.official.livebid.R;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.activities.liveAuction.MyBidsAdapter;

import java.util.ArrayList;

public class MyBids extends MenuActivity {
    private RecyclerView rcMyBids;
    private LinearLayoutManager linearLayoutManager;
    private MyBidsAdapter myBidsAdapter;
    private ArrayList<String> myBids;
    @Override
    public int getLayoutId() {
        return R.layout.activity_my_bids;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_my_bids);
        getSupportActionBar().hide();
        rcMyBids= (RecyclerView) findViewById(R.id.rc_my_bids);
        linearLayoutManager=new LinearLayoutManager(this);
        rcMyBids.setLayoutManager(linearLayoutManager);
        myBids=new ArrayList<>();
        myBids.add("shiv");
        myBids.add("shiv");
        myBidsAdapter=new MyBidsAdapter(this,myBids);
        rcMyBids.setAdapter(myBidsAdapter);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_bids, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}

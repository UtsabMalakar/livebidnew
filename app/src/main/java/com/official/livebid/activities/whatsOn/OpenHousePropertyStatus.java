package com.official.livebid.activities.whatsOn;

/**
 * Created by Ics on 5/26/2016.
 */
public enum OpenHousePropertyStatus {
    OPEN_HOUSE(1),
    SCHEDULED(2);
    private int OHPStatus;
    OpenHousePropertyStatus(int i) {
        this.OHPStatus=i;
    }
    public int getOHPStatus(){
        return OHPStatus;
    }
}

package com.official.livebid.activities;

import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TimingLogger;
import android.widget.ProgressBar;

import com.official.livebid.R;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.FileDownloader;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;


public class SplashScreen extends AppCompatActivity {

    private int _showTime = 3000;
    private boolean _isActive = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
//        setContentView(R.layout.activity_splash_screen);
        StartLooping();
    }
    void StartLooping() {

//        printKeyHash(this);
        Thread showThread = new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                try {
                    int waitTime = 0;
                    while (waitTime < _showTime) {
                        sleep(100);
                        waitTime += 100;

                        if (waitTime >= _showTime && _isActive) {
                            Opener.LandingScreen(SplashScreen.this);
                        }
                    }
                } catch (InterruptedException e) {
                }
                Looper.loop();
            }
        };
        showThread.start();
    }
}

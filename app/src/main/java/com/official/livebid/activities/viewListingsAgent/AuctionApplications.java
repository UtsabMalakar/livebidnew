package com.official.livebid.activities.viewListingsAgent;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.AuctionApplicationModel;
import com.official.livebid.objects.PropertyListInfo;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AuctionApplications extends AppCompatActivity implements AsyncInterface {
    private RecyclerView rvAuctionApplication;

    private ImageView ivPropertyImg;
    private TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
            tvPropertyStatusIdentifier, tvReservePrice, tvApplicationReceived;
    private Alerts alerts;
    LinearLayoutManager linearLayoutManager;
    private PropertyListInfo propertyListInfo;
    private AuctionApplicationAdapter auctionApplicationAdapter;
    private boolean isToolbarShown=false;
    private LinearLayout cabContainer;
    private ImageView ibtnBack;
    private TextView tvCenter;
    private ImageView ibtnRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_auction_applications);
        init();
//        showPropertyInfo();
        getAuctionApplications();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAuctionApplications();
    }

    private void getAuctionApplications() {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("Auction Applications params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(AuctionApplications.this, Request.Method.POST, params, UrlHelper.GET_PRE_AUCTION_APPLICATIONS, headers, false, CommonDef.REQ_PRE_AUCTION_APPLICATIONS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void init() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            propertyListInfo = (PropertyListInfo) bundle.getSerializable(CommonDef.PROPERTY_LIST_INFO);
        }
        alerts = new Alerts(this);
        new CustomActionBar.PreAuctionOfferAgentCAB(this,propertyListInfo.propertyCode);
        //property details
//        ivPropertyImg = (ImageView) findViewById(R.id.iv_property_img);
//        tvStatusTag = (TextView) findViewById(R.id.tv_property_status_tag);
//        tvDateTag = (TextView) findViewById(R.id.tv_property_date_tag);
//        tvPropertySuburbPostcode = (TextView) findViewById(R.id.tv_property_suburb_postcode);
//        tvPropertyStreet = (TextView) findViewById(R.id.tv_property_street);
//        tvPropertyStatusIdentifier = (TextView) findViewById(R.id.tv_property_status_identifier);
//        tvReservePrice = (TextView) findViewById(R.id.tv_reserve_price);
//        tvApplicationReceived = (TextView) findViewById(R.id.tv_no_of_applications);
        // auction Applications
        cabContainer = (LinearLayout) findViewById(R.id.cab);
        ibtnBack = (ImageView) findViewById(R.id.ibtn_left);
        ibtnBack.setImageResource(R.drawable.back_icon);
        tvCenter = (TextView) findViewById(R.id.tv_center);
        tvCenter.setVisibility(View.INVISIBLE);

        tvCenter.setTextColor(Color.WHITE);
        ibtnRight = (ImageView) findViewById(R.id.ibtn_right);
        cabContainer.setBackgroundColor(Color.TRANSPARENT);
        rvAuctionApplication = (RecyclerView) findViewById(R.id.rv_auction_application);
        linearLayoutManager = new LinearLayoutManager(this);
        rvAuctionApplication.setLayoutManager(linearLayoutManager);
        rvAuctionApplication.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.computeVerticalScrollOffset() > 100) {
                    showToolbar();
                    isToolbarShown = true;
                } else {
                    hideToolbar();
                    isToolbarShown = false;
                }

            }
        });

    }
    private void showPropertyInfo() {

        tvReservePrice.setText(propertyListInfo.price);
        if (!propertyListInfo.statusTag.isEmpty()) {
            tvStatusTag.setVisibility(View.VISIBLE);
            tvStatusTag.setText(propertyListInfo.statusTag);
        } else {
            tvStatusTag.setVisibility(View.GONE);
        }
        tvPropertyStreet.setText(propertyListInfo.street);
        tvPropertySuburbPostcode.setText(propertyListInfo.suburb + ", " + propertyListInfo.postCode);
        if (!propertyListInfo.propertyImgUrl.isEmpty()) {
            Picasso.with(this).load(propertyListInfo.propertyImgUrl).into(ivPropertyImg);
        } else {
            Picasso.with(this).load(R.drawable.img_coming_soon).into(ivPropertyImg);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_auction_applications, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("Auction Application Response:" + result);
        switch (requestCode) {
            case CommonDef.REQ_PRE_AUCTION_APPLICATIONS:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        JSONArray jaPreOffers = joResult.getJSONArray("list");
                        if (jaPreOffers.length() > 0) {
                            JSONObject joOffer = null;
                            ArrayList<AuctionApplicationModel> auctionApplications = new ArrayList<>();
                            AuctionApplicationModel auctionApplication = null;
                            for (int i = 0; i < jaPreOffers.length(); i++) {
                                joOffer = jaPreOffers.getJSONObject(i);
                                auctionApplication = new AuctionApplicationModel(
                                        joOffer.getString("user_id"),
                                        joOffer.getString("unique_id"),
                                        joOffer.getString("profile_image"),
                                        joOffer.getInt("application_status"),
                                        propertyListInfo.id
                                );
                                auctionApplications.add(auctionApplication);
                            }
                            auctionApplicationAdapter = new AuctionApplicationAdapter(AuctionApplications.this, auctionApplications,propertyListInfo);
                            rvAuctionApplication.setAdapter(auctionApplicationAdapter);
                        }


                    } else {
                        auctionApplicationAdapter = new AuctionApplicationAdapter(AuctionApplications.this,new  ArrayList<AuctionApplicationModel>() ,propertyListInfo);
                        rvAuctionApplication.setAdapter(auctionApplicationAdapter);
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }
    public void showToolbar() {
        if (isToolbarShown)
            return;
        ibtnBack.setImageResource(R.drawable.left_arrow);
        tvCenter.setVisibility(View.VISIBLE);
        tvCenter.setTextColor(Color.BLACK);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(cabContainer, "backgroundColor", new ArgbEvaluator(), Color.argb(0, 255, 255, 255), Color.argb(255, 255, 255, 255));//getResources().getColor(R.color.theme_yellow)
        colorFade.setDuration(1000);
        colorFade.start();
    }


    public void hideToolbar() {
        if (!isToolbarShown)
            return;
        ibtnBack.setImageResource(R.drawable.back_icon);
        tvCenter.setVisibility(View.INVISIBLE);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(cabContainer, "backgroundColor", new ArgbEvaluator(), Color.argb(255, 255, 255, 255), Color.argb(0, 255, 255, 255));
        colorFade.setDuration(1000);
        colorFade.start();
    }
}

package com.official.livebid.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.activities.newRegistration.RegType;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.staticClasses.RegisterUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class MobileVerification extends AppCompatActivity implements View.OnClickListener, AsyncInterface {
    private Button btnVerifyCode;
    private Button btnResendSms;
    CustomEditText ctMobile,ctVerificationCode;
    private String pinCode = "";
    private Alerts alerts;
    TextView mobileVerificationTitle;
    TextView mobileVerificationDesc;
    ImageButton ibtnBack;

    private CustomActionBar.MobileVerificationTitleBar mobileVerificationTitleBar;
    private SharedPreference prefs;
    private static int regAttemp;
    private static int resetPwdAttemp;
    private RegType regType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);
        getSupportActionBar().hide();
        init();

    }

    private void init() {
        Bundle b=getIntent().getExtras();
        if(b!=null)
            regType=(RegType)b.getSerializable(CommonDef.REG_TYPE);
        alerts = new Alerts(this);
        prefs = new SharedPreference(this);
        ibtnBack= (ImageButton) findViewById(R.id.ibtn_left);
        ibtnBack.setOnClickListener(this);
        ctMobile= (CustomEditText) findViewById(R.id.cv_mobile_no);
        ctMobile.setText(RegisterUser.mobileNumber);
        ctMobile.setFocusable(false);
        ctVerificationCode= (CustomEditText) findViewById(R.id.cv_v_code);
        ctVerificationCode.enableEditMode(true);
        ctVerificationCode.addTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                enableVerification();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        regAttemp = prefs.getIntValues(CommonDef.SharedPrefKeys.REG_ATTEMP);
        resetPwdAttemp = prefs.getIntValues(CommonDef.SharedPrefKeys.RESET_PWD_ATTEMP);
//        mobileVerificationTitleBar = new CustomActionBar.MobileVerificationTitleBar(this);
//        mobileVerificationTitle.setText(com.official.livebid.staticClasses.MobileVerification.mode == 1 ? "Mobile Verification" : "Confirm your Actions");
        btnVerifyCode = (Button) findViewById(R.id.btn_verify_code);
        btnResendSms = (Button) findViewById(R.id.btn_resend_sms);
        btnVerifyCode.setOnClickListener(this);
        btnResendSms.setOnClickListener(this);
        findViewById(R.id.ibtn_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mobile_verification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_verify_code:
                verifyCode();
                break;
            case R.id.btn_resend_sms:
                retryVerification();
                resendCode();
                break;
            case  R.id.ibtn_left:
                onBackPressed();
                break;

        }

    }


    private void enableVerification() {
        if (ctVerificationCode.getText().toString().length() >= 4) {
            btnVerifyCode.setEnabled(true);
        } else {
            btnVerifyCode.setEnabled(false);
        }
    }

    private void verifyCode() {

        HashMap<String, String> headers = new HashMap<>();
        headers.put("user_id", prefs.getStringValues(CommonDef.SharedPrefKeys.USER_ID));
        headers.put("hash_code", prefs.getStringValues(CommonDef.SharedPrefKeys.HASH_CODE));
        headers.put("device_id", prefs.getStringValues(CommonDef.SharedPrefKeys.DEVICE_ID));
        headers.put("device_type", "2");
        headers.put("user_type", regType.getRegType()+"");
        HashMap<String, String> params = new HashMap<>();
        params.put("mobile_number", RegisterUser.mobileNumber);
        params.put("code", getPincode());
        System.out.println("mobileVerification:" + params.toString());

        VolleyRequest volleyRequest = new VolleyRequest(MobileVerification.this, Request.Method.POST, params,
                getMobileVerificationUrl(com.official.livebid.staticClasses.MobileVerification.mode),
                headers,
                false,
                getMobileVerificationRequestCode(com.official.livebid.staticClasses.MobileVerification.mode));
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {

        switch (requestCode) {
            case CommonDef.REQUEST_ACTIVATE_USER:
                System.out.println("Mobile Verification =>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0027")) {
                        this.finish();
//                        Toast.makeText(MobileVerification.this, "User Activated successfully.", Toast.LENGTH_SHORT).show();
//                        Opener.SignIn(MobileVerification.this);
                        Opener.newRegister(MobileVerification.this, UserType.EXISTING);
                    } else if (code.equals("0028")) {
                        alerts.alertInvalidPin(msg);
//                        if(regAttemp <3) {
//                            regAttemp++;
//                            prefs.setKeyValues(CommonDef.SharedPrefKeys.REG_ATTEMP,regAttemp);
//                        }else {
//                            blockMobileVerification();
//                        }


                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_NEW_CODE:
            case CommonDef.REQUEST_NEW_CODE_RESET_PWD:
            case CommonDef.REQUEST_NEW_CODE_MOBILE_CHANGE:
                System.out.println("resend Code =>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0030")) {
                        alerts.showOkMessage(msg);
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_FORGOT_PASSWORD_VERIFICATION:
                System.out.println("Mobile Verification =>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        Toast.makeText(MobileVerification.this, "reset password successfully.", Toast.LENGTH_SHORT).show();
                        Opener.ResetPassword(MobileVerification.this,regType);
                    } else if (code.equals("0028")) {
                        alerts.alertInvalidPin(msg);
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_VERIFY_MOBILE:
                System.out.println("Mobile Verification =>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0038")) {
//                        Toast.makeText(MobileVerification.this, "Mobile no changed successfully.", Toast.LENGTH_SHORT).show();
                        this.finish();
                        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);

                    } else if (code.equals("0028")) {
                        alerts.alertInvalidPin(msg);
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
        }
    }

    private String getPincode() {
        pinCode = ctVerificationCode.getText().toString();
        return pinCode;

    }

    public void retryVerification() {
        ctVerificationCode.setText("");
        enableVerification();

    }

    public void cancelVerification() {
        finish();
    }

    private void resendCode() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("device_type", "2");
        headers.put("user_type", regType.getRegType()+"");

        HashMap<String, String> params = new HashMap<>();
        params.put("mobile_number", RegisterUser.mobileNumber);
        System.out.println("mobileVerification:" + params.toString());

        VolleyRequest volleyRequest = new VolleyRequest(
                MobileVerification.this,
                Request.Method.POST, params,
                getMobileVerificationResend(com.official.livebid.staticClasses.MobileVerification.mode),
                headers,
                false,
                getMobileVerificationRequestCodeResend(com.official.livebid.staticClasses.MobileVerification.mode));
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private String getMobileVerificationUrl(int mode) {
        String url = "";
        switch (mode) {
            case 1: //verify account
                url = UrlHelper.VERIFY_USER;
                break;
            case 2: //verify reset password password
                url = UrlHelper.FORGET_PASSWORD_VERIFICATION;
                break;
            case 3: //verify mobile number
                url = UrlHelper.VERIFY_MOBILE;
                break;
        }
        return url;
    }

    private String getMobileVerificationResend(int mode) {
        String url = "";
        switch (mode) {
            case 1: //verify account
                url = UrlHelper.RESEND_CODE;
                break;
            case 2: //verify reset password password
                url = UrlHelper.RESEND_CODE_RESET_PWD;
                break;
            case 3: //verify mobile number
                url = UrlHelper.RESEND_CODE_MOBILE_CHANGE;
                break;
        }
        return url;
    }

    private int getMobileVerificationRequestCode(int mode) {
        int rc = -1;
        switch (mode) {
            case 1: //verify account
                rc = CommonDef.REQUEST_ACTIVATE_USER;
                break;
            case 2: //verify reset password password
                rc = CommonDef.REQUEST_FORGOT_PASSWORD_VERIFICATION;
                break;
            case 3: //verify mobile number
                rc = CommonDef.REQUEST_VERIFY_MOBILE;
                break;
        }
        return rc;
    }

    private int getMobileVerificationRequestCodeResend(int mode) {
        int rc = -1;
        switch (mode) {
            case 1: //verify account
                rc = CommonDef.REQUEST_NEW_CODE;
                break;
            case 2: //verify reset password password
                rc = CommonDef.REQUEST_NEW_CODE_RESET_PWD;
                break;
            case 3: //verify mobile number
                rc = CommonDef.REQUEST_NEW_CODE_MOBILE_CHANGE;
                break;
        }
        return rc;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing,R.anim.slide_out_right
        );
    }
}


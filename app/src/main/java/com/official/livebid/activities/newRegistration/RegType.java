package com.official.livebid.activities.newRegistration;

import java.io.Serializable;

/**
 * Created by Ics on 5/16/2016.
 */
public enum RegType implements Serializable{
    USER(2),
    AGENT(3);
    private int regType;

    RegType(int regType) {
        this.regType = regType;
    }
    public int getRegType(){
        return regType;
    }
}

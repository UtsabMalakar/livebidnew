package com.official.livebid.activities.myListings;

import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.activities.watchList.FragPropertyListView;
import com.official.livebid.activities.watchList.FragPropertyMapView;
import com.official.livebid.adapters.MyListingAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.utils.EndlessRecyclerViewScrollListener;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.utils.GPSTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MyListings extends MenuActivity  {
    RecyclerView rvMyListing;
    ImageButton ibtnSearch;
    ArrayList<PropertyListInfo> alPropertyListInfo;
    double lat, lng;
    boolean isLocationSet = false;
    GPSTracker gps;
    Alerts alerts;
    private LinearLayoutManager linearLayoutManager;
    private MyListingAdapter myListingAdapter;
    private boolean isLastPage = false;
    private int offset = 0;
    private int curSize;

    @Override
    public int getLayoutId() {
        return R.layout.activity_my_listings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        init();
        getFragmentManager()
                .beginTransaction()
                .add(R.id.my_listing_container, new FragMyListingsListView())
                .addToBackStack(null)
                .commit();
    }



    @Override
    protected void onPause() {
        super.onPause();
        gps.stopUsingGPS();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gps.stopUsingGPS();
    }

    private void initFakePropertyList() {
        alPropertyListInfo = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            PropertyListInfo propertyListInfo = new PropertyListInfo();
            propertyListInfo.agentAgencyLogoUrl = "";
            propertyListInfo.agentFName = "Rabin";
            propertyListInfo.agentLName = "Shrestha";
            propertyListInfo.statusTag = "New";
            propertyListInfo.dateTag = "Auction start due date 40 days.";
            propertyListInfo.setFav("1");
            propertyListInfo.nosOfBathroom = "4";
            propertyListInfo.nosOfBedroom = "4";
            propertyListInfo.nosOfParking = "2";
            propertyListInfo.price = "$142,345,678";
            propertyListInfo.propertyStatusIdentifier = "from";
            propertyListInfo.suburb = "Kingsford";
            propertyListInfo.propertyState = "North Sydney";
            propertyListInfo.propertyImgUrl = "";
            propertyListInfo.distance = "";
            alPropertyListInfo.add(propertyListInfo);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        menuAdapter.refreshMenu();
        imgMyBids = (ImageButton) findViewById(R.id.imgLeftMenu);
        imgMyBids.setImageResource(R.drawable.ic_my_listing_on_icon);
//        getPropertyList();
//        if (gps.canGetLocation()) {
//            lat = gps.getLatitude();
//            lng = gps.getLongitude();
//            isLocationSet = true;
//            getPropertyList();
//        } else {
//            gps.showSettingsAlert();
//        }
    }

    private void init() {
        gps = new GPSTracker(MyListings.this);
        alerts = new Alerts(MyListings.this);
        new CustomActionBar.MyListing(this);

    }

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void openMapView(){
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.fragment_slide_left_enter,
                        R.animator.fragment_slide_left_exit,
                        R.animator.fragment_slide_right_enter,
                        R.animator.fragment_slide_right_exit)
                .replace(R.id.my_listing_container, new FragMyListingsMapView())
                .addToBackStack(null)
                .commit();

    }
    public void openListView(){
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.fragment_slide_left_enter,
                        R.animator.fragment_slide_left_exit,
                        R.animator.fragment_slide_right_enter,
                        R.animator.fragment_slide_right_exit)
                .replace(R.id.my_listing_container, new FragMyListingsListView())
                .addToBackStack(null)
                .commit();
    }


}

package com.official.livebid.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.activities.newRegistration.RegType;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.staticClasses.RegisterUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class ResetPassword extends ActionBarActivity implements View.OnClickListener,AsyncInterface{
    CustomEditText cvNewPassword;
    EditText etNewPassword;
    CustomEditText cvConfirmPassword;
    EditText etConfirmPassword;
    Button btnConfirm;
    private String newPassword;
    private String confirmPassword;
    private Alerts alerts;
    RelativeLayout mainView;
    private CustomActionBar.MobileVerificationTitleBar resetPasswordTitleBar;
    private RegType regType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        getSupportActionBar().hide();
        init();
    }

    private void init() {
        Bundle b=getIntent().getExtras();
        if(b!=null)
            regType=(RegType)b.getSerializable(CommonDef.REG_TYPE);
        alerts=new Alerts(this);
        resetPasswordTitleBar=new CustomActionBar.MobileVerificationTitleBar(this);
        mainView= (RelativeLayout) findViewById(R.id.rl_main_view);
        CommonMethods.setupUI(mainView,this);
        cvNewPassword= (CustomEditText) findViewById(R.id.cv_new_password);
        etNewPassword= (EditText) cvNewPassword.findViewById(R.id.textEdit);
        cvConfirmPassword= (CustomEditText) findViewById(R.id.cv_confirm_password);
        etConfirmPassword= (EditText) cvConfirmPassword.findViewById(R.id.textEdit);
        btnConfirm= (Button) findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(this);
        etConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                enableConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                enableConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reset_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_confirm:
//                Toast.makeText(ResetPassword.this,"password reseted",Toast.LENGTH_SHORT).show();
                resetPassword();
                break;
        }
    }
    private void enableConfirm(){
        getPasswords();
        if (!newPassword.isEmpty() && newPassword.length()>=8 && !confirmPassword.isEmpty() && newPassword.equals(confirmPassword)) {
            btnConfirm.setBackground(getResources().getDrawable(R.drawable.rounded_button));
            btnConfirm.setEnabled(true);
        } else {
            btnConfirm.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
            btnConfirm.setEnabled(false);
        }
    }
   private void getPasswords(){
       newPassword=cvNewPassword.getText().toString();
       confirmPassword=cvConfirmPassword.getText().toString();
   }
    private void resetPassword(){
        HashMap<String, String> headers = new HashMap<>();
        headers.put("device_type", "2");
        headers.put("user_type", regType.getRegType()+"");
        HashMap<String, String> params = new HashMap<>();
        params.put("new",newPassword);
        params.put("mobile_number", RegisterUser.mobileNumber);
        System.out.println("Reset Password:" + params.toString());

        VolleyRequest volleyRequest = new VolleyRequest(ResetPassword.this, Request.Method.POST, params, UrlHelper.RESET_PASSWORD,headers, false, CommonDef.REQUEST_RESET_PASSWORD);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_RESET_PASSWORD:
                System.out.println("Reset password=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0033")) {
                           alerts.successResetPassword("",msg);
                    }
                    else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
        }

    }

  public void okResetPassword(){
      this.finish();
      Opener.newRegister(ResetPassword.this, UserType.EXISTING);
  }
}

package com.official.livebid.activities.profile;


import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.Views.TimePickerFragment;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.staticClasses.AgentDetailsModel;
import com.official.livebid.utils.RangeSeekBar;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgentDetails extends Fragment implements AsyncInterface, OnMapReadyCallback {
    private static final String TAG = "gmap";
    private FragmentActivity myContext;
    TextView tvAvailabilityRange;
    final static int minRange = 1;
    final static int maxRange = 7;
    public EditText etFrom;
    public EditText etTo;
    ImageView ivProfileImg;
    CustomEditText cvFirstName, cvLastName, cvMobileNumber, cvEmailAddress;
    CustomEditText cvAgentName, cvAgentAddress;
    RelativeLayout mainView, confirmDialog;
    Button btnSave;
    Button btnConfirmOk;
    private Alerts alerts;
    private Activity activity;
    private String description, timeFrom, timeTo;
    private EditText etDescription;
    private RangeSeekBar<Integer> seekBar;
    static double lat, lng;
    LinearLayout llAgencyDetails;

    CustomEditText cvAgencyName, cvAgencyAddress;

    String fromDay;
    String toDay;
    String toTime;
    String fromTime;
    String desc;


    public AgentDetails() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        this.activity = activity;
        super.onAttach(activity);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_agent_details, container, false);
        alerts = new Alerts(getActivity());
        ivProfileImg = (ImageView) view.findViewById(R.id.iv_profile_img);
        CommonMethods.setImageviewRatio(getActivity(), ivProfileImg, (int) CommonMethods.pxFromDp(getActivity(), 26));
        cvFirstName = (CustomEditText) view.findViewById(R.id.cv_first_name);
        cvFirstName.setFocusable(false);
        cvFirstName.enableEditMode(false);
        cvLastName = (CustomEditText) view.findViewById(R.id.cv_last_name);
        cvLastName.setFocusable(false);
        cvLastName.enableEditMode(false);
        cvMobileNumber = (CustomEditText) view.findViewById(R.id.cv_mobile_no);
        cvMobileNumber.setFocusable(false);
        cvMobileNumber.enableEditMode(false);
        cvEmailAddress = (CustomEditText) view.findViewById(R.id.cv_email);
        cvEmailAddress.setFocusable(false);
        cvEmailAddress.enableEditMode(false);
        cvAgencyName = (CustomEditText) view.findViewById(R.id.cv_agency_name);
        cvAgencyName.setFocusable(false);
        cvAgencyName.enableEditMode(false);
        cvAgencyAddress = (CustomEditText) view.findViewById(R.id.cv_agency_address);
        cvAgencyAddress.setFocusable(false);
        cvAgencyAddress.enableEditMode(false);
        llAgencyDetails = (LinearLayout) view.findViewById(R.id.ll_agency_details);

        ViewGroup layout = (ViewGroup) view.findViewById(R.id.ll_availability_rangeBar);

        tvAvailabilityRange = (TextView) view.findViewById(R.id.tv_availability_range);
        seekBar = new RangeSeekBar<>(minRange, maxRange, getActivity());
        seekBar.setSelectedMaxValue(maxRange);
        seekBar.setSelectedMinValue(minRange);
        seekBar.setNotifyWhileDragging(true);
        tvAvailabilityRange.setText("Monday-Sunday");
        seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                if (minValue.intValue() == maxValue.intValue()) {
                    tvAvailabilityRange.setText(getDay(minValue));
                    seekBar.setSelectedMaxValue(maxValue);
                    seekBar.setSelectedMinValue(maxValue);
                } else {
                    tvAvailabilityRange.setText(getAvailableDay(minValue, maxValue));
                    seekBar.setSelectedMaxValue(maxValue);
                    seekBar.setSelectedMinValue(minValue);
                }
                enableUpdate();
            }
        });
        layout.addView(seekBar);
        etDescription = (EditText) view.findViewById(R.id.et_description);
        etDescription.addTextChangedListener(tw1);
        etFrom = (EditText) view.findViewById(R.id.et_time_from);
        etFrom.setFocusable(false);
        etFrom.setClickable(true);
        etTo = (EditText) view.findViewById(R.id.et_time_to);
        etTo.setFocusable(false);
        etTo.setClickable(true);
        etFrom.setOnClickListener(clickListener);
        etTo.setOnClickListener(clickListener);
        etTo.addTextChangedListener(tw);
        etFrom.addTextChangedListener(tw);
        btnSave = (Button) view.findViewById(R.id.btn_save_changes);
        btnSave.setEnabled(false);
        confirmDialog = (RelativeLayout) view.findViewById(R.id.rl_confirmation);

        btnConfirmOk = (Button) view.findViewById(R.id.btn_ok);
        btnSave.setOnClickListener(clickListener);
//        btnSave.setEnabled(true);
        btnConfirmOk.setOnClickListener(clickListener);
        CommonMethods.printKeyHash(getActivity());
        getAgentDetails();
        initialiseMap();
        return view;
    }

    private String getAvailableDay(int min, int max) {
        String temp = "";
        temp = getDay(min) + " - " + getDay(max);

        return temp;
    }

    private String getDay(int n) {
        String day = "";
        switch (n) {
            case 1:
                day = "Monday";
                break;
            case 2:
                day = "Tuesday";
                break;
            case 3:
                day = "Wednesday";
                break;
            case 4:
                day = "Thursday";
                break;
            case 5:
                day = "Friday";
                break;
            case 6:
                day = "Saturday";
                break;
            case 7:
                day = "Sunday";
                break;
        }
        return day;
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DialogFragment newFragment = null;
            switch (view.getId()) {
                case R.id.et_time_from:
                    newFragment = new TimePickerFragment().setArguments(etFrom.getId());
                    newFragment.show(myContext.getSupportFragmentManager(), "timePicker");
                    break;
                case R.id.et_time_to:
                    newFragment = new TimePickerFragment().setArguments(etTo.getId());
                    newFragment.show(myContext.getSupportFragmentManager(), "timePicker");
                    break;
                case R.id.btn_save_changes:
//                    new UpdateProfileAsyncTask().execute(UrlHelper.UPDATE_USER_PROFILE);
                    updateAgentDetails();
                    break;
                case R.id.btn_ok:
//                   //
                    confirmDialog.setVisibility(View.GONE);
                    AgentDetailsModel.desc = desc;
                    AgentDetailsModel.fromDay = fromDay;
                    AgentDetailsModel.toDay = toDay;
                    AgentDetailsModel.fromTime = fromTime;
                    AgentDetailsModel.toTime = toTime;
                    enableUpdate();
                    btnSave.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };


    public void getAgentDetails() {
        HashMap<String, String> headers = CommonMethods.getHeaders(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("device_type", "2");
        System.out.println("agent profile=>" + headers.toString());
        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.POST, params, UrlHelper.GET_AGENT_DETAILS, headers, false, CommonDef.REQUEST_AGENT_DETAILS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    public void updateAgentDetails() {
        HashMap<String, String> headers = CommonMethods.getHeaders(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("desc", etDescription.getText().toString());
        params.put("from_day", seekBar.getSelectedMinValue() + "");
        params.put("to_day", seekBar.getSelectedMaxValue() + "");
        params.put("from_time", etFrom.getText().toString());
        params.put("to_time", etTo.getText().toString());
        System.out.println("agent details=>" + headers.toString());
        System.out.println("agent details params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.POST, params, UrlHelper.UPDATE_AGENT_DETAILS, headers, false, CommonDef.REQUEST_UPDATE_AGENT_DETAILS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }


    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_AGENT_DETAILS:
                System.out.println("agent details=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        String imgUrl;
                        String dayFrom, dayTo;
                        int min, max;
                        String name;
                        JSONObject joUserInfo = joResult.getJSONObject("detail");
//                        cvFirstName.setText(joUserInfo.getString("first_name"));
                        name = joUserInfo.getString("name").trim();
                        cvFirstName.setText(name.split(" ")[0]);
                        try {
                            cvLastName.setText(name.split(" ")[1]);
                        } catch (Exception exx) {
                        }
//                        cvMobileNumber.setText(joUserInfo.getString("mobile_number"));
                        cvMobileNumber.setText(joUserInfo.getString("mobile"));
                        cvEmailAddress.setText(joUserInfo.getString("email"));
                        imgUrl = joUserInfo.getString("profile_image");
                        if (imgUrl != null && imgUrl.length() > 0) {
                            Picasso.with(getActivity())
                                    .load(imgUrl)
                                    .resize(320, 240)
                                    .error(R.drawable.profile_icon)
//                                    .placeholder( R.drawable.img_loading)
//                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .into(ivProfileImg);
                        }
                        dayFrom = AgentDetailsModel.fromDay = joUserInfo.getString("from_day");
                        dayTo = AgentDetailsModel.toDay = joUserInfo.getString("to_day");
                        min = dayFrom.isEmpty() ? 1 : Integer.parseInt(dayFrom);
                        max = dayTo.isEmpty() ? 7 : Integer.parseInt(dayTo);
                        seekBar.setSelectedMinValue(min);
                        seekBar.setSelectedMaxValue(max);
                        tvAvailabilityRange.setText(getAvailableDay(min, max));
                        etDescription.setText(AgentDetailsModel.desc = joUserInfo.getString("description"));
                        etFrom.setText(AgentDetailsModel.fromTime = joUserInfo.getString("from_time"));
                        etTo.setText(AgentDetailsModel.toTime = joUserInfo.getString("to_time"));
                        JSONObject jObjAgency = joResult.getJSONObject("agency");
                        if(jObjAgency.length()>0) {
                            cvAgencyName.setText(jObjAgency.getString("name"));
                            String agencyAddress = jObjAgency.getString("street") + ", " + jObjAgency.getString("suburb") + " " + jObjAgency.getString("postcode");
                            cvAgencyAddress.setText(agencyAddress);
                            lat = Double.parseDouble(jObjAgency.getString("lat"));
                            lng = Double.parseDouble(jObjAgency.getString("lng"));

                            initialiseMap();
                        }
                        else
                        {
                            llAgencyDetails.setVisibility(View.GONE);
                        }
                        // latitude and longitude
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_UPDATE_AGENT_DETAILS:
                System.out.println("update agent details=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001") || code.equals("0032")) {
//                        alerts.showOkMessage(msg);
                        btnSave.setVisibility(View.GONE);
                        confirmDialog.setVisibility(View.VISIBLE);
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
        }
    }

    public long compateTime(String from, String to) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa", Locale.US);
        long diff = 0;
        try {
            Date timeFrom = dateFormat.parse(from);
            Date timeTo = dateFormat.parse(to);

            diff = timeTo.getTime() - timeFrom.getTime();
            Log.e("Time", diff + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diff;
    }

    private TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (!etTo.getText().toString().isEmpty() && !etFrom.getText().toString().isEmpty())
                if (compateTime(etFrom.getText().toString().trim(), etTo.getText().toString().trim()) > 0) {

                } else if (compateTime(etFrom.getText().toString().trim(), etTo.getText().toString().trim()) < 0) {
                    alerts.showOkMessage("Time To is less than Time from.");
                    etTo.setText("");
                } else {
                    alerts.showOkMessage("Time To is equal to Time from.");
                    etTo.setText("");
                }
            enableUpdate();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        initialiseMap();
    }

    private void initialiseMap() {
//        MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
        MapFragment mapFragment = getMapFragment();
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng myPlace = new LatLng(lat, lng);
        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    CommonDef.REQUEST_LOCATION);
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 15));
        googleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon))
                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                .position(myPlace));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Log.d(TAG, "using getFragmentManager");
            FragmentManager fm = getActivity().getFragmentManager();
            Fragment fragment = (fm.findFragmentById(R.id.map));
            FragmentTransaction ft = fm.beginTransaction();
            ft.remove(fragment);
            ft.commit();
        }
        AgentDetailsModel.clearAgentDetails();

    }

    private MapFragment getMapFragment() {
        FragmentManager fm = null;

        Log.d(TAG, "sdk: " + Build.VERSION.SDK_INT);
        Log.d(TAG, "release: " + Build.VERSION.RELEASE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Log.d(TAG, "using getFragmentManager");
            fm = getFragmentManager();
        } else {
            Log.d(TAG, "using getChildFragmentManager");
            fm = getChildFragmentManager();
        }

        return (MapFragment) fm.findFragmentById(R.id.map);
    }

    private void getData() {
        fromDay = seekBar.getSelectedMinValue() + "";
        toDay = seekBar.getSelectedMaxValue() + "";
        toTime = etTo.getText().toString();
        fromTime = etFrom.getText().toString();
        desc = etDescription.getText().toString();
    }

    public boolean isUpdatable() {
        getData();
        return !desc.isEmpty() && !desc.equalsIgnoreCase(AgentDetailsModel.desc)
                || !fromDay.equalsIgnoreCase(AgentDetailsModel.fromDay)
                || !toDay.equalsIgnoreCase(AgentDetailsModel.toDay)
                || !fromTime.equalsIgnoreCase(AgentDetailsModel.fromTime)
                || !toTime.equalsIgnoreCase(AgentDetailsModel.toTime);
    }

    private void enableUpdate() {
        if (isUpdatable()) {
            btnSave.setEnabled(true);
        } else {
            btnSave.setEnabled(false);
        }
    }

    private TextWatcher tw1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            enableUpdate();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };


}

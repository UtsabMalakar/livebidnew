package com.official.livebid.activities.watchList;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.activities.whatsOn.AuctionTodayAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.PropertyLists;
import com.official.livebid.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;


public class FragPropertyListView extends Fragment implements AsyncInterface {

    RecyclerView rvPropertyListing;
    ImageButton ibtnSearch;
    RelativeLayout rlSearchBar;
    String result; // in json format;
    Alerts alerts;
    private ArrayList<PropertyListInfo> alPropertyListInfo;
    private int offset = 0;
    private boolean isLastPage = false;
    WatchListAdapter watchListAdapter;
    private LinearLayoutManager linearLayoutManager;
    private TextView tvNoProperty;

    public FragPropertyListView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_property_list_view, container, false);
        init(view);
        getPropertyList();
        return view;

    }

    private void getPropertyList() {
        HashMap<String, String> params = new HashMap<>();
        params.put("offset", "0");
        HashMap headers = CommonMethods.getHeaders(getActivity());
        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.POST, params, UrlHelper.GET_WATCH_LIST, headers, false, CommonDef.RC_WATCH_LIST);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void init(View view) {
        alerts = new Alerts(getActivity());
        rvPropertyListing = (RecyclerView) view.findViewById(R.id.rc_propertyListing);
        rlSearchBar = (RelativeLayout) view.findViewById(R.id.rlSearchBar);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rlSearchBar.setVisibility(View.GONE);
        tvNoProperty = (TextView) view.findViewById(R.id.tvNoPropertiesFound);
        rvPropertyListing.setLayoutManager(linearLayoutManager);
        rvPropertyListing.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (!isLastPage) {
                    getPropertyList();
                }
            }
        });
        ibtnSearch = (ImageButton) view.findViewById(R.id.ibtn_search);
        ibtnSearch.setOnClickListener(clickListener);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_search:
                    Opener.Search(getActivity(),"");
                    break;
            }
        }
    };

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.RC_WATCH_LIST:
                System.out.println("Result " + result);
                initPropertyList(result);
                break;
        }
    }

    private void initPropertyList(String result) {

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            PropertyLists.alPropertyListInfo = new ArrayList<>();
            if (code.equals("0001")) {
                JSONArray jaList = joResult.getJSONArray("list");
                alPropertyListInfo = new ArrayList<>();
                if (jaList.length() > 0) {
                    for (int i = 0; i < jaList.length(); i++) {
                        JSONObject joList = jaList.getJSONObject(i);
                        PropertyListInfo propertyListInfo = new PropertyListInfo();
                        propertyListInfo.id = joList.getString("id");
                        propertyListInfo.agentId = joList.getString("agent_id");
                        propertyListInfo.agencyId = joList.getString("agency_id");
                        propertyListInfo.auctionDate = joList.getString("auction_date");
                        propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
                        HashMap<String ,String> agentName = CommonMethods.getName(joList.getString("agent_name").split(" "));
                        propertyListInfo.agentFName = agentName.get("fName");
                        propertyListInfo.agentLName = agentName.get("lName");
                        propertyListInfo.propertyStatus = joList.getInt("property_status");
                        propertyListInfo.statusTag = "";
                        propertyListInfo.dateTag = "Auction due to 40 days";
                        propertyListInfo.setFav(joList.getString("favorite"));
                        propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
                        propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
                        propertyListInfo.nosOfParking = joList.getString("parking");
                        propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(joList.getString("price")));
                        propertyListInfo.propertyStatusIdentifier = "from";
                        propertyListInfo.price_text=joList.getString("price_text");
                        propertyListInfo.suburb = joList.getString("suburb");
                        propertyListInfo.postCode = joList.getString("postcode");
                        propertyListInfo.propertyTitle = joList.getString("property_title");
                        propertyListInfo.propertyDesc = joList.getString("property_description");
                        propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
                        propertyListInfo.street = joList.getString("street");
                        propertyListInfo.propertyState = joList.getString("state");
                        propertyListInfo.propertyImgUrl = joList.getString("property_image");
                        propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
                        propertyListInfo.distance = joList.getString("distance");
                        propertyListInfo.lat = joList.getString("lat");
                        propertyListInfo.lng = joList.getString("lng");
                        propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
                        propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
                        propertyListInfo.auctionDateTimeStamp = joList.getLong("auction_date_timestamp");
                        propertyListInfo.propertyStatus2 = joList.getInt("property_status_2");
                        JSONArray JPropertyImageMore = joList.getJSONArray("property_images_more");
                        if (JPropertyImageMore.length() > 0) {
                            for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                propertyListInfo.property_images_more.add(JPropertyImageMore.getString(j));
                            }
                        }
                        alPropertyListInfo.add(propertyListInfo);
                    }
                    PropertyLists.alPropertyListInfo = alPropertyListInfo;
//                    rvPropertyListing.setAdapter(new WatchListAdapter(getActivity(), PropertyLists.alPropertyListInfo));

                    if (watchListAdapter == null) {
                        watchListAdapter = new WatchListAdapter(getActivity(), PropertyLists.alPropertyListInfo);
                        rvPropertyListing.setAdapter(watchListAdapter);
                    } else {
//                        curSize = myListingAdapter.getItemCount();
//                        myListingAdapter.notifyItemRangeInserted(curSize, alPropertyListInfo.size()-1);
                        watchListAdapter.addMoreData(alPropertyListInfo);

                    }
                    isLastPage = joResult.getBoolean("last_page");
                    offset = joResult.getInt("offset");
                }
                else
                {
                    tvNoProperty.setText(Html.fromHtml("<b>No Watchlist Properties.</b><br/><br/> Add properties to your watchlist"));
                    tvNoProperty.setVisibility(View.VISIBLE);
                }
            } else {
                alerts.showOkMessage(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

package com.official.livebid.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.helpers.FontHelper;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;

import java.lang.reflect.Type;
import java.sql.Types;


public class LandingScreen extends ActionBarActivity {
    private Button btnRegister;
    private Button btnSignIn;
    private Button btnDiscover;
    private TextView tvAboveTxt;
    private TextView tvBelowTxt;
    FontHelper fontHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_screen);
        getSupportActionBar().hide();
        btnRegister = (Button) findViewById(R.id.btn_sign_up);
        btnSignIn = (Button) findViewById(R.id.btn_sign_in);
        btnDiscover = (Button) findViewById(R.id.btn_discover);
        btnDiscover.setOnClickListener(btnClickListener);
        btnRegister.setOnClickListener(btnClickListener);
        btnSignIn.setOnClickListener(btnClickListener);
        tvAboveTxt = (TextView) findViewById(R.id.above_txt);
        tvBelowTxt = (TextView) findViewById(R.id.below_txt);

        //Font Helper
//        fontHelper = new FontHelper(this);
//        tvAboveTxt.setTypeface(fontHelper.getDefaultFont());
//        tvBelowTxt.setTypeface(fontHelper.getDefaultFont());

    }


    private View.OnClickListener btnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_sign_up:
//                    Opener.Register(LandingScreen.this);
                    Opener.newRegister(LandingScreen.this, UserType.NEW);
                    break;
                case R.id.btn_sign_in:
                    Opener.newRegister(LandingScreen.this,UserType.EXISTING);
//                    Opener.SignIn(LandingScreen.this);
                    break;
                case R.id.btn_discover:
                    new SharedPreference(LandingScreen.this).clearData();
                    Opener.Discover(LandingScreen.this);
                    break;
            }

        }
    };
}

package com.official.livebid.activities.whatsOn;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.PropertyLists;
import com.official.livebid.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class FragOpenHouseListView extends Fragment implements AsyncInterface {

    RecyclerView rvPropertyListing;
    ImageButton ibtnSearch;
    RelativeLayout rlSearchBar;
    TextView tvSearchSuburb;
    String result; // in json format;
    Alerts alerts;
    private ArrayList<PropertyListInfo> alPropertyListInfo;
    private ArrayList<OpenHouseProperty> openHouseProperties;
    private LinearLayoutManager linearLayoutManager;
    private boolean isLastPage = false;
    private OpenHouseTodayAdapter openHouseTodayAdapter;
    private int offset = 0;
    SharedPreference prefs;
    private TextView tvNoProperty;

    public FragOpenHouseListView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_property_list_view, container, false);
        init(view);
        setSearchHistory();
        getPropertyList();
        return view;

    }

    private void setSearchHistory() {
        String searched_suburb = prefs.getStringValues(CommonDef.SharedPrefKeys.Search_SuburbNames);
        System.out.print("Searched suburb" + searched_suburb);
        List<String> list = new ArrayList<String>(Arrays.asList(searched_suburb.split(",")));
        tvSearchSuburb.setText(getSearchedNames(list));
    }

    private String getSearchedNames(List<String> list) {
        int no_of_search=0;
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<list.size();i++)
        {
            if(i==0)
            {
                sb.append(list.get(i));
            }
            else
            {
                no_of_search++;
            }
        }
        if(list.size()==1)
        {
            return sb.toString();
        }
        else {
            sb.append(" and " + no_of_search + " more");
            return sb.toString();
        }
    }

    private void getPropertyList() {
        HashMap<String, String> params = new HashMap<>();
        params.put("lat", "0");
        params.put("lng", "0");
        params.put("offset", offset + "");
        params.put("discover_home", "0");
        params.put("is_trending", "0");
        params.put("this_week_auction", "0");
        params.put("this_week_open_house", "1");
        params.put("post_code_ids",prefs.getStringValues("searchPostCodeIds"));
        HashMap headers = CommonMethods.getHeaders(getActivity());
        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.POST, params, UrlHelper.GET_WHATS_ON, headers, false, CommonDef.RC_WHATS_ON);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void init(View view) {
        alerts = new Alerts(getActivity());
        prefs = new SharedPreference(getActivity());
        rvPropertyListing = (RecyclerView) view.findViewById(R.id.rc_propertyListing);
        tvSearchSuburb = (TextView) view.findViewById(R.id.tvSearchSuburb);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvPropertyListing.setLayoutManager(linearLayoutManager);
        tvNoProperty = (TextView) view.findViewById(R.id.tvNoPropertiesFound);
        rvPropertyListing.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (!isLastPage) {
                    getPropertyList();
                }
            }
        });
        ibtnSearch = (ImageButton) view.findViewById(R.id.ibtn_search);
        rlSearchBar = (RelativeLayout) view.findViewById(R.id.rlSearchBar);
        ibtnSearch.setOnClickListener(clickListener);
        rlSearchBar.setOnClickListener(clickListener);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_search:
                    Opener.Search(getActivity(),CommonDef.CallingActivity.Open_House);
                    break;
                case R.id.rlSearchBar:
                    Opener.Search(getActivity(),CommonDef.CallingActivity.Open_House);
                    break;
            }
        }
    };


    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.RC_WHATS_ON:
                System.out.println("Result " + result);
                initPropertyList(result);
                break;
        }
    }

    private void initPropertyList(String result) {

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            if (code.equals("0001")) {
                JSONArray jaList = joResult.getJSONArray("list");
                PropertyLists.alPropertyListInfo=new ArrayList<>();
                if (jaList.length() > 0) {
                    openHouseProperties=new OpenHouseProperty().getOpenHouseProperties(jaList);
                    PropertyLists.alPropertyListInfo = new OpenHouseProperty().getAllProperties(jaList);
//                    rvPropertyListing.setAdapter(new OpenHouseTodayAdapter(getActivity(), PropertyLists.alPropertyListInfo));
                    isLastPage = joResult.getBoolean("last_page");
                    if (openHouseTodayAdapter == null) {
                        openHouseTodayAdapter = new OpenHouseTodayAdapter(getActivity(), openHouseProperties);
                        rvPropertyListing.setAdapter(openHouseTodayAdapter);
                    } else {
//                        curSize = myListingAdapter.getItemCount();
//                        myListingAdapter.notifyItemRangeInserted(curSize, alPropertyListInfo.size()-1);
                        openHouseTodayAdapter.addMoreData(openHouseProperties);

                    }
                    offset = joResult.getInt("offset");
                }
                else
                    tvNoProperty.setVisibility(View.VISIBLE);
            } else {
                alerts.showOkMessage(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

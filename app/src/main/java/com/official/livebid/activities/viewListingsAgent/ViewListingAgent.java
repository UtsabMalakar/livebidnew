package com.official.livebid.activities.viewListingsAgent;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.activities.liveAuction.PropertyAuctionModel;
import com.official.livebid.activities.liveAuction.SoldPropertyModel;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.objects.RoomDetail;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ViewListingAgent extends AppCompatActivity implements AsyncInterface, View.OnClickListener {
    private ImageView ivPropertyImg;
    private TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
            tvPropertyStatusIdentifier, tvReservePrice, tvPropertyNo;

    private PropertyListInfo propertyListInfo;
    private com.official.livebid.alerts.Alerts alerts;

    private TextView tvPropertyDetails, tvPreOffers, tvApplications, tvRegisteredBidders;
    private Button btnStartAuction;
    private SharedPreference prefs;
    private SoldPropertyModel soldProp;
    private TextView tvPropertyImageIndicator;
    private ViewPager mViewPager;
    private CheckBox  chkFav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_view_listing_agent);
        init();
        showPropertyInfo();
        getAgentsListing();
        enableStartAuction();
    }

    private void enableStartAuction() {
        if (propertyListInfo.propertyStatus == 0) {
            if (propertyListInfo.propertyStatus2 == 4 && prefs.getStringValues(CommonDef.SharedPrefKeys.AGENT_ID).equalsIgnoreCase(propertyListInfo.agentId)) {
                btnStartAuction.setText("Start Auction");
                btnStartAuction.setTag(15);
                btnStartAuction.setEnabled(true);
            } else {
                btnStartAuction.setText("Start Auction");
                btnStartAuction.setTag(15);
                btnStartAuction.setEnabled(false);// for test purpose btn is enabled
            }
        } else if (propertyListInfo.propertyStatus == 1 || propertyListInfo.propertyStatus == 4) {
            if (prefs.getStringValues(CommonDef.SharedPrefKeys.AGENT_ID).equalsIgnoreCase(propertyListInfo.agentId)) {
                btnStartAuction.setText("Continue To Auction");
                btnStartAuction.setTag(16);
//                    btnBottom.setTag(13);//join room just like user
                btnStartAuction.setEnabled(true);
            } else {
                btnStartAuction.setText("Continue To Auction");
                btnStartAuction.setTag(16);
//                    btnBottom.setTag(13);
                btnStartAuction.setEnabled(false);// for test purpose btn is enabled
            }
            // live now and on the market
        } else if (propertyListInfo.propertyStatus == 2) {
            btnStartAuction.setText("View Summary Report");
            btnStartAuction.setEnabled(true);
            btnStartAuction.setTag(12);
        } else if (propertyListInfo.propertyStatus == 3) {
            btnStartAuction.setText("View Summary Report");
            btnStartAuction.setEnabled(true);
            btnStartAuction.setTag(11);
        }
    }

    private void getAgentsListing() {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("agentListings params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(ViewListingAgent.this, Request.Method.POST, params, UrlHelper.GET_AGENTS_MY_LISTING, headers, false, CommonDef.REQ_AGENTS_LISTING);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void showPropertyInfo() {


        tvReservePrice.setText(propertyListInfo.price);

        if(propertyListInfo.propertyCode != null && !propertyListInfo.propertyCode.isEmpty() )
        {
            tvPropertyNo.setText("#" + propertyListInfo.propertyCode);
        }
        else
        {
            tvPropertyNo.setText("-");
        }
        if (propertyListInfo.propertyStatus == 0) {
            if (propertyListInfo.propertyStatus2 == 4) {
                tvStatusTag.setVisibility(View.VISIBLE);
                tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                tvDateTag.setVisibility(View.GONE);

            } else {
                if (propertyListInfo.propertyStatus2 != 0)
                tvStatusTag.setVisibility(View.VISIBLE);
                tvDateTag.setVisibility(View.VISIBLE);
                tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
            }

        } else {
            tvDateTag.setVisibility(View.GONE);
            tvStatusTag.setVisibility(View.VISIBLE);
            tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
        }
        tvPropertyStreet.setText(propertyListInfo.street);
        tvPropertySuburbPostcode.setText(propertyListInfo.suburb + ", " + propertyListInfo.postCode);

        //view pager adapter
        ImageSlideAdapter mImageSlideAdapter;
        mImageSlideAdapter = new ImageSlideAdapter(this, propertyListInfo.property_images_more, propertyListInfo.propertyStatus);
        mViewPager.setAdapter(mImageSlideAdapter);

        if (propertyListInfo.propertyStatus == 2) {
//            tvStatusTag.setVisibility(View.GONE);
            tvPropertyImageIndicator.setVisibility(View.GONE);

        } else {
            if (propertyListInfo.propertyStatus==5){
                tvStatusTag.setVisibility(View.INVISIBLE);
            }
            else {
                tvStatusTag.setVisibility(View.VISIBLE);
            }
            tvPropertyImageIndicator.setVisibility(View.GONE);
            tvPropertyImageIndicator.setText( 1 + "/" + propertyListInfo.property_images_more.size());
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    tvPropertyImageIndicator.setText(position + 1 + "/" + propertyListInfo.property_images_more.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        }

    }

    private void init() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            propertyListInfo = (PropertyListInfo) bundle.getSerializable(CommonDef.PROPERTY_LIST_INFO);
        }
        new CustomActionBar.ViewListingAgentCAB(this);
        alerts = new Alerts(this);
        prefs = new SharedPreference(this);
        chkFav= (CheckBox) findViewById(R.id.cbox_fav);
        chkFav.setVisibility(View.GONE);
        mViewPager = (ViewPager) findViewById(R.id.vp_property_img);
        tvPropertyImageIndicator = (TextView) findViewById(R.id.tvPropertyImageIndicator);
        tvStatusTag = (TextView) findViewById(R.id.tv_property_status_tag);
        tvDateTag = (TextView) findViewById(R.id.tv_property_date_tag);
        tvPropertySuburbPostcode = (TextView) findViewById(R.id.tv_property_suburb_postcode);
        tvPropertyStreet = (TextView) findViewById(R.id.tv_property_street);
        tvPropertyStatusIdentifier = (TextView) findViewById(R.id.tv_property_status_identifier);
        tvReservePrice = (TextView) findViewById(R.id.tv_reserve_price);
        tvPropertyNo = (TextView) findViewById(R.id.tv_property_no);
        tvPropertyDetails = (TextView) findViewById(R.id.tv_property_details);
        tvPreOffers = (TextView) findViewById(R.id.tv_pre_auction_offers);
        tvApplications = (TextView) findViewById(R.id.tv_auction_applications);
        tvRegisteredBidders = (TextView) findViewById(R.id.tv_registered_bidders);
        btnStartAuction = (Button) findViewById(R.id.btn_start_auction);
        btnStartAuction.setOnClickListener(this);
        tvPropertyDetails.setOnClickListener(this);
        tvPreOffers.setOnClickListener(this);
        tvApplications.setOnClickListener(this);
        tvRegisteredBidders.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_listing_agent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("AgentListings Response:" + result);
        switch (requestCode) {
            case CommonDef.REQ_AGENTS_LISTING:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        int offers = joResult.getInt("pre_offers");
                        int applications = joResult.getInt("total_applications");
                        int reg_bidders = joResult.getInt("registered_bidders");
                        tvPreOffers.setText("Pre-Auction Offers(" + offers + ")");
                        tvApplications.setText("Auction Applications(" + applications + ")");
                        tvRegisteredBidders.setText("Registered Bidders(" + reg_bidders + ")");


                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_START_AUCTION:
                System.out.println("join live auction" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        RoomDetail roomDetail = new RoomDetail();
                        roomDetail.auctionId = joResult.getString("property_auction_id");
                        roomDetail.propertyId = propertyListInfo.id;
                        roomDetail.roomName = joResult.getString("chat_room");
                        roomDetail.callCount = 0;
                        roomDetail.soldTo = "";
                        roomDetail.soldAmount = "";
                        roomDetail.status = "";
                        roomDetail.startDateTime = joResult.getString("auction_start_date_time_stamp");

                        PropertyAuctionModel propertyModel = new PropertyAuctionModel();
                        propertyModel.propertyId = propertyListInfo.id;
                        propertyModel.propertyImgUrl = propertyListInfo.propertyImgUrl;
                        propertyModel.street = propertyListInfo.street;
                        propertyModel.regionPostcode = propertyListInfo.suburb + " "+ propertyListInfo.propertyState+" " + propertyListInfo.postCode;
                        propertyModel.propertyStatus = propertyListInfo.propertyStatus;
                        propertyModel.propertyStatus2 = propertyListInfo.propertyStatus2;
                        propertyModel.agentId = propertyListInfo.agentId;
                        propertyModel.propertyPrice = propertyListInfo.price;
                        propertyModel.moreImages = propertyListInfo.property_images_more;
                        propertyModel.setFav(propertyListInfo.isFav());


                        Opener.PropertyAuction(ViewListingAgent.this, propertyModel, roomDetail);

                    } else if (code.equals("0067")) {
                        joinLiveAuction();
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
            case CommonDef.RC_JOIN_LIVE_AUCTION:
                System.out.println("join live auction" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONObject jObjRoomDetail = joResult.getJSONObject("room_detail");
                        RoomDetail roomDetail = new RoomDetail();
                        roomDetail.auctionId = jObjRoomDetail.getString("id");
                        roomDetail.propertyId = jObjRoomDetail.getString("property_id");
                        roomDetail.roomName = jObjRoomDetail.getString("room_name");
                        roomDetail.callCount = jObjRoomDetail.getInt("count_call");
                        roomDetail.soldTo = jObjRoomDetail.getString("sold_to");
                        roomDetail.soldAmount = jObjRoomDetail.getString("sold_amount");
                        roomDetail.status = jObjRoomDetail.getString("status");
                        roomDetail.startDateTime = jObjRoomDetail.getString("auction_start_date_time_stamp");

                        PropertyAuctionModel propertyModel = new PropertyAuctionModel();
                        propertyModel.propertyId = propertyListInfo.id;
                        propertyModel.propertyImgUrl = propertyListInfo.propertyImgUrl;
                        propertyModel.street = propertyListInfo.street;
                        propertyModel.regionPostcode = propertyListInfo.suburb + " "+ propertyListInfo.propertyState+" " + propertyListInfo.postCode;
                        propertyModel.propertyStatus = propertyListInfo.propertyStatus;
                        propertyModel.propertyStatus2 = propertyListInfo.propertyStatus2;
                        propertyModel.agentId = propertyListInfo.agentId;
                        propertyModel.propertyPrice = propertyListInfo.price;
                        propertyModel.moreImages = propertyListInfo.property_images_more;
                        propertyModel.setFav(propertyListInfo.isFav());


                        Opener.PropertyAuction(ViewListingAgent.this, propertyModel, roomDetail);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
            case CommonDef.RC_SUMMARY_REPORT_SOLD:
                System.out.println("sumary report" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONObject jObjBidSummary = joResult.getJSONObject("bid_summary");
                        soldProp = new SoldPropertyModel(propertyListInfo);
                        soldProp.userId = propertyListInfo.agentId;
                        soldProp.propertyId = propertyListInfo.id;
                        soldProp.propertyImgUrl = propertyListInfo.propertyImgUrl;
                        soldProp.street = propertyListInfo.street;
                        soldProp.regionPostcode = propertyListInfo.suburb + " "+ propertyListInfo.propertyState+" " + propertyListInfo.postCode;
                        soldProp.participations = jObjBidSummary.getString("participation");
                        soldProp.highestBidAmount = soldProp.getHighestBidAmount(jObjBidSummary.getString("highest_bid"));
                        soldProp.highestBidder = jObjBidSummary.getString("highest_bidder");
                        String tempHB = jObjBidSummary.getString("highest_bidder");
                        if (tempHB.equalsIgnoreCase(CommonMethods.getLoggedInUserUniqueId(getApplicationContext())))
                            soldProp.highestBidder = "You";
                        soldProp.soldDate = jObjBidSummary.getString("auction_end_date_time");
                        soldProp.soldDate = soldProp.getSoldDateTime(jObjBidSummary.getString("auction_end_date_time"))[0];
                        soldProp.soldTime = soldProp.getSoldDateTime(jObjBidSummary.getString("auction_end_date_time"))[1];
                        soldProp.bidDuration = soldProp.getDuration(Long.parseLong(jObjBidSummary.getString("duration")));
                        soldProp.setWon(jObjBidSummary.getString("check_myself"));
                        if (propertyListInfo.propertyStatus == 2)
                            Opener.SoldPropertySummary(ViewListingAgent.this, soldProp);
                        else
                            Opener.PassedPropertySummary(ViewListingAgent.this, soldProp);


                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;


        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_property_details:
                Opener.PropertyDetails(ViewListingAgent.this, propertyListInfo);
                break;
            case R.id.tv_pre_auction_offers:
                Opener.PreAuctionOfferAgent(ViewListingAgent.this, propertyListInfo);
                break;
            case R.id.tv_auction_applications:
                Opener.AuctionApplications(ViewListingAgent.this, propertyListInfo);
                break;
            case R.id.tv_registered_bidders:
                Opener.RegisteredBidders(ViewListingAgent.this, propertyListInfo);
                break;
            case R.id.btn_start_auction:
                int action = (int) btnStartAuction.getTag();
                switch (action) {
                    case 11:// passed in property
                        getSoldPropertySummaryReport();
                        break;

                    case 12:// sold property summary
                        getSoldPropertySummaryReport();
                        break;
                    case 13:// join auction
                        joinLiveAuction();
                        break;
                    case 15:// start auction
//                        Toast.makeText(getApplicationContext(), "start Auction", Toast.LENGTH_SHORT).show();
                        startAuction();
                        break;
                    case 16:// join auction agent
//                        Toast.makeText(getApplicationContext(), "start Auction", Toast.LENGTH_SHORT).show();
                        joinLiveAuction();
                        break;

                }
                break;


        }

    }

    private void startAuction() {
        HashMap<String, String> headers = CommonMethods.getHeaders(ViewListingAgent.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("start auction=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(ViewListingAgent.this, Request.Method.POST, params, UrlHelper.START_AUCTION, headers, false, CommonDef.RC_START_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void joinLiveAuction() {
        HashMap<String, String> headers = CommonMethods.getHeaders(ViewListingAgent.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("join room=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(ViewListingAgent.this, Request.Method.POST, params, UrlHelper.JOIN_LIVE_AUCTION, headers, false, CommonDef.RC_JOIN_LIVE_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void getSoldPropertySummaryReport() {
        HashMap<String, String> headers = CommonMethods.getHeaders(ViewListingAgent.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("SUMMARY REPORT=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(ViewListingAgent.this, Request.Method.POST, params, UrlHelper.GET_SUMMARY_REPORT_SOLD, headers, false, CommonDef.RC_SUMMARY_REPORT_SOLD);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }
}

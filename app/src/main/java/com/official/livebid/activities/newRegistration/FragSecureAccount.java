package com.official.livebid.activities.newRegistration;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.alerts.Alerts;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragSecureAccount.OnPasswordSetListener} interface
 * to handle interaction events.
 */
public class FragSecureAccount extends Fragment implements View.OnClickListener {
    private CustomEditText ctPassword, ctConfirmPassword;
    private Button btnNext;
    private String password, confirmPassword;

    private OnPasswordSetListener mListener;

    public FragSecureAccount() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frag_secure_account, container, false);
        ctPassword = (CustomEditText) v.findViewById(R.id.cv_password);
        ctPassword.enableEditMode(true);
        ctPassword.addTextWatcher(tw);
        ctConfirmPassword = (CustomEditText) v.findViewById(R.id.cv_confirm_password);
        ctConfirmPassword.enableEditMode(true);
        ctConfirmPassword.addTextWatcher(tw);
        btnNext = (Button) v.findViewById(R.id.btn_next_secure_account);
        btnNext.setOnClickListener(this);
        return v;
    }


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnPasswordSetListener) {
            mListener = (OnPasswordSetListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPasswordSetListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next_secure_account:
                if (isValidatePassword())
                    mListener.onSetPassword(password);
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnPasswordSetListener {
        // TODO: Update argument type and name
        void onSetPassword(String Password);
    }

    private TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            enableProcessNext();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void enableProcessNext() {
        getPassword();
        if (!password.isEmpty() || !confirmPassword.isEmpty()) {
            btnNext.setEnabled(true);
        } else {
            btnNext.setEnabled(false);
        }
    }

    private boolean isValidatePassword() {
        Alerts alerts = new Alerts(getActivity());
        if (password.isEmpty()) {
            alerts.showOkMessage("Password is empty");
            return false;
        } else if (!isPasswordValid(password)) {
            alerts.showOkMessage("Password should at least 8 characters long.");
            return false;
        } else if (confirmPassword.isEmpty()) {
            alerts.showOkMessage("Confirm password is empty");
            return false;
        } else if (!password.equalsIgnoreCase(confirmPassword)) {
            alerts.showOkMessage("Password mismatched");
            return false;
        }
        return true;
    }

    private void getPassword() {
        password = ctPassword.getText().toString();
        confirmPassword = ctConfirmPassword.getText().toString();
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8 ? true : false;

    }
}

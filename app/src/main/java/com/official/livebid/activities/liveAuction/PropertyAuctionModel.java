package com.official.livebid.activities.liveAuction;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ics on 4/19/2016.
 */
public class PropertyAuctionModel implements Serializable {
    public String propertyId;
    public String propertyImgUrl;
    public String street;
    public String regionPostcode;
    public String highestBidAmount;
    public String highestBidder;
    public String agentId;
    public int propertyStatus;
    public int propertyStatus2;
    public String propertyPrice;
    public ArrayList<String> moreImages;

    public Boolean isFav() {
        return isFav;
    }

    public void setFav(Boolean fav) {
        isFav = fav;
    }

    private Boolean isFav;



    public long getPrice(){
     return    Long.parseLong(propertyPrice.replaceAll("[$,]", ""));
    }
}

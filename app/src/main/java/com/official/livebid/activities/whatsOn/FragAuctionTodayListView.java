package com.official.livebid.activities.whatsOn;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.PropertyLists;
import com.official.livebid.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class FragAuctionTodayListView extends Fragment implements AsyncInterface {

    RecyclerView rvPropertyListing;
    ImageButton ibtnSearch;
    String result; // in json format;
    Alerts alerts;
    SharedPreference prefs;
    RelativeLayout rlSearchBar;
    TextView tvSearchBar;
    private ArrayList<PropertyListInfo> alPropertyListInfo;
    private int offset = 0;
    private boolean isLastPage = false;
    private AuctionTodayAdapter auctionTodayAdapter;
    private LinearLayoutManager linearLayoutManager;
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_search:
                    Opener.Search(getActivity(),CommonDef.CallingActivity.Auctions);
                    break;
                case R.id.rlSearchBar:
                    Opener.Search(getActivity(),CommonDef.CallingActivity.Open_House);
                    break;
            }
        }
    };
    private TextView tvNoProperty;


    public FragAuctionTodayListView() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_property_list_view, container, false);
        init(view);
        setSearchHistory();
        getAuctionToday();
        return view;

    }

    private void setSearchHistory() {
        String searched_suburb = prefs.getStringValues(CommonDef.SharedPrefKeys.Search_SuburbNames);
        System.out.print("Searched suburb" + searched_suburb);
        List<String> list = new ArrayList<String>(Arrays.asList(searched_suburb.split(",")));
        tvSearchBar.setText(getSearchedNames(list));
    }

    private String getSearchedNames(List<String> list) {
        int no_of_search=0;
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<list.size();i++)
        {
            if(i==0)
            {
                sb.append(list.get(i));
            }
            else
            {
                no_of_search++;
            }
        }
        if(list.size()==1)
        {
            return sb.toString();
        }
        else {
            sb.append(" and " + no_of_search + " more");
            return sb.toString();
        }
    }

    private void getAuctionToday() {
        HashMap<String, String> params = new HashMap<>();
        params.put("lat", "0");
        params.put("lng", "0");
        params.put("offset", offset + "");
        params.put("is_trending", "0");
        params.put("discover_home", "0");
        params.put("this_week_auction", "1");
        params.put("this_week_open_house", "0");
        params.put("post_code_ids",prefs.getStringValues("searchPostCodeIds"));
        HashMap headers = CommonMethods.getHeaders(getActivity());
        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.POST, params, UrlHelper.GET_WHATS_ON, headers, false, CommonDef.RC_WHATS_ON);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void init(View view) {
        alerts = new Alerts(getActivity());
        prefs = new SharedPreference(getActivity());
        rvPropertyListing = (RecyclerView) view.findViewById(R.id.rc_propertyListing);
        tvSearchBar = (TextView) view.findViewById(R.id.tvSearchSuburb);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvPropertyListing.setLayoutManager(linearLayoutManager);
        tvNoProperty = (TextView) view.findViewById(R.id.tvNoPropertiesFound);
        rvPropertyListing.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (!isLastPage) {
                    getAuctionToday();
                }
            }
        });
        ibtnSearch = (ImageButton) view.findViewById(R.id.ibtn_search);
        rlSearchBar = (RelativeLayout) view.findViewById(R.id.rlSearchBar);
        ibtnSearch.setOnClickListener(clickListener);
        rlSearchBar.setOnClickListener(clickListener);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.RC_WHATS_ON:
                System.out.println("Result " + result);
                initPropertyList(result);
                break;
        }
    }

    private void initPropertyList(String result) {

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            if (code.equals("0001")) {
                JSONArray jaList = joResult.getJSONArray("list");
                PropertyLists.alPropertyListInfo=new ArrayList<>();
                if (jaList.length() > 0) {
                    alPropertyListInfo = new ArrayList<>();
                    for (int i = 0; i < jaList.length(); i++) {
                        JSONObject joList = jaList.getJSONObject(i);
                        PropertyListInfo propertyListInfo = new PropertyListInfo();
                        propertyListInfo.id = joList.getString("id");
                        propertyListInfo.agentId = joList.getString("agent_id");
                        propertyListInfo.agencyId = joList.getString("agency_id");
                        propertyListInfo.auctionDate = joList.getString("auction_date");
                        propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
                        HashMap<String ,String> agentName = CommonMethods.getName(joList.getString("agent_name").split(" "));
                        propertyListInfo.agentFName = agentName.get("fName");
                        propertyListInfo.agentLName = agentName.get("lName");
                        propertyListInfo.propertyStatus = joList.getInt("property_status");
                        propertyListInfo.statusTag = "";
                        propertyListInfo.dateTag = "Auction due to 40 days";
                        propertyListInfo.setFav(joList.getString("favorite"));
                        propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
                        propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
                        propertyListInfo.nosOfParking = joList.getString("parking");
                        propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(joList.getString("price")));
                        propertyListInfo.propertyStatusIdentifier = "from";
                        propertyListInfo.price_text=joList.getString("price_text");
                        propertyListInfo.suburb = joList.getString("suburb");
                        propertyListInfo.postCode = joList.getString("postcode");
                        propertyListInfo.propertyTitle = joList.getString("property_title");
                        propertyListInfo.propertyDesc = joList.getString("property_description");
                        propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
                        propertyListInfo.street = joList.getString("street");
                        propertyListInfo.propertyState = joList.getString("state");
                        propertyListInfo.propertyImgUrl = joList.getString("property_image");
                        propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
                        propertyListInfo.distance = joList.getString("distance");
                        propertyListInfo.lat = joList.getString("lat");
                        propertyListInfo.lng = joList.getString("lng");
                        propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
                        propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
                        propertyListInfo.auctionDateTimeStamp = joList.getLong("auction_date_timestamp");
                        propertyListInfo.propertyStatus2 = joList.getInt("property_status_2");
                        JSONArray JPropertyImageMore = joList.getJSONArray("property_images_more");
                        if (JPropertyImageMore.length() > 0) {
                            for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                propertyListInfo.property_images_more.add(JPropertyImageMore.getString(j));
                            }
                        }

                        alPropertyListInfo.add(propertyListInfo);
                    }
                    PropertyLists.alPropertyListInfo = alPropertyListInfo;

//                    rvPropertyListing.setAdapter(new AuctionTodayAdapter(getActivity(), PropertyLists.alPropertyListInfo));

                    if (auctionTodayAdapter == null) {
                        auctionTodayAdapter = new AuctionTodayAdapter(getActivity(), PropertyLists.alPropertyListInfo);
                        rvPropertyListing.setAdapter(auctionTodayAdapter);
                    } else {
//                        curSize = myListingAdapter.getItemCount();
//                        myListingAdapter.notifyItemRangeInserted(curSize, alPropertyListInfo.size()-1);
                        auctionTodayAdapter.addMoreData(alPropertyListInfo);

                    }
                    isLastPage = joResult.getBoolean("last_page");
                    offset = joResult.getInt("offset");
                }
                else
                    tvNoProperty.setVisibility(View.VISIBLE);
            } else {
                alerts.showOkMessage(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

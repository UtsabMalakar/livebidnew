package com.official.livebid.activities.myListings;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SlideShowViewPager;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.PropertyLists;

import java.util.ArrayList;

/**
 * Created by Ics on 3/10/2016.
 */
public class MyListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<PropertyListInfo> alPropertyListInfo;

    public MyListingAdapter(Context context, ArrayList<PropertyListInfo> alPropertyListInfo) {
        this.context = context;
        this.alPropertyListInfo = alPropertyListInfo;
        PropertyLists.alPropertyListInfo = alPropertyListInfo;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_header, parent, false);
        return new PropertyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final PropertyListInfo propertyListInfo = alPropertyListInfo.get(position);
        final PropertyViewHolder propertyViewHolder = (PropertyViewHolder) holder;
        propertyViewHolder.tvReservePrice.setText(propertyListInfo.price);
        propertyViewHolder.tvPropertyNo.setText("#" + propertyListInfo.propertyCode);
        if (propertyListInfo.propertyStatus == 0) {
            if ( propertyListInfo.propertyStatus2 == 4) {
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                propertyViewHolder.tvDateTag.setVisibility(View.GONE);

            } else {
                if (propertyListInfo.propertyStatus2 != 0)
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvDateTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                propertyViewHolder.tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
            }

        } else {
            propertyViewHolder.tvDateTag.setVisibility(View.GONE);
            propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
            propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
        }
        propertyViewHolder.tvPropertyStreet.setText(propertyListInfo.street);
        propertyViewHolder.tvPropertySuburbPostcode.setText(propertyListInfo.suburb + " " + propertyListInfo.propertyState + " " + propertyListInfo.postCode);

        ImageSlideAdapter mImageSlideAdapter;
        mImageSlideAdapter = new ImageSlideAdapter(context, propertyListInfo.property_images_more,propertyListInfo.propertyStatus);
        propertyViewHolder.mViewPager.setAdapter(mImageSlideAdapter);
        propertyViewHolder.mViewPager.setOnViewPagerClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Opener.ViewListingsAgent((Activity) context, propertyListInfo);

            }
        });
        if (propertyListInfo.propertyStatus == 2) {
            propertyViewHolder.ivPropertySoldTag.setVisibility(View.VISIBLE);
            propertyViewHolder.tvStatusTag.setVisibility(View.GONE);
            propertyViewHolder.tvPropertyImageIndicator.setVisibility(View.GONE);
//                propertyViewHolder.mViewPager.beginFakeDrag();

        }else {
            propertyViewHolder.ivPropertySoldTag.setVisibility(View.GONE);
            if (propertyListInfo.propertyStatus==5){
                propertyViewHolder.tvStatusTag.setVisibility(View.INVISIBLE);
            }
            else {
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
            }
            propertyViewHolder.tvPropertyImageIndicator.setVisibility(View.GONE);
            propertyViewHolder.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    propertyViewHolder.tvPropertyImageIndicator.setText(position+1 + "/" + propertyListInfo.property_images_more.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        }
        ((PropertyViewHolder) holder).rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Opener.ViewListingsAgent((Activity) context, propertyListInfo);
            }
        });



    }

    @Override
    public int getItemCount() {
        return alPropertyListInfo.size();
    }

    public static class PropertyViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvPropertyImageIndicator;
        private final SlideShowViewPager mViewPager;
        private final CheckBox chkFav;
        protected ImageView ivPropertyImg,ivPropertySoldTag;
        protected TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
                tvPropertyStatusIdentifier, tvReservePrice, tvPropertyNo;
        View rootView;

        public PropertyViewHolder(View view) {
            super(view);
            rootView = view;
            chkFav= (CheckBox)view.findViewById(R.id.cbox_fav);
            chkFav.setVisibility(View.GONE);
            mViewPager = (SlideShowViewPager) view.findViewById(R.id.vp_property_img);
            tvPropertyImageIndicator = (TextView) view.findViewById(R.id.tvPropertyImageIndicator);
            ivPropertySoldTag=(ImageView)  view.findViewById(R.id.iv_property_sold_tag);
            tvStatusTag = (TextView) view.findViewById(R.id.tv_property_status_tag);
            tvDateTag = (TextView) view.findViewById(R.id.tv_property_date_tag);
            tvPropertySuburbPostcode = (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
            tvPropertyStreet = (TextView) view.findViewById(R.id.tv_property_street);
            tvPropertyStatusIdentifier = (TextView) view.findViewById(R.id.tv_property_status_identifier);
            tvReservePrice = (TextView) view.findViewById(R.id.tv_reserve_price);
            tvPropertyNo = (TextView) view.findViewById(R.id.tv_property_no);
        }
    }

    public void addMoreData(ArrayList<PropertyListInfo> data){
        int startPosition = alPropertyListInfo.size() ;
        this.alPropertyListInfo.addAll(data);
        PropertyLists.alPropertyListInfo = alPropertyListInfo;
        this.notifyItemRangeInserted(startPosition, data.size());
    }


}

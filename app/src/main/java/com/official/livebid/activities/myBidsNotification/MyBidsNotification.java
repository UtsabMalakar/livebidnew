package com.official.livebid.activities.myBidsNotification;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.official.livebid.R;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.helpers.CustomActionBar;

import java.util.ArrayList;

public class MyBidsNotification extends MenuActivity {
    RecyclerView rvMyBidsNotification;
    private LinearLayoutManager linearLayoutManager;
    private MybidsNotificationAdapter myBidsAdapter;
    private ArrayList<MyBidsPropertyModel> myBids;


    @Override
    public int getLayoutId() {
        return R.layout.activity_my_bids_notification;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_my_bids_notification);
        getSupportActionBar().hide();
        new CustomActionBar.MyBidsCAB(this);
        rvMyBidsNotification= (RecyclerView) findViewById(R.id.rv_my_bids_notification);
        linearLayoutManager=new LinearLayoutManager(this);
        rvMyBidsNotification.setLayoutManager(linearLayoutManager);
        myBids=new ArrayList<>();
        myBids.add(new MyBidsPropertyModel("124ssf","adfafsfa","","1","1234543"));
        myBids.add(new MyBidsPropertyModel("124ssf","adfafsfa","","2","1234543"));
        myBids.add(new MyBidsPropertyModel("124ssf","adfafsfa","","1","1234543"));
        myBids.add(new MyBidsPropertyModel("124ssf","adfafsfa","","2","1234543"));
        myBids.add(new MyBidsPropertyModel("124ssf","adfafsfa","","1","1234543"));
        myBids.add(new MyBidsPropertyModel("124ssf","adfafsfa","","2","1234543"));
        myBidsAdapter=new MybidsNotificationAdapter(this,myBids);
        rvMyBidsNotification.setAdapter(myBidsAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }
}

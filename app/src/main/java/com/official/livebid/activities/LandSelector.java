package com.official.livebid.activities;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.NumberPicker;

import com.official.livebid.R;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.staticClasses.SearchParams;

public class LandSelector extends ActionBarActivity implements NumberPicker.OnValueChangeListener {
    private static int[] landList;
    private NumberPicker np_min;
    private NumberPicker np_max;
    private CustomActionBar.SelectLandTitleBar titleBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_land_selector);
        landList = new int[]{
                0
                , 50
                , 75
                , 100
                , 200
                , 300
                , 400
                , 500
                , 600
                , 700
                , 800
                , 900
                , 1000
                , 1200
                , 1500
                , 1750
                , 2000
                , 3000
                , 4000
                , 5000
                , 10000

        };
        final String[] nums = new String[landList.length];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = formatLand(landList[i]);
        }

        // Set the max and min values of the numberpicker, and give it the
        // array of numbers created above to be the displayed numbers
        np_min = (NumberPicker) findViewById(R.id.np_min);
        setDividerColor(np_min, Color.RED);
        np_min.setMaxValue(landList.length - 1);
        np_min.setMinValue(0);
        np_min.setWrapSelectorWheel(false);
        np_min.setDisplayedValues(nums);
        np_min.setValue(getIndexFromValue(Refine.minL));


        np_max = (NumberPicker) findViewById(R.id.np_max);
        setDividerColor(np_max, Color.RED);
        setDividerColor(np_min, Color.RED);
        np_max.setMaxValue(landList.length - 1);
        np_max.setMinValue(0);
        np_max.setWrapSelectorWheel(false);
        np_max.setDisplayedValues(nums);
        np_max.setValue(getIndexFromValue(Refine.maxL));
//        Button button = (Button) findViewById(R.id.button);
//        button.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Show the selected value of the numberpicker when the button is clicked
//                Toast.makeText(Main.this, "Selected value: " + nums[np.getValue()], Toast.LENGTH_SHORT).show();
//            }
//        });
//
        np_min.setOnValueChangedListener(this);
        np_max.setOnValueChangedListener(this);
        titleBar=new CustomActionBar.SelectLandTitleBar(this,"Land");
    }

    private String formatLand(int i) {
        if (i == 0)
            return "Any";
        else
            return i + " m²";

    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_land_selector, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i2) {
        switch (numberPicker.getId()) {
            case R.id.np_min:
                if (i2 > np_max.getValue()) {
                    np_max.setValue(i2 + 1);

                }
                break;
            case R.id.np_max:
                if (i2 < np_min.getValue()) {
                    np_min.setValue(i2 - 1);

                }
                break;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Refine.minL = landList[np_min.getValue()];
        Refine.maxL = landList[np_max.getValue()];
        SearchParams.landRangeMin=landList[np_min.getValue()]+"";
        SearchParams.landRangeMax=landList[np_max.getValue()]+"";
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }
    private int getIndexFromValue(long pValue) {
        int indx = 0;
        for (int i = 0; i < landList.length; i++) {
            if (pValue == landList[i]) {
                indx = i;
                break;
            }
        }
        return indx;
    }
}

package com.official.livebid.activities.liveAuction;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.RoomDetail;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.ping.PingManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class PropertyAuction extends AppCompatActivity implements AsyncInterface, View.OnClickListener, IXMPPRequest, PropertyAuctionAdapter.PlaceOnSiteBidListener, PlaceBidDialog.OnPlaceBidListener, PlaceOnSiteBidDialog.OnPlaceOnSiteBidListener {
    private RecyclerView rvPropertyAuction;
    //    private ListView rvPropertyAuction;
    private LinearLayoutManager linearLayoutManager;
    private PropertyAuctionAdapter propertyAuctionAdapter;
    private ArrayList<CurrentBidModel> alCurrentBids;
    private PropertyAuctionModel property;
    private Button btnPlaceVendorBid;
    private Button btnCallReminder;
    private Button btnPlaceBid;
    private long vendorBidAmount;
    private SharedPreference prefs;
    private AppMode appMode;
    RoomDetail roomDetail;
    private Alerts alerts;
    //xmpp variables
    XMPPTCPConnection connection;
    private ArrayList<String> messages = new ArrayList();
    private String text;
    private CurrentBidModel cb;
    private XMPPSettings xmppSettings;
    private boolean isPlacedVendorBid = false;
    private boolean isFirstCall = false;
    private boolean isSecondCall = false;
    private boolean isThirdCall = false;
    private boolean isSold = false;
    private boolean isPassed = false;
    public static long highestBidAmount = 0;
    public String highestBidBidder = "-";

    private CustomActionBar.PropertyAuctionCAB cab;
    private LiveBidXmlParser xmlParser;
    private boolean isWon = false;
    private LinearLayout cabContainer;
    private ImageView ibtnBack;
    private boolean isToolbarShown = false;
    private TextView tvCenter;
    private ImageView ibtnRight;
    private PlaceBidDialog placeBidDialog;
    private PlaceOnSiteBidDialog placeOnSiteBidDialog;
    private ArrayList<OnSiteBidder> onSiteBidders;
    private PingManager pingManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_auction);
        getSupportActionBar().hide();
        alerts = new Alerts(this);
        prefs = new SharedPreference(this);
        btnPlaceVendorBid = (Button) findViewById(R.id.btn_place_vendor_bid);
        btnCallReminder = (Button) findViewById(R.id.btn_call_reminder);
        btnPlaceBid = (Button) findViewById(R.id.btn_place_bid);
        btnPlaceBid.setTag(0);

        resetCallReminderButton();
        appMode = CommonMethods.getAppMode(this);
        cab = new CustomActionBar.PropertyAuctionCAB(this, appMode);
        cabContainer = (LinearLayout) findViewById(R.id.inc_toolbar);
        cabContainer.setBackgroundColor(Color.TRANSPARENT);
        ibtnBack = (ImageView) findViewById(R.id.ibtn_left);
        ibtnBack.setImageResource(R.drawable.back_icon);
        tvCenter = (TextView) findViewById(R.id.tv_center);

        tvCenter.setTextColor(Color.WHITE);
        ibtnRight = (ImageView) findViewById(R.id.ibtn_right);
        ibtnRight.setImageResource(R.drawable.ic_saved_white_icon);
        setAppMode(appMode);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            roomDetail = (RoomDetail) b.getSerializable(CommonDef.ROOM_DETAIL);
            property = (PropertyAuctionModel) b.getSerializable(CommonDef.PROP_AUCTION);
            highestBidAmount = 0;
//            property.highestBidAmount = "$" + NumberFormat.getNumberInstance(Locale.US).format(highestBidAmount);
            property.highestBidAmount = "-";
        }
        xmppSettings = new XMPPSettings(this);
        xmppSettings.connect();
        xmlParser = new LiveBidXmlParser(this, roomDetail);
        loadPreviousBids();
        btnPlaceVendorBid.setOnClickListener(this);
        btnCallReminder.setOnClickListener(this);
        btnPlaceBid.setOnClickListener(this);
        alCurrentBids = new ArrayList<>();


        rvPropertyAuction = (RecyclerView) findViewById(R.id.rv_property_auction);
        linearLayoutManager = new LinearLayoutManager(this);
        rvPropertyAuction.setLayoutManager(linearLayoutManager);
        rvPropertyAuction.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.computeVerticalScrollOffset() > 100) {
                    showToolbar();
                    isToolbarShown = true;
                } else {
                    hideToolbar();
                    isToolbarShown = false;
                }

            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }


    private void loadPreviousBids() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyAuction.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", property.propertyId);
        System.out.println("previous bids=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyAuction.this, Request.Method.POST, params, UrlHelper.FETCH_BIDS, headers, false, CommonDef.RC_FETCH_BIDS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void setAppMode(AppMode appMode) {
        if (appMode == AppMode.agent) {
            btnPlaceVendorBid.setVisibility(View.VISIBLE);
            btnPlaceBid.setVisibility(View.GONE);
        } else {
//            btnCallReminder.setVisibility(View.GONE);
            btnPlaceVendorBid.setVisibility(View.GONE);
            btnPlaceBid.setVisibility(View.VISIBLE);
        }
        if (appMode == AppMode.user) {
            if (CommonDef.canPlaceBid) {
                btnPlaceVendorBid.setVisibility(View.GONE);
                btnPlaceBid.setVisibility(View.GONE);
                btnCallReminder.setVisibility(View.GONE);
//                btnPlaceBid.setEnabled(false);
            }
        }
//        btnPlaceVendorBid.setVisibility(View.VISIBLE);
    }

    private void resetCallReminderButton() {
        btnCallReminder.setTag(3);
        btnCallReminder.setText(updateCallReminderMessage(1));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_call_reminder:
                int bidType = (int) view.getTag();
                if (bidType == 9) {// send report

                    HashMap<String, String> headers = CommonMethods.getHeaders(this);
                    HashMap<String, String> params = new HashMap<>();
                    params.put("property_id", property.propertyId);

                    System.out.println("registered bidders params=>" + params.toString());
                    VolleyRequest volleyRequest = new VolleyRequest(PropertyAuction.this, Request.Method.POST, params, UrlHelper.GET_PROPERTY_BID_DTL, headers, false, CommonDef.REQ_PROPERTY_BID_DLT);
                    volleyRequest.asyncInterface = this;
                    volleyRequest.request();
//                    alerts.showOkMessage("Not available");
                    return;
                }
                placeBid(bidType, highestBidAmount + "", "");
                break;
            case R.id.btn_place_vendor_bid:
                showVendorBidPopUp();
                break;
            case R.id.btn_place_bid:
                int action = (int) view.getTag();
                if (action == 10)//contact agent
                    Opener.contactAgent(PropertyAuction.this, property.agentId);
                else //placebid 0->userbid 1-> onsite bid
//                    showPlaceBidPopUp(0);
                    showPlaceBidFialog();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), "canceled", Toast.LENGTH_SHORT).show();
        }
        if (requestCode == RESULT_OK) {
            if (requestCode == CommonDef.RC_PLACE_BID) {
                Toast.makeText(getApplicationContext(), "bid Placed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showAlertReminder(String msg) {
        View view = getLayoutInflater().inflate(R.layout.layout_confirm_alert, null);
        Button btnOk = (Button) view.findViewById(R.id.btn_ok);
        TextView tvMsg = (TextView) view.findViewById(R.id.tv_msg);
        tvMsg.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_bell_white), null, null, null);
        tvMsg.setText(msg);
        final Dialog mBottomSheetDialog = new Dialog(PropertyAuction.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

            }
        });
    }

    private String updateCallReminderMessage(int val) {
        if (val == 1) {
            return "1st Call Reminder";

        }
        if (val == 2) {
            return "2nd Call Reminder";

        }
        if (val == 3) {
            return "Last Call Reminder";

        }
        if (val == 4) {
            return "Sell To Highest Bidder";

        }
        if (val == 5) {
            return "Export Summary Report";

        }
        return null;
    }


    private void updateCallReminderButton(CurrentBidModel.MessageType bidType, CurrentBidModel bidModel) {
        if (bidType == CurrentBidModel.MessageType.firstCAll) {
            btnCallReminder.setTag(4);
            btnCallReminder.setText(updateCallReminderMessage(2));
        } else if (bidType == CurrentBidModel.MessageType.secondCall) {
            btnCallReminder.setTag(5);
            btnCallReminder.setText(updateCallReminderMessage(3));
        } else if (bidType == CurrentBidModel.MessageType.thirdCall) {
            btnCallReminder.setTag(7);
            btnCallReminder.setText(updateCallReminderMessage(4));
        } else if (bidType == CurrentBidModel.MessageType.propertySold) {
            if (appMode == AppMode.agent) {
                btnCallReminder.setTag(9);//send report
                btnCallReminder.setText(updateCallReminderMessage(5));//send teport
                btnCallReminder.setEnabled(true);
                btnPlaceVendorBid.setEnabled(false);
            } else {
                btnCallReminder.setVisibility(View.GONE);
            }
            if (bidModel.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(getApplicationContext()))) {
                btnPlaceBid.setEnabled(true);
            } else {
                btnPlaceBid.setEnabled(false);
            }
//            btnPlaceBid.setEnabled(false);
            cab.desablePassProperty();
        } else if (bidType == CurrentBidModel.MessageType.propertyPassedIn) {
            btnCallReminder.setEnabled(false);
            btnPlaceVendorBid.setEnabled(false);
            btnPlaceBid.setEnabled(false);
            cab.desablePassProperty();
        } else {
            resetCallReminderButton();
        }
    }

    private void showVendorBidPopUp() {
        View view = getLayoutInflater().inflate(R.layout.layout_vendor_bid_popup, null);
        final Button btnConfirm = (Button) view.findViewById(R.id.btn_confirm);
        final Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        final EditText etOfferPrice = (EditText) view.findViewById(R.id.et_offer_price);
        long highest = highestBidAmount;
        String formattedamount = "$" + NumberFormat.getNumberInstance(Locale.US).format((highest));
        etOfferPrice.setText(formattedamount);
        etOfferPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {
                    etOfferPrice.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,]", "");


                    String formatted = "";
                    if (cleanString.equalsIgnoreCase("") || cleanString.equalsIgnoreCase("0"))
                        formatted = "";
                    else {
                        vendorBidAmount = Long.parseLong(cleanString);
                        formatted = "$" + NumberFormat.getNumberInstance(Locale.US).format((vendorBidAmount));
                    }
                    current = formatted;
                    etOfferPrice.setText(formatted);
                    etOfferPrice.setSelection(formatted.length());

                    etOfferPrice.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();
                showVendorBidConfirmation();

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();

            }
        });


    }

    public void showPassPropertyPopup() {
        View view = getLayoutInflater().inflate(R.layout.layout_vendor_bid_popup, null);
        final Button btnConfirm = (Button) view.findViewById(R.id.btn_confirm);
        final Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnCancel.setVisibility(View.GONE);
        final EditText etOfferPrice = (EditText) view.findViewById(R.id.et_offer_price);
        final TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        final View sep = view.findViewById(R.id.view_sep);
        sep.setVisibility(View.GONE);
        tvTitle.setText("Reason for closing");
        etOfferPrice.setHint("Passed in");
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(100);
        etOfferPrice.setFilters(fArray);
        etOfferPrice.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etOfferPrice.getText().toString().isEmpty()) {
                    etOfferPrice.setError("Reason should not be empty");
                    return;
                }
                passProperty(etOfferPrice.getText().toString());
                mBottomSheetDialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();

            }
        });


    }

    private void showVendorBidConfirmation() {
        View view = getLayoutInflater().inflate(R.layout.layout_vendor_bid_confirm, null);
        final Button btnYes = (Button) view.findViewById(R.id.btn_confirm);
        final Button btnNo = (Button) view.findViewById(R.id.btn_cancel);
        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeBid(2, vendorBidAmount + "", "");//vendor Bid
                mBottomSheetDialog.dismiss();

            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();

            }
        });


    }


    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.RC_FETCH_BIDS:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONArray jaList = joResult.getJSONArray("live_conversation");
                        if (jaList.length() > 0) {
                            CurrentBidModel bm;
                            JSONObject joBid;
                            alCurrentBids = new ArrayList<>();
                            for (int i = 0; i < jaList.length(); i++) {
                                joBid = jaList.getJSONObject(i);
                                bm = new CurrentBidModel();
                                bm.msgId = joBid.getString("id");
                                bm.setUserUniqueId(joBid.getString("user_unique_id"));
                                bm.setBidAmount(joBid.getString("bid_amount"));
                                bm.setBidType(joBid.getInt("bid_type"));
                                bm.setBidTime(joBid.getLong("bid_timestamp"));
                                bm.status = joBid.getString("status");
                                bm.userId = joBid.getString("user_id");
                                bm.startDateTime = roomDetail.startDateTime;
                                updateCallReminderButton(bm.bidType, bm);
                                highestBidBidder = setHighestBidder(bm);
                                property.highestBidder = highestBidBidder;
                                if (i != 0) {
                                    property.highestBidAmount = bm.bidAmount;
                                    highestBidAmount = (long) Math.ceil(Double.parseDouble(joBid.getString("bid_amount")));
                                }
                                if (bm.bidType == CurrentBidModel.MessageType.propertySold) {
                                    if (appMode == AppMode.user && bm.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(getApplicationContext()))) {
                                        isWon = true;
                                        btnPlaceBid.setEnabled(true);
                                        btnPlaceBid.setTag(10);//contact agent
                                        btnPlaceBid.setText("Contact Agent");
                                    }
                                    isSold = true;
                                    showWinnerPopup(bm);

                                } else if (bm.bidType == CurrentBidModel.MessageType.propertyPassedIn) {
                                    showPassedInPopup(bm);
                                    isPassed = true;
                                } else {

                                }
                                alCurrentBids.add(0, bm);
                            }
                            propertyAuctionAdapter = new PropertyAuctionAdapter(this, alCurrentBids, property, appMode, isWon, this);
                            rvPropertyAuction.setAdapter(propertyAuctionAdapter);
                        }
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_SEND_VENDOR_BID:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        // success

                        isPlacedVendorBid = true;
                        resetCallReminderButton();
                        disableVendorBid();
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_SEND_GENERAL_BID:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        // success
                        isFirstCall = false;
                        isSecondCall = false;
                        isThirdCall = false;
                        resetCallReminderButton();
                        placeBidDialog.dismissDialog();
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_SEND_ON_SITE_BID:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        // success
                        isFirstCall = false;
                        isSecondCall = false;
                        isThirdCall = false;
                        placeOnSiteBidDialog.dismissDialog();
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_SEND_FIRST_CALL:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        // success
                        isFirstCall = true;
//                        showAlertReminder(updateCallReminderMessage(1));
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_SEND_SECOND_CALL:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        // success
                        isSecondCall = true;
//                        showAlertReminder(updateCallReminderMessage(2));
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_SEND_THIRD_CALL:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        // success
                        isThirdCall = true;
//                        showAlertReminder(updateCallReminderMessage(3));
//                        updateCallreminderButton();
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_PASS_PROPERTY:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        // success
                        isPassed = true;
                        btnCallReminder.setEnabled(false);
                        btnPlaceBid.setEnabled(false);
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_SOLD_PROPERTY:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        // success
                        isSold = true;
                        cab.desablePassProperty();
                        btnPlaceBid.setEnabled(false);
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.REQ_REG_BIDDERS:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        onSiteBidders = new OnSiteBidder(Parcel.obtain()).getOnSiteBidders(joResult.getJSONArray("list"));
                        showOnsiteBidDialog(onSiteBidders);
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.REQ_PROPERTY_BID_DLT:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    alerts.showOkMessage(msg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

        }


    }

    private String setHighestBidder(CurrentBidModel bm) {
        String hb = highestBidBidder;
        if (bm.bidType == CurrentBidModel.MessageType.vendorBid) {
            isPlacedVendorBid = true;
            hb = "Vendor";
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    disableVendorBid();
                }
            });
        } else if (bm.bidType == CurrentBidModel.MessageType.onSiteBid) {
//            hb = "On-Site";
            hb = bm.userUniqueId + "(On-Site)";
        } else if (bm.bidType == CurrentBidModel.MessageType.generalBid)
            if (bm.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(getApplicationContext())))
                hb = "You";
            else
                hb = bm.userUniqueId;
        else {

        }
        return hb;
    }

    private void disableVendorBid() {
        if (isPlacedVendorBid)
            btnPlaceVendorBid.setVisibility(View.GONE);

    }

    @Override
    public void onBackPressed() {
        if(appMode==AppMode.agent) {
            if (!isSold && !isPassed) {
                showOnbackConfirmation();
                return;
            }
        }
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);

    }

    void showOnbackConfirmation() {
        new AlertDialog.Builder(PropertyAuction.this)
                .setTitle("Are You Sure?")
                .setMessage("The property has not been sold yet.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        finish();
                        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
        ;
    }


    @Override
    public void XMPPConSuccess(XMPPTCPConnection connection) {
        if (connection != null) {
            System.out.println("Testing testing on success:");
            setConnection(connection);
        } else
            Toast.makeText(getApplicationContext(), "Connection failure", Toast.LENGTH_LONG).show();
    }

    @Override
    public void XMPPConFailure(Exception ex) {
        Log.d("XMPP ERROR", ex.getMessage());
    }

    @Override
    public void onReceiveMessage(Message message) throws IOException, XmlPullParserException {
        System.out.println("My message: " + message.toString().replace("&lt;", "<").replace("&gt;", ">"));
        String xmlString = message.toString().replace("&lt;", "<").replace("&gt;", ">");
        CurrentBidModel bidModel = xmlParser.parseXML(xmlString);
        if (!propertyAuctionAdapter.isDuplicate(bidModel.msgId)) {
            try {
                highestBidAmount = Long.parseLong(bidModel.bidAmount.replaceAll("[$,]", ""));

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            property.highestBidAmount = "$" + NumberFormat.getNumberInstance(Locale.US).format((long) highestBidAmount);
            ;
            highestBidBidder = setHighestBidder(bidModel);
            property.highestBidder = highestBidBidder;
            pushCurrentBid(bidModel, property);
        }
    }

    private void pushCurrentBid(final CurrentBidModel bidModel, final PropertyAuctionModel property) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (bidModel.bidType == CurrentBidModel.MessageType.firstCAll) {
                    showAlertReminder(updateCallReminderMessage(1));
                } else if (bidModel.bidType == CurrentBidModel.MessageType.secondCall)
                    showAlertReminder(updateCallReminderMessage(2));
                else if (bidModel.bidType == CurrentBidModel.MessageType.thirdCall)
                    showAlertReminder(updateCallReminderMessage(3));
                else if (bidModel.bidType == CurrentBidModel.MessageType.propertySold) {
                    if (bidModel.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(getApplicationContext()))) {
                        isWon = true;
                        btnPlaceBid.setEnabled(true);
                        btnPlaceBid.setTag(10);//contact agent
                        btnPlaceBid.setText("Contact Agent");
                    }
                    isSold = true;
                    showWinnerPopup(bidModel);
                } else if (bidModel.bidType == CurrentBidModel.MessageType.propertyPassedIn) {
                    isPassed = true;
                    showPassedInPopup(bidModel);
                } else {

                }
                propertyAuctionAdapter.addMessage(bidModel, property, isWon);
                updateCallReminderButton(bidModel.bidType, bidModel);
            }
        });
    }

    private void showPassedInPopup(CurrentBidModel bidModel) {
        alerts.showOkMessage("This property is closed.\n" + "for reason " + bidModel.bidAmount);
    }

    private void showWinnerPopup(CurrentBidModel bm) {
        String msg = "";
        if (appMode == AppMode.user) {
            if (bm.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(getApplicationContext()))) {
                msg = "Congratulation!! You have purchased the property for " + bm.bidAmount;
            } else {
                msg = "Sorry you were not the highest bidder. Better luck next time.";
            }
        } else {
            msg = "Auction completed successfully.";
        }
        alerts.showOkMessage(msg);
    }

    /**
     * Called by Settings dialog when a connection is establised with the XMPP server
     *
     * @param connection
     */
    public void setConnection(XMPPTCPConnection connection) {
        Presence presence = new Presence(Presence.Type.available);
        try {
            connection.sendPacket(presence);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
        xmppSettings.joinMultiUserRoom(connection, roomDetail.roomName);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        xmppSettings.disconnect();
    }


    public void placeBid(int bidType, String bidAmount, String bidderId) {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyAuction.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("send_to", roomDetail.roomName);
        params.put("property_auction_id", roomDetail.auctionId);
        params.put("bid_type", bidType + "");
        params.put("bid_amount", bidAmount);
        params.put("bidder_id", bidderId);
        params.put("temp-id", "3019");
        System.out.println("send bids=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyAuction.this, Request.Method.POST, params, UrlHelper.SEND_BID, headers, false, getRequestCode(bidType));
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }

    private int getRequestCode(int bidType) {
        int rc = 0;
        switch (bidType) {
            case 0:
                rc = CommonDef.RC_SEND_GENERAL_BID;
                break;
            case 1:
                rc = CommonDef.RC_SEND_ON_SITE_BID;
                break;
            case 2:
                rc = CommonDef.RC_SEND_VENDOR_BID;
                break;
            case 3:
                rc = CommonDef.RC_SEND_FIRST_CALL;
                break;
            case 4:
                rc = CommonDef.RC_SEND_SECOND_CALL;
                break;
            case 5:
                rc = CommonDef.RC_SEND_THIRD_CALL;
                break;
            case 6:
                rc = CommonDef.RC_PASS_PROPERTY;
                break;
            case 7:
                rc = CommonDef.RC_SOLD_PROPERTY;
                break;
            default:
                rc = CommonDef.RC_SEND_GENERAL_BID;
                break;
        }
        return rc;
    }

    @Override
    public void onPlaceOnSiteBid() {
//        placeBid(1, amount + "");
//        showPlaceBidPopUp(1);
        if (onSiteBidders != null) {
            showOnsiteBidDialog(onSiteBidders);
            return;
        }
        getRegisteredBidders();
    }

    @Override
    public void onCancelOnSiteBid() {
    }

    private void showPlaceBidFialog() {
        FragmentManager fm = getSupportFragmentManager();
        placeBidDialog = PlaceBidDialog.newInstance(highestBidAmount, property.getPrice());
        placeBidDialog.show(fm, "placeBid");
    }

    private void showOnsiteBidDialog(ArrayList<OnSiteBidder> bidders) {
        FragmentManager fm = getSupportFragmentManager();
        placeOnSiteBidDialog = PlaceOnSiteBidDialog.newInstance(highestBidAmount, property.getPrice(), bidders);
        placeOnSiteBidDialog.show(fm, "onSiteBid");
    }

    public void passProperty(String msg) {
        placeBid(6, msg, "");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    public void showToolbar() {
        if (isToolbarShown)
            return;
        ibtnBack.setImageResource(R.drawable.left_arrow);
        tvCenter.setTextColor(getResources().getColor(R.color.theme_blue));
        ibtnRight.setImageResource(R.drawable.ic_saved_black_icon);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(cabContainer, "backgroundColor", new ArgbEvaluator(), Color.argb(0, 255, 255, 255), Color.argb(255, 255, 255, 255));//getResources().getColor(R.color.theme_yellow)
        colorFade.setDuration(1000);
        colorFade.start();
    }


    public void hideToolbar() {
        if (!isToolbarShown)
            return;
        ibtnBack.setImageResource(R.drawable.back_icon);
        tvCenter.setTextColor(Color.WHITE);
        ibtnRight.setImageResource(R.drawable.ic_saved_white_icon);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(cabContainer, "backgroundColor", new ArgbEvaluator(), Color.argb(255, 255, 255, 255), Color.argb(0, 255, 255, 255));
        colorFade.setDuration(1000);
        colorFade.start();
    }

    @Override
    public void onPlaceBid(long amount) {
        //  bidtype 0->general bid
        placeBid(0, amount + "", "");
    }


    @Override
    public void onPlaceOnSiteBid(long amount, String bidderId) {
        placeBid(1, amount + "", bidderId);
    }

    private void getRegisteredBidders() {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", property.propertyId);
        params.put("registered_from_cms", "1");//0->default 1-> onsiteb bidders
        System.out.println("registered bidders params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyAuction.this, Request.Method.POST, params, UrlHelper.GET_REG_BIDDERS, headers, false, CommonDef.REQ_REG_BIDDERS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }
}

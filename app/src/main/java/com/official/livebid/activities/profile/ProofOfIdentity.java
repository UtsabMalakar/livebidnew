package com.official.livebid.activities.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.official.livebid.R;

/**
 * Created by prajit on 8/24/16.
 */
public class ProofOfIdentity extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_proof_of_identity,null);
        return view;
    }
}

package com.official.livebid.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.official.livebid.R;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;

public class GuestNotifications extends MenuActivity implements View.OnClickListener {
    Button btnSignup;
    private CustomActionBar.TitleBarWithBackAndTitle guestNotificationTitlebar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_guest_notifications;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_guest_notifications);
        btnSignup= (Button) findViewById(R.id.btn_sign_up);
        btnSignup.setOnClickListener(this);
        guestNotificationTitlebar=new CustomActionBar.TitleBarWithBackAndTitle(this,getResources().getString(R.string.notification));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_sign_up:
//                Opener.Register(GuestNotifications.this);
                Opener.newRegister(GuestNotifications.this, UserType.NEW);
                break;
        }
    }
}

package com.official.livebid.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.helpers.Opener;
import com.official.livebid.listeners.FavouriteCompleteListener;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.PropertyLists;
import com.official.livebid.utils.GPSTracker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    GPSTracker gpsTracker;
    double lat = 0, lng = 0;
    Marker clickedMarker;
    private boolean isLocationSet = false;
    RelativeLayout rlMapDetails;
    ImageView ivPropertyImg;
    TextView tvPropertyStreet, tvPropertySuburbPostcode,
            tvPrice, tvNosBedroom, tvNosBathroom,
            tvNosParking;
    private CheckBox chkFav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        new CustomActionBar.MapsActivityTitleBar(MapsActivity.this, "Search Results");
        gpsTracker = new GPSTracker(MapsActivity.this);
        if (gpsTracker.canGetLocation()) {
            lat = gpsTracker.getLatitude();
            lng = gpsTracker.getLongitude();
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        ArrayList<Marker> markers = new ArrayList<>();

        if (PropertyLists.alPropertyListInfo != null && !PropertyLists.alPropertyListInfo.isEmpty()) {
            for (PropertyListInfo propertyListsInfo : PropertyLists.alPropertyListInfo) {
                if (propertyListsInfo.lat != null && !propertyListsInfo.lat.isEmpty()) {
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(propertyListsInfo.lat), Double.parseDouble(propertyListsInfo.lng)))
                            .anchor(0.5f, 0.5f)
                            .snippet(PropertyLists.alPropertyListInfo.indexOf(propertyListsInfo) + "")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_inside_home_icon)));
                    markers.add(marker);
                }
            }
            manageMapView(markers);
        } else{
            LatLng latLng = new LatLng(lat, lng);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    private void manageMapView(ArrayList<Marker> markers) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        // Then obtain a movement description object by using the factory: CameraUpdateFactory:
        int padding = (int) CommonMethods.pxFromDp(MapsActivity.this, 50); // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.animateCamera(cu);

        rlMapDetails = (RelativeLayout) findViewById(R.id.rl_map_details);
        rlMapDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickedMarker != null) {
                    PropertyListInfo propertyListInfo = PropertyLists.alPropertyListInfo.get(Integer.parseInt(clickedMarker.getSnippet()));
                    Opener.PropertyDetails(MapsActivity.this, propertyListInfo);
                }
            }
        });
        initMapDetailsView();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (clickedMarker != null)
                    clickedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_inside_home_icon));

                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon));
                clickedMarker = marker;

                PropertyListInfo propertyListInfo = PropertyLists.alPropertyListInfo.get(Integer.parseInt(marker.getSnippet()));
                setMarkerDetails(propertyListInfo);
                if (rlMapDetails.getVisibility() != View.VISIBLE) {
                    Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_up);
                    rlMapDetails.startAnimation(bottomUp);
                    rlMapDetails.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (clickedMarker != null) {
                    Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
                    rlMapDetails.startAnimation(slideDown);
                    rlMapDetails.setVisibility(View.INVISIBLE);
                    clickedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_inside_home_icon));
                    clickedMarker = null;
                }
            }
        });
    }

    private void setMarkerDetails(final PropertyListInfo propertyInfo) {
        tvPropertyStreet.setText(propertyInfo.street);
        tvPropertySuburbPostcode.setText(propertyInfo.suburb + ", " + propertyInfo.postCode);
        tvPrice.setText(propertyInfo.price);
        tvNosBathroom.setText(propertyInfo.nosOfBathroom);
        tvNosBedroom.setText(propertyInfo.nosOfBedroom);
        tvNosParking.setText(propertyInfo.nosOfParking);

        if (!propertyInfo.propertyImgUrl.isEmpty()) {
            Picasso.with(getApplicationContext())
                    .load(propertyInfo.propertyImgUrl)
                    .resize((int) CommonMethods.pxFromDp(MapsActivity.this, 150), (int) CommonMethods.pxFromDp(MapsActivity.this, 150))
                    .centerCrop()
                    .into(ivPropertyImg);
        } else {
            Picasso.with(getApplicationContext())
                    .load(R.drawable.img_coming_soon)
                    .resize((int) CommonMethods.pxFromDp(MapsActivity.this, 150), (int) CommonMethods.pxFromDp(MapsActivity.this, 150))
                    .centerCrop()
                    .into(ivPropertyImg);
        }
        chkFav.setChecked(propertyInfo.getFav());
        chkFav.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final CheckBox cboxTemp = (CheckBox) view;
                        new FavoriteHelper(MapsActivity.this).makeFavorite(propertyInfo.id, propertyInfo.getFav(), new FavouriteCompleteListener() {
                            @Override
                            public void onSuccess(boolean b) {
                                 int position = 0;
                                for (int i = 0; i < PropertyLists.alPropertyListInfo.size(); i++) {
                                    if (propertyInfo.id.equalsIgnoreCase(PropertyLists.alPropertyListInfo.get(i).id)) {
                                        position = i;
                                    }
                                }
                                PropertyLists.alPropertyListInfo.get(position).setFav(b);
                            }

                            @Override
                            public void onFailure() {
                                cboxTemp.setChecked(propertyInfo.getFav());
                            }

                            @Override
                            public void onNotLoggedIn() {
                                cboxTemp.setChecked(false);
                                new Alerts(MapsActivity.this).signIn();
                            }
                        });


                        // Webservice call for change.
                    }
                });
    }

    private void initMapDetailsView() {
        ivPropertyImg = (ImageView) rlMapDetails.findViewById(R.id.iv_property_img);
        tvPropertyStreet = (TextView) rlMapDetails.findViewById(R.id.tv_property_street);
        tvPropertySuburbPostcode = (TextView) rlMapDetails.findViewById(R.id.tv_property_suburb_postcode);
        tvPrice = (TextView) rlMapDetails.findViewById(R.id.tv_property_price);
        tvNosBathroom = (TextView) rlMapDetails.findViewById(R.id.tv_nosBathroom);
        tvNosBedroom = (TextView) rlMapDetails.findViewById(R.id.tv_nosBedroom);
        tvNosParking = (TextView) rlMapDetails.findViewById(R.id.tv_nosParking);
        chkFav = (CheckBox) rlMapDetails.findViewById(R.id.cbox_fav);
        com.official.livebid.enums.AppMode appMode = CommonMethods.getAppMode(this);
        if (appMode == AppMode.guest){
            chkFav.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gpsTracker.canGetLocation()) {
            lat = gpsTracker.getLatitude();
            lng = gpsTracker.getLongitude();
            isLocationSet = true;
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gpsTracker.stopUsingGPS();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gpsTracker.stopUsingGPS();
    }
}

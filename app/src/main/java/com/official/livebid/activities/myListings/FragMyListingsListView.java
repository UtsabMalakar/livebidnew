package com.official.livebid.activities.myListings;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.adapters.MyListingAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.PropertyLists;
import com.official.livebid.utils.EndlessRecyclerViewScrollListener;
import com.official.livebid.utils.GPSTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragMyListingsListView extends Fragment implements AsyncInterface {
    RecyclerView rvMyListing;
    ImageButton ibtnSearch;
    ArrayList<PropertyListInfo> alPropertyListInfo;
    double lat, lng;
    boolean isLocationSet = false;
    GPSTracker gps;
    Alerts alerts;
    private LinearLayoutManager linearLayoutManager;
    private MyListingAdapter myListingAdapter;
    private boolean isLastPage = false;
    private int offset = 0;
    private SharedPreference prefs;


    public FragMyListingsListView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view= inflater.inflate(R.layout.fragment_frag_my_listings_list_view, container, false);
        init(view);
        getPropertyList(false);
        return view;
    }

    private void init(View view) {
        alerts=new Alerts(getActivity());
        prefs=new SharedPreference(getActivity());
        rvMyListing = (RecyclerView)view.findViewById(R.id.rc_my_listings);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvMyListing.setLayoutManager(linearLayoutManager);
        rvMyListing.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {


            @Override
            public void onLoadMore(int current_page) {
                if (!isLastPage) {
                    getPropertyList(true);
                }
            }
        });

        ibtnSearch = (ImageButton)view.findViewById(R.id.ibtn_search);
        ibtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Opener.Search(getActivity(),"");
            }
        });
    }


    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_PROPERTY_LISTINGS:
                System.out.println("Result " + result);
                initPropertyList(result);
                break;
        }
    }
    private void getPropertyList(boolean hidePd) {
        HashMap<String, String> headers = CommonMethods.getHeaders(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", prefs.getStringValues(CommonDef.SharedPrefKeys.AGENT_ID));
        params.put("listing_type", "1");
        params.put("offset", offset + "");
        params.put("lat", "0");
        params.put("lng", "0");
        System.out.println("property Listings params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.POST, params, UrlHelper.GET_PROPERTY_LISTINGS, headers, false, CommonDef.REQUEST_PROPERTY_LISTINGS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(hidePd);
    }
    private void initPropertyList(String result) {

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            PropertyLists.alPropertyListInfo = new ArrayList<>();
            if (code.equals("0001")) {
                JSONArray jaList = joResult.getJSONArray("list");
                alPropertyListInfo = new ArrayList<>();
                if (jaList.length() > 0) {
                    for (int i = 0; i < jaList.length(); i++) {
                        JSONObject joList = jaList.getJSONObject(i);
                        PropertyListInfo propertyListInfo = new PropertyListInfo();
                        propertyListInfo.id = joList.getString("id");
                        propertyListInfo.agentId = joList.getString("agent_id");
                        propertyListInfo.agencyId = joList.getString("agency_id");
                        propertyListInfo.auctionDate = joList.getString("auction_date");
                        propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
                        propertyListInfo.agentFName = joList.getString("agent_name");
                        propertyListInfo.agentLName = joList.getString("agent_name");
                        propertyListInfo.propertyStatus = joList.getInt("property_status");
                        propertyListInfo.statusTag = "";
                        propertyListInfo.dateTag = "Auction due to 40 days";
                        propertyListInfo.setFav(joList.getString("favorite"));
                        propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
                        propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
                        propertyListInfo.nosOfParking = joList.getString("parking");
                        propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(joList.getString("price")));
                        propertyListInfo.propertyStatusIdentifier = "from";
                        propertyListInfo.price_text=joList.getString("price_text");
                        propertyListInfo.suburb = joList.getString("suburb");
                        propertyListInfo.postCode = joList.getString("postcode");
                        propertyListInfo.propertyTitle = joList.getString("property_title");
                        propertyListInfo.propertyCode = joList.getString("property_code");
                        propertyListInfo.propertyDesc = joList.getString("property_description");
                        propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
                        propertyListInfo.street = joList.getString("street");
                        propertyListInfo.propertyState = joList.getString("state");
                        propertyListInfo.propertyImgUrl = joList.getString("property_image");
                        propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
                        propertyListInfo.distance = joList.getString("distance");
                        propertyListInfo.lat = joList.getString("lat");
                        propertyListInfo.lng = joList.getString("lng");
                        propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
                        propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
                        propertyListInfo.auctionDateTimeStamp = joList.getLong("auction_date_timestamp");
                        propertyListInfo.propertyStatus2 = joList.getInt("property_status_2");
                        JSONArray JPropertyImageMore = joList.getJSONArray("property_images_more");
                        if (JPropertyImageMore.length() > 0) {
                            for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                propertyListInfo.property_images_more.add(JPropertyImageMore.getString(j));
                            }
                        }
                        alPropertyListInfo.add(propertyListInfo);
                    }
                    isLastPage = joResult.getBoolean("last_page");
                    if (myListingAdapter == null) {
                        myListingAdapter = new MyListingAdapter(getActivity(), alPropertyListInfo);
                        rvMyListing.setAdapter(myListingAdapter);
                    } else {
                        myListingAdapter.addMoreData(alPropertyListInfo);

                    }
                    offset = joResult.getInt("offset");
                }
            } else {
                alerts.showOkMessage(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

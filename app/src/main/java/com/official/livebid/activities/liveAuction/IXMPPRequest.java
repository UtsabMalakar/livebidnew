package com.official.livebid.activities.liveAuction;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public interface IXMPPRequest {
        void XMPPConSuccess(XMPPTCPConnection connection);
        void XMPPConFailure(Exception ex);
        void onReceiveMessage(Message message) throws IOException, XmlPullParserException;
    }
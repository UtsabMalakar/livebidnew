package com.official.livebid.activities.myBids;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.RoomDetail;
import com.official.livebid.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MyRegisteredProperty extends MenuActivity implements AsyncInterface {

    private ArrayList<RegisteredPropertyModel> registeredProperties;
    private Alerts alerts;
    private int offset = 0;
    private boolean isLastPage = false;
    private RegisteredPropertyAdapter registeredPropertyAdapter;
    private RegisteredPropertyModel propertyListInfo;
    private RecyclerView rvRegisteredProperties;
    private LinearLayoutManager linearLayoutManager;
    private RegisteredPropertyModel registeredProperty;
    int selector;

    @Override
    public int getLayoutId() {
        return R.layout.activity_activity_registered_properties;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
//        setContentView(R.layout.activity_activity_registered_properties);
        alerts = new Alerts(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            selector = b.getInt(CommonDef.MY_BID_SELECTOR);
        }
        new CustomActionBar.MyRegisteredPropertyCAB(this, selector);
        rvRegisteredProperties = (RecyclerView) findViewById(R.id.rc_registered_property);
        linearLayoutManager = new LinearLayoutManager(this);
        rvRegisteredProperties.setLayoutManager(linearLayoutManager);
        rvRegisteredProperties.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (!isLastPage) {
                    getRegisteredProperties();
                }

            }
        });
        getRegisteredProperties();
    }

    private void getRegisteredProperties() {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("offset", offset + "");
        Log.d("regd prop", params.toString());
        String url = getMyBidsUrl(selector);
        VolleyRequest volleyRequest = new VolleyRequest(MyRegisteredProperty.this, Request.Method.POST, params, url, headers, false, CommonDef.RC_MY_REGISTERED_PROPERTIES);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private String getMyBidsUrl(int selector) {
        if (selector == CommonDef.MY_BIDS_BIDS)
            return UrlHelper.MY_BID_BIDS;
        else if (selector == CommonDef.MY_BIDS_OFFERS)
            return UrlHelper.MY_BID_OFFERS;
        else if (selector == CommonDef.MY_BIDS_REGISTERED_PROPERTY)
            return UrlHelper.MY_REGISTERED_PROPERTIES;
        else return "";

    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.RC_MY_REGISTERED_PROPERTIES:
                System.out.println("Result " + result);
                initPropertyList(result);

                break;
            case CommonDef.RC_JOIN_LIVE_AUCTION:
                System.out.println("join live auction" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONObject jObjRoomDetail = joResult.getJSONObject("room_detail");
                        RoomDetail roomDetail = new RoomDetail();
                        roomDetail.auctionId = jObjRoomDetail.getString("id");
                        roomDetail.propertyId = jObjRoomDetail.getString("property_id");
                        roomDetail.roomName = jObjRoomDetail.getString("room_name");
                        roomDetail.callCount = jObjRoomDetail.getInt("count_call");
                        roomDetail.soldTo = jObjRoomDetail.getString("sold_to");
                        roomDetail.soldAmount = jObjRoomDetail.getString("sold_amount");
                        roomDetail.status = jObjRoomDetail.getString("status");
                        roomDetail.startDateTime = jObjRoomDetail.getString("auction_start_date_time_stamp");
                        Opener.registeredPropertyDetail(MyRegisteredProperty.this, registeredProperty, selector);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
        }
    }

    private void initPropertyList(String result) {

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            if (code.equals("0001")) {
                JSONArray jaList = joResult.getJSONArray("list");
                if (jaList.length() > 0) {
                    String currentBidAmount="";
                    registeredProperties = new ArrayList<>();
                    for (int i = 0; i < jaList.length(); i++) {
                        JSONObject joList = jaList.getJSONObject(i);
                        propertyListInfo = new RegisteredPropertyModel();
                        propertyListInfo.id = joList.getString("id");
                        propertyListInfo.agentId = joList.getString("agent_id");
                        propertyListInfo.agencyId = joList.getString("agency_id");
                        propertyListInfo.auctionDate = joList.getString("auction_date");
                        propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
                        propertyListInfo.agentFName = joList.getString("agent_name");
                        propertyListInfo.agentLName = joList.getString("agent_name");
                        propertyListInfo.propertyStatus = joList.getInt("property_status");
                        propertyListInfo.dateTag = "Auction due to 40 days";
                        propertyListInfo.setFav(joList.getString("favorite"));
                        propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
                        propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
                        propertyListInfo.nosOfParking = joList.getString("parking");
                        propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((long) Double.parseDouble(joList.getString("price")));
                        propertyListInfo.propertyStatusIdentifier = "from";
                        propertyListInfo.price_text=joList.getString("price_text");
                        propertyListInfo.suburb = joList.getString("suburb");
                        propertyListInfo.postCode = joList.getString("postcode");
                        propertyListInfo.propertyTitle = joList.getString("property_title");
                        propertyListInfo.propertyDesc = joList.getString("property_description");
                        propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
                        propertyListInfo.street = joList.getString("street");
                        propertyListInfo.propertyState = joList.getString("state");
                        propertyListInfo.propertyImgUrl = joList.getString("property_image");
                        propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
                        propertyListInfo.distance = joList.getString("distance");
                        propertyListInfo.lat = joList.getString("lat");
                        propertyListInfo.lng = joList.getString("lng");
                        propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
                        propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
                        propertyListInfo.auctionDateTimeStamp = joList.getLong("auction_date_timestamp");
                        propertyListInfo.propertyStatus2 = joList.getInt("property_status_2");
                        propertyListInfo.property_images_more = new ArrayList<>();

                        JSONArray JPropertyImageMore = joList.getJSONArray("property_images_more");
                        if (JPropertyImageMore.length() > 0) {
                            for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                propertyListInfo.property_images_more.add(JPropertyImageMore.getString(j));
                            }
                        }
                        if (joList.has("max_bid_amount"))
                            currentBidAmount = joList.getString("max_bid_amount");
                        propertyListInfo.currentBidAmount = "";
                        if (!currentBidAmount.isEmpty())
                            propertyListInfo.currentBidAmount = "$" + NumberFormat.getNumberInstance(Locale.US).format((long) Double.parseDouble(currentBidAmount));
                        registeredProperties.add(propertyListInfo);
                    }
                    isLastPage = joResult.getBoolean("last_page");
                    if (registeredPropertyAdapter == null) {
                        registeredPropertyAdapter = new RegisteredPropertyAdapter(this, registeredProperties, selector);
                        rvRegisteredProperties.setAdapter(registeredPropertyAdapter);
                    } else {
                        registeredPropertyAdapter.addMoreData(registeredProperties);

                    }
                    offset = joResult.getInt("offset");
                }
                else
                    findViewById(R.id.no_property).setVisibility(View.VISIBLE);
            } else {
                alerts.showOkMessage(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    public void joinLiveAuction(RegisteredPropertyModel property) {
        registeredProperty = property;
        HashMap<String, String> headers = CommonMethods.getHeaders(MyRegisteredProperty.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", property.id);
        System.out.println("join room=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(MyRegisteredProperty.this, Request.Method.POST, params, UrlHelper.JOIN_LIVE_AUCTION, headers, false, CommonDef.RC_JOIN_LIVE_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }
}

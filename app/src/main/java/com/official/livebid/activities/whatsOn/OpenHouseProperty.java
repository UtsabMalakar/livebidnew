package com.official.livebid.activities.whatsOn;

import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.objects.PropertyListInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Ics on 5/26/2016.
 */
public class OpenHouseProperty extends PropertyListInfo {
    private int ohpStatus;
    private String hasMore;
    private String openHouseTime;
    private ArrayList<OpenHouseProperty> openHouseProperties;
    private ArrayList<PropertyListInfo> alPropertyListInfo;
    private boolean isOHP;

    public boolean isOHP() {
        return isOHP;
    }

    public void setOHP(boolean OHP) {
        isOHP = OHP;
    }

    public int getOhpStatus() {
        return ohpStatus;
    }

    public void setOhpStatus(int ohpStatus) {
        this.ohpStatus = ohpStatus;
    }

    public String getHasMore() {
        return hasMore;
    }

    public void setHasMore(String hasMore) {
        this.hasMore = hasMore;
    }

    public String getOpenHouseTime() {
        return openHouseTime;
    }

    public void setOpenHouseTime(String openHouseTime) {
        this.openHouseTime = openHouseTime;
    }

    public ArrayList<OpenHouseProperty> getOpenHouseProperties(JSONArray jaList) throws JSONException {
        openHouseProperties = new ArrayList<>();
        OpenHouseProperty propertyListInfo;
        JSONObject joOpenHouse;
        for (int i = 0; i < jaList.length(); i++) {
            JSONObject joList = jaList.getJSONObject(i);
            propertyListInfo = new OpenHouseProperty();
            propertyListInfo.id = joList.getString("id");
            propertyListInfo.agentId = joList.getString("agent_id");
            propertyListInfo.agencyId = joList.getString("agency_id");
            propertyListInfo.auctionDate = joList.getString("auction_date");
            propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
            HashMap<String ,String> agentName = CommonMethods.getName(joList.getString("agent_name").split(" "));
            propertyListInfo.agentFName = agentName.get("fName");
            propertyListInfo.agentLName = agentName.get("lName");
            propertyListInfo.propertyStatus = joList.getInt("property_status");
            propertyListInfo.statusTag = "";
            propertyListInfo.dateTag = "Auction due to 40 days";
            propertyListInfo.setFav(joList.getString("favorite"));
            propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
            propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
            propertyListInfo.nosOfParking = joList.getString("parking");
            propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(joList.getString("price")));
            propertyListInfo.propertyStatusIdentifier = "from";
            propertyListInfo.suburb = joList.getString("suburb");
            propertyListInfo.price_text=joList.getString("price_text");
            propertyListInfo.postCode = joList.getString("postcode");
            propertyListInfo.propertyTitle = joList.getString("property_title");
            propertyListInfo.propertyDesc = joList.getString("property_description");
            propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
            propertyListInfo.street = joList.getString("street");
            propertyListInfo.propertyState = joList.getString("state");
            propertyListInfo.propertyImgUrl = joList.getString("property_image");
            propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
            propertyListInfo.distance = joList.getString("distance");
            propertyListInfo.lat = joList.getString("lat");
            propertyListInfo.lng = joList.getString("lng");
            propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
            propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
            propertyListInfo.auctionDateTimeStamp = joList.getLong("auction_date_timestamp");
            propertyListInfo.propertyStatus2 = joList.getInt("property_status_2");
            JSONArray JPropertyImageMore = joList.getJSONArray("property_images_more");
            if (JPropertyImageMore.length() > 0) {
                for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                   /* moreImage = new PropertyInfo.Property_images_more(
                                            JPropertyImageMore.getJSONObject(j).getString("image_url")
                                    );*/

                    propertyListInfo.property_images_more.add(JPropertyImageMore.getString(j));
                }
            }
            JSONArray jaOpenHouse = null;
            jaOpenHouse = joList.getJSONArray("open_house");
            propertyListInfo.setOHP(false);
            if (jaOpenHouse.length() > 0) {
                joOpenHouse = jaOpenHouse.getJSONObject(0);
                propertyListInfo.setOhpStatus(joOpenHouse.getInt("open_house_status"));
                propertyListInfo.setHasMore(joOpenHouse.getString("has_more"));
                propertyListInfo.setOpenHouseTime(joOpenHouse.getString("open_house_time"));
                propertyListInfo.setOHP(true);
            }
            if (propertyListInfo.isOHP())
                openHouseProperties.add(propertyListInfo);
        }
        return openHouseProperties;
    }

    public ArrayList<PropertyListInfo> getAllProperties(JSONArray jaList) throws JSONException {
        alPropertyListInfo = new ArrayList<>();
        for (int i = 0; i < jaList.length(); i++) {
            JSONObject joList = jaList.getJSONObject(i);
            PropertyListInfo propertyListInfo = new PropertyListInfo();
            propertyListInfo.id = joList.getString("id");
            propertyListInfo.agentId = joList.getString("agent_id");
            propertyListInfo.agencyId = joList.getString("agency_id");
            propertyListInfo.auctionDate = joList.getString("auction_date");
            propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
            propertyListInfo.agentFName = joList.getString("agent_name");
            propertyListInfo.agentLName = joList.getString("agent_name");
            propertyListInfo.propertyStatus = joList.getInt("property_status");
            propertyListInfo.statusTag = "";
            propertyListInfo.dateTag = "Auction due to 40 days";
            propertyListInfo.setFav(joList.getString("favorite"));
            propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
            propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
            propertyListInfo.nosOfParking = joList.getString("parking");
            propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(joList.getString("price")));
            propertyListInfo.propertyStatusIdentifier = "from";
            propertyListInfo.suburb = joList.getString("suburb");
            propertyListInfo.postCode = joList.getString("postcode");
            propertyListInfo.propertyTitle = joList.getString("property_title");
            propertyListInfo.propertyDesc = joList.getString("property_description");
            propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
            propertyListInfo.street = joList.getString("street");
            propertyListInfo.propertyState = joList.getString("state");
            propertyListInfo.propertyImgUrl = joList.getString("property_image");
            propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
            propertyListInfo.distance = joList.getString("distance");
            propertyListInfo.lat = joList.getString("lat");
            propertyListInfo.lng = joList.getString("lng");
            propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
            propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
            propertyListInfo.auctionDateTimeStamp = joList.getLong("auction_date_timestamp");
            propertyListInfo.propertyStatus2 = joList.getInt("property_status_2");
            JSONArray JPropertyImageMore = joList.getJSONArray("property_images_more");
            if (JPropertyImageMore.length() > 0) {
                propertyListInfo.property_images_more = new ArrayList<>();
                for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                   /* moreImage = new PropertyInfo.Property_images_more(
                                            JPropertyImageMore.getJSONObject(j).getString("image_url")
                                    );*/
                    propertyListInfo.property_images_more.add(JPropertyImageMore.getString(j));
                }
            }
            alPropertyListInfo.add(propertyListInfo);
        }
        return alPropertyListInfo;
    }


    public String getStatusTag() {
        if (ohpStatus == 1 || ohpStatus==0)
            return "OPEN HOUSE";
        else if (ohpStatus == 2)
            return "SCHEDULED";
        else return "";

    }

    public String getOHTimeTag() {
        if (ohpStatus != 2 && ohpStatus != 1) {
            if (hasMore != null && hasMore.equalsIgnoreCase("1"))
                return "<strong>Open House</strong> " + openHouseTime + "/ More";
            else
                return "<strong>Open House</strong> " + openHouseTime;
        }

        return openHouseTime;

    }
}

package com.official.livebid.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.official.livebid.R;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public abstract class MenuActivity extends AppCompatActivity {


    public DrawerLayout mDrawerlayout;
    RelativeLayout rlDrawer;
    ListView mDrawerList_Right;
    ActionBarDrawerToggle mDrawerToggle;

    private Alerts alerts;

    public RelativeLayout tabbar;

    public MenuAdapter menuAdapter;
    public static ImageButton imgWhatsOn;
    public static ImageButton imgMyBids;
    public static ImageButton imgMenu;

    public abstract int getLayoutId();


    RelativeLayout contentView;
    public SharedPreference prefs;
    public static AppMode userType;
    static ArrayList<MenuItem> userMenu;
    public static int selectedPos = 1;
    public final static int WHATS_ON = 1;
    public final static int MY_BIDS = 0;
    public final static int NOT_SELECTED = 2;
    public final static int MY_LISTINGS = 3;
    public static int activeTab = NOT_SELECTED;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        View content = getLayoutInflater().inflate(getLayoutId(), null, false);
        contentView = (RelativeLayout) findViewById(R.id.rl_content);
        alerts = new Alerts(this);
        contentView.addView(content);
        getSupportActionBar().hide();
        prefs = new SharedPreference(this);
        if (prefs != null && prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).equals("2")) {
            userType = AppMode.user;
        } else if (prefs != null && prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).equals("3")) {
            userType = AppMode.agent;
        } else {
            userType = AppMode.guest;
        }
        //===============Initialization of Variables=========================//

        //============menu list============//

        userMenu = getMenuItems();


        mDrawerlayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        mDrawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerList_Right = (ListView) findViewById(R.id.drawer_list_right);
        rlDrawer = (RelativeLayout) findViewById(R.id.rl_drawer);

        imgMenu = (ImageButton) findViewById(R.id.imgRightMenu);
        imgWhatsOn = (ImageButton) findViewById(R.id.imgCentertMenu);
        imgMyBids = (ImageButton) findViewById(R.id.imgLeftMenu);
        tabbar = (RelativeLayout) findViewById(R.id.tabbar);
//        tabbar.setVisibility(View.VISIBLE);

//        if (userType==AppMode.guest){
//            tabbar.setVisibility(View.GONE);
//        }
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerlayout, R.drawable.ic_menu, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                Animation slideUp= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_up_in);
//                tabbar.startAnimation(slideUp);
                tabbar.setVisibility(View.VISIBLE);


            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                Animation slideDown= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);
//                tabbar.startAnimation(slideDown);
                tabbar.setVisibility(View.GONE);

            }
        };


        mDrawerlayout.setDrawerListener(mDrawerToggle);


        imgMenu.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mDrawerlayout.openDrawer(rlDrawer);
                Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
                tabbar.startAnimation(slideDown);
                tabbar.setVisibility(View.GONE);


            }
        });
        resetTabBar();
        imgWhatsOn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//                if (userType == AppMode.guest) {
//                    selectedPos = 2;
//                    activeTab = WHATS_ON;
//                    Opener.guestMode(MenuActivity.this, "Whats On");
//                    return;
//                }

//                    imgWhatsOn.setImageResource(R.drawable.ic_whats_on_dark);
                    selectedPos = 2;
                    activeTab = WHATS_ON;
                    Opener.WhatsOn(MenuActivity.this);

//                else {
//                    imgWhatsOn.setImageResource(R.drawable.ic_whats_on);
//                    imgWhatsOn.setTag(0);
//
//                }

            }
        });
        imgMyBids.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (userType == AppMode.guest) {
                    selectedPos = 4;
                    activeTab = MY_BIDS;
                    Opener.guestMode(MenuActivity.this, "My Bids");
                    return;
                }
                if (userType == AppMode.agent) {
//                    if (activeTab != MY_LISTINGS) {
                        selectedPos=4;
                        activeTab = MY_LISTINGS;
                        resetTabBar();
                        Opener.MyListings(MenuActivity.this);
//                    }
                    return;
                }
                if (userType == AppMode.user) {
//                    if (activeTab != MY_BIDS) {
//                    imgWhatsOn.setImageResource(R.drawable.ic_whats_on_dark);
                        selectedPos = 4;
                        activeTab = MY_BIDS;
//                        Opener.MyBidsHome(MenuActivity.this);
//                        Opener.MyBidsHome(MenuActivity.this);
                        alerts.MyBids();
//                    }
                }


            }
        });


        menuAdapter = new MenuAdapter(this, userMenu);
        mDrawerList_Right.setAdapter(menuAdapter);
        if (userType == AppMode.guest) {
            mDrawerList_Right.setOnItemClickListener(guestUserClickListener);
        } else if (userType == AppMode.user) {
            mDrawerList_Right.setOnItemClickListener(userClickListener);
        } else if (userType == AppMode.agent) {
            mDrawerList_Right.setOnItemClickListener(agentClickListener);
        }


    }

    public static void resetTabBar() {
        imgWhatsOn.setImageResource(R.drawable.ic_whats_on);
        if (userType == AppMode.agent)
            imgMyBids.setImageResource(R.drawable.ic_my_listing_off_icon);
        else
            imgMyBids.setImageResource(R.drawable.ic_my_bids);

        if (activeTab == WHATS_ON)
            imgWhatsOn.setImageResource(R.drawable.ic_whats_on_dark);
        if (activeTab == MY_BIDS)
            imgMyBids.setImageResource(R.drawable.ic_my_bids_icon);
        if (activeTab == MY_LISTINGS)
            imgMyBids.setImageResource(R.drawable.ic_my_listing_on_icon);
    }

    private ArrayList<MenuItem> getMenuItems() {
        ArrayList<MenuItem> userMenu = new ArrayList<>();
        if (userType == AppMode.guest) {
            userMenu.add(new MenuItem("Profile", R.drawable.profile_icon));
            userMenu.add(new MenuItem("Discover", R.drawable.discover_icon));
            userMenu.add(new MenuItem("What's On", R.drawable.what_on_icon));
            userMenu.add(new MenuItem("Watchlist", R.drawable.watchlist_icon));
            userMenu.add(new MenuItem("My Bids", R.drawable.mybids_icon));
//            userMenu.add(new MenuItem("My Listings", R.drawable.my_listing));
            userMenu.add(new MenuItem("Notifications", R.drawable.notifications_icon));
            userMenu.add(new MenuItem("Sign Up", R.drawable.sign_up_icon));
        } else if (userType == AppMode.user) {
            if (prefs.getStringValues(CommonDef.SharedPrefKeys.PROFILE_IMG).length() > 0) {
                userMenu.add(new MenuItem(prefs.getStringValues(CommonDef.SharedPrefKeys.FIRST_NAME), prefs.getStringValues(CommonDef.SharedPrefKeys.PROFILE_IMG)));
            } else {
                userMenu.add(new MenuItem(prefs.getStringValues(CommonDef.SharedPrefKeys.FIRST_NAME), R.drawable.profile_icon));
            }

            userMenu.add(new MenuItem("Discover", R.drawable.discover_icon));
            userMenu.add(new MenuItem("What's On", R.drawable.what_on_icon));
            userMenu.add(new MenuItem("Watchlist", R.drawable.watchlist_icon));
            userMenu.add(new MenuItem("My Bids", R.drawable.mybids_icon));
//            userMenu.add(new MenuItem("My Listings", R.drawable.my_listing));
            userMenu.add(new MenuItem("Notifications", R.drawable.notifications_icon));
//            userMenu.add(new MenuItem("Sign Up", R.drawable.sign_up_icon));
        } else if (userType == AppMode.agent) {
            if (prefs.getStringValues(CommonDef.SharedPrefKeys.PROFILE_IMG).length() > 0) {
                userMenu.add(new MenuItem(prefs.getStringValues(CommonDef.SharedPrefKeys.FIRST_NAME), prefs.getStringValues(CommonDef.SharedPrefKeys.PROFILE_IMG)));
            } else {
                userMenu.add(new MenuItem(prefs.getStringValues(CommonDef.SharedPrefKeys.FIRST_NAME), R.drawable.profile_icon));
            }
            userMenu.add(new MenuItem("Discover", R.drawable.discover_icon));
            userMenu.add(new MenuItem("What's On", R.drawable.what_on_icon));
            userMenu.add(new MenuItem("Watchlist", R.drawable.watchlist_icon));
//            userMenu.add(new MenuItem("My Bids", R.drawable.mybids_icon));
            userMenu.add(new MenuItem("My Listings", R.drawable.my_listing));
            userMenu.add(new MenuItem("Notifications", R.drawable.notifications_icon));
//            userMenu.add(new MenuItem("Sign Up", R.drawable.sign_up_icon));
        } else {

        }
        return userMenu;
    }


    private class ViewHolder {
        TextView text, textcounter;
        ImageView iv;
        RelativeLayout rlMenuItem;

    }

    public class MenuItem<T> {

        private String menuTitle;
        private T menuIcon;

        public MenuItem(String menuTitle, T menuIcon) {
            this.menuTitle = menuTitle;
            this.menuIcon = menuIcon;
        }

        public String getMenuTitle() {
            return menuTitle;
        }

        public void setMenuTitle(String menuTitle) {
            this.menuTitle = menuTitle;
        }

        public T getMenuIcon() {
            return menuIcon;
        }

        public void setMenuIcon(T menuIcon) {
            this.menuIcon = menuIcon;
        }
    }


    public class MenuAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<MenuItem> menuItems;

        public MenuAdapter(Context context, ArrayList<MenuItem> menuItems) {
            this.context = context;
            this.menuItems = menuItems;
        }

        @Override
        public int getCount() {
            return menuItems.size();
        }

        @Override
        public Object getItem(int i) {
            return menuItems.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder holder = null;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            if (view == null) {
                view = inflater.inflate(R.layout.menu_item, null);
                holder = new ViewHolder();

                holder.iv = (ImageView) view.findViewById(R.id.imgView);
                holder.text = (TextView) view.findViewById(R.id.txtData);
                holder.rlMenuItem = (RelativeLayout) view.findViewById(R.id.rl_menu_item);
                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();
            }
            MenuItem menuItem = menuItems.get(i);

//            holder.iv.setImageResource(menuItem.getMenuIcon());
            if (menuItem.getMenuIcon().getClass().equals(String.class)) {
                Picasso.with(context)
                        .load(menuItem.getMenuIcon().toString())
                        .resize((int) CommonMethods.pxFromDp(MenuActivity.this, 30), (int) CommonMethods.pxFromDp(MenuActivity.this, 30))
                        .centerCrop()
//                        .placeholder( R.drawable.img_loading)
//                        .memoryPolicy(MemoryPolicy.NO_CACHE)
//                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .transform(new CircleTransform())
                        .into(holder.iv);
            } else {
                Picasso.with(context).load((int) menuItem.getMenuIcon()).into(holder.iv);
            }

            holder.text.setText(menuItem.getMenuTitle());
            holder.rlMenuItem.setBackgroundColor(
                    i == selectedPos ?
                            context.getResources().getColor(R.color.theme_blue_shade)
                            : context.getResources().getColor(R.color.theme_blue)
            );


            return view;
        }

        public void refreshMenu() {
//            Toast.makeText(context,"refresh menu",Toast.LENGTH_SHORT).show();
            menuItems = getMenuItems();
            menuAdapter.notifyDataSetChanged();

        }

    }

    private AdapterView.OnItemClickListener guestUserClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            selectedPos = i;
            mDrawerlayout.closeDrawers();
            switch (i) {

                case 0:
//                    Opener.Profile(MenuActivity.this);
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.guestMode(MenuActivity.this, "My Profile");
                    break;
                case 1:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.Discover(MenuActivity.this);
                    break;
                case 2:
                    if (activeTab != WHATS_ON) {
                        activeTab = WHATS_ON;
                        resetTabBar();
                        Opener.WhatsOn(MenuActivity.this);
                    }

                    break;
                case 3:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.guestMode(MenuActivity.this, getMenuItems().get(i).getMenuTitle());
                    break;
                case 4:
                    if (activeTab != MY_BIDS) {
                        activeTab = MY_BIDS;
                        resetTabBar();
                        Opener.guestMode(MenuActivity.this, getMenuItems().get(i).getMenuTitle());
//                        alerts.MyBids();
                    }

                    break;
                case 5:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
//                    Opener.SignIn(MenuActivity.this);
                    Opener.guestMode(MenuActivity.this, getMenuItems().get(i).getMenuTitle());
                    break;
                case 6:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
//                    Opener.Register(MenuActivity.this);
                    Opener.newRegister(MenuActivity.this, UserType.NEW);
                    break;


            }

        }
    };
    private AdapterView.OnItemClickListener userClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            selectedPos = i;
            mDrawerlayout.closeDrawers();
            switch (i) {
                case 0:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.Profile(MenuActivity.this);
                    break;
                case 1:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.Discover(MenuActivity.this);
                    break;
                case 2:
                    if (activeTab != WHATS_ON) {
                        activeTab = WHATS_ON;
                        resetTabBar();
                        Opener.WhatsOn(MenuActivity.this);
                    }
                    break;
                case 4:
                    if (activeTab != MY_BIDS) {
                        activeTab = MY_BIDS;
                        resetTabBar();
//                        Opener.MyBidsHome(MenuActivity.this);
//                        Opener.MyBidsHome(MenuActivity.this);
                        alerts.MyBids();
                    }
                    break;
                case 3:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.WatchList(MenuActivity.this);
                    break;
                case 5:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.Notification(MenuActivity.this);
                    break;
                default:
                    Toast.makeText(getApplicationContext(), "Under Development", Toast.LENGTH_SHORT).show();


            }

        }
    };
    private AdapterView.OnItemClickListener agentClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            selectedPos = i;
            mDrawerlayout.closeDrawers();
//            Toast.makeText(MenuActivity.this, "Not available for now", Toast.LENGTH_LONG).show();

            switch (i) {
                case 0:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.Profile(MenuActivity.this);
                    break;
                case 1:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.Discover(MenuActivity.this);
                    break;
                case 2:
                    if (activeTab != WHATS_ON) {
                        activeTab = WHATS_ON;
                        resetTabBar();
                        Opener.WhatsOn(MenuActivity.this);
//                        alerts.MyBids();
                    }
                    break;
                case 3:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.WatchList(MenuActivity.this);
                    break;
                case 4:
                    if (activeTab != MY_LISTINGS) {
                        activeTab = MY_LISTINGS;
                        resetTabBar();
                        Opener.MyListings(MenuActivity.this);
                    }
                    break;
                case 5:
                    activeTab = NOT_SELECTED;
                    resetTabBar();
                    Opener.Notification(MenuActivity.this);
                    break;
                default:
                    Toast.makeText(getApplicationContext(), "Under Development", Toast.LENGTH_SHORT).show();


            }

        }
    };




    public void hideTabBar() {

        tabbar.setVisibility(View.GONE);
    }

    public void showTabBar() {

        tabbar.setVisibility(View.VISIBLE);

    }


}





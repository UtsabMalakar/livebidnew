package com.official.livebid.activities;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.NumberPicker;

import com.official.livebid.R;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.staticClasses.SearchParams;

public class PriceSelector extends ActionBarActivity implements NumberPicker.OnValueChangeListener {

    private static int[] priceList;
    private NumberPicker np_min;
    private NumberPicker np_max;
    private CustomActionBar.SelectPriceTitleBar titleBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_price_selector);
        priceList = new int[]{
                0
                , 50000
                , 100000
                , 150000
                , 200000
                , 250000
                , 300000
                , 350000
                , 400000
                , 450000
                , 500000
                , 550000
                , 600000
                , 650000
                , 700000
                , 750000
                , 800000
                , 850000
                , 900000
                , 950000
                , 1000000
                , 1100000
                , 1200000
                , 1300000
                , 1400000
                , 1500000
                , 1600000
                , 1700000
                , 1800000
                , 1900000
                , 2000000
                , 2250000
                , 2500000
                , 2750000
                , 3000000
                , 3500000
                , 4000000
                , 4500000
                , 5000000
        };
        final String[] nums = new String[priceList.length];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = formatPrice(priceList[i]);
        }

        // Set the max and min values of the numberpicker, and give it the
        // array of numbers created above to be the displayed numbers
        np_min = (NumberPicker) findViewById(R.id.np_min);
        setDividerColor(np_min, Color.RED);
        np_min.setMaxValue(priceList.length - 1);
        np_min.setMinValue(0);
        np_min.setWrapSelectorWheel(false);
        np_min.setDisplayedValues(nums);
        np_min.setValue(getIndexFromValue(Refine.minP));


        np_max = (NumberPicker) findViewById(R.id.np_max);
        setDividerColor(np_max, Color.RED);
        setDividerColor(np_min, Color.RED);
        np_max.setMaxValue(priceList.length - 1);
        np_max.setMinValue(0);
        np_max.setWrapSelectorWheel(false);
        np_max.setDisplayedValues(nums);
        np_max.setValue(getIndexFromValue(Refine.maxP));
//        Button button = (Button) findViewById(R.id.button);
//        button.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Show the selected value of the numberpicker when the button is clicked
//                Toast.makeText(Main.this, "Selected value: " + nums[np.getValue()], Toast.LENGTH_SHORT).show();
//            }
//        });
//
        np_min.setOnValueChangedListener(this);
        np_max.setOnValueChangedListener(this);
        titleBar= new CustomActionBar.SelectPriceTitleBar(this,"Price");
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_price_selector, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * method to add k,m into long price.
     *
     * @param price
     * @return
     */

    private String formatPrice(long price) {

        if (price > 999999)
            if (price % 1000000 == 0)
                return "$" + price / 1000000 + "M";
            else
                return "$ " + (double) price / 1000000 + "M";
        else if (price > 999)
            if (price % 1000 == 0)
                return "$" + price / 1000 + "K";
            else
                return "$" + (double) price / 1000 + "K";
        else if (price > 0)
            return "$" + price;
        else
            return "Any";
    }

    /**
     * method to set divider color of numberPicker
     *
     * @param picker
     * @param color
     */
    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Refine.minP = priceList[np_min.getValue()];
        Refine.maxP = priceList[np_max.getValue()];
        SearchParams.priceRangeMin=priceList[np_min.getValue()]+"";
        SearchParams.priceRangeMax=priceList[np_max.getValue()]+"";
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    private int getIndexFromValue(int pValue) {
        int indx = 0;
        for (int i = 0; i < priceList.length; i++) {
            if (pValue == priceList[i]) {
                indx = i;
                break;
            }
        }
        return indx;
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i2) {
        switch (numberPicker.getId()) {
            case R.id.np_min:
                if (i2 > np_max.getValue()) {
                    np_max.setValue(i2 + 1);

                }
                break;
            case R.id.np_max:
                if (i2 < np_min.getValue()) {
                    np_min.setValue(i2 - 1);

                }
                break;
        }

    }
}

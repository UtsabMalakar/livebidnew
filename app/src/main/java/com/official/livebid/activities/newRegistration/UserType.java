package com.official.livebid.activities.newRegistration;

import java.io.Serializable;

/**
 * Created by Ics on 5/16/2016.
 */
public enum UserType implements Serializable{
    NEW(1),
    EXISTING(2);
    private int userType;

    UserType(int userType) {
        this.userType = userType;
    }
    public int getUserType(){
        return userType;
    }

}

package com.official.livebid.activities;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.AnimatedExpandableListView;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class KnowledgeBase extends ActionBarActivity implements AsyncInterface {

    private AnimatedExpandableListView listView;
    private ExampleAdapter adapter;
    private int lastExpandedPosition = -1;
    private Alerts alerts;
    List<GroupItem> groupItems;
    private CustomActionBar.TitleBarWithBackAndTitle KnowledgeBaseActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knowledge_base);
        getSupportActionBar().hide();
        KnowledgeBaseActionBar = new CustomActionBar.TitleBarWithBackAndTitle(this, "Knowledge base");
        alerts = new Alerts(this);
        groupItems = new ArrayList<GroupItem>();
        getKnowledgeBased();
    }


    private static class GroupItem {
        String title;
        List<ChildItem> items = new ArrayList<ChildItem>();
    }

    private static class ChildItem {
        String title;
        String hint;
    }

    private static class ChildHolder {
        TextView title;
        WebView webView;
    }

    private static class GroupHolder {
        TextView title;
    }

    /**
     * Adapter for our list of {@link GroupItem}s.
     */
    private class ExampleAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;

        private List<GroupItem> items;

        public ExampleAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> items) {
            this.items = items;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildHolder holder;
            ChildItem item = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.knowledge_base_list_item, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.webView = (WebView) convertView.findViewById(R.id.webView);
                convertView.setTag(holder);
            } else {
                holder = (ChildHolder) convertView.getTag();
            }

            holder.title.setText(item.title);
            holder.webView.loadData(item.hint, "text/html", "UTF-8");

            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return items.get(groupPosition).items.size();
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder holder;
            GroupItem item = getGroup(groupPosition);
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.knowledge_base_group_item, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }

            holder.title.setText(item.title);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            listView.setIndicatorBounds(listView.getRight() - 80, listView.getWidth());
        } else {
            listView.setIndicatorBoundsRelative(listView.getRight() - 80, listView.getWidth());
        }
    }

    private void getKnowledgeBased() {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("device_type", "2");
        System.out.println("logout header=>" + headers.toString());
        VolleyRequest volleyRequest = new VolleyRequest(KnowledgeBase.this, Request.Method.POST, params, UrlHelper.KNOWLEDGE_BASE, headers, false, CommonDef.REQUEST_KNOWLEDGE_BASE);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_KNOWLEDGE_BASE:
                System.out.println("knowledge base=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        JSONArray lists = joResult.getJSONArray("list");
                        JSONObject jItem = null;
                        GroupItem item = null;
                        ChildItem child = null;
                        for (int i = 0; i < lists.length(); i++) {
                            jItem = lists.getJSONObject(i);
                            item = new GroupItem();
                            item.title = jItem.getString("title");
                            child = new ChildItem();
//                            child.title = jItem.getString("title");
                            child.title = "";
                            child.hint = jItem.getString("desc");
                            item.items.add(child);
                            groupItems.add(item);

                        }
                        setExpandableListViewSettings();

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
        }
    }

    private void setExpandableListViewSettings() {
        adapter = new ExampleAdapter(this);
        adapter.setData(groupItems);

        listView = (AnimatedExpandableListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

        // In order to show animations, we need to use a custom click handler
        // for our ExpandableListView.
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                // We call collapseGroupWithAnimation(int) and
                // expandGroupWithAnimation(int) to animate group
                // expansion/collapse.
                if (listView.isGroupExpanded(groupPosition)) {
                    listView.collapseGroupWithAnimation(groupPosition);
                } else {
                    listView.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }

        });
        listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    listView.collapseGroupWithAnimation(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            listView.setIndicatorBounds(listView.getRight() - 80, listView.getWidth());
        } else {
            listView.setIndicatorBoundsRelative(listView.getRight() - 80, listView.getWidth());
        }
    }

}


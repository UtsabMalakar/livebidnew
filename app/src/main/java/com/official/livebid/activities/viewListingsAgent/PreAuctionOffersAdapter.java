package com.official.livebid.activities.viewListingsAgent;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.objects.PreAuctionOfferModel;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.utils.CircleTransform;
import com.official.livebid.utils.GrayscaleTransformation;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Ics on 3/10/2016.
 */
public class PreAuctionOffersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<PreAuctionOfferModel> preAuctionOffers;
    private Context context;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private PropertyListInfo propertyListInfo;

    public PreAuctionOffersAdapter(Context context, ArrayList<PreAuctionOfferModel> preAuctionOffers, PropertyListInfo prop) {
        this.preAuctionOffers = preAuctionOffers;
        this.context = context;
        this.propertyListInfo = prop;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_agent_view_item, parent, false);
            return new PreAuctionOfferViewHolder(view);
        } else if (viewType == TYPE_HEADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_header, parent, false);
            return new PreAuctionOfferHeader(view);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final PreAuctionOfferModel preAuctionOffer;
        if (holder instanceof PreAuctionOfferViewHolder) {
            preAuctionOffer = preAuctionOffers.get(position-1);
            PreAuctionOfferViewHolder preAuctionOfferViewHolder = (PreAuctionOfferViewHolder) holder;
            if (!preAuctionOffer.profileImage.trim().isEmpty()) {
                Picasso.with(context)
                        .load(preAuctionOffer.profileImage)
                        .resize((int) CommonMethods.pxFromDp((Activity) context, 60), (int) CommonMethods.pxFromDp((Activity) context, 60))
                        .centerCrop()
                        .transform(new CircleTransform())
                        .into(preAuctionOfferViewHolder.ivUserProfile);
            } else {
                Picasso.with(context)
                        .load(R.drawable.img_coming_soon)
                        .resize((int) CommonMethods.pxFromDp((Activity) context, 60), (int) CommonMethods.pxFromDp((Activity) context, 60))
                        .centerCrop()
                        .transform(new CircleTransform())
                        .into(preAuctionOfferViewHolder.ivUserProfile);
            }
            if (preAuctionOffer.settlementTime.isEmpty()) {
                preAuctionOfferViewHolder.tvFirstLine.setText(Html.fromHtml("<b>#" + preAuctionOffer.uniqueId.toUpperCase() + "</b>"));
                preAuctionOfferViewHolder.tvSecondLine.setText(Html.fromHtml("has sent an offer of " + "<b>" + "$" + NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(preAuctionOffer.offerAmount)) + "</b>"));
            } else {
                preAuctionOfferViewHolder.tvFirstLine.setText(Html.fromHtml("<b>#" + preAuctionOffer.uniqueId.toUpperCase() + "</b>" + " has sent an offer of"));
                preAuctionOfferViewHolder.tvSecondLine.setText(Html.fromHtml("<b>" + "$" + NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(preAuctionOffer.offerAmount)) + "</b>" + " to be settled " + "<b>" + DateTimeHelper.getdateEEEdMMM(preAuctionOffer.settlementTime) + "</b>"));
            }
            preAuctionOfferViewHolder.tvOfferTime.setText(DateTimeHelper.getLaggingTimeFormated(preAuctionOffer.offerTime));
            preAuctionOfferViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((PreAuctionOffersAgent)context).showContactBottomSheet(preAuctionOffer);
                }
            });
        } else if (holder instanceof PreAuctionOfferHeader) {
            final PreAuctionOfferHeader header = (PreAuctionOfferHeader) holder;
            header.tvReservePrice.setText(propertyListInfo.price);
            header.tvPropertyNo.setText(propertyListInfo.averagePrice);// average price
            header.tvPropertyTitle.setText("Average Price");// average price
            if (propertyListInfo.propertyStatus == 0) {
                if ( propertyListInfo.propertyStatus2 == 4) {
                    header.tvStatusTag.setVisibility(View.VISIBLE);
                    header.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                    header.tvDateTag.setVisibility(View.GONE);

                } else {
                    if (propertyListInfo.propertyStatus2 != 0)
                    header.tvStatusTag.setVisibility(View.VISIBLE);
                    header.tvDateTag.setVisibility(View.VISIBLE);
                    header.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                    header.tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
                }

            } else {
                header.tvDateTag.setVisibility(View.GONE);
                header.tvStatusTag.setVisibility(View.VISIBLE);
                header.tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
            }
            header.tvPropertyStreet.setText(propertyListInfo.street);
            header.tvPropertySuburbPostcode.setText(propertyListInfo.suburb + ", " + propertyListInfo.postCode);

            ImageSlideAdapter mImageSlideAdapter;
            mImageSlideAdapter = new ImageSlideAdapter(context, propertyListInfo.property_images_more,propertyListInfo.propertyStatus);
            header.mViewPager.setAdapter(mImageSlideAdapter);
            if (propertyListInfo.propertyStatus == 2) {
                header.tvStatusTag.setVisibility(View.GONE);
                header.tvPropertyImageIndicator.setVisibility(View.GONE);
//                propertyViewHolder.mViewPager.beginFakeDrag();

            }else {
                header.tvStatusTag.setVisibility(View.VISIBLE);
                header.tvPropertyImageIndicator.setVisibility(View.GONE);
                header.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        header.tvPropertyImageIndicator.setText(position+1 + "/" + propertyListInfo.property_images_more.size());
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });

            }
        }


    }

    @Override
    public int getItemCount() {
        if(preAuctionOffers.size()==0)
            return 1;
        return preAuctionOffers.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public static class PreAuctionOfferViewHolder extends RecyclerView.ViewHolder {
        protected ImageView ivUserProfile;
        protected TextView tvFirstLine, tvSecondLine, tvOfferTime;
        protected View rootView;

        public PreAuctionOfferViewHolder(View v) {
            super(v);
            rootView = v;
            ivUserProfile = (ImageView) v.findViewById(R.id.iv_profile_img);
            tvFirstLine = (TextView) v.findViewById(R.id.tv_first_line);
            tvSecondLine = (TextView) v.findViewById(R.id.tv_second_line);
            tvOfferTime = (TextView) v.findViewById(R.id.tv_offer_time);
        }
    }

    public static class PreAuctionOfferHeader extends RecyclerView.ViewHolder {
        private final TextView tvPropertyImageIndicator;
        private final ViewPager mViewPager;
        private final CheckBox chkFav;
        protected ImageView ivPropertyImg;
        protected TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
                tvPropertyStatusIdentifier, tvReservePrice, tvPropertyNo, tvPropertyTitle;
        protected View rootView;

        public PreAuctionOfferHeader(View view) {
            super(view);
            rootView = view;
            chkFav= (CheckBox) view.findViewById(R.id.cbox_fav);
            chkFav.setVisibility(View.GONE);
            mViewPager = (ViewPager) view.findViewById(R.id.vp_property_img);
            tvPropertyImageIndicator = (TextView) view.findViewById(R.id.tvPropertyImageIndicator);
            tvStatusTag = (TextView) view.findViewById(R.id.tv_property_status_tag);
            tvDateTag = (TextView) view.findViewById(R.id.tv_property_date_tag);
            tvPropertySuburbPostcode = (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
            tvPropertyStreet = (TextView) view.findViewById(R.id.tv_property_street);
            tvPropertyStatusIdentifier = (TextView) view.findViewById(R.id.tv_property_status_identifier);
            tvReservePrice = (TextView) view.findViewById(R.id.tv_reserve_price);
            tvPropertyNo = (TextView) view.findViewById(R.id.tv_property_no);
            tvPropertyTitle = (TextView) view.findViewById(R.id.tv_property_no_title);
        }
    }
}

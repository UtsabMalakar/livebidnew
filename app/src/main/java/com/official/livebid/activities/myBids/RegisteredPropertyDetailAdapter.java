package com.official.livebid.activities.myBids;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.official.livebid.R;
import com.official.livebid.activities.liveAuction.CurrentBidModel;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.listeners.FavouriteCompleteListener;
import com.official.livebid.utils.GrayscaleTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ics on 5/12/2016.
 */
public class RegisteredPropertyDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM_HEADER = 1;
    private static final int ITEM_MY_BIDS_DESC = 2;
    private static final int ITEM_MY_BID = 3;
    int myBidTypeSelector;
    private Context context;
    private RegisteredPropertyModel property;
    private ArrayList<CurrentBidModel> myBids;

    public RegisteredPropertyDetailAdapter(Context context, RegisteredPropertyModel property, ArrayList<CurrentBidModel> myBids, int myBidTypeSelector) {
        this.context = context;
        this.property = property;
        this.myBids = myBids;
        this.myBidTypeSelector = myBidTypeSelector;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return ITEM_HEADER;
        else if (position == 1)
            return ITEM_MY_BIDS_DESC;
        else
            return ITEM_MY_BID;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == ITEM_HEADER) {
            view = LayoutInflater.from(context).inflate(R.layout.my_bids_property_header, parent, false);
            return new MyBidsPropertyViewHolder(view);
        } else if (viewType == ITEM_MY_BIDS_DESC) {
            view = LayoutInflater.from(context).inflate(R.layout.layout_current_bid_desc, parent, false);
            return new MyBidDescViewHolder(view);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.layout_current_bid_item, parent, false);
            return new MyBidViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyBidsPropertyViewHolder)
            setPropertyHeader(holder, property);
        else if (holder instanceof MyBidDescViewHolder) {
            setMyBidTitle(holder, property);
        } else
            setMyBids(holder, position - 2, property);

    }

    private void setMyBids(RecyclerView.ViewHolder holder, int i, RegisteredPropertyModel property) {
        //set my Bids
        MyBidViewHolder myBidViewHolder = (MyBidViewHolder) holder;
        CurrentBidModel myBid = myBids.get(i);
//        if (property.propertyStatus == 0) {
//            myBidViewHolder.tvAmount.setText(myBid.bidAmount);
//            myBidViewHolder.tvTimeEllapsed.setText(DateTimeHelper.getFormatedDatedTime(myBid.startDateTime)[0]);
//            myBidViewHolder.tvBidder.setVisibility(View.VISIBLE);
//            myBidViewHolder.tvBidder.setText(DateTimeHelper.getFormatedDatedTime(myBid.startDateTime)[1]);
//        } else {
//            myBidViewHolder.tvAmount.setText(myBid.bidAmount);
//            myBidViewHolder.tvTimeEllapsed.setText(myBid.bidTime);
//            myBidViewHolder.tvBidder.setVisibility(View.GONE);
//        }
        if (myBidTypeSelector == CommonDef.MY_BIDS_OFFERS) {
            myBidViewHolder.tvAmount.setText(myBid.bidAmount);
            myBidViewHolder.tvTimeEllapsed.setText(DateTimeHelper.getFormatedDatedTimeFromDate(myBid.startDateTime)[0]);
            myBidViewHolder.tvBidder.setVisibility(View.VISIBLE);
            myBidViewHolder.tvBidder.setText(DateTimeHelper.getFormatedDatedTimeFromDate(myBid.startDateTime)[1]);
        } else {
            myBidViewHolder.tvAmount.setText(myBid.bidAmount);
            myBidViewHolder.tvTimeEllapsed.setText(myBid.bidTime);
            myBidViewHolder.tvBidder.setVisibility(View.GONE);
        }
    }

    private void setMyBidTitle(RecyclerView.ViewHolder holder, RegisteredPropertyModel property) {
        //set my bids title
        MyBidDescViewHolder mbh = (MyBidDescViewHolder) holder;
        mbh.llVendorBidHolder.setVisibility(View.GONE);
        if (myBidTypeSelector == CommonDef.MY_BIDS_OFFERS) {
            mbh.tvTitle.setText("My Offers");
            mbh.tvDescription.setText("description");
        } else if (myBidTypeSelector == CommonDef.MY_BIDS_BIDS) {
            mbh.tvTitle.setText("My Bids");
            mbh.tvDescription.setText("description");
        }


    }


    @Override
    public int getItemCount() {
        return myBids.size() + 2;
    }

    private void setPropertyImage(RegisteredPropertyModel propertyListInfo, MyBidsPropertyViewHolder propertyViewHolder) {

        if (propertyListInfo.propertyStatus == 2) {
            propertyViewHolder.ivPropertySoldTag.setVisibility(View.VISIBLE);
            propertyViewHolder.tvDateTag.setVisibility(View.GONE);
            if (!propertyListInfo.propertyImgUrl.trim().isEmpty()) {

                Picasso.with(context)
                        .load(propertyListInfo.propertyImgUrl)
                        .placeholder(R.mipmap.img_property_placeholder)
                        .transform(new GrayscaleTransformation())
                        .into(propertyViewHolder.ivPropertyImg);
            } else {
                Picasso.with(context)
                        .load(R.drawable.img_coming_soon)
                        .into(propertyViewHolder.ivPropertyImg);

            }

        } else {
            propertyViewHolder.ivPropertySoldTag.setVisibility(View.GONE);
            propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load(propertyListInfo.propertyImgUrl)
                    .placeholder(R.mipmap.img_property_placeholder)
                    .into(propertyViewHolder.ivPropertyImg);
        }
    }

    private void setCurrentBidStatus(RegisteredPropertyModel propertyListInfo, MyBidsPropertyViewHolder propertyViewHolder) {
        if (propertyListInfo.propertyStatus == 0) {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("Current Bid");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        } else if (propertyListInfo.propertyStatus == 1 || propertyListInfo.propertyStatus == 4) {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.VISIBLE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("Current Bid");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.VISIBLE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        } else if (propertyListInfo.propertyStatus == 2) {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.VISIBLE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("SOLD");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.VISIBLE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        } else if (propertyListInfo.propertyStatus == 3) {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("Current Bid");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        } else {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("Current Bid");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        }
    }

    private void setPropertyHeader(RecyclerView.ViewHolder holder, final RegisteredPropertyModel propertyListInfo) {
        MyBidsPropertyViewHolder propertyViewHolder = (MyBidsPropertyViewHolder) holder;
        setPropertyStatusTag(propertyListInfo, propertyViewHolder);
        setCurrentBidStatus(propertyListInfo, propertyViewHolder);
        propertyViewHolder.tvPropertyStreet.setText(propertyListInfo.street);
        propertyViewHolder.tvPropertySuburbPostcode.setText(propertyListInfo.suburb + ", "+propertyListInfo.propertyState+" " + propertyListInfo.postCode);
        setPropertyImage(propertyListInfo, propertyViewHolder);
        propertyViewHolder.chkFav.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final CheckBox cboxTemp = (CheckBox) view;
                        new FavoriteHelper(context).makeFavorite(property.id, property.getFav(), new FavouriteCompleteListener() {
                            @Override
                            public void onSuccess(boolean b) {
                                property.setFav(b);
                                cboxTemp.setChecked(b);
                            }

                            @Override
                            public void onFailure() {
                                cboxTemp.setChecked(property.getFav());
                            }

                            @Override
                            public void onNotLoggedIn() {
                                cboxTemp.setChecked(false);
                                new Alerts((Activity) context).signIn();
                            }
                        });

                        // Webservice call for change.

                    }
                });


        // set property
    }

    private void setPropertyStatusTag(RegisteredPropertyModel propertyListInfo, MyBidsPropertyViewHolder propertyViewHolder) {
        if (propertyListInfo.propertyStatus == 0) {
            if (propertyListInfo.propertyStatus2 == 4) {
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                propertyViewHolder.tvDateTag.setVisibility(View.GONE);

            } else {
                if (propertyListInfo.propertyStatus2 != 0)
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvDateTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                propertyViewHolder.tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
            }

        } else {
            propertyViewHolder.tvDateTag.setVisibility(View.GONE);
            propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
            propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
        }
    }

    public void upPropertyHeader(RegisteredPropertyModel property) {
        this.property = property;
        notifyItemChanged(0);

    }

}

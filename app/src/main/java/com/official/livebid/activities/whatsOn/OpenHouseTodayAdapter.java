package com.official.livebid.activities.whatsOn;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SlideShowViewPager;
import com.official.livebid.listeners.FavouriteCompleteListener;
import com.official.livebid.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Robz on 1/6/2016.
 */
public class OpenHouseTodayAdapter extends RecyclerView.Adapter<OpenHouseTodayAdapter.PropertyViewHolder> {

    Context context;
    ArrayList<OpenHouseProperty> alPropertyListInfo;
    int AUCTION_TRENDING = 0, PROPERTY = 1;
    boolean details = true;

    public OpenHouseTodayAdapter(Context context, ArrayList<OpenHouseProperty> alPropertyListInfo) {
        this.context = context;
        this.alPropertyListInfo = alPropertyListInfo;
    }

    public OpenHouseTodayAdapter(Context context, ArrayList<OpenHouseProperty> alPropertyListInfo, boolean details) {
        this.context = context;
        this.alPropertyListInfo = alPropertyListInfo;
        this.details = details;
    }

    @Override
    public PropertyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_list_item, parent, false);
        return new PropertyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PropertyViewHolder holder, final int position) {
        initProperties(holder, position);
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (details) {
                    System.out.println("id: main: " + (int) getItemId(position) + " " + getItemId(position) + " " + position);
                    Opener.PropertyDetails((Activity) context, alPropertyListInfo.get(position));
                }
            }
        });
    }


    private void initProperties(PropertyViewHolder holder, final int position) {
        final OpenHouseProperty propertyListInfo = alPropertyListInfo.get(position);
        final PropertyViewHolder propertyViewHolder = holder;
        propertyViewHolder.tvPrice.setText(propertyListInfo.price);

        if (propertyListInfo.getOhpStatus() == 0) {
            propertyViewHolder.tvStatusTag.setText(propertyListInfo.getStatusTag());
            propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
        } else {
            propertyViewHolder.tvStatusTag.setText(propertyListInfo.getStatusTag());
            propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
        }

        propertyViewHolder.tvDateTag.setBackgroundColor(context.getResources().getColor(R.color.theme_blue));
        propertyViewHolder.tvDateTag.setText(Html.fromHtml(propertyListInfo.getOHTimeTag()));

        propertyViewHolder.tvAgentFName.setText(propertyListInfo.agentFName);
        propertyViewHolder.tvAgentLName.setText(propertyListInfo.agentLName);
        propertyViewHolder.cboxFav.setChecked(propertyListInfo.isFav());
        propertyViewHolder.tvPropertyStreet.setText(propertyListInfo.street);
        propertyViewHolder.tvPropertySuburbPostcode.setText(propertyListInfo.suburb + ", " + propertyListInfo.postCode);
        if (!propertyListInfo.agentAgencyLogoUrl.trim().isEmpty())
            Picasso.with(context)
                    .load(propertyListInfo.agentAgencyLogoUrl)
                    .resize((int) CommonMethods.pxFromDp(context,96),(int)CommonMethods.pxFromDp(context,60))
                    .into(propertyViewHolder.ivAgentAgencyIcon);
        else {
            Picasso.with(context)
                    .load(R.drawable.black_livebid_logo)
                    .into(propertyViewHolder.ivAgentAgencyIcon);
        }
        if (!propertyListInfo.agentProfileImg.trim().isEmpty()) {
            Picasso.with(context)
                    .load(propertyListInfo.agentProfileImg)
                    .placeholder(R.drawable.profile_icon)
                    .transform(new CircleTransform((Activity) context))
                    .into(propertyViewHolder.ivAgentImg);
        } else {
            Picasso.with(context)
                    .load(R.drawable.profile_icon)
//                    .transform(new CircleTransform((Activity) context))
                    .into(propertyViewHolder.ivAgentImg);
        }
        //set view pager adapter
        ImageSlideAdapter mImageSlideAdapter;
        mImageSlideAdapter = new ImageSlideAdapter(context, propertyListInfo.property_images_more,propertyListInfo.propertyStatus);
        propertyViewHolder.mViewPager.setAdapter(mImageSlideAdapter);
        propertyViewHolder.mViewPager.setOnViewPagerClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Opener.PropertyDetails((Activity) context, alPropertyListInfo.get(position));

            }
        });
        if (propertyListInfo.propertyStatus == 2) {
            propertyViewHolder.tvStatusTag.setVisibility(View.GONE);
            propertyViewHolder.ivSoldPropertyTag.setVisibility(View.VISIBLE);
            propertyViewHolder.tvPropertyImageIndicator.setVisibility(View.GONE);
//                propertyViewHolder.mViewPager.beginFakeDrag();

        }else {
            propertyViewHolder.ivSoldPropertyTag.setVisibility(View.GONE);
            propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
            propertyViewHolder.tvPropertyImageIndicator.setVisibility(View.GONE);
            propertyViewHolder.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    propertyViewHolder.tvPropertyImageIndicator.setText(position+1 + "/" + propertyListInfo.property_images_more.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        }


        propertyViewHolder.tvNosBathroom.setText(propertyListInfo.nosOfBathroom);
        propertyViewHolder.tvNosBedroom.setText(propertyListInfo.nosOfBedroom);
        propertyViewHolder.tvNosParking.setText(propertyListInfo.nosOfParking);
        propertyViewHolder.tvPriceText.setVisibility(View.VISIBLE);
        propertyViewHolder.tvPriceText.setText(propertyListInfo.price_text);

        propertyViewHolder.cboxFav.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final CheckBox cboxTemp = (CheckBox) view;
                        new FavoriteHelper(context).makeFavorite(alPropertyListInfo.get(position).id, alPropertyListInfo.get(position).getFav(), new FavouriteCompleteListener() {
                            @Override
                            public void onSuccess(boolean b) {
                                alPropertyListInfo.get(position).setFav(b);
                                cboxTemp.setChecked(b);
                            }

                            @Override
                            public void onFailure() {
                                cboxTemp.setChecked(alPropertyListInfo.get(position).getFav());
                            }

                            @Override
                            public void onNotLoggedIn() {
                                cboxTemp.setChecked(false);
                                new Alerts((Activity) context).signIn();
                            }
                        });


                        // Webservice call for change.
                    }
                });
        propertyViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    if(details) {
                System.out.println("id: main: " + (int) getItemId(position) + " " + getItemId(position) + " " + position);
                Opener.PropertyDetails((Activity) context, alPropertyListInfo.get(position));
//                    }
            }
        });

    }

    @Override
    public int getItemCount() {
        return alPropertyListInfo.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 || position == 1) {
            return AUCTION_TRENDING;
        } else {
            return PROPERTY;
        }
    }

    @Override
    public long getItemId(int position) {
        System.out.println("Wrong things here: " + alPropertyListInfo.get(position).id);
        if (alPropertyListInfo.get(position) != null && alPropertyListInfo.get(position).id != null)
            return Long.parseLong(alPropertyListInfo.get(position).id);
        else return position;
    }

    public class PropertyViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvPropertyImageIndicator;
        private final SlideShowViewPager mViewPager;
        private final TextView tvPriceText;
        ImageView ivPropertyImg, ivAgentImg, ivAgentAgencyIcon, ivSoldPropertyTag;
        CheckBox cboxFav;
        TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
                tvPropertyStatusIdentifier, tvPrice, tvNosBedroom, tvNosBathroom,
                tvNosParking, tvAgentFName, tvAgentLName;
        View rootView;

        public PropertyViewHolder(View view) {
            super(view);
            rootView = view;
            mViewPager = (SlideShowViewPager) view.findViewById(R.id.vp_property_img);
            tvPropertyImageIndicator = (TextView) view.findViewById(R.id.tvPropertyImageIndicator);
            ivAgentImg = (ImageView) view.findViewById(R.id.iv_agent_img);
            ivSoldPropertyTag = (ImageView) view.findViewById(R.id.iv_property_sold_tag);
            ivAgentAgencyIcon = (ImageView) view.findViewById(R.id.iv_agent_agency_icon);
            cboxFav = (CheckBox) view.findViewById(R.id.cbox_fav);

            tvStatusTag = (TextView) view.findViewById(R.id.tv_property_status_tag);
            tvPriceText = (TextView) view.findViewById(R.id.tvPriceText);
            tvDateTag = (TextView) view.findViewById(R.id.tv_property_date_tag);
            tvPropertyStreet = (TextView) view.findViewById(R.id.tv_property_street);
            tvPropertySuburbPostcode = (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
            tvPropertyStatusIdentifier = (TextView) view.findViewById(R.id.tv_property_status_identifier);
            tvAgentFName = (TextView) view.findViewById(R.id.tv_agent_fname);
            tvAgentLName = (TextView) view.findViewById(R.id.tv_agent_lname);
            tvPrice = (TextView) view.findViewById(R.id.tv_property_price);
            tvNosBedroom = (TextView) view.findViewById(R.id.tv_nosBedroom);
            tvNosBathroom = (TextView) view.findViewById(R.id.tv_nosBathroom);
            tvNosParking = (TextView) view.findViewById(R.id.tv_nosParking);
        }
    }

    public void addMoreData(ArrayList<OpenHouseProperty> data) {
        int startPosition = alPropertyListInfo.size();
        this.alPropertyListInfo.addAll(data);
        this.notifyItemRangeInserted(startPosition, data.size());
    }


}

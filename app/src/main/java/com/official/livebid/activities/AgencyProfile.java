package com.official.livebid.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.official.livebid.R;
import com.official.livebid.SalesTeamActivity;
import com.official.livebid.Views.EndlessListView;
import com.official.livebid.Views.ExpandableTextView;
import com.official.livebid.adapters.AgentProfilePropertiesListingsAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.AgencyInfo;
import com.official.livebid.objects.AgentInfo;
import com.official.livebid.objects.AvailabilityModel;
import com.official.livebid.objects.PropertyInfo;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.utils.CircleTransform;
import com.official.livebid.utils.GPSTracker;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AgencyProfile extends AppCompatActivity implements AsyncInterface, OnMapReadyCallback, View.OnClickListener, EndlessListView.EndlessListener {

    private Alerts alerts;
    private CustomActionBar.AgentPublicProfileTitleBar titleBar;
    private EndlessListView lvAgencyProfilePropertyListings;
    private ExpandableTextView etvAgencyDesc;
    private TextView tvAgencyName, tvAgencyStreet, tvAgencySubUrbPostCode, tvAgencyUrl;
    private TextView tvCurrentStatus, tvAgencyAbout;
    ImageView ivAgencyImg;
    AgencyInfo agencyInfo;
    private ArrayList<AvailabilityModel> availabilities;
    private CustomActionBar.AgentPublicProfileTitleBar agencyProfileTitlebar;
    private String TAG = "gmap";
    private LinearLayout llAvailability;
    Button btnContactAgency;
    TextView viewMore;
    Button btnGetDirection;
    Button btnViewMoreSalesAgent;
    LinearLayout llSalesAgentContainer;
    LinearLayout llSalesAgentViewMore;
    GoogleMap googleMap;
    ArrayList<PropertyInfo> properties;
    private AgentProfilePropertiesListingsAdapter propertiesListingsAdapter;
    double lat, lng;
    GPSTracker gps;
    private boolean isLocationSet = false;
    int offset = 0;//property
    boolean isLastPage = false;//property
    int salesAgentOffset = 0;//sales agent
    boolean isLastSalesAgent = false;

    private AgentInfo agentInfo;
    private ArrayList<AvailabilityModel> agentAvailabilities;
    private AgentInfo salesAgent;
    private ArrayList<AgentInfo> salesTeam;
    private String agencyId = "1";
    private SalesTeamActivity activity;
    private ArrayList<AgentInfo> salesAgentsMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agency_profile);
        getSupportActionBar().hide();

//        rl_contactAgent = (RelativeLayout) findViewById(R.id.rl_contact_agent);
//        ll_ContactAgent = (LinearLayout) findViewById(R.id.ll_contact_agent);
//        btnCancel = (Button) findViewById(R.id.btn_cancel);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            agencyId = b.getString("agency_id");
        }
        activity = new SalesTeamActivity();
        gps = new GPSTracker(AgencyProfile.this);
        alerts = new Alerts(this);
        salesTeam = new ArrayList<>();
        titleBar = new CustomActionBar.AgentPublicProfileTitleBar(this, "agency name");
        View header = getLayoutInflater().inflate(R.layout.agency_profile_header, null, false);

        ivAgencyImg = (ImageView) header.findViewById(R.id.iv_agency_img);
        tvAgencyName = (TextView) header.findViewById(R.id.tv_agency_name);
        tvAgencyStreet = (TextView) header.findViewById(R.id.tv_agency_street);
        tvAgencySubUrbPostCode = (TextView) header.findViewById(R.id.tv_agency_suburb_postcode);
        tvAgencyUrl = (TextView) header.findViewById(R.id.tv_agency_url);
        viewMore = (TextView) header.findViewById(R.id.expand_collapse);
        tvCurrentStatus = (TextView) header.findViewById(R.id.tv_reg_status);
        tvAgencyAbout = (TextView) header.findViewById(R.id.tv_agency_about);
        etvAgencyDesc = (ExpandableTextView) header.findViewById(R.id.expand_text_view);
        llAvailability = (LinearLayout) header.findViewById(R.id.ll_availability);
        llSalesAgentContainer = (LinearLayout) header.findViewById(R.id.ll_agency_sales_team);
        btnContactAgency = (Button) findViewById(R.id.btn_contact_agency);
        btnGetDirection = (Button) header.findViewById(R.id.btn_agency_get_direction);
        btnViewMoreSalesAgent = (Button) header.findViewById(R.id.btn_sales_team_view_more);
        llSalesAgentViewMore = (LinearLayout) header.findViewById(R.id.ll_sales_team_view_more);
        btnContactAgency.setOnClickListener(this);
        btnGetDirection.setOnClickListener(this);
        btnViewMoreSalesAgent.setOnClickListener(this);
        lvAgencyProfilePropertyListings = (EndlessListView) findViewById(R.id.lv_agency_profile_property_listings);
        lvAgencyProfilePropertyListings.addHeaderView(header);
        getAgencyProfile();
        loadSalesAgent();
        getPropertyList();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_agency_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private ArrayList<PropertyListInfo> initFakePropertyList() {
        ArrayList<PropertyListInfo> propertyListInfos = new ArrayList<>();
        PropertyListInfo propertyListInfo = null;
        for (int i = 0; i < 5; i++) {
            propertyListInfo = new PropertyListInfo();
            propertyListInfo.nosOfBathroom = "4";
            propertyListInfo.nosOfBedroom = "4";
            propertyListInfo.nosOfParking = "2";
            propertyListInfo.price = "$142,345,678";
            propertyListInfo.street = "8H / 22 ROSS ST";
            propertyListInfo.suburb = "North Sydney, 2060";
            propertyListInfo.postCode = " 2060";
            propertyListInfo.propertyImgUrl = "";
            propertyListInfos.add(propertyListInfo);

        }
        return propertyListInfos;

    }

    public void getAgencyProfile() {
        HashMap<String, String> headers = CommonMethods.getHeaders(AgencyProfile.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("agency_id", agencyId);
        System.out.println("agency details params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(AgencyProfile.this, Request.Method.POST, params, UrlHelper.GET_AGENCY_PROFILE, headers, false, CommonDef.REQUEST_AGENCY_PROFILE);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_AGENCY_PROFILE:
                System.out.println("agency details=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        agencyInfo = new AgencyInfo();
                        availabilities = new ArrayList<AvailabilityModel>();
                        JSONObject jObjDetails = joResult.getJSONObject("detail");
                        agencyInfo.setId(jObjDetails.getString("id"));
                        agencyInfo.setName(jObjDetails.getString("name"));
                        agencyInfo.setStreet(jObjDetails.getString("street"));
                        agencyInfo.setState(jObjDetails.getString("state"));
                        agencyInfo.setSuburb(jObjDetails.getString("suburb"));
                        agencyInfo.setPostcode(jObjDetails.getString("postcode"));
                        agencyInfo.setWebsite(jObjDetails.getString("website"));
                        agencyInfo.setLogoUrl(jObjDetails.getString("logo_url"));
                        agencyInfo.setCurrentlyOpen(jObjDetails.getString("currently_open"));
                        agencyInfo.setLat(jObjDetails.getString("lat"));
                        agencyInfo.setLng(jObjDetails.getString("lng"));
                        agencyInfo.setDescription(jObjDetails.getString("description"));
                        JSONArray jObjAvailabilities = joResult.getJSONArray("availabilities");
                        JSONObject jObjAvailability = null;
                        for (int i = 0; i < jObjAvailabilities.length(); i++) {
                            jObjAvailability = jObjAvailabilities.getJSONObject(i);
                            availabilities.add(new AvailabilityModel(
                                    jObjAvailability.getString("from_time"),
                                    jObjAvailability.getString("to_time"),
                                    jObjAvailability.getInt("to_day"),
                                    jObjAvailability.getInt("from_day")

                            ));
                        }
                        agencyInfo.setAvailabilities(availabilities);
                        showAgencyInfo();
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_PROPERTY_LISTINGS:
                System.out.println("property Listings =>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        JSONArray propertyList = joResult.getJSONArray("list");
                        JSONObject prop = null;
                        PropertyInfo mProp = null;
                        PropertyInfo.OHI mOhi = null;
                        properties = new ArrayList<>();
                        PropertyInfo.Property_images_more moreImage = null;
                        ArrayList<PropertyInfo.OHI> inspectionTimes = null;
                        ArrayList<PropertyInfo.Property_images_more> propertyMoreImages = null;
                        for (int i = 0; i < propertyList.length(); i++) {
                            inspectionTimes = new ArrayList<PropertyInfo.OHI>();
                            propertyMoreImages = new ArrayList<PropertyInfo.Property_images_more>();
                            prop = propertyList.getJSONObject(i);
                            mProp = new PropertyInfo();
                            mProp.setId(prop.getString("id"));
                            mProp.setAgent_id(prop.getString("agent_id"));
                            mProp.setAgency_id(prop.getString("agency_id"));
                            mProp.setAuction_date(prop.getString("auction_date"));
                            mProp.setProperty_image(prop.getString("property_image"));
                            mProp.setProperty_title(prop.getString("property_title"));
                            mProp.setProperty_description(prop.getString("property_description"));
                            mProp.setPrice(prop.getString("price"));
                            mProp.setStreet(prop.getString("street"));
                            mProp.setPostcode(prop.getString("postcode"));
                            mProp.setSuburb(prop.getString("suburb"));
                            mProp.setState(prop.getString("state"));
                            mProp.setParking(prop.getString("parking"));
                            mProp.setBathrooms(prop.getString("bathrooms"));
                            mProp.setBedrooms(prop.getString("bedrooms"));
                            mProp.setLat(prop.getString("lat"));
                            mProp.setLng(prop.getString("lng"));
                            mProp.setProperty_status(prop.getInt("property_status"));
                            mProp.setProperty_code(prop.getString("property_code"));
                            mProp.setAuthority(prop.getString("authority"));
                            mProp.setAgent_name(prop.getString("agent_name"));
                            mProp.setUnique_number(prop.getString("unique_number"));
                            mProp.setAgent_profile_image(prop.getString("agent_profile_image"));
                            mProp.setAgency_logo_url(prop.getString("agency_logo_url"));
                            mProp.setFavorite(prop.getString("favorite"));
                            mProp.setDistance(prop.getString("distance"));
                            mProp.setRegisteredForProperty(prop.getString("registered_for_property"));
                            mProp.setAuctionDateTimeStamp(prop.getLong("auction_date_timestamp"));
                            mProp.setPropertyStatus2(prop.getInt("property_status_2"));
                            JSONArray jOhi = prop.getJSONArray("inspection_times");
                            if (jOhi.length() > 0) {
                                for (int j = 0; j < jOhi.length(); j++) {
                                    mOhi = new PropertyInfo.OHI(
                                            jOhi.getJSONObject(j).getString("inspection_date"),
                                            jOhi.getJSONObject(j).getString("inspection_time_from"),
                                            jOhi.getJSONObject(j).getString("inspection_time_to")
                                    );
                                    inspectionTimes.add(mOhi);
                                }
                            }
                            mProp.setInspectionTimeString(jOhi.toString());
                           /* JSONArray JPropertyImageMore = prop.getJSONArray("property_images_more");
                            if (JPropertyImageMore.length() > 0) {
                                for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                    moreImage = new PropertyInfo.Property_images_more(
                                            JPropertyImageMore.getString(j));

                                    propertyMoreImages.add(moreImage);
                                }
                            }*/
                            JSONArray JPropertyImageMore = prop.getJSONArray("property_images_more");
                            if (JPropertyImageMore.length() > 0) {
                                for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                    mProp.property_images_more.add(JPropertyImageMore.getString(j));
                                }
                            }
                            mProp.setInspectionTimes(inspectionTimes);
                            properties.add(mProp);


                        }
                        isLastPage = joResult.getBoolean("last_page");

                        if (offset == 0) {
                            propertiesListingsAdapter = new AgentProfilePropertiesListingsAdapter(this, properties);
                            lvAgencyProfilePropertyListings.setAdapter(propertiesListingsAdapter);
                            lvAgencyProfilePropertyListings.setLoadingView(R.layout.layout_endless_loading);
                            lvAgencyProfilePropertyListings.setListener(this);
                        } else {
                            lvAgencyProfilePropertyListings.addNewData(properties);
                        }
                        offset = joResult.getInt("offset");

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
            case CommonDef.REQUEST_AGENT_LISTINGS:
                System.out.println("agent listing=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        salesAgentsMain = new ArrayList<>();
                        JSONArray jObjAgentList = joResult.getJSONArray("agents");
                        JSONObject agent = null;
                        for (int i = 0; i < jObjAgentList.length(); i++) {
                            agentInfo = new AgentInfo();
                            agentAvailabilities = new ArrayList<AvailabilityModel>();
                            agent = jObjAgentList.getJSONObject(i);
                            agentInfo.setUserId(agent.getString("agent_id"));
                            agentInfo.setUniqueNumber(agent.getString("unique_number"));
                            agentInfo.setName(agent.getString("agent_name"));
                            agentInfo.setEmail(agent.getString("email"));
                            agentInfo.setMobile(agent.getString("mobile_number"));
                            agentInfo.setProfileImage(agent.getString("profile_image"));
                            agentInfo.setStatus(agent.getString("status"));
                            JSONArray jaAgentAvailabilities = agent.getJSONArray("availabilities");
                            JSONObject joAvailability;
                            for (int j = 0; j < jaAgentAvailabilities.length(); j++) {
                                joAvailability = jaAgentAvailabilities.getJSONObject(j);
                                agentAvailabilities.add(new AvailabilityModel(
                                        joAvailability.getString("from_time"),
                                        joAvailability.getString("to_time"),
                                        joAvailability.getInt("to_day"),
                                        joAvailability.getInt("from_day")

                                ));
                            }
                            agentInfo.setAvailabilities(agentAvailabilities);
                            salesAgentsMain.add(agentInfo);
                        }
                        setSalesAgent(salesAgentsMain);
//                        activity.getArrayList(salesAgents);
                        isLastSalesAgent = joResult.getBoolean("last_page");
                        if (isLastSalesAgent) {
//                            llSalesAgentViewMore.setVisibility(View.GONE);
                        }
                        salesAgentOffset = joResult.getInt("offset");

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
        }
    }

    private void showAgencyInfo() {
        agencyProfileTitlebar = new CustomActionBar.AgentPublicProfileTitleBar(this, agencyInfo.getName() + " - " + agencyInfo.getSuburb());
        tvAgencyName.setText(agencyInfo.getName());
        tvAgencyStreet.setText(agencyInfo.getStreet());
        tvAgencySubUrbPostCode.setText(agencyInfo.getSuburb() + " " +agencyInfo.getState()+" "+agencyInfo.getPostcode());
        tvAgencyUrl.setText(agencyInfo.getWebsite());
        tvCurrentStatus.setText(agencyInfo.getCurrentlyOpen().equalsIgnoreCase("0") ? "Currently Closed" : "Currently Open");
        tvAgencyAbout.setText("About " + agencyInfo.getName() + " - " + agencyInfo.getSuburb());
        etvAgencyDesc.setText(agencyInfo.getDescription());
        if(!agencyInfo.getLogoUrl().trim().isEmpty()) {
            Picasso.with(AgencyProfile.this)
                    .load(agencyInfo.getLogoUrl())
                    .resize((int) CommonMethods.pxFromDp(AgencyProfile.this, 120), (int) CommonMethods.pxFromDp(AgencyProfile.this, 120))
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(ivAgencyImg);
        }
        else {
            Picasso.with(AgencyProfile.this)
                    .load(R.drawable.img_coming_soon)
                    .resize((int) CommonMethods.pxFromDp(AgencyProfile.this, 120), (int) CommonMethods.pxFromDp(AgencyProfile.this, 120))
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(ivAgencyImg);
        }
        for (int i = 0; i < agencyInfo.getAvailabilities().size(); i++) {
            llAvailability.addView(addAvailabilityView(agencyInfo.getAvailabilities().get(i)));
            if (i < agencyInfo.getAvailabilities().size() - 1) {
                llAvailability.addView(addDivider());
            }
        }

        initialiseMap();

    }

    private void initialiseMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
//        MapFragment mapFragment = getMapFragment();
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        LatLng myPlace = new LatLng(Double.parseDouble(agencyInfo.getLat()), Double.parseDouble(agencyInfo.getLng()));
        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    CommonDef.REQUEST_LOCATION);
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 15));
        googleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon))
                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                .position(myPlace));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//            Log.d(TAG, "using getFragmentManager");
//            FragmentManager fm = getFragmentManager();
//            Fragment fragment = (fm.findFragmentById(R.id.map));
//            FragmentTransaction ft = fm.beginTransaction();
//            ft.remove(fragment);
//            ft.commit();
//        }
    }

//    private MapFragment getMapFragment() {
//        FragmentManager fm = null;
//
//        Log.d(TAG, "sdk: " + Build.VERSION.SDK_INT);
//        Log.d(TAG, "release: " + Build.VERSION.RELEASE);
//
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//            Log.d(TAG, "using getFragmentManager");
//            fm = getFragmentManager();
//        } else {
//            Log.d(TAG, "using getChildFragmentManager");
////            fm =getChildFragmentManager();
//            fm = getFragmentManager();
//        }
//
//        return (MapFragment) fm.findFragmentById(R.id.map);
//    }

    private View addAvailabilityView(AvailabilityModel a) {
        View view = getLayoutInflater().inflate(R.layout.layout_availability, null, false);
        TextView avDay = (TextView) view.findViewById(R.id.tv_agency_available_day);
        TextView avTime = (TextView) view.findViewById(R.id.tv_agency_available_time);
        avDay.setText(CommonMethods.getAvailableDay(a.getFromDay(), a.getToDay()));
        avTime.setText(a.getFromTime() + " - " + a.getToTime());
        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_contact_agency:
                alerts.contactAgencyBottomSheet(agencyInfo);
                break;
            case R.id.btn_agency_get_direction:
//                Toast.makeText(getApplicationContext(), "direction", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=18.015365,-77.499382&daddr=18.012590, -77.500659"));
                startActivity(intent);
                break;
            case R.id.btn_contact:
//                Toast.makeText(getApplicationContext(), "contact  " + view.getTag(), Toast.LENGTH_SHORT).show();
                alerts.contactAgentBottomSheet(salesTeam.get((Integer) view.getTag()));
                break;
            case R.id.rl_agent_details:
                Opener.AgentPublicProfile(AgencyProfile.this, salesTeam.get((Integer) view.getTag()).getUserId());
                break;
            case R.id.btn_sales_team_view_more:
                loadSalesAgent();
                Opener.SalesTeam(this,salesAgentsMain,agencyId);
                break;

        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    CommonMethods.makeCall(activity, agencyInfo.getMobile());
                break;

        }
    }

    @Override
    public void loadData() {
        if (!isLastPage)
            getPropertyList();
        else
            lvAgencyProfilePropertyListings.finaliseLoading();
    }

    @Override
    public void showToolbar() {

    }

    @Override
    public void hideToolbar() {

    }

    private void loadSalesAgentFake() {
        ViewHolder holder;
//        salesAgents = new ArrayList<>();
        AgentInfo agentInfo;
        for (int i = 0; i < 3; i++) {
            agentInfo = new AgentInfo();
            agentInfo.setUserId(93 + i + "");
            holder = new ViewHolder();
            View view = getLayoutInflater().inflate(R.layout.sales_team_item, null, false);
            holder.rlAgentDetails = (RelativeLayout) view.findViewById(R.id.rl_agent_details);
            holder.rlAgentDetails.setTag(i);
            holder.ivAgentImg = (ImageView) view.findViewById(R.id.iv_agent_img);
            holder.btnContact = (Button) view.findViewById(R.id.btn_contact);
            holder.btnContact.setTag(i);
            llSalesAgentContainer.addView(view);
            if (i < 2) {
                llSalesAgentContainer.addView(addDivider());
            }
//            salesAgents.add(agentInfo);
            holder.rlAgentDetails.setOnClickListener(this);
            holder.btnContact.setOnClickListener(this);

        }
    }

    private static class ViewHolder {
        RelativeLayout rlAgentDetails;
        ImageView ivAgentImg;
        TextView tvAgentName,
                tvSaleAgentNo;
        Button btnContact;


    }

    private void setSalesAgent(ArrayList<AgentInfo> agents) {
        ViewHolder holder;
        HashMap<String,String> name=null;
        if (agents.size()>3) {
            llSalesAgentViewMore.setVisibility(View.VISIBLE);
            btnViewMoreSalesAgent.setVisibility(View.VISIBLE);
            for (int i = 0; i < 3; i++) {
                salesAgent = agents.get(i);
                holder = new ViewHolder();
                View view = getLayoutInflater().inflate(R.layout.sales_team_item, null, false);
                holder.rlAgentDetails = (RelativeLayout) view.findViewById(R.id.rl_agent_details);
//            holder.rlAgentDetails.setTag(salesAgent.getUserId());
                holder.rlAgentDetails.setTag(i);
                holder.ivAgentImg = (ImageView) view.findViewById(R.id.iv_agent_img);
                holder.tvAgentName = (TextView) view.findViewById(R.id.tv_agent_name);
                holder.tvSaleAgentNo = (TextView) view.findViewById(R.id.tv_agent_unique_no);
                holder.btnContact = (Button) view.findViewById(R.id.btn_contact);
//            holder.btnContact.setTag(salesAgent.getUserId());
                holder.btnContact.setTag(i);
                name = CommonMethods.getName(salesAgent.getName().split(" "));
                holder.tvAgentName.setText(Html.fromHtml("<b>" + name.get("fName") + "</b>" + " " + name.get("lName")));
                holder.tvSaleAgentNo.setText("Sales Agent- " + salesAgent.getUniqueNumber());
                if (!salesAgent.getProfileImage().trim().isEmpty()) {
                    Picasso.with(AgencyProfile.this)
                            .load(salesAgent.getProfileImage())
                            .resize((int) CommonMethods.pxFromDp(AgencyProfile.this, 60), (int) CommonMethods.pxFromDp(AgencyProfile.this, 60))
                            .centerCrop()
                            .transform(new CircleTransform())
                            .into(holder.ivAgentImg);
                }else {
                    Picasso.with(AgencyProfile.this)
                            .load(R.drawable.profile_icon)
//                            .transform(new CircleTransform())
                            .into(holder.ivAgentImg);
                }
                if (llSalesAgentContainer.getChildCount() > 0) {
                    llSalesAgentContainer.addView(addDivider());
                }

                llSalesAgentContainer.addView(view);
                expand(view);

                salesTeam.add(salesAgent);


                holder.rlAgentDetails.setOnClickListener(this);
                holder.btnContact.setOnClickListener(this);
            }
        }
        else{
            for (int i = 0; i < agents.size(); i++) {
                btnViewMoreSalesAgent.setVisibility(View.GONE);
                salesAgent = agents.get(i);
                holder = new ViewHolder();
                View view = getLayoutInflater().inflate(R.layout.sales_team_item, null, false);
                holder.rlAgentDetails = (RelativeLayout) view.findViewById(R.id.rl_agent_details);
//            holder.rlAgentDetails.setTag(salesAgent.getUserId());
                holder.rlAgentDetails.setTag(i);
                holder.ivAgentImg = (ImageView) view.findViewById(R.id.iv_agent_img);
                holder.tvAgentName = (TextView) view.findViewById(R.id.tv_agent_name);
                holder.tvSaleAgentNo = (TextView) view.findViewById(R.id.tv_agent_unique_no);
                holder.btnContact = (Button) view.findViewById(R.id.btn_contact);
//            holder.btnContact.setTag(salesAgent.getUserId());
                holder.btnContact.setTag(i);
                name = CommonMethods.getName(salesAgent.getName().split(" "));
                holder.tvAgentName.setText(Html.fromHtml("<b>" + name.get("fName") + "</b>" + " " + name.get("lName")));
                holder.tvSaleAgentNo.setText("Sales Agent- " + salesAgent.getUniqueNumber());
                if (!salesAgent.getProfileImage().trim().isEmpty()) {
                    Picasso.with(AgencyProfile.this)
                            .load(salesAgent.getProfileImage())
                            .resize((int) CommonMethods.pxFromDp(AgencyProfile.this, 60), (int) CommonMethods.pxFromDp(AgencyProfile.this, 60))
                            .centerCrop()
                            .transform(new CircleTransform())
                            .into(holder.ivAgentImg);
                }else {
                    Picasso.with(AgencyProfile.this)
                            .load(R.drawable.io_white_profile_default)
                            .transform(new CircleTransform())
                            .into(holder.ivAgentImg);
                }
                if (llSalesAgentContainer.getChildCount() > 0) {
                    llSalesAgentContainer.addView(addDivider());
                }

                llSalesAgentContainer.addView(view);
                expand(view);

                salesTeam.add(salesAgent);


                holder.rlAgentDetails.setOnClickListener(this);
                holder.btnContact.setOnClickListener(this);
            }

        }
    }


    private void loadSalesAgent() {
        if (!isLastSalesAgent) {
            HashMap<String, String> headers = CommonMethods.getHeaders(AgencyProfile.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("agency_id", agencyId);
            params.put("offset", salesAgentOffset + "");

            System.out.println("agent Listings params=>" + params.toString());
            VolleyRequest volleyRequest = new VolleyRequest(AgencyProfile.this, Request.Method.POST, params, UrlHelper.GET_AGENT_LISTINGS, headers, false, CommonDef.REQUEST_AGENT_LISTINGS);
            volleyRequest.asyncInterface = this;
            volleyRequest.request();
        }
    }

    private View addDivider() {
        FrameLayout v = new FrameLayout(this);
        v.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 1));
        v.setBackgroundColor(getResources().getColor(R.color.form_divider));
        return v;
    }

    private void getPropertyList() {
        HashMap<String, String> headers = CommonMethods.getHeaders(AgencyProfile.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", agencyId);
        params.put("listing_type", "2");
        params.put("offset", offset + "");
        params.put("lat", lat + "");
        params.put("lng", lng + "");
        System.out.println("peopertyListings params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(AgencyProfile.this, Request.Method.POST, params, UrlHelper.GET_PROPERTY_LISTINGS, headers, false, CommonDef.REQUEST_PROPERTY_LISTINGS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
            isLocationSet = true;
        } else {
            gps.showSettingsAlert();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gps.stopUsingGPS();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gps.stopUsingGPS();
    }

    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();
//        Log.d("height", String.valueOf(targetHeight));

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? RelativeLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(2 * (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public ArrayList<AgentInfo> getSalesTeamArray(){
        return salesTeam;
    }
}

package com.official.livebid.activities.profile;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.activities.LandingScreen;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Profile extends MenuActivity implements AsyncInterface {
    private ImageView iv_profile;
    private SharedPreference userPrefs;
    private CustomActionBar.UserProfileTitleBar userProfileTitleBar;
    private TextView tvLogout;
    private TextView tvPrivacyPolicy;
    private TextView tvTOS;
    private TextView tvHelpAndSupport;
    private TextView tvSettings;
    private Alerts alerts;

    @Override
    public int getLayoutId() {
        return R.layout.activity_profile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.user_profile);
        getSupportActionBar().hide();
        alerts = new Alerts(this);
        userPrefs = new SharedPreference(this);
        userProfileTitleBar = new CustomActionBar.UserProfileTitleBar(this, "My Profile");
//        userProfileTitleBar.enableEdit(false);
//        iv_profile = (ImageView) findViewById(R.id.iv_profile);
//        CommonMethods.setImageviewRatio(this, iv_profile, 0);
//        userPrefs = new SharedPreference(this);
//        tvLogout = (TextView) findViewById(R.id.tv_log_out);
//        tvPrivacyPolicy = (TextView) findViewById(R.id.tv_privacy_policy);
//        tvTOS = (TextView) findViewById(R.id.tv_tos);
//        tvHelpAndSupport = (TextView) findViewById(R.id.tv_help_support);
//        tvSettings = (TextView) findViewById(R.id.tv_settings);
//        tvLogout.setOnClickListener(textViewClickListener);
//        tvPrivacyPolicy.setOnClickListener(textViewClickListener);
//        tvTOS.setOnClickListener(textViewClickListener);
//        tvHelpAndSupport.setOnClickListener(textViewClickListener);
//        tvSettings.setOnClickListener(textViewClickListener);
        if(userType == AppMode.user) {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.profile_container, new FragmentUserProfile())
                    .addToBackStack(null)
                    .commit();
        }
        else if(userType== AppMode.guest){
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.profile_container, new FragmentGuestUserProfile())
                    .addToBackStack(null)
                    .commit();
        }
        else if(userType== AppMode.agent){
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.profile_container, new FragmentAgentProfile())
                    .addToBackStack(null)
                    .commit();
        }



    }
    public void hideEdit(){
        userProfileTitleBar.enableEdit(false);
    }
    public void showEdit(){
        userProfileTitleBar.enableEdit(true);
    }


    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_LOGOUT:
                System.out.println("LOGOUT=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                       alerts.showOkMessage(msg);
                        performLogout();


                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;

        }

    }

    public void performLogout() {
        String user_id = userPrefs.getStringValues(CommonDef.SharedPrefKeys.USER_ID);
        String searchPostCodeIds = userPrefs.getStringValues(CommonDef.SharedPrefKeys.Search_PostCodeIds);
        String  searchSuburbsNames= userPrefs.getStringValues(CommonDef.SharedPrefKeys.Search_SuburbNames);
        userPrefs.clearData();
        userPrefs.setKeyValues(CommonDef.SharedPrefKeys.OLD_USER_ID,user_id);
        userPrefs.setKeyValues(CommonDef.SharedPrefKeys.Search_PostCodeIds,searchPostCodeIds);
        userPrefs.setKeyValues(CommonDef.SharedPrefKeys.Search_SuburbNames,searchSuburbsNames);
        Intent intent = new Intent(this, LandingScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void logout() {
        HashMap<String, String> headers = CommonMethods.getHeaders(Profile.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("device_type", "2");
        System.out.println("logout header=>" + headers.toString());
        VolleyRequest volleyRequest = new VolleyRequest(Profile.this, Request.Method.POST, params, UrlHelper.LOG_OUT, headers, false, CommonDef.REQUEST_LOGOUT);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }


    @Override
    public void onBackPressed() {
//        if(userProfileTitleBar.editMode){
//            userProfileTitleBar.editMode=false;
//            Fragment fragment = getFragmentManager().findFragmentById(R.id.profile_container);
//            if (fragment instanceof UserPersonalDetails) {
//                ((UserPersonalDetails) fragment).enableEditMode(false);
//                showEdit();
//            }
//            else if (fragment instanceof AgentPersonalDetails) {
//                ((AgentPersonalDetails) fragment).enableEditMode(false);
//                showEdit();
//            }
//            return;
//        }
        if( mDrawerlayout.isDrawerOpen(GravityCompat.END)){
            mDrawerlayout.closeDrawers();
        }
        else {
            if (getFragmentManager().getBackStackEntryCount() == 1) {
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                return;
            }
            getFragmentManager().popBackStackImmediate();
            menuAdapter.refreshMenu();
            showTabBar();
        }
    }

    @Override
    public void hideTabBar() {
        super.hideTabBar();
    }

    @Override
    public void showTabBar() {
        super.showTabBar();
    }


}

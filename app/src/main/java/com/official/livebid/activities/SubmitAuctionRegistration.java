package com.official.livebid.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class SubmitAuctionRegistration extends ActionBarActivity implements View.OnClickListener, AsyncInterface {
    ImageView ivSignature;
    private SharedPreference prefs;
    private Alerts alert;
    private ImageView ivDoc,ivSelf;
    private Bitmap bmtSign;
    private Button btnSubmitApplication,btn_modify;
    private Alerts alerts;
    private TextView tvFullName, tvAddress, tvMobileNumber;
    private Uri doc,self;
    private Bitmap docBitmap;
    private Bitmap selfBitmap;
    private String docId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_submit_auction_registration);
        prefs = new SharedPreference(this);
        alerts = new Alerts(this);
        new CustomActionBar.AuctionRegistrationMenu(SubmitAuctionRegistration.this, 3);
        tvFullName = (TextView) findViewById(R.id.tv_full_name);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        tvMobileNumber = (TextView) findViewById(R.id.tv_mobile_number);
        ivSignature = (ImageView) findViewById(R.id.iv_signature);
        btnSubmitApplication = (Button) findViewById(R.id.btn_bottom);
        btn_modify  = (Button) findViewById(R.id.btn_edit);
        ivDoc = (ImageView) findViewById(R.id.iv_doc);
        ivSelf = (ImageView) findViewById(R.id.iv_selfie);


        btnSubmitApplication.setOnClickListener(this);
        ivSignature.setOnClickListener(this);
        btn_modify.setOnClickListener(this);
        Bundle bundle = getIntent().getExtras();
        doc = bundle.getParcelable("docUri");
        self = bundle.getParcelable("self");
        docId = String.valueOf(bundle.getInt("selectedId"));

        setDocImages(doc,self);
        setPersonalDetails();
    }

    private void setDocImages(Uri doc, Uri self) {

        ivDoc.setImageURI(doc);
        ivSelf.setImageURI(self);
        try {
            docBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), doc);
            selfBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), doc);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setPersonalDetails() {
        tvFullName.setText(
                prefs.getStringValues(CommonDef.SharedPrefKeys.FIRST_NAME)
                        + " "
                        + prefs.getStringValues(CommonDef.SharedPrefKeys.LAST_NAME)
        );
        tvAddress.setText(prefs.getStringValues(CommonDef.SharedPrefKeys.ADDRESS));
        tvMobileNumber.setText(prefs.getStringValues(CommonDef.SharedPrefKeys.MOBILE_NUMBER));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_submit_auction_registration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_signature:
//                bmtSign=null;
                enableSubmit();
                Intent i = new Intent(SubmitAuctionRegistration.this, Signature.class);
                startActivityForResult(i, CommonDef.CAPTURE_SIGN);
                break;
            case R.id.btn_bottom:
                registerAuction(docId,docBitmap);
                break;
            case R.id.btn_edit:
                Opener.docVerification(this);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CommonDef.CAPTURE_SIGN) {
                byte[] byteArray = data.getByteArrayExtra("image");
                bmtSign = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                ivSignature.setImageBitmap(bmtSign);
            }
            enableSubmit();
        }

    }

    /**
     * method to check if the form is submittable or not
     */
    private void enableSubmit() {
        if (bmtSign != null) {
            btnSubmitApplication.setEnabled(true);
        } else {
            btnSubmitApplication.setEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        cancelAuctionReg();
    }

    private void registerAuction(String docTypeID,Bitmap doc) {
        HashMap<String, String> headers = CommonMethods.getHeaders(SubmitAuctionRegistration.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", PropertyDetails.propertyId);
        params.put("document_type_id",docTypeID);
        params.put("signature", "signature");
        System.out.println("register Auction params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(SubmitAuctionRegistration.this, Request.Method.POST, params, UrlHelper.REGISTER_TO_AUCTION, headers, true, CommonDef.REQUEST_REGISTER_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.profileImg = doc;
        volleyRequest.request();
    }

    private void registerAuctionSecondDoc(String docTypeID,Bitmap doc) {
        HashMap<String, String> headers = CommonMethods.getHeaders(SubmitAuctionRegistration.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", PropertyDetails.propertyId);
        params.put("document_type_id",docTypeID);
        params.put("signature", "signature");
        System.out.println("register Auction params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(SubmitAuctionRegistration.this, Request.Method.POST, params, UrlHelper.REGISTER_TO_AUCTION, headers, true, CommonDef.REQUEST_REGISTER_AUCTION_SECOND);
        volleyRequest.asyncInterface = this;
        volleyRequest.profileImg = doc;
        volleyRequest.request();
    }

    private void registerAuctionThirdDoc(String docTypeID) {
        HashMap<String, String> headers = CommonMethods.getHeaders(SubmitAuctionRegistration.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", PropertyDetails.propertyId);
        params.put("document_type_id",docTypeID);
        params.put("signature", "signature");
        System.out.println("register Auction params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(SubmitAuctionRegistration.this, Request.Method.POST, params, UrlHelper.REGISTER_TO_AUCTION, headers, true, CommonDef.REQUEST_REGISTER_AUCTION_THIRD);
        volleyRequest.asyncInterface = this;
        volleyRequest.profileImg = bmtSign;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("Register auction response:" + result);
        switch (requestCode) {
            case CommonDef.REQUEST_REGISTER_AUCTION:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        prefs.setKeyValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS, joResult.getString("auction_register_status"));
//                        PropertyDetails.propertyListInfo.registeredForProperty = "1";
//                        PropertyDetails.needToUpdate=true;
                        registerAuctionSecondDoc("3",selfBitmap);


                    } else {
                        alerts.confirmAuctionRegistration(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;

            case CommonDef.REQUEST_REGISTER_AUCTION_SECOND:
                try{
                    JSONObject jsonObject = new JSONObject(result);
                    String msg,code;
                    code = jsonObject.getString("code");
                    msg = jsonObject.getString("msg");
                    if (code.equalsIgnoreCase("0001")) {
                        registerAuctionThirdDoc("4");
                    } else{
                        alerts.confirmAuctionRegistration(msg);
                    }
                } catch(JSONException e){
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());
                }
                break;

            case CommonDef.REQUEST_REGISTER_AUCTION_THIRD:
                try{
                    JSONObject jsonObject = new JSONObject(result);
                    String msg,code;
                    code = jsonObject.getString("code");
                    msg = jsonObject.getString("msg");
                    if (code.equalsIgnoreCase("0001")) {
                        prefs.setKeyValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS, jsonObject.getString("auction_register_status"));
                        PropertyDetails.propertyListInfo.registeredForProperty = "1";
                        PropertyDetails.needToUpdate=true;
                        alerts.confirmAuctionRegistration("Your Application has been submitted.");
                    } else{
                        alerts.confirmAuctionRegistration(msg);
                    }
                } catch(JSONException e){
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());
                }
                break;

        }

    }
    public void cancelAuctionReg() {

        View view = getLayoutInflater().inflate(R.layout.cancel_auction_reg_popup, null);
        Button btnYes = (Button) view.findViewById(R.id.btn_yes);
        Button btnNo = (Button) view.findViewById(R.id.btn_no);
        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);

            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

            }
        });


    }

}

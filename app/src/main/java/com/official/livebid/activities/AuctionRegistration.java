package com.official.livebid.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.utils.GrayscaleTransformation;
import com.squareup.picasso.Picasso;

import static android.view.View.OnClickListener;

public class AuctionRegistration extends AppCompatActivity {
    CustomEditText cvFirstName, cvLastName, cvMobileNumber;
    AutoCompleteTextView etAddress;
    TextView tvAddressLabel;
    CheckBox chkAgreeToRNC, chkRequestToAmend;
    private SharedPreference prefs;
    private PropertyListInfo propertyListInfo;
    private ImageView ivPropertyImg;
    private TextView tvStatusTag;
    private TextView tvDateTag;
    private TextView tvPropertyStreet;
    private TextView tvPropertySuburbPostcode;
    private TextView tvPropertyStatusIdentifier;
    private TextView tvPrice;
    private TextView tvNosBedroom;
    private TextView tvNosBathroom;
    private TextView tvNosParking;
    private TextView tvPropertyTitle;
    private Button btnButtom;
    private Alerts alert;
    private Button btnSaleOfContract;

    private OnClickListener bntClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_sale_of_contract:
//                    alert.saleOfContract();
                    Opener.SaleOfContract(AuctionRegistration.this);
                    break;
                case R.id.btn_bottom:
                    finish();
                    Opener.docVerification(AuctionRegistration.this);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_auction_registration);
        init();
        setPropertyElements();
        getRegistrationDetails();
    }

    private void init() {
        prefs = new SharedPreference(this);
        alert = new Alerts(this);
        new CustomActionBar.AuctionRegistrationMenu(AuctionRegistration.this, 1);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            propertyListInfo = (PropertyListInfo) bundle.getSerializable(CommonDef.PROPERTY_LIST_INFO);
        }
        ivPropertyImg = (ImageView) findViewById(R.id.iv_property_img);
        tvStatusTag = (TextView) findViewById(R.id.tv_property_status_tag);
        tvDateTag = (TextView) findViewById(R.id.tv_property_date_tag);
        tvPropertyStreet = (TextView) findViewById(R.id.tv_property_street);
        tvPropertySuburbPostcode = (TextView) findViewById(R.id.tv_property_suburb_postcode);
        tvPropertyStatusIdentifier = (TextView) findViewById(R.id.tv_property_status_identifier);
        tvPrice = (TextView) findViewById(R.id.tv_property_price);
        tvNosBedroom = (TextView) findViewById(R.id.tv_nosBedroom);
        tvNosBathroom = (TextView) findViewById(R.id.tv_nosBathroom);
        tvNosParking = (TextView) findViewById(R.id.tv_nosParking);
        tvPropertyTitle = (TextView) findViewById(R.id.tv_property_title);

        cvFirstName = (CustomEditText) findViewById(R.id.cv_first_name);
        cvLastName = (CustomEditText) findViewById(R.id.cv_last_name);
        cvMobileNumber = (CustomEditText) findViewById(R.id.cv_mobile_no);
        tvAddressLabel = (TextView) findViewById(R.id.txt_address);
        etAddress = (AutoCompleteTextView) findViewById(R.id.et_address);
        chkAgreeToRNC = (CheckBox) findViewById(R.id.cbox_agree_to_tnc);
        chkRequestToAmend = (CheckBox) findViewById(R.id.cbox_request_for_amend);
        btnButtom = (Button) findViewById(R.id.btn_bottom);
        btnSaleOfContract = (Button) findViewById(R.id.btn_sale_of_contract);
        btnSaleOfContract.setOnClickListener(bntClickListener);
        btnButtom.setEnabled(true);
        btnButtom.setOnClickListener(bntClickListener);
        chkAgreeToRNC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
//                    btnButtom.setEnabled(true);
                } else {
//                    btnButtom.setEnabled(false);
                }
            }
        });
    }

    private void getRegistrationDetails() {
        cvFirstName.setText(prefs.getStringValues(CommonDef.SharedPrefKeys.FIRST_NAME));
        cvLastName.setText(prefs.getStringValues(CommonDef.SharedPrefKeys.LAST_NAME));
        cvMobileNumber.setText(prefs.getStringValues(CommonDef.SharedPrefKeys.MOBILE_NUMBER));
        etAddress.setText(prefs.getStringValues(CommonDef.SharedPrefKeys.ADDRESS));
        tvAddressLabel.setText("Address");
        etAddress.setFocusable(false);
        etAddress.setClickable(false);
        etAddress.setFocusableInTouchMode(false);
        cvFirstName.enableEditMode(false);
        cvLastName.enableEditMode(false);
        cvMobileNumber.enableEditMode(false);

    }

    private void setPropertyElements() {
        tvPrice.setText(propertyListInfo.price);
        if (propertyListInfo.propertyStatus == 0) {
            if (propertyListInfo.propertyStatus2 == 4) {
                tvStatusTag.setVisibility(View.VISIBLE);
                tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                tvDateTag.setVisibility(View.GONE);

            } else {
                if (propertyListInfo.propertyStatus2 != 0)
                    tvStatusTag.setVisibility(View.VISIBLE);
                tvDateTag.setVisibility(View.VISIBLE);
                tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
            }

        } else {
            tvDateTag.setVisibility(View.GONE);
            tvStatusTag.setVisibility(View.VISIBLE);
            tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
        }
        tvPropertyStreet.setText(propertyListInfo.street);
        tvPropertySuburbPostcode.setText(propertyListInfo.suburb + ", " + propertyListInfo.postCode);
        if (!propertyListInfo.propertyImgUrl.trim().isEmpty()) {
            if (propertyListInfo.propertyStatus == 2) {
                Picasso.with(this)
                        .load(propertyListInfo.propertyImgUrl)
                        .transform(new GrayscaleTransformation())
                        .placeholder(R.mipmap.img_property_placeholder)
                        .into(ivPropertyImg);
            } else {
                Picasso.with(this)
                        .load(propertyListInfo.propertyImgUrl)
                        .placeholder(R.mipmap.img_property_placeholder)
                        .into(ivPropertyImg);
            }
        } else {
            Picasso.with(this)
                    .load(R.drawable.img_coming_soon)
                    .placeholder(R.mipmap.img_property_placeholder)
                    .into(ivPropertyImg);
        }


        tvNosBathroom.setText(propertyListInfo.nosOfBathroom);
        tvNosBedroom.setText(propertyListInfo.nosOfBedroom);
        tvNosParking.setText(propertyListInfo.nosOfParking);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }
}

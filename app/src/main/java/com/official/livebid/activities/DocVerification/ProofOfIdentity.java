package com.official.livebid.activities.DocVerification;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.official.livebid.R;
import com.official.livebid.helpers.CommonMethods;
import com.theartofdev.edmodo.cropper.CropImageView;

public class ProofOfIdentity extends AppCompatActivity implements View.OnClickListener, CropImageView.OnSetImageUriCompleteListener, CropImageView.OnGetCroppedImageCompleteListener {
    private static final int IMG_DOC = 123;
    private static final int IMG_FACE = 124;
    private ImageView imgDoc,imgFace;
    private Button btnCaptureDoc,btnCaptureFace;
    private RelativeLayout rlCropView,rlMainView;
    private CropImageView cropImageView;
    private Button btnCrop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_proof_of_identity);
        imgDoc= (ImageView) findViewById(R.id.img_doc);
        imgFace= (ImageView) findViewById(R.id.img_face);
        btnCaptureDoc= (Button) findViewById(R.id.btn_capture_doc);
        btnCaptureFace= (Button) findViewById(R.id.btn_capture_face);
        cropImageView= (CropImageView) findViewById(R.id.CropImageView);
        cropImageView.setCropShape(CropImageView.CropShape.RECTANGLE);
        cropImageView.setFixedAspectRatio(false);
        cropImageView.setOnGetCroppedImageCompleteListener(this);
        cropImageView.setOnSetImageUriCompleteListener(this);
        btnCrop = (Button) findViewById(R.id.btn_crop);
        rlCropView= (RelativeLayout) findViewById(R.id.rl_crop_view);
        rlMainView= (RelativeLayout) findViewById(R.id.rl_main_view);
        btnCrop.setOnClickListener(this);
        btnCaptureDoc.setOnClickListener(this);
        btnCaptureFace.setOnClickListener(this);
    }

    @Override
    public void onGetCroppedImageComplete(CropImageView view, Bitmap bitmap, Exception error) {
        if (error == null) {
//            new ImageHelper().with(ProofOfIdentity.this).setFileName("imgdoc.png").save(bitmap);
            imgDoc.setImageBitmap(bitmap);


        } else {
            Toast.makeText(cropImageView.getContext(), "Image crop failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
//            Toast.makeText(cropImageView.getContext(), "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(cropImageView.getContext(), "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_capture_doc:
                startActivityForResult(CommonMethods.getPickImageChooserIntent(this), IMG_DOC);
                break;
            case R.id.btn_capture_face:
                startActivityForResult(CommonMethods.getPickImageChooserIntent(this), IMG_FACE);
                break;
            case R.id.btn_crop:
                cropImageView.getCroppedImageAsync(cropImageView.getCropShape(), 0, 0);
                rlCropView.setVisibility(View.GONE);
                btnCrop.setVisibility(View.GONE);
                rlMainView.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case IMG_DOC:
                if (resultCode == Activity.RESULT_OK) {
                    rlCropView.setVisibility(View.VISIBLE);
                    btnCrop.setVisibility(View.VISIBLE);
                    rlMainView.setVisibility(View.GONE);
                    Uri imageUri = CommonMethods.getPickImageResultUri(data, ProofOfIdentity.this);
                    cropImageView.setImageUriAsync(imageUri);

                }
                break;
            case IMG_FACE:
                break;
        }

    }
}

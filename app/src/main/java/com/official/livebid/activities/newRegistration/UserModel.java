package com.official.livebid.activities.newRegistration;

import java.io.Serializable;

/**
 * Created by Ics on 5/16/2016.
 */
public class UserModel implements Serializable {
    private String firstName;
    private String lastName;
    private String Email;
    private String mobile;
    private boolean isFreeLancer;
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public void setLoginInfo(String user, String pass){
        this.userName=user;
        this.password=pass;
    }

    public boolean isFreeLancer() {
        return isFreeLancer;
    }

    public void setFreeLancer(boolean freeLancer) {
        isFreeLancer = freeLancer;
    }

    public UserModel() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public UserModel(String firstName, String lastName, String email, String mobile,boolean isFreeLancer) {
        this.firstName = firstName;
        this.lastName = lastName;
        Email = email;
        this.mobile = mobile;
        this.isFreeLancer=isFreeLancer;
    }
}

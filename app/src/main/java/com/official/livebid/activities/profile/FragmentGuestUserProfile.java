package com.official.livebid.activities.profile;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.helpers.Opener;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentGuestUserProfile extends Fragment  {

    Button btnSignup;
    private TextView tvPopUPtitle, tvHaveAnAccount;

    public FragmentGuestUserProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_guest_user_profile, container, false);
        btnSignup= (Button) view.findViewById(R.id.btn_sign_up);
        tvHaveAnAccount=(TextView)view.findViewById(R.id.btn_login);
        tvHaveAnAccount.setOnClickListener(clicklistener);
        btnSignup.setOnClickListener(clicklistener);

        return view;
    }
    private View.OnClickListener clicklistener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btn_sign_up:
//                    Opener.Register(getActivity());
                    Opener.newRegister(getActivity(), UserType.NEW);
                    break;
                case R.id.tv_login:
                    Opener.newRegister(getActivity(), UserType.EXISTING);
                    break;
            }

        }
    };




}

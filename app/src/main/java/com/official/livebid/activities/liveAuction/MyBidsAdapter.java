package com.official.livebid.activities.liveAuction;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;

import java.util.ArrayList;

/**
 * Created by Ics on 3/25/2016.
 */
public class MyBidsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<String> mybids;
    private Context context;
    private static final int TYPE_EMPTY = 0;
    private static final int TYPE_ITEM = 1;

    public MyBidsAdapter(Context context, ArrayList<String> mybids) {
        this.mybids = mybids;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (mybids.size() > 0)
            return TYPE_ITEM;

        return TYPE_EMPTY;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.mybid_list_item, parent, false);
            return new MyBidsViewHolder(viewItem);
        } else if (viewType == TYPE_EMPTY) {
            View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_no_mybids, parent, false);
            return new EmptyViewHolder(viewItem);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyBidsViewHolder) {
            final MyBidsViewHolder myBidsViewHolder = (MyBidsViewHolder) holder;
            myBidsViewHolder.tvExpandCollapse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myBidsViewHolder.ll_my_offers.setVisibility(myBidsViewHolder.ll_my_offers.isShown()?View.GONE:View.VISIBLE);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return mybids.size() > 0 ? mybids.size() : 1;
    }

    private static class MyBidsViewHolder extends RecyclerView.ViewHolder {
        protected TextView tvExpandCollapse;
        protected LinearLayout ll_my_offers;

        public MyBidsViewHolder(View itemView) {
            super(itemView);
            tvExpandCollapse = (TextView) itemView.findViewById(R.id.tv_expand_collapse);
            ll_my_offers = (LinearLayout) itemView.findViewById(R.id.ll_my_offers);
        }
    }

    private static class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

}

package com.official.livebid.activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.swipemenulistview.SwipeMenu;
import com.official.livebid.Views.swipemenulistview.SwipeMenuCreator;
import com.official.livebid.Views.swipemenulistview.SwipeMenuItem;
import com.official.livebid.Views.swipemenulistview.SwipeMenuListView;
import com.official.livebid.adapters.NotificationAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.NotificationModel;
import com.official.livebid.objects.PropertyListInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Notifications extends MenuActivity implements AsyncInterface {
    SwipeMenuListView lvNotification;
    private NotificationAdapter notificationAdapter;
    private SharedPreference customPrefs;
    private String offset = "0";
    Boolean lastPage = false;
    boolean isLoading = false;
    TextView noNotification;
    Animation anim;
    int menuItemClickedPosition; //position needed when animations are used in listview
    int markSeenItemPosition; //position needed when opened items color change in listview
    AppMode appMode;
    private ArrayList<NotificationModel> notifications;
    private android.view.View loadingView;

    @Override
    public int getLayoutId() {
        return R.layout.activity_notifications;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new CustomActionBar.NotificationTitleBar(Notifications.this);
        tabbar = (RelativeLayout) findViewById(R.id.tabbar);
        tabbar.setVisibility(View.GONE);

        // get notification from server offset 0 for start in lazy loading
//        contentView = (RelativeLayout) findViewById(R.id.rl_content);
//        contentView.setVisibility(View.GONE);

        appMode = CommonMethods.getAppMode(this);
        fetchNotification(false);
        loadingView = getLayoutInflater().inflate(R.layout.layout_endless_loading, null, false);
        noNotification = (TextView) findViewById(R.id.no_notification_tv);
        lvNotification = (SwipeMenuListView) findViewById(R.id.lv_notifications);
        anim = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);


        lvNotification.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, final int i, long l) {
//                Toast.makeText(getApplicationContext(), "clicked" + i, Toast.LENGTH_SHORT).show();
                markSeenItemPosition = i;
                markNotificationSeen(markSeenItemPosition);
                getNotifiedPropertyInfo(notificationAdapter.getItem(i).getPropertyId());
//                notificationAdapter.markRead(i);


            }
        });

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // create "delete" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.RED));
                // set item width
                openItem.setWidth((int) CommonMethods.pxFromDp(Notifications.this, 90));
                // set item title
                openItem.setTitle("Delete");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

            }
        };
        // set creator
        lvNotification.setMenuCreator(creator);
        lvNotification.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:

                        menuItemClickedPosition = position; //needed when animations to delete in listview
                        deleteNotification(position);
                        break;

                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });


        lvNotification.setOnScrollListener(new AbsListView.OnScrollListener() {

            int count = lvNotification.getCount();

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d("zorro is testing... ", "visible posion: " + lvNotification.getLastVisiblePosition() + " count is:" + count + " last page value:" + lastPage);
                if (!isLoading && ((totalItemCount - visibleItemCount) <= (firstVisibleItem)) && !lastPage) {
                    Log.d("zorro is testing... ", "count is:" + count + " last page value:" + lastPage);
                    lvNotification.addFooterView(loadingView);
                    fetchNotification(true);
                }
            }
        });

        lvNotification.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);


    }

    private void markNotificationSeen(int position) {
        String notificationId = notificationAdapter.getIdFromPosition(position);
        HashMap<String, String> params = new HashMap<>();
        HashMap headers = CommonMethods.getHeaders(this);
        params.put("notification_id", notificationId);
        VolleyRequest volleyRequest = new VolleyRequest(Notifications.this, Request.Method.POST, params, UrlHelper.MARK_NOTIFICATION_SEEN, headers, false, CommonDef.RC_MARK_NOTIFICATION_SEEN);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(true);
    }

    private void deleteNotification(int position) {
        String notificationId = notificationAdapter.getIdFromPosition(position);

        HashMap<String, String> params = new HashMap<>();
        HashMap headers = CommonMethods.getHeaders(this);
        params.put("notification_id", notificationId);
        VolleyRequest volleyRequest = new VolleyRequest(Notifications.this, Request.Method.POST, params, UrlHelper.DELETE_NOTIFICATION, headers, false, CommonDef.RC_DELETE_NOTIFICATION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }

    private void fetchNotification(boolean hidePd) {
        isLoading = true;
        HashMap<String, String> params = new HashMap<>();
        HashMap headers = CommonMethods.getHeaders(this);
        params.put("offset", offset);
        VolleyRequest volleyRequest = new VolleyRequest(Notifications.this, Request.Method.POST, params, UrlHelper.FETCH_NOTIFICATION, headers, false, CommonDef.RC_FETCH_NOTIFICATION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(hidePd);

    }


    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.RC_FETCH_NOTIFICATION:
                System.out.println("Notification listings =>" + result);

                try {
                    JSONObject jObjResult = new JSONObject(result);
                    String msg, code;
                    code = jObjResult.getString("code");
                    msg = jObjResult.getString("msg");
                    offset = jObjResult.getString("offset");
                    lastPage = jObjResult.getBoolean("last_page");

                    JSONArray jArrList = jObjResult.getJSONArray("list");
                    JSONObject jObjNInfo = null;
                    if (jArrList.length() > 0) {
                        noNotification.setVisibility(View.GONE);
                        if (appMode == AppMode.user)
                            notifications = new NotificationModel().getUserNotifications(jArrList);
                        else
                            notifications = new NotificationModel().getAgentNotifications(jArrList);

                        if (notificationAdapter == null) {
                            notificationAdapter = new NotificationAdapter(this, notifications, appMode);
                            lvNotification.setAdapter(notificationAdapter);
                        } else {
                            notificationAdapter.addMoreData(notifications);

                        }


                    } else {
                        noNotification.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    lvNotification.removeFooterView(loadingView);
                    isLoading = false;
                }
                break;

            case CommonDef.RC_DELETE_NOTIFICATION:
                System.out.println("Notification listings =>" + result);
                try {
                    JSONObject jObjResult = new JSONObject(result);
                    String msg, code;
                    code = jObjResult.getString("code");
                    msg = jObjResult.getString("msg");

                    if (code.equalsIgnoreCase("0001")) {
                        Toast.makeText(Notifications.this, "Deleted successfully", Toast.LENGTH_SHORT).show();

                        final View view = getViewByPosition(menuItemClickedPosition, lvNotification);
                        anim.setAnimationListener(new Animation.AnimationListener() {

                            @Override
                            public void onAnimationStart(Animation animation) {

                                view.setHasTransientState(true);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {


                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                notificationAdapter.deleteItem(menuItemClickedPosition);
                                view.setHasTransientState(false);
                                if (lvNotification.getAdapter().getCount() == 0) {
                                    noNotification.setVisibility(View.VISIBLE);
                                }

                            }
                        });
                        view.startAnimation(anim);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case CommonDef.RC_MARK_NOTIFICATION_SEEN:
                System.out.println("Notification listings =>" + result);
                try {
                    JSONObject jObjResult = new JSONObject(result);
                    String msg, code;
                    code = jObjResult.getString("code");
                    msg = jObjResult.getString("msg");

                    if (code.equalsIgnoreCase("0001")) {
                        notificationAdapter.markRead(markSeenItemPosition);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            case CommonDef.RC_GET_PROPERTY_INFO:
                System.out.println("Property Details =>" + result);
                try {
                    JSONObject jObjResult = new JSONObject(result);
                    String msg, code;
                    code = jObjResult.getString("code");
                    msg = jObjResult.getString("msg");

                    if (code.equalsIgnoreCase("0001")) {
                        PropertyListInfo property = new PropertyListInfo().getPropertyInfo(jObjResult.getJSONObject("detail"));
                        if (appMode == AppMode.user)
                            Opener.PropertyDetails(Notifications.this, property);
                        else
                            Opener.ViewListingsAgent(Notifications.this, property);
                    }
                    else
                    {
                        new Alerts(this).showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;

        }

    }

    private void getNotifiedPropertyInfo(String propertyId) {
        HashMap<String, String> headers = CommonMethods.getHeaders(Notifications.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyId);
        VolleyRequest volleyRequest = new VolleyRequest(Notifications.this, Request.Method.POST, params, UrlHelper.GET_PROPERTY_INFO, headers, false, CommonDef.RC_GET_PROPERTY_INFO);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }
}

package com.official.livebid.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.activities.newRegistration.RegType;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.staticClasses.MobileVerification;
import com.official.livebid.staticClasses.RegisterUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Pattern;


public class ProblemSignIn extends ActionBarActivity implements View.OnClickListener, AsyncInterface {
    TextView tvFooter;
    CustomEditText cvEmail;
    EditText etEmail;
    EditText etMobile;
    CustomEditText cvMobile;
    Button btnResetPassword;
    private String emailAddress, mobileNumber;
    private Alerts alerts;
    RelativeLayout mainView;
    ImageView imageView;
    private CustomActionBar.TitleBarWithBackButtonOnly problemSignInTitleBar;
    private RegType regType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_signin);
        getSupportActionBar().hide();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            regType = (RegType) b.getSerializable(CommonDef.REG_TYPE);
        }
        init();


    }

    private void init() {
        alerts = new Alerts(this);
        mainView = (RelativeLayout) findViewById(R.id.rl_main_view);
        CommonMethods.setupUI(mainView, this);
        problemSignInTitleBar = new CustomActionBar.TitleBarWithBackButtonOnly(this);
        tvFooter = (TextView) findViewById(R.id.reset_pwd_footer);
        cvEmail = (CustomEditText) findViewById(R.id.cv_email);
        etEmail = (EditText) cvEmail.findViewById(R.id.textEdit);
        cvMobile = (CustomEditText) findViewById(R.id.cv_mobile_no);
        etMobile = (EditText) cvMobile.findViewById(R.id.textEdit);
        imageView = (ImageView) findViewById(R.id.iv_center);
        btnResetPassword = (Button) findViewById(R.id.btn_reset_pwd);
        btnResetPassword.setOnClickListener(this);
        imageView.setBackground(getResources().getDrawable(R.drawable.black_livebid_logo));
        SpannableString ss = new SpannableString(getResources().getString(R.string.reset_pwd_footer_text));

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                CommonMethods.sendEmail(ProblemSignIn.this, CommonDef.LIVEBID_URL);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
                ds.setFakeBoldText(true);
                ds.setColor(Color.BLUE);
            }
        };
        ss.setSpan(clickableSpan1, 85, 108, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvFooter.setText(ss);
        tvFooter.setMovementMethod(LinkMovementMethod.getInstance());
        cvEmail.setPreferred();
        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    cvEmail.clearPreferred();
                    etMobile.setText("");

                } else {
                    cvEmail.setPreferred();
                }
            }
        });
        etMobile.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    etEmail.setText("");
                }
            }
        });


        etMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                enableResetButton();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                enableResetButton();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reset_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_reset_pwd:
                if (isValidContact())
                    requestProblemSignIn();
                break;
        }
    }

    private void enableResetButton() {
        emailAddress = cvEmail.getText().toString().trim();
        mobileNumber = cvMobile.getText().toString().trim();
        if (!emailAddress.isEmpty() || !mobileNumber.isEmpty()) {
            btnResetPassword.setEnabled(true);
        } else {
            btnResetPassword.setEnabled(false);
        }
    }

    private boolean isValidContact() {
        emailAddress = cvEmail.getText().toString().trim();
        mobileNumber = cvMobile.getText().toString().trim();
        if (!emailAddress.isEmpty()) {
            if (!isEmailValid(emailAddress)) {
                alerts.showOkMessage("Email address is invalid.");
                return false;
            }
        }
        if (!mobileNumber.isEmpty()) {
            if (!isMobileValid(mobileNumber)) {
                alerts.showOkMessage("Mobile Number is invalid.");
                return false;
            }
        }
        return true;

    }

    private boolean isEmailValid(String emailAddress) {

        return !emailAddress.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress.trim()).matches(); // 2.2 froyo and above

    }

    private boolean isMobileValid(String mobile) {
//        isMibileOk = mobile.length() >= 9 ? true : false;
//        final Pattern phonePattern= Pattern.compile("^(?:\\+?61|0)4 ?(?:(?:[01] ?[0-9]|2 ?[0-57-9]|3 ?[1-9]|4 ?[7-9]|5 ?[018]) ?[0-9]|3 ?0 ?[0-5])(?: ?[0-9]){5}$");
        final Pattern phonePattern = Pattern.compile("^(?:\\(?(?:\\+?61|0)4\\)?(?:[ -]?[0-9]){7}[0-9]$)");
        return phonePattern.matcher(mobile).matches();
//        return isMibileOk;
    }

    private void requestProblemSignIn() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("device_type", "2");
        headers.put("device_id", android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID));
        headers.put("user_type", regType.getRegType() + "");
        emailAddress = cvEmail.getText().toString().trim();
        mobileNumber = cvMobile.getText().toString().trim();
        HashMap<String, String> params = new HashMap<>();
        params.put(!emailAddress.isEmpty() ? "email" : "mobile_number", !emailAddress.isEmpty() ? emailAddress : mobileNumber);
        com.official.livebid.staticClasses.ProblemSignIn.reportingID = !emailAddress.isEmpty() ? emailAddress : mobileNumber;
        System.out.println("Problem sign in:" + params.toString());

        VolleyRequest volleyRequest = new VolleyRequest(ProblemSignIn.this, Request.Method.POST, params, UrlHelper.FORGET_PASSWORD, headers, false, CommonDef.REQUEST_FORGOT_PASSWORD);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_FORGOT_PASSWORD:
                System.out.println("USER DETAILS=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0031")) {
                        ;//email
                        RegisterUser.mobileNumber = joResult.getString("mobile_number");
                        alerts.requestPaswordChange(msg + " \n\n" + com.official.livebid.staticClasses.ProblemSignIn.reportingID);

                    } else if (code.equals("0032")) {
                        //mobile
                        RegisterUser.mobileNumber = joResult.getString("mobile_number");
                        alerts.requestPaswordChange(msg + " \n\n" + com.official.livebid.staticClasses.ProblemSignIn.reportingID);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
        }
    }

    public void verifyChangePassword() {
        MobileVerification.mode = 2;
        Opener.MobileVerification(ProblemSignIn.this, regType);
    }

    private void validateForm() {


        if (!cvMobile.getText().isEmpty() && !isMobileValid(cvMobile.getText()))
            cvMobile.setInvalid();
        else {
            cvMobile.setValid();
//            count++;
        }

        if (!cvEmail.getText().isEmpty() && !isEmailValid(cvEmail.getText()))
            cvEmail.setInvalid();
        else {
            cvEmail.setValid();
//            count++;
        }


    }
}

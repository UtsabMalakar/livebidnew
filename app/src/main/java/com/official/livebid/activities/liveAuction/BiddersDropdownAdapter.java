package com.official.livebid.activities.liveAuction;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.official.livebid.helpers.CommonMethods;

import java.util.ArrayList;

/**
 * Created by Ics on 7/8/2016.
 */
public class BiddersDropdownAdapter implements SpinnerAdapter {
    private Context context;
    private ArrayList<OnSiteBidder> onSiteBidders;

    public BiddersDropdownAdapter(Context context, ArrayList<OnSiteBidder> onSiteBidders) {
        this.context = context;
        this.onSiteBidders = onSiteBidders;
    }

    public int getPosition(OnSiteBidder bidder) {
        return onSiteBidders.indexOf(bidder);
    }


    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(android.R.layout.simple_spinner_item, null);

        }
        ((TextView) view).setHeight((int) CommonMethods.pxFromDp(context, 25));
        ((TextView) view).setText(onSiteBidders.get(i).getBidderCode());
        return view;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        return onSiteBidders.size();
    }

    @Override
    public Object getItem(int i) {
        return onSiteBidders.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView = (TextView) View.inflate(context, android.R.layout.simple_spinner_item, null);
        textView.setHeight((int) CommonMethods.pxFromDp(context, 25));
        textView.setText(onSiteBidders.get(i).getBidderCode());
        return textView;
    }

    @Override
    public int getItemViewType(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}

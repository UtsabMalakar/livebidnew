package com.official.livebid.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.CustomDateTimePicker;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.OfferType;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class PreAuctionOfferUser extends ActionBarActivity implements View.OnClickListener, AsyncInterface {
    private SharedPreference prefs;
    private PropertyListInfo propertyListInfo;
    private ImageView ivPropertyImg;
    private TextView tvStatusTag;
    private TextView tvDateTag;
    private TextView tvPropertyStreet;
    private TextView tvPropertySuburbPostcode;
    private TextView tvPropertyStatusIdentifier;
    private TextView tvPrice;
    private TextView tvNosBedroom;
    private TextView tvNosBathroom;
    private TextView tvNosParking;
    private TextView tvPropertyTitle;
    private Button btnMakeAnOffer;
    private Alerts alert;
    CustomDateTimePicker custom;
    private String settlementDate = "";
    private long longPriveValue = 0;
    private LinearLayout llMyOfferItemContainer;
    private OfferType offerType;
    private TextView tvPropertyImageIndicator;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_pre_auction_offer);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            propertyListInfo = (PropertyListInfo) bundle.getSerializable(CommonDef.PROPERTY_LIST_INFO);
            offerType = (OfferType) bundle.getSerializable(CommonDef.OFFER_TYPE);
            settlementDate = bundle.getString("settlementDate");
            longPriveValue = bundle.getLong("PriceValue");
            makeOffer();
        }
        init();
        setPropertyElements();
//        listMyOffer();
    }

    private void setPropertyElements() {
        tvPrice.setText(propertyListInfo.price);
        if (propertyListInfo.propertyStatus == 0) {
            if ( propertyListInfo.propertyStatus2 == 4) {
                tvStatusTag.setVisibility(View.VISIBLE);
               tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
               tvDateTag.setVisibility(View.GONE);

            } else {
                if (propertyListInfo.propertyStatus2 != 0)
                    tvStatusTag.setVisibility(View.VISIBLE);
               tvDateTag.setVisibility(View.VISIBLE);
                tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
            }

        } else {
            tvDateTag.setVisibility(View.GONE);
            tvStatusTag.setVisibility(View.VISIBLE);
            tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
        }
        tvPropertyStreet.setText(propertyListInfo.street);
        tvPropertySuburbPostcode.setText(propertyListInfo.suburb + ", " + propertyListInfo.postCode);
//        if (propertyListInfo.agentProfileImg != null && !propertyListInfo.agentProfileImg.isEmpty())
//            Picasso.with(this).load(propertyListInfo.agentProfileImg).transform(new CircleTransform(PropertyDetails.this)).into(ivAgentImg);
       //view pager adapter
        ImageSlideAdapter mImageSlideAdapter;
        mImageSlideAdapter = new ImageSlideAdapter(this, propertyListInfo.property_images_more,propertyListInfo.propertyStatus);
        mViewPager.setAdapter(mImageSlideAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                tvPropertyImageIndicator.setVisibility(View.GONE);
                tvPropertyImageIndicator.setText(position+1 + "/" + propertyListInfo.property_images_more.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
       /* if (!propertyListInfo.propertyImgUrl.isEmpty()) {
            if (propertyListInfo.propertyStatus == 2) {
                Picasso.with(this)
                        .load(propertyListInfo.propertyImgUrl)
                        .transform(new GrayscaleTransformation())
                        .placeholder(R.mipmap.img_property_placeholder)
                        .into(ivPropertyImg);
            } else {
                Picasso.with(this)
                        .load(propertyListInfo.propertyImgUrl)
                        .placeholder(R.mipmap.img_property_placeholder)
                        .into(ivPropertyImg);
            }
        } else {
            Picasso.with(this)
                    .load(R.drawable.img_coming_soon)
                    .placeholder(R.mipmap.img_property_placeholder)
                    .into(ivPropertyImg);
        }*/

        tvNosBathroom.setText(propertyListInfo.nosOfBathroom);
        tvNosBedroom.setText(propertyListInfo.nosOfBedroom);
        tvNosParking.setText(propertyListInfo.nosOfParking);
    }

    private void init() {
        prefs = new SharedPreference(this);
        alert = new Alerts(this);
        if (offerType== OfferType.PRE_OFFER)
            new CustomActionBar.PreAuctionOffer(PreAuctionOfferUser.this, "Pre-Auction Offer");
        else
            new CustomActionBar.PreAuctionOffer(PreAuctionOfferUser.this, "Post-Auction Offer");
        mViewPager = (ViewPager) findViewById(R.id.vp_property_img);
        tvPropertyImageIndicator = (TextView) findViewById(R.id.tvPropertyImageIndicator);
        tvStatusTag = (TextView) findViewById(R.id.tv_property_status_tag);
        tvDateTag = (TextView) findViewById(R.id.tv_property_date_tag);
        tvPropertyStreet = (TextView) findViewById(R.id.tv_property_street);
        tvPropertySuburbPostcode = (TextView) findViewById(R.id.tv_property_suburb_postcode);
        tvPropertyStatusIdentifier = (TextView) findViewById(R.id.tv_property_status_identifier);
        tvPrice = (TextView) findViewById(R.id.tv_property_price);
        tvNosBedroom = (TextView) findViewById(R.id.tv_nosBedroom);
        tvNosBathroom = (TextView) findViewById(R.id.tv_nosBathroom);
        tvNosParking = (TextView) findViewById(R.id.tv_nosParking);
        tvPropertyTitle = (TextView) findViewById(R.id.tv_property_title);
        btnMakeAnOffer = (Button) findViewById(R.id.btn_bottom);
        btnMakeAnOffer.setOnClickListener(this);
        llMyOfferItemContainer = (LinearLayout) findViewById(R.id.ll_my_offer_Container);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pre_auction_offer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showMakeOfferPopup() {

        View view = getLayoutInflater().inflate(R.layout.make_offer_dialog, null);
        final Button btnSubmit = (Button) view.findViewById(R.id.btn_submit);
        final EditText etOfferPrice = (EditText) view.findViewById(R.id.et_offer_price);
        if (offerType == OfferType.PRE_OFFER)
            etOfferPrice.setHint("Pre-Auction Offer");
        else
            etOfferPrice.setHint("Post-Auction Offer");
        final EditText etSettlementDateTime = (EditText) view.findViewById(R.id.et_settlement_date_time);
        etOfferPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {
                    etOfferPrice.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,]", "");


                    String formatted = "";
                    if (cleanString.equalsIgnoreCase("") || cleanString.equalsIgnoreCase("0"))
                        formatted = "";
                    else {
                        longPriveValue = Long.parseLong(cleanString);
                        formatted = "$" + NumberFormat.getNumberInstance(Locale.US).format((longPriveValue));
                    }
                    current = formatted;
                    etOfferPrice.setText(formatted);
                    etOfferPrice.setSelection(formatted.length());

                    etOfferPrice.addTextChangedListener(this);
                    enableOffer(etOfferPrice.getText().toString(), btnSubmit);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isValidPrice(longPriveValue)) {
                    alert.showOkMessage("Offer price must be greater than 80% of property price.");
                    return;
                }
                if (!isValidSettlementDate(settlementDate)) {
                    alert.showOkMessage("Please verify settlement date.");
                    return;
                }
                mBottomSheetDialog.dismiss();
                makeOffer();

            }
        });
        etSettlementDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                custom = new CustomDateTimePicker(PreAuctionOfferUser.this,
                        new CustomDateTimePicker.ICustomDateTimeListener() {

                            @Override
                            public void onSet(Dialog dialog, Calendar calendarSelected,
                                              Date dateSelected, int year, String monthFullName,
                                              String monthShortName, int monthNumber, int date,
                                              String weekDayFullName, String weekDayShortName,
                                              int hour24, int hour12, int min, int sec,
                                              String AM_PM) {

                                etSettlementDateTime.setText(calendarSelected
                                        .get(Calendar.DAY_OF_MONTH)
                                        + " " + (monthShortName) + " at "
                                        + hour12 + ":" + min
                                        + " " + AM_PM);
                                String m = (monthNumber + 1) < 10 ? "0" + (monthNumber + 1) : (monthNumber + 1) + "";
                                String d = calendarSelected.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendarSelected.get(Calendar.DAY_OF_MONTH)
                                        : calendarSelected.get(Calendar.DAY_OF_MONTH) + "";
                                settlementDate = year + "-" + m + "-" + d + " "
                                        + hour24 + ":"
                                        + min + ":"
                                        + sec;
                                System.out.println("sDate:" + settlementDate);
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                custom.set24HourFormat(false);
                custom.setDate(Calendar.getInstance());
                custom.showDialog();
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_bottom:
                showMakeOfferPopup();
                break;

        }
    }

    private void enableOffer(String price, Button submit) {
        if (!price.isEmpty()) {
            submit.setEnabled(true);
        } else {
            submit.setEnabled(false);
        }
    }

    private void makeOffer() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PreAuctionOfferUser.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        params.put("offer_type", offerType.getOfferType() + "");
        params.put("offer_amount", longPriveValue + ".00");
        params.put("preferred_settlement", settlementDate);
        System.out.println("pre offer Auction params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PreAuctionOfferUser.this, Request.Method.POST, params, UrlHelper.MAKE_PRE_OFFER, headers, false, CommonDef.REQUEST_MAKE_PRE_OFFER);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void listMyOffer() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PreAuctionOfferUser.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        params.put("offer_type", offerType.getOfferType() + "");
        System.out.println("my offer params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PreAuctionOfferUser.this, Request.Method.POST, params, UrlHelper.MY_OFFER_DETAILS, headers, false, CommonDef.REQUEST_MY_OFFER);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }


    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_MAKE_PRE_OFFER:
                System.out.println("pre offer auction response:" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alert.showOkMessage(msg);
                        settlementDate = "";
                        llMyOfferItemContainer.removeAllViews();
                        listMyOffer();
                    } else {
                        alert.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alert.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_MY_OFFER:
                System.out.println("my offer response:" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alert.showOkMessage(msg);
                        JSONArray jaMyOffers = joResult.getJSONArray("list");
                        JSONObject joMyOffer = null;
                        if (jaMyOffers.length() > 0) {
                            for (int i = 0; i < jaMyOffers.length(); i++) {
                                joMyOffer = jaMyOffers.getJSONObject(i);
                                if (llMyOfferItemContainer.getChildCount() > 0) {
                                    llMyOfferItemContainer.addView(addDivider());
                                }
                                llMyOfferItemContainer.addView(addMyOfferView(
                                        joMyOffer.getString("offer_amount")
                                        , joMyOffer.getLong("offer_date_timestamp")
                                ));

                            }
                        }

                    } else {
//                        alert.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alert.showOkMessage(e.getMessage());

                }

                break;

        }
    }

    public View addMyOfferView(String offerPrice, long timeStamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE d MMM");
        SimpleDateFormat timeFormat = new SimpleDateFormat("h" + "h:mm aa");
        Date date = null;
        date =new Date(timeStamp*1000);//convert php timestamp to java timestamp

        View view = getLayoutInflater().inflate(R.layout.my_offer_item, null, false);
        TextView tvPrice = (TextView) view.findViewById(R.id.tv_price);
        TextView tvTime = (TextView) view.findViewById(R.id.tv_time);
        TextView tvDate = (TextView) view.findViewById(R.id.tv_date);
        tvPrice.setText("$" + NumberFormat.getNumberInstance(Locale.US).format((Double.parseDouble(offerPrice))));
        tvDate.setText(dateFormat.format(date));
        tvTime.setText(timeFormat.format(date));

        return view;
    }

    private View addDivider() {
        FrameLayout v = new FrameLayout(this);
        v.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 1));
        v.setBackgroundColor(getResources().getColor(R.color.form_divider));
        return v;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }


    private boolean isValidPrice(long price) {
        boolean result = false;
        if (price > Long.parseLong(propertyListInfo.price.toString().replaceAll("[$,]", "")) * .8)
            result = true;
        return result;
    }

    private boolean isValidSettlementDate(String sDate) {
        boolean result = false;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        if (sDate.isEmpty())
            return true;

        Date notifiedDate = null;
        try {
            notifiedDate = dateFormat.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = Calendar.getInstance().getTime();
        long different = notifiedDate.getTime() - (now.getTime() + 24 * 60 * 60 * 1000);
        if (different >= 0)
            result = true;
        return result;
    }
}

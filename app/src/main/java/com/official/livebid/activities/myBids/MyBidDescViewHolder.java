package com.official.livebid.activities.myBids;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;

/**
 * Created by Ics on 5/12/2016.
 */
public class MyBidDescViewHolder extends RecyclerView.ViewHolder {
    protected View rootView;
    protected LinearLayout llVendorBidHolder;
    protected EditText etOnSiteBidder;
    protected TextView tvTitle;
    protected TextView tvDescription;

    public MyBidDescViewHolder(View view) {
        super(view);
        rootView = view;
        llVendorBidHolder = (LinearLayout) view.findViewById(R.id.ll_vendor_bid_holder);
        etOnSiteBidder = (EditText) view.findViewById(R.id.et_onsite_bid_amount);
        tvTitle=(TextView)view.findViewById(R.id.tv_title);
        tvDescription=(TextView)view.findViewById(R.id.tv_description);
    }
}

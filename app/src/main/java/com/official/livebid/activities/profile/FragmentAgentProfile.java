package com.official.livebid.activities.profile;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAgentProfile extends Fragment {

    private TextView tvLogout;
    private TextView tvPrivacyPolicy;
    private TextView tvTOS;
    private TextView tvHelpAndSupport;
    private TextView tvSettings;
    private TextView tvPersonalDetails;
    private TextView tvAgentDetails;
    private TextView tvProofOfIdentity;
    private Alerts alerts;
    private ImageView iv_profile;
    SharedPreference userPrefs;
    Activity activity;

    public FragmentAgentProfile() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        alerts = new Alerts(activity);
        userPrefs = new SharedPreference(getActivity());
        View rootView;
        rootView = inflater.inflate(R.layout.fragment_agent_profile, container, false);
        iv_profile = (ImageView) rootView.findViewById(R.id.iv_profile);
        CommonMethods.setImageviewRatio(getActivity(), iv_profile, 0);
        if (userPrefs.getStringValues(CommonDef.SharedPrefKeys.PROFILE_IMG).length() > 0) {
            Picasso.with(getActivity())
                    .load(userPrefs.getStringValues(CommonDef.SharedPrefKeys.PROFILE_IMG))
                    .resize(320,240)
                    .error(R.drawable.profile_icon)
//                    .placeholder( R.drawable.img_loading)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(iv_profile);
        }
        else{
            Picasso.with(getActivity())
                    .load(R.mipmap.img_property_placeholder)
                    .resize(320,240)
                    .error(R.drawable.profile_icon)
//                    .placeholder( R.drawable.img_loading)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(iv_profile);
        }
        tvLogout = (TextView) rootView.findViewById(R.id.tv_log_out);
        tvPrivacyPolicy = (TextView) rootView.findViewById(R.id.tv_privacy_policy);
        tvTOS = (TextView) rootView.findViewById(R.id.tv_tos);
        tvHelpAndSupport = (TextView) rootView.findViewById(R.id.tv_help_support);
        tvSettings = (TextView) rootView.findViewById(R.id.tv_settings);
        tvPersonalDetails = (TextView) rootView.findViewById(R.id.tv_personal_details);
        tvAgentDetails = (TextView) rootView.findViewById(R.id.tv_agent_details);
        tvProofOfIdentity = (TextView) rootView.findViewById(R.id.tv_proof_of_identity);
        tvLogout.setOnClickListener(textViewClickListener);
        tvPrivacyPolicy.setOnClickListener(textViewClickListener);
        tvTOS.setOnClickListener(textViewClickListener);
        tvHelpAndSupport.setOnClickListener(textViewClickListener);
        tvSettings.setOnClickListener(textViewClickListener);
        tvPersonalDetails.setOnClickListener(textViewClickListener);
        tvAgentDetails.setOnClickListener(textViewClickListener);
        tvProofOfIdentity.setOnClickListener(textViewClickListener);
        tvProofOfIdentity.setEnabled(false);
        return rootView;
    }

    public View.OnClickListener textViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tv_log_out:
                    alerts.confirmLogout();
                    break;
                case R.id.tv_privacy_policy:
                    Opener.PrivacyPolicy(getActivity());
                    break;
                case R.id.tv_tos:
                    Opener.TOS(getActivity());
                    break;
                case R.id.tv_help_support:
                    Opener.HelpNSupport(getActivity());
                    break;
                case R.id.tv_settings:
//                    Toast.makeText(getActivity(), "Under Construction.", Toast.LENGTH_SHORT).show();
                    Opener.Settings(getActivity());
//                    Opener.AgencyProfile(getActivity(),"1");
                    break;
                case R.id.tv_agent_details:
//                    Toast.makeText(getActivity(), "Under Construction.", Toast.LENGTH_SHORT).show();
                    ((Profile) activity).hideTabBar();
                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.animator.fragment_slide_left_enter,
                                    R.animator.fragment_slide_left_exit,
                                    R.animator.fragment_slide_right_enter,
                                    R.animator.fragment_slide_right_exit)
                            .replace(R.id.profile_container, new AgentDetails())
                            .addToBackStack(null)
                            .commit();
                    break;
                case R.id.tv_personal_details:
                    ((Profile) activity).hideTabBar();
                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.animator.fragment_slide_left_enter,
                                    R.animator.fragment_slide_left_exit,
                                    R.animator.fragment_slide_right_enter,
                                    R.animator.fragment_slide_right_exit)
                            .replace(R.id.profile_container, new AgentPersonalDetails())
                            .addToBackStack(null)
                            .commit();

                    break;
                case R.id.tv_proof_of_identity:
                    ((Profile) activity).hideTabBar();
                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.animator.fragment_slide_left_enter,
                                    R.animator.fragment_slide_left_exit,
                                    R.animator.fragment_slide_right_enter,
                                    R.animator.fragment_slide_right_exit)
                            .replace(R.id.profile_container, new ProofOfIdentity())
                            .addToBackStack(null)
                            .commit();
                    break;
            }
        }
    };

}

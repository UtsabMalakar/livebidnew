package com.official.livebid.activities.myBids;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.helpers.Opener;
import com.official.livebid.listeners.FavouriteCompleteListener;
import com.official.livebid.utils.GrayscaleTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ics on 5/12/2016.
 */
public class RegisteredPropertyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<RegisteredPropertyModel> registeredProperties;
    private Context context;
    private int selector;

    public RegisteredPropertyAdapter(Context context, ArrayList<RegisteredPropertyModel> registeredProperties, int selector) {
        this.context = context;
        this.registeredProperties = registeredProperties;
        this.selector = selector;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.my_bids_property_header, parent, false);
        return new MyBidsPropertyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RegisteredPropertyModel propertyListInfo = registeredProperties.get(position);
        MyBidsPropertyViewHolder propertyViewHolder = (MyBidsPropertyViewHolder) holder;
        setPropertyStatusTag(propertyListInfo, propertyViewHolder);
        setCurrentBidStatus(propertyListInfo, propertyViewHolder);
        propertyViewHolder.chkFav.setChecked(propertyListInfo.isFav());

        propertyViewHolder.tvPropertyStreet.setText(propertyListInfo.street);
        propertyViewHolder.tvPropertySuburbPostcode.setText(propertyListInfo.suburb + ", "+propertyListInfo.propertyState+" " + propertyListInfo.postCode);
        setPropertyImage(propertyListInfo, propertyViewHolder);
        propertyViewHolder.chkFav.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final CheckBox cboxTemp = (CheckBox) view;
                        new FavoriteHelper(context).makeFavorite(registeredProperties.get(position).id, registeredProperties.get(position).getFav(), new FavouriteCompleteListener() {
                            @Override
                            public void onSuccess(boolean b) {
                                registeredProperties.get(position).setFav(b);
                                cboxTemp.setChecked(b);
                            }

                            @Override
                            public void onFailure() {
                                cboxTemp.setChecked(registeredProperties.get(position).getFav());
                            }

                            @Override
                            public void onNotLoggedIn() {
                                cboxTemp.setChecked(false);
                                new Alerts((Activity) context).signIn();
                            }
                        });

                        // Webservice call for change.

                    }
                });
        propertyViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selector == CommonDef.MY_BIDS_REGISTERED_PROPERTY
                        )
                    Opener.PropertyDetails((Activity) context, propertyListInfo);
                else
                    Opener.registeredPropertyDetail((Activity) context, propertyListInfo, selector);
            }
        });


    }

    private void setPropertyImage(RegisteredPropertyModel propertyListInfo, MyBidsPropertyViewHolder propertyViewHolder) {

            if (propertyListInfo.propertyStatus == 2) {
                propertyViewHolder.ivPropertySoldTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvDateTag.setVisibility(View.GONE);
                if (!propertyListInfo.propertyImgUrl.trim().isEmpty()) {
                Picasso.with(context)
                        .load(propertyListInfo.propertyImgUrl)
                        .placeholder(R.mipmap.img_property_placeholder)
                        .transform(new GrayscaleTransformation())
                        .into(propertyViewHolder.ivPropertyImg);
            } else {
                    Picasso.with(context)
                            .load(R.drawable.img_coming_soon)
                            .into(propertyViewHolder.ivPropertyImg);

            }

        } else {
                propertyViewHolder.ivPropertySoldTag.setVisibility(View.GONE);
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(propertyListInfo.propertyImgUrl)
                        .placeholder(R.mipmap.img_property_placeholder)
                        .into(propertyViewHolder.ivPropertyImg);
        }
    }

    private void setCurrentBidStatus(RegisteredPropertyModel propertyListInfo, MyBidsPropertyViewHolder propertyViewHolder) {
        if (propertyListInfo.propertyStatus == 0) {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("Current Bid");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        } else if (propertyListInfo.propertyStatus == 1 || propertyListInfo.propertyStatus == 4) {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.VISIBLE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("Current Bid");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.VISIBLE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        } else if (propertyListInfo.propertyStatus == 2) {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.VISIBLE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("SOLD");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.VISIBLE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        } else if (propertyListInfo.propertyStatus == 3) {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("Current Bid");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        } else {
            propertyViewHolder.tvCurrentBidAmountTitle.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmountTitle.setText("Current Bid");
            propertyViewHolder.tvCurrentBidAmount.setVisibility(View.GONE);
            propertyViewHolder.tvCurrentBidAmount.setText(propertyListInfo.currentBidAmount);
        }
    }

    private void setClickHandler(final RegisteredPropertyModel propertyListInfo, MyBidsPropertyViewHolder propertyViewHolder) {
        if (propertyListInfo.propertyStatus == 0) {
            propertyViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "Auction not started yer", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (propertyListInfo.propertyStatus == 1 || propertyListInfo.propertyStatus == 4) {
            propertyViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        } else if (propertyListInfo.propertyStatus == 2) {
            propertyViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "property sold", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (propertyListInfo.propertyStatus == 3) {
            propertyViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "passed in", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            propertyViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "nothing", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setPropertyStatusTag(RegisteredPropertyModel propertyListInfo, MyBidsPropertyViewHolder propertyViewHolder) {
        if (propertyListInfo.propertyStatus == 0) {
            if ( propertyListInfo.propertyStatus2 == 4) {
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                propertyViewHolder.tvDateTag.setVisibility(View.GONE);

            } else {
                if (propertyListInfo.propertyStatus2 != 0)
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvDateTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                propertyViewHolder.tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
            }

        } else {
            propertyViewHolder.tvDateTag.setVisibility(View.GONE);
            propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
            propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
        }
    }

    @Override
    public int getItemCount() {
        return registeredProperties.size();
    }


    public void addMoreData(ArrayList<RegisteredPropertyModel> data) {
        int startPosition = registeredProperties.size();
        this.registeredProperties.addAll(data);
        this.notifyItemRangeInserted(startPosition, data.size());
    }
}

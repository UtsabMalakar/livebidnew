package com.official.livebid.activities.liveAuction;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ics on 7/7/2016.
 */
public class OnSiteBidder implements Parcelable {
    private String id;
    private String userId;
    private String uniqueID;
    private String bidderCode;

    public String getBidderCode() {
        return bidderCode;
    }

    public void setBidderCode(String bidderCode) {
        this.bidderCode = bidderCode;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public String getUserId() {
        return userId;
    }

    protected OnSiteBidder(Parcel in) {
        id = in.readString();
        userId = in.readString();
        uniqueID = in.readString();
        bidderCode = in.readString();
    }

    public static final Creator<OnSiteBidder> CREATOR = new Creator<OnSiteBidder>() {
        @Override
        public OnSiteBidder createFromParcel(Parcel in) {
            return new OnSiteBidder(in);
        }

        @Override
        public OnSiteBidder[] newArray(int size) {
            return new OnSiteBidder[size];
        }
    };

    public ArrayList<OnSiteBidder> getOnSiteBidders(JSONArray jaList) throws JSONException {
        JSONObject jObjBidder = null;
        ArrayList<OnSiteBidder> onSiteBidders = new ArrayList<>();
        OnSiteBidder onSiteBidder;
        onSiteBidder = new OnSiteBidder(Parcel.obtain());
        onSiteBidder.id = "";
        onSiteBidder.userId = "";
        onSiteBidder.uniqueID = "Select Bidder";
        onSiteBidder.bidderCode="Select an Option";
        onSiteBidders.add(onSiteBidder);
        for (int i = 0; i < jaList.length(); i++) {
            jObjBidder = jaList.getJSONObject(i);
            onSiteBidder = new OnSiteBidder(Parcel.obtain());
            onSiteBidder.id = i + "";
            onSiteBidder.userId = jObjBidder.getString("user_id");
            onSiteBidder.uniqueID = jObjBidder.getString("unique_id").toUpperCase();
            onSiteBidder.bidderCode = jObjBidder.getString("bidder_code");
            onSiteBidders.add(onSiteBidder);
        }
        return onSiteBidders;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(userId);
        parcel.writeString(uniqueID);
        parcel.writeString(bidderCode);
    }
}

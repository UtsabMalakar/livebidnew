package com.official.livebid.activities.liveAuction;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonMethods;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Ics on 7/7/2016.
 */
public class PlaceBidDialog extends DialogFragment implements View.OnClickListener {
    private EditText etBidAmount;
    private Button btnPlaceBid500;
    private Button btnPlaceBid1k;
    private Button btnPlaceBid5k;
    private Button btnPlaceBid10k;
    private Button btnPlaceBid15k;
    private Button btnPlaceBid25k;
    private Button btnConfirmBidBid;
    private long bidAmount;
    private long addedBidAmount;
    private long highestBidAmount;
    private long propertyPrice;
    private static final String HIGHEST_BID = "highest_bid";
    private static final String PROPERTY_PRICE = "property_price";
    OnPlaceBidListener listener;

    public PlaceBidDialog() {
    }

    public static PlaceBidDialog newInstance(long highestBidAmount, long propertyPrice) {
        PlaceBidDialog frag = new PlaceBidDialog();
        Bundle args = new Bundle();
        args.putLong(HIGHEST_BID, highestBidAmount);
        args.putLong(PROPERTY_PRICE, propertyPrice);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_place_bid_popup, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listener = (OnPlaceBidListener) getActivity();
        highestBidAmount = getArguments().getLong(HIGHEST_BID);
        propertyPrice = getArguments().getLong(PROPERTY_PRICE);
        etBidAmount = (EditText) view.findViewById(R.id.et_bid_amount);
        btnConfirmBidBid = (Button) view.findViewById(R.id.btn_confirm_bid);
        btnConfirmBidBid.setEnabled(false);
        btnPlaceBid1k = (Button) view.findViewById(R.id.btn_price_tag_1k);
        btnPlaceBid1k.setTag(1000);
        btnPlaceBid5k = (Button) view.findViewById(R.id.btn_price_tag_5k);
        btnPlaceBid5k.setTag(5000);
        btnPlaceBid10k = (Button) view.findViewById(R.id.btn_price_tag_10k);
        btnPlaceBid10k.setTag(10000);
        btnPlaceBid15k = (Button) view.findViewById(R.id.btn_price_tag_15k);
        btnPlaceBid15k.setTag(15000);
        btnPlaceBid25k = (Button) view.findViewById(R.id.btn_price_tag_25k);
        btnPlaceBid25k.setTag(25000);
        btnPlaceBid500 = (Button) view.findViewById(R.id.btn_price_tag_500);
        btnPlaceBid500.setTag(500);
        etBidAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    etBidAmount.removeTextChangedListener(this);
                    String cleanString = s.toString().replaceAll("[$,]", "");
                    if(cleanString.length()>10){
                        new Alerts(getActivity()).showOkMessage("Your bid amount exceeded maximum limit. ");
                        resetPriceTag();
                        cleanString=highestBidAmount+"";
                    }

                    String formatted = "";
                    if (cleanString.equalsIgnoreCase("") || cleanString.equalsIgnoreCase("0"))
                        formatted = "";
                    else {
                        bidAmount = Long.parseLong(cleanString);
                        formatted = "$" + NumberFormat.getNumberInstance(Locale.US).format((bidAmount));
                    }
                    current = formatted;
                    etBidAmount.setText(formatted);
                    etBidAmount.setSelection(formatted.length());

                    etBidAmount.addTextChangedListener(this);
                    resetPriceTag();
                    enableConfirmBid();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        if (highestBidAmount > 0)
            etBidAmount.setText(highestBidAmount + "");
        etBidAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    btnConfirmBidBid.callOnClick();
                }
                return false;
            }
        });
        btnPlaceBid500.setOnClickListener(this);
        btnConfirmBidBid.setOnClickListener(this);
        btnPlaceBid1k.setOnClickListener(this);
        btnPlaceBid5k.setOnClickListener(this);
        btnPlaceBid10k.setOnClickListener(this);
        btnPlaceBid15k.setOnClickListener(this);
        btnPlaceBid25k.setOnClickListener(this);
    }

    @Override
    public void setStyle(int style, int theme) {
        super.setStyle(style, theme);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_price_tag_1k:
            case R.id.btn_price_tag_5k:
            case R.id.btn_price_tag_10k:
            case R.id.btn_price_tag_15k:
            case R.id.btn_price_tag_25k:
            case R.id.btn_price_tag_500:
                resetPriceTag();
                setPriceTage(view);
                break;
            case R.id.btn_confirm_bid:
                if (highestBidAmount > 0) {
                    if ((float) ((bidAmount - highestBidAmount) / highestBidAmount) <= 0.1)
                        listener.onPlaceBid(bidAmount);
                    else
                        alertBidAmount(bidAmount);


                    ;// user bid
                } else {
                    if (bidAmount > propertyPrice)
                        listener.onPlaceBid(bidAmount);
                    else
                        alertBidAmount(bidAmount);
                }
                break;
        }

    }

    private void resetPriceTag() {
        btnPlaceBid500.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid1k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid5k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid10k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid15k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid25k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));

    }

    private void setPriceTage(View view) {

        addedBidAmount = (int) view.getTag();
        etBidAmount.setText(highestBidAmount + addedBidAmount + "");
        view.setBackground(getResources().getDrawable(R.drawable.rounded_button_active));
        return;


    }

    public void enableConfirmBid() {
        if (!etBidAmount.getText().toString().isEmpty())
            btnConfirmBidBid.setEnabled(true);
        else
            btnConfirmBidBid.setEnabled(false);
    }

    public static interface OnPlaceBidListener {
        void onPlaceBid(long amount);
    }

    private void alertBidAmount(final long bidAmount) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Livebid")
                .setMessage("Are you sure want to place bid of amount " + CommonMethods.getFormattedPrice(bidAmount))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        listener.onPlaceBid(bidAmount);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .show();
    }

    public void dismissDialog(){
        this.dismiss();
    }
}

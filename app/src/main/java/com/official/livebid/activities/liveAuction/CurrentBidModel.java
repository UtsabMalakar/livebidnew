package com.official.livebid.activities.liveAuction;

import com.official.livebid.helpers.DateTimeHelper;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Ics on 4/20/2016.
 */
public class CurrentBidModel {
    public String msgId;
    public String userId;
    public String userUniqueId;
    public String propertyId;
    public MessageType bidType;
    public String bidAmount;
    public String bidTime;
    public String status;
    public String startDateTime;

    public String getBidderCode() {
        return bidderCode;
    }

    public void setBidderCode(String bidderCode) {
        this.bidderCode = bidderCode;
    }

    public String bidderCode;

    public static enum MessageType {
        generalBid,
        onSiteBid,
        vendorBid,
        firstCAll,
        secondCall,
        thirdCall,
        propertyPassedIn,
        propertySold,
        auctionStart
    }

    public void setBidTime(long timestamp) {
        bidTime = DateTimeHelper.getLaggingTimeFromTimeStamp(timestamp * 1000);
    }

    public void setBidAmount(String amount) {
        try {
            double p = Math.ceil(Double.parseDouble(amount));
            bidAmount = "$" + NumberFormat.getNumberInstance(Locale.US).format((long) p);
        }
        catch (NumberFormatException e){
            bidAmount=amount;
        }


    }

    public void setUserUniqueId(String id) {
        userUniqueId = "#" + id;

    }

    public void setBidType(int Type) {
        switch (Type) {
            case 0:
                bidType = MessageType.generalBid;
                break;
            case 1:
                bidType = MessageType.onSiteBid;
                break;
            case 2:
                bidType = MessageType.vendorBid;
                break;
            case 3:
                bidType = MessageType.firstCAll;
                break;
            case 4:
                bidType = MessageType.secondCall;
                break;
            case 5:
                bidType = MessageType.thirdCall;
                break;
            case 6:
                bidType = MessageType.propertyPassedIn;
                break;
            case 7:
                bidType = MessageType.propertySold;
                break;
            case 8:
                bidType = MessageType.auctionStart;
                break;

        }
    }


}

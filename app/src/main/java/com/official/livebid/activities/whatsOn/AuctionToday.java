package com.official.livebid.activities.whatsOn;

import android.os.Bundle;
import android.view.Menu;

import com.official.livebid.R;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.helpers.CustomActionBar;

public class AuctionToday extends MenuActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_auction_today;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_auction_today);
        getSupportActionBar().hide();
        new CustomActionBar.AuctionTodayCAB(this);
        getFragmentManager()
                .beginTransaction()
                .add(R.id.ll_auction_today_container, new FragAuctionTodayListView())
                .addToBackStack(null)
                .commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_auction_today, menu);
        return true;
    }
    public void openMapView(){
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.fragment_slide_left_enter,
                        R.animator.fragment_slide_left_exit,
                        R.animator.fragment_slide_right_enter,
                        R.animator.fragment_slide_right_exit)
                .replace(R.id.ll_auction_today_container, new FragAuctionTodayMapView())
                .addToBackStack(null)
                .commit();

    }
    public void openListView(){
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.fragment_slide_left_enter,
                        R.animator.fragment_slide_left_exit,
                        R.animator.fragment_slide_right_enter,
                        R.animator.fragment_slide_right_exit)
                .replace(R.id.ll_auction_today_container, new FragAuctionTodayListView())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       finish();
       overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }
}

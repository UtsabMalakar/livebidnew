package com.official.livebid.activities.viewListingsAgent;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PreAuctionOfferModel;
import com.official.livebid.objects.PropertyListInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PreAuctionOffersAgent extends AppCompatActivity implements AsyncInterface {
    RecyclerView rvPreAuctionOffer;
    LinearLayoutManager linearLayoutManager;
    private PropertyListInfo propertyListInfo;
    private com.official.livebid.alerts.Alerts alerts;
    private PreAuctionOffersAdapter preAuctionOfferAdapter;
    ArrayList<PreAuctionOfferModel> preAuctionOffers;
    private boolean isToolbarShown=false;
    private LinearLayout cabContainer;
    private ImageView ibtnBack;
    private TextView tvCenter;
    private ImageView ibtnRight;
    private String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_pre_auction_offers_agent);
        init();
//        showPropertyInfo();
        getPreAuctionOffersAgent();
    }

    private void getPreAuctionOffersAgent() {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("preAuctionOffer params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PreAuctionOffersAgent.this, Request.Method.POST, params, UrlHelper.GET_PRE_OFFERS_AGENT, headers, false, CommonDef.REQ_PRE_OFFERS_AGENT);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void init() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            propertyListInfo = (PropertyListInfo) bundle.getSerializable(CommonDef.PROPERTY_LIST_INFO);
        }
        alerts = new Alerts(this);
        new CustomActionBar.PreAuctionOfferAgentCAB(this, propertyListInfo.propertyCode);
        //property details
//        ivPropertyImg = (ImageView) findViewById(R.id.iv_property_img);
//        tvStatusTag = (TextView) findViewById(R.id.tv_property_status_tag);
//        tvDateTag = (TextView) findViewById(R.id.tv_property_date_tag);
//        tvPropertySuburbPostcode = (TextView) findViewById(R.id.tv_property_suburb_postcode);
//        tvPropertyStreet = (TextView) findViewById(R.id.tv_property_street);
//        tvPropertyStatusIdentifier = (TextView) findViewById(R.id.tv_property_status_identifier);
//        tvReservePrice = (TextView) findViewById(R.id.tv_reserve_price);
//        tvAverageOffer = (TextView) findViewById(R.id.tv_average_offer);
        // pre offer listings
        cabContainer = (LinearLayout) findViewById(R.id.cab);
        ibtnBack = (ImageView) findViewById(R.id.ibtn_left);
        ibtnBack.setImageResource(R.drawable.back_icon);
        tvCenter = (TextView) findViewById(R.id.tv_center);
        tvCenter.setVisibility(View.INVISIBLE);

        tvCenter.setTextColor(Color.WHITE);
        ibtnRight = (ImageView) findViewById(R.id.ibtn_right);
        cabContainer.setBackgroundColor(Color.TRANSPARENT);
        rvPreAuctionOffer = (RecyclerView) findViewById(R.id.rv_pre_auction_offers);
        linearLayoutManager = new LinearLayoutManager(this);
        rvPreAuctionOffer.setLayoutManager(linearLayoutManager);
        rvPreAuctionOffer.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.computeVerticalScrollOffset() > 100) {
                    showToolbar();
                    isToolbarShown = true;
                } else {
                    hideToolbar();
                    isToolbarShown = false;
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pre_auction_offers_agent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("PreAuctionOffer Agent Response:" + result);
        switch (requestCode) {
            case CommonDef.REQ_PRE_OFFERS_AGENT:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        JSONArray jaPreOffers = joResult.getJSONArray("list");
                        propertyListInfo.averagePrice = joResult.getString("average_offer");
                        preAuctionOffers = new ArrayList<>();
                        if (jaPreOffers.length() > 0) {
                            JSONObject joOffer = null;
                            PreAuctionOfferModel preAuctionOffer = null;
                            for (int i = 0; i < jaPreOffers.length(); i++) {
                                joOffer = jaPreOffers.getJSONObject(i);
                                preAuctionOffer = new PreAuctionOfferModel(
                                        joOffer.getString("user_id"),
                                        joOffer.getString("unique_id"),
                                        joOffer.getString("offer_amount"),
                                        joOffer.getString("offer_date_time"),
                                        joOffer.getString("preferred_settlement"),
                                        joOffer.getString("email"),
                                        joOffer.getString("mobile_number"),
                                        joOffer.getString("profile_image")
                                );
                                preAuctionOffers.add(preAuctionOffer);
                            }

                        }
                        preAuctionOfferAdapter = new PreAuctionOffersAdapter(PreAuctionOffersAgent.this, preAuctionOffers, propertyListInfo);
                        rvPreAuctionOffer.setAdapter(preAuctionOfferAdapter);


                    } else {
                        preAuctionOfferAdapter = new PreAuctionOffersAdapter(PreAuctionOffersAgent.this, new ArrayList<PreAuctionOfferModel>(), propertyListInfo);
                        rvPreAuctionOffer.setAdapter(preAuctionOfferAdapter);
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    public void showContactBottomSheet(final PreAuctionOfferModel offer) {

        View view = getLayoutInflater().inflate(R.layout.contact_agent_bottom_sheet, null);
        Button btnEmail = (Button) view.findViewById(R.id.btn_email);
        btnEmail.setText("Email");
        Button bntCall = (Button) view.findViewById(R.id.btn_call);
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);

        final Dialog mBottomSheetDialog = new Dialog(PreAuctionOffersAgent.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        bntCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                String mobile = offer.mobileNumber;
                if (mobile != null && !mobile.isEmpty())
                    permissionCheckCall(mobile);
            }
        });
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                CommonMethods.sendEmail(PreAuctionOffersAgent.this, offer.emailAddress);
            }
        });


    }

    private void permissionCheckCall(String offer) {
        mobile = offer;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && this.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.CALL_PHONE},
                    1300);

            return;
        } else {
            CommonMethods.makeCall(PreAuctionOffersAgent.this, offer);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    CommonMethods.makeCall(PreAuctionOffersAgent.this, mobile);
                break;

        }
    }
    public void showToolbar() {
        if (isToolbarShown)
            return;
        ibtnBack.setImageResource(R.drawable.left_arrow);
        tvCenter.setVisibility(View.VISIBLE);
        tvCenter.setTextColor(Color.BLACK);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(cabContainer, "backgroundColor", new ArgbEvaluator(), Color.argb(0, 255, 255, 255), Color.argb(255, 255, 255, 255));//getResources().getColor(R.color.theme_yellow)
        colorFade.setDuration(1000);
        colorFade.start();
    }


    public void hideToolbar() {
        if (!isToolbarShown)
            return;
        ibtnBack.setImageResource(R.drawable.back_icon);
        tvCenter.setVisibility(View.INVISIBLE);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(cabContainer, "backgroundColor", new ArgbEvaluator(), Color.argb(255, 255, 255, 255), Color.argb(0, 255, 255, 255));
        colorFade.setDuration(1000);
        colorFade.start();
    }
}

package com.official.livebid.activities.myBidsNotification;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Ics on 6/27/2016.
 */
public class MyBidsPropertyModel {
    private String propertyId;
    private String propertyImage;
    private String status;
    private String propertyCode;
    private String propertyStatus;
    private String type;
    private String amount;

    public MyBidsPropertyModel(String propertyId, String propertyCode,String propertyImage, String type, String amount) {
        this.propertyImage=propertyImage;
        this.propertyId = propertyId;
        this.propertyCode = propertyCode;
        this.type = type;
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyImage() {
        return propertyImage;
    }

    public void setPropertyImage(String propertyImage) {
        this.propertyImage = propertyImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getPropertyStatus() {
        return propertyStatus;
    }

    public void setPropertyStatus(String propertyStatus) {
        this.propertyStatus = propertyStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        String msg = "";
        if (type.equalsIgnoreCase("1")) {
            msg = "You have Bid " + getFormatedAmount() + " on property";
        } else if (type.equalsIgnoreCase("2")) {
            msg = "You have made an offer of " + getFormatedAmount() + " on property";
        }
        return msg;
    }

    private String getFormatedAmount() {
        String amt = "";
        double p = Math.ceil(Double.parseDouble(amount));
        amt = "$" + NumberFormat.getNumberInstance(Locale.US).format((long) p);
        return amt;
    }
}

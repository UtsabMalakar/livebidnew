package com.official.livebid.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;

/**
 * Created by Ics on 5/27/2016.
 */
public class ActivityGuestMode extends MenuActivity implements View.OnClickListener {
    Button btnSignup;
    String title = "";
    private CustomActionBar.TitleBarWithBackAndTitle guestNotificationTitlebar;
    private TextView tvPopUPtitle, tvHaveAnAccount;

    @Override
    public int getLayoutId() {
        return R.layout.activity_guest_notifications;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_guest_notifications);
        Bundle b = getIntent().getExtras();

        if (b != null)
            title = b.getString(CommonDef.GUEST_TITLE);
        tvPopUPtitle = (TextView) findViewById(R.id.tv_popup_title);
        tvHaveAnAccount = (TextView) findViewById(R.id.tv_login);
        tvPopUPtitle.setText(title);
        tvHaveAnAccount.setOnClickListener(this);
        btnSignup = (Button) findViewById(R.id.btn_sign_up);
        btnSignup.setOnClickListener(this);
        guestNotificationTitlebar = new CustomActionBar.TitleBarWithBackAndTitle(this, title);
        hideTabBar();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_up:
//                Opener.Register(GuestNotifications.this);
                Opener.newRegister(ActivityGuestMode.this, UserType.NEW);
                break;
            case R.id.tv_login:
                Opener.newRegister(ActivityGuestMode.this, UserType.EXISTING);
                break;
        }
    }

    @Override
    public void hideTabBar() {
        super.hideTabBar();
    }
}

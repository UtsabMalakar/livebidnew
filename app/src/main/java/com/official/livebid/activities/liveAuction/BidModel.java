package com.official.livebid.activities.liveAuction;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Ics on 4/8/2016.
 */
public class BidModel {
    public String bidId;
    public String bidAmount;
    public String bidderId;
    public String status;
    public CurrentBidModel.MessageType bidType;
    private boolean isMyBid;

    public boolean isMyBid() {
        return isMyBid;
    }

    public void setMyBid(String myBid) {
        if (myBid.equalsIgnoreCase("1"))
            isMyBid = true;
        else
            isMyBid = false;
    }
    public String getBidAmount(String amount){
        double p = Math.ceil(Double.parseDouble(amount));
        return "$" + NumberFormat.getNumberInstance(Locale.US).format((int) p);
    }
    public String getBidderId(String id){

        return "#" + id;
    }

    public void setBidType(int Type) {
        switch (Type) {
            case 0:
                bidType = CurrentBidModel.MessageType.generalBid;
                break;
            case 1:
                bidType = CurrentBidModel.MessageType.onSiteBid;
                break;
            case 2:
                bidType = CurrentBidModel.MessageType.vendorBid;
                break;
            case 3:
                bidType = CurrentBidModel.MessageType.firstCAll;
                break;
            case 4:
                bidType = CurrentBidModel.MessageType.secondCall;
                break;
            case 5:
                bidType = CurrentBidModel.MessageType.thirdCall;
                break;
            case 6:
                bidType = CurrentBidModel.MessageType.propertyPassedIn;
                break;
            case 7:
                bidType = CurrentBidModel.MessageType.propertySold;
                break;
            case 8:
                bidType = CurrentBidModel.MessageType.auctionStart;
                break;

        }
    }

}

package com.official.livebid.activities.liveAuction;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.CustomDateTimePicker;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.AppMode;
import com.official.livebid.enums.OfferType;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class PassedPropertySummaryReport extends AppCompatActivity implements AsyncInterface, BidHistoryAdapter.IBidHistory, View.OnClickListener {
    private RecyclerView rvSoldPropertySummary;
    private LinearLayoutManager linearLayoutManager;
    private Alerts alerts;
    private SharedPreference prefs;
    private SoldPropertyModel soldProperty;
    private BidHistoryAdapter bidHistoryAdapter;
    private ArrayList<BidModel> bids;
    private boolean isLastPage = false;
    private int offset = 0;
    private AppMode appMode;
    private Button btnButtom;

    private OfferType offerType;
    CustomDateTimePicker custom;
    private String settlementDate = "";
    private long longPriveValue=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sold_property_summary_report);
        getSupportActionBar().hide();
        new CustomActionBar.SoldPropertySummaryCAB(this);
        prefs = new SharedPreference(this);
        appMode = CommonMethods.getAppMode(this);
        btnButtom = (Button) findViewById(R.id.btn_bottom);
        btnButtom.setOnClickListener(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            soldProperty = (SoldPropertyModel) b.getSerializable(CommonDef.SOLD_PROPERTY);
        }
        if (appMode == AppMode.agent) {
            btnButtom.setText("Make an Offer");
            btnButtom.setEnabled(false);
            btnButtom.setVisibility(View.GONE);
        } else {
            btnButtom.setText("Make an Offer");
            btnButtom.setEnabled(true);
//            btnButtom.setVisibility(View.GONE);

        }


        linearLayoutManager = new LinearLayoutManager(this);
        alerts = new Alerts(this);
        rvSoldPropertySummary = (RecyclerView) findViewById(R.id.rv_sold_property_summary);
        rvSoldPropertySummary.setLayoutManager(linearLayoutManager);
        getSoldPropertySummaryReport();


    }

    private void getSoldPropertySummaryReport() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PassedPropertySummaryReport.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", soldProperty.propertyId);
        params.put("offset", offset + "");
        System.out.println("Bid History=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PassedPropertySummaryReport.this, Request.Method.POST, params, UrlHelper.GET_BID_HISTORY, headers, false, CommonDef.RC_GET_BID_HISTORY);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sold_property_summary_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewMore() {
        if (!isLastPage)
            getSoldPropertySummaryReport();
        else
            bidHistoryAdapter.stopLoading();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);

    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.RC_GET_BID_HISTORY:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONArray jaList = joResult.getJSONArray("bid_history");
                        if (jaList.length() > 0) {
                            BidModel bm;
                            JSONObject joBid;
                            bids = new ArrayList<>();
                            for (int i = 0; i < jaList.length(); i++) {
                                joBid = jaList.getJSONObject(i);
                                bm = new BidModel();
                                bm.bidId = i + "";
                                bm.bidAmount = bm.getBidAmount(joBid.getString("bid_amount"));
                                bm.bidderId = bm.getBidderId(joBid.getString("unique_id"));
                                bm.setBidType(joBid.getInt("bid_type"));
                                bm.setMyBid(joBid.getString("check_myself"));
                                bm.status = joBid.getString("status");
                                if (bm.bidType == CurrentBidModel.MessageType.onSiteBid || bm.bidType == CurrentBidModel.MessageType.vendorBid || bm.bidType == CurrentBidModel.MessageType.generalBid)
                                    bids.add(bm);
                            }
                            isLastPage = joResult.getBoolean("last_page");
                            offset = joResult.getInt("offset");
                            if(bidHistoryAdapter==null){
                                bidHistoryAdapter = new BidHistoryAdapter(this, soldProperty, bids, this, BidHistoryAdapter.SummaryReportType.passed_in,isLastPage);
                                rvSoldPropertySummary.setAdapter(bidHistoryAdapter);
                            }
                            else {
                                bidHistoryAdapter.addMoreData(bids,isLastPage);
                            }


                        }
                        else {
                            bidHistoryAdapter = new BidHistoryAdapter(this, soldProperty, new ArrayList<BidModel>(), this, BidHistoryAdapter.SummaryReportType.passed_in,isLastPage);
                            rvSoldPropertySummary.setAdapter(bidHistoryAdapter);
                        }
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_bottom:
                showMakeOfferPopup();
//                Opener.MakeAnOffer(PassedPropertySummaryReport.this, soldProperty, OfferType.POST_OFFER, 0, "");
                break;
        }

    }

    public void showMakeOfferPopup() {
        View view = getLayoutInflater().inflate(R.layout.make_offer_dialog, null);
        final Button btnSubmit = (Button) view.findViewById(R.id.btn_submit);
        final EditText etOfferPrice = (EditText) view.findViewById(R.id.et_offer_price);
        if (offerType == OfferType.PRE_OFFER)
            etOfferPrice.setHint("Pre-Auction Offer");
        else
            etOfferPrice.setHint("Post-Auction Offer");
        final EditText etSettlementDateTime = (EditText) view.findViewById(R.id.et_settlement_date_time);
        etOfferPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {
                    etOfferPrice.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,]", "");


                    String formatted = "";
                    if (cleanString.equalsIgnoreCase("") || cleanString.equalsIgnoreCase("0"))
                        formatted = "";
                    else {
                        longPriveValue = Long.parseLong(cleanString);
                        formatted = "$" + NumberFormat.getNumberInstance(Locale.US).format((longPriveValue));
                    }
                    current = formatted;
                    etOfferPrice.setText(formatted);
                    etOfferPrice.setSelection(formatted.length());

                    etOfferPrice.addTextChangedListener(this);
                    enableOffer(etOfferPrice.getText().toString(), btnSubmit);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isValidPrice(longPriveValue)) {
                    alerts.showOkMessage("Offer price must be greater than 80% of property price.");
                    return;
                }
                if (!isValidSettlementDate(settlementDate)) {
                    alerts.showOkMessage("Please verify settlement date.");
                    return;
                }

                Opener.MakeAnOffer(PassedPropertySummaryReport.this, soldProperty,  OfferType.POST_OFFER, longPriveValue,settlementDate);//propertystatus 3=passed
                mBottomSheetDialog.dismiss();

            }
        });
        etSettlementDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                custom = new CustomDateTimePicker(PassedPropertySummaryReport.this,
                        new CustomDateTimePicker.ICustomDateTimeListener() {

                            @Override
                            public void onSet(Dialog dialog, Calendar calendarSelected,
                                              Date dateSelected, int year, String monthFullName,
                                              String monthShortName, int monthNumber, int date,
                                              String weekDayFullName, String weekDayShortName,
                                              int hour24, int hour12, int min, int sec,
                                              String AM_PM) {

                                etSettlementDateTime.setText(calendarSelected
                                        .get(Calendar.DAY_OF_MONTH)
                                        + " " + (monthShortName) + " at "
                                        + hour12 + ":" + min
                                        + " " + AM_PM);
                                String m = (monthNumber + 1) < 10 ? "0" + (monthNumber + 1) : (monthNumber + 1) + "";
                                String d = calendarSelected.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendarSelected.get(Calendar.DAY_OF_MONTH)
                                        : calendarSelected.get(Calendar.DAY_OF_MONTH) + "";
                                settlementDate = year + "-" + m + "-" + d + " "
                                        + hour24 + ":"
                                        + min + ":"
                                        + sec;
                                System.out.println("sDate:" + settlementDate);
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                custom.set24HourFormat(false);
                custom.setDate(Calendar.getInstance());
                custom.showDialog();
            }
        });
    }

    private boolean isValidPrice(long price) {
        boolean result = false;
        if (price > Long.parseLong(soldProperty.price.toString().replaceAll("[$,]", "")) * .8)
            result = true;
        return result;
    }

    private boolean isValidSettlementDate(String sDate) {
        boolean result = false;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        if (sDate.isEmpty())
            return true;

        Date notifiedDate = null;
        try {
            notifiedDate = dateFormat.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = Calendar.getInstance().getTime();
        long different = notifiedDate.getTime() - (now.getTime() + 24 * 60 * 60 * 1000);
        if (different >= 0)
            result = true;
        return result;
    }

    private void enableOffer(String price, Button submit) {
        if (!price.isEmpty()) {
            submit.setEnabled(true);
        } else {
            submit.setEnabled(false);
        }
    }
}

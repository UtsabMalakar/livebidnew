package com.official.livebid.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.adapters.SearchAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SearchHelper;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.SearchObject;
import com.official.livebid.staticClasses.SearchParams;
import com.official.livebid.utils.GPSTracker;

import org.apmem.tools.layouts.FlowLayout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Search extends AppCompatActivity implements AsyncInterface, SearchHelper.OnSearchComplete {
    EditText etACSearch;
    CharSequence lastCharSequence = "";
    ListView lvAutoCompleteSuggestions;
    Button btnRefine;
    ImageButton ibtnBack;
    boolean isTypeable = true;
    GPSTracker gpsTracker;
    private ArrayList<SearchObject> alSearchObject;
    private ArrayList<SearchObject> searchKeys;
    private String TAG = "";
    private String tempSearchText;
    private com.official.livebid.alerts.Alerts alerts;
    private FlowLayout searchView;
    private Button selectedKey;
    private ScrollView svSearch;
    SharedPreference pref;
    private String callingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_search);
        init();
    }

    private void init() {
        gpsTracker = new GPSTracker(Search.this);
        alerts = new Alerts(this);
        pref = new SharedPreference(this);
        svSearch = (ScrollView) findViewById(R.id.sv_search);
        searchView = (FlowLayout) findViewById(R.id.fl_search_keys);
        etACSearch = (EditText) findViewById(R.id.et_acAddressSearch);
        etACSearch.addTextChangedListener(twAutoComplete);
        lvAutoCompleteSuggestions = (ListView) findViewById(R.id.lv_autoCompleteSuggestions);
        btnRefine = (Button) findViewById(R.id.btn_refine);
        ibtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        btnRefine.setOnClickListener(clickListener);
        ibtnBack.setOnClickListener(clickListener);
        searchKeys = new ArrayList<>();
//        lvAutoCompleteSuggestions.setOnItemClickListener(itemClickListener);
        if(getIntent().getExtras()!=null)
        {
            callingActivity = getIntent().getExtras().getString(CommonDef.CallingActivity.callingActivity);
        }

        addPreviousSearchViews();

        etACSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                System.out.println("Rabin is testing: actionId " + actionId);


                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        if (searchView.getChildCount()<2){
                            alerts.showOkMessage("Please provide at least one search criteria.");
                            pref.setKeyValues(CommonDef.SharedPrefKeys.Search_PostCodeIds,"");
                            pref.setKeyValues(CommonDef.SharedPrefKeys.Search_SuburbNames, "");
                            CommonMethods.hideSoftKeyboard(Search.this);
                            return true;
                        }
                        else{
                        SearchParams.postCodeId = getPostCodeIds();
                        SearchParams.suburbNames = getSuburbNames();
                        SearchParams.autoKeyword = etACSearch.getText() + "";
                        if (SearchParams.postCodeId.isEmpty()) {
                            if (gpsTracker.canGetLocation()) {
                                SearchParams.lat = gpsTracker.getLatitude() + "";
                                SearchParams.lng = gpsTracker.getLongitude() + "";
                            } else {
                                gpsTracker.showSettingsAlert();
                            }
                        }
                        SearchHelper searchHelper = new SearchHelper(Search.this);
                        searchHelper.search();
                        CommonMethods.hideSoftKeyboard(Search.this);
                        return true;
                    }
                    }


                return false;
            }
        });
        etACSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                        deleteKey();
                        return true;
                    }
                }

                if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (searchView.getChildCount()<2){
                        alerts.showOkMessage("Please provide at least one search criteria.");
                        CommonMethods.hideSoftKeyboard(Search.this);
                        return true;
                    }else{
                    Log.d(TAG, "enter_key_called");
                    SearchParams.postCodeId = getPostCodeIds();
                    SearchParams.autoKeyword = etACSearch.getText() + "";
                    if (SearchParams.postCodeId.isEmpty()) {
                        if (gpsTracker.canGetLocation()) {
                            SearchParams.lat = gpsTracker.getLatitude() + "";
                            SearchParams.lng = gpsTracker.getLongitude() + "";
                        } else {
                            gpsTracker.showSettingsAlert();
                        }
                    }
                    SearchHelper searchHelper = new SearchHelper(Search.this);
                    searchHelper.search();
                    CommonMethods.hideSoftKeyboard(Search.this);
                    return true;

                }}
                return false;
            }
        });
        lvAutoCompleteSuggestions.setOnItemClickListener(new MyOnItemClickListener());
    }

    private void addPreviousSearchViews() {
        String searchIds = pref.getStringValues(CommonDef.SharedPrefKeys.Search_PostCodeIds);
        String searchSuburb = pref.getStringValues(CommonDef.SharedPrefKeys.Search_SuburbNames);
        if(!searchIds.equalsIgnoreCase(""))
        {
            List<String> idList = new ArrayList<String>(Arrays.asList(searchIds.split(",")));
            List<String> suburbList = new ArrayList<String>(Arrays.asList(searchSuburb.split(",")));
            for(int i=0;i<idList.size();i++)
            {
                SearchObject so = new SearchObject();
                so.setPostcodeId(idList.get(i));
                so.setSuburb(suburbList.get(i));

                searchView.addView(addSuburb(so), searchView.getChildCount() - 1);
                setSearchBarHeight();
                svSearch.fullScroll(View.FOCUS_DOWN);
                etACSearch.setText("");
                etACSearch.setHint(" ");
                etACSearch.requestFocus();
            }
        }
    }


    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_refine:
                    SearchParams.postCodeId = getPostCodeIds();
                    SearchParams.suburbNames= getSuburbNames();
                    if (SearchParams.postCodeId.isEmpty()) {
                        if (gpsTracker.canGetLocation()) {
                            SearchParams.lat = gpsTracker.getLatitude() + "";
                            SearchParams.lng = gpsTracker.getLongitude() + "";
                        } else {
                            gpsTracker.showSettingsAlert();
                        }
                    }
                    Log.v("postCodeIds:", SearchParams.postCode);
                    Opener.Refine(Search.this,callingActivity,SearchParams.suburbNames);
                    break;
                case R.id.ibtn_back:
                    onBackPressed();
                    break;
            }
        }
    };

    private SearchAdapter searchAdapter;
    private TextWatcher twAutoComplete = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (SearchParams.postCodeId != "") {
                SearchParams.postCodeId = "";
            }
            if (charSequence.length() >= 2 && isTypeable) {
                HashMap<String, String> params = new HashMap<>();
                params.put("keyword", charSequence.toString());
                VolleyRequest volleyRequest = new VolleyRequest(Search.this, Request.Method.POST, params, UrlHelper.AUTOCOMPLETE_SEARCH, false, CommonDef.REQUEST_AUTOCOMPLETE_SEARCH);
                volleyRequest.asyncInterface = Search.this;
                volleyRequest.setTag(charSequence);
                volleyRequest.showPd = false;
                volleyRequest.request();
            }
            if (charSequence.length() > 0)
                unSelectKey();
//            isTypeable = true;
//            lastCharSequence = charSequence;

//            if (charSequence.length() < 2) {
//                if (searchAdapter != null) {
//                    searchAdapter.clearList();
//                }
//                lvAutoCompleteSuggestions.setVisibility(View.GONE);
//            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("Autocomplete result: " + result);
        switch (requestCode) {
            case CommonDef.REQUEST_AUTOCOMPLETE_SEARCH:
                initAutoCompleteList(result);
                break;
        }
    }

    private void initAutoCompleteList(String result) {

        alSearchObject = new ArrayList<>();

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            if (code.equals("0001")) {
                JSONArray jaLists = joResult.getJSONArray("list");
                for (int i = 0; i < jaLists.length(); i++) {
                    SearchObject searchObject = new SearchObject();
                    JSONObject joList = jaLists.getJSONObject(i);
                    searchObject.setPostcodeId(joList.getString("id"));
                    searchObject.setPostcode(joList.getString("postcode"));
                    searchObject.setSuburb(joList.getString("suburb"));
                    searchObject.setState(joList.getString("state"));
                    searchObject.setLatitude(joList.getString("latitude"));
                    searchObject.setLongitude(joList.getString("longitude"));
                    alSearchObject.add(searchObject);
                }
                searchAdapter = new SearchAdapter(Search.this, alSearchObject);
                if (jaLists.length() <= 0) {
                    searchAdapter.clearList();
                    lvAutoCompleteSuggestions.setVisibility(View.GONE);
                } else {
                    lvAutoCompleteSuggestions.setVisibility(View.VISIBLE);
                    lvAutoCompleteSuggestions.setAdapter(searchAdapter);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    @Override
    public void onRresultFound(String result) {
        Opener.PropertyListing(this, result,callingActivity);
    }

    @Override
    public void onResultNotFound(String msg) {
        alerts.showOkMessage(msg);
    }

    @Override
    public void onError(String msg) {
        alerts.showOkMessage(msg);
    }


    private class MyOnItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // get string that the user clicked
//            isTypeable = false;
            SearchObject so = alSearchObject.get(position);
            // add text that is going to be wrapped in bubble
            if(!isDublicate(so)) {
                searchView.addView(addSuburb(so), searchView.getChildCount() - 1);
                setSearchBarHeight();
                svSearch.fullScroll(View.FOCUS_DOWN);
                etACSearch.setText("");
                etACSearch.setHint(" ");
                etACSearch.requestFocus();
            }
//            lvAutoCompleteSuggestions.setVisibility(View.GONE);
        }
    }

    private void setSearchBarHeight() {
        if (searchView.getHeight() >= 300) {
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 400);
            p.addRule(RelativeLayout.LEFT_OF, btnRefine.getId());
            p.addRule(RelativeLayout.RIGHT_OF, ibtnBack.getId());
            p.addRule(RelativeLayout.CENTER_IN_PARENT);
            p.setMargins(10, 10, 10, 10);
            svSearch.setLayoutParams(p);

        } else {
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            svSearch.setLayoutParams(p);
            p.addRule(RelativeLayout.LEFT_OF, btnRefine.getId());
            p.addRule(RelativeLayout.RIGHT_OF, ibtnBack.getId());
            p.addRule(RelativeLayout.CENTER_IN_PARENT);
            p.setMargins(10, 10, 10, 10);
            svSearch.setLayoutParams(p);
        }
    }


    private View addSuburb(SearchObject searchObject) {
        final Button buttonOrientation = new Button(this);
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, (int) CommonMethods.pxFromDp(this, 25));
        params.setMargins(5, 10, 5, 10);
        buttonOrientation.setLayoutParams(params);
        buttonOrientation.setBackground(getResources().getDrawable(R.drawable.search_key_inactive));
        buttonOrientation.setTextSize(10);
        buttonOrientation.setAllCaps(false);
        buttonOrientation.setPadding(20, 0, 20, 0);
        buttonOrientation.setText(searchObject.getSuburb());
        buttonOrientation.setTextColor(Color.WHITE);
        buttonOrientation.setTag(searchObject);
        buttonOrientation.setOnClickListener(serchKeyClickListener);
        return buttonOrientation;
    }

    View.OnClickListener serchKeyClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            selectKey(view);
        }
    };

    private void selectKey(View view) {
        selectedKey = (Button) view;
        View v;
        for (int i = 0; i < searchView.getChildCount() - 1; i++) {
            v = searchView.getChildAt(i);
            if (v instanceof Button) {
                v.setBackground(getResources().getDrawable(R.drawable.search_key_inactive));
                selectedKey.setTextColor(Color.WHITE);
                v.setPadding(20, 0, 20, 0);
            }
        }
        selectedKey.setBackground(getResources().getDrawable(R.drawable.search_key_active));
        selectedKey.setTextColor(Color.WHITE);
        selectedKey.setPadding(20, 0, 20, 0);


    }

    private void unSelectKey() {
        View v;
        for (int i = 0; i < searchView.getChildCount() - 1; i++) {
            v = searchView.getChildAt(i);
            if (v instanceof Button) {
                v.setBackground(getResources().getDrawable(R.drawable.search_key_inactive));
                if (selectedKey != null)
                    selectedKey.setTextColor(Color.WHITE);
                v.setPadding(20, 0, 20, 0);
            }
        }
        selectedKey = null;


    }


    private void deleteKey() {
        if (selectedKey != null) {
            searchView.removeView(selectedKey);
            setSearchBarHeight();
            selectedKey = null;
            etACSearch.setText("");
            etACSearch.setHint(" ");
            etACSearch.requestFocus();
        } else {
            if (etACSearch.getText().toString().trim().isEmpty()) {
                int lastKey = searchView.getChildCount() - 2;
                if (lastKey >= 0) {
                    if (searchView.getChildAt(lastKey) instanceof Button) {
                        selectedKey = (Button) searchView.getChildAt(lastKey);
                        selectedKey.setBackground(getResources().getDrawable(R.drawable.search_key_active));
                        selectedKey.setTextColor(Color.WHITE);
                        selectedKey.setPadding(20, 0, 20, 0);
                    }
                }
            }
        }
    }

    public String getPostCodeIds() {
        StringBuilder sb = new StringBuilder();
        View v;
        for (int i = 0; i < searchView.getChildCount() - 1; i++) {
            v = searchView.getChildAt(i);
            if (v instanceof Button) {
                if(i==0)
                {
                    sb.append(((SearchObject) v.getTag()).getPostcodeIds());
                }
                else {
                    sb.append(",");
                    sb.append(((SearchObject) v.getTag()).getPostcodeIds());
                }
            }
        }
        return sb.toString();
    }

    private String getSuburbNames() {
        StringBuilder sb = new StringBuilder();
        View v;
        for (int i = 0; i < searchView.getChildCount() - 1; i++) {
            v = searchView.getChildAt(i);
            if (v instanceof Button) {
                if(i==0)
                {
                    sb.append(((SearchObject) v.getTag()).getSuburb());
                }
                else
                {
                    sb.append(",");
                    sb.append(((SearchObject) v.getTag()).getSuburb());
                }
            }
        }
        return sb.toString();
    }



    public boolean isDublicate(SearchObject so) {
        View v;
        for (int i = 0; i < searchView.getChildCount() - 1; i++) {
            v = searchView.getChildAt(i);
            if (v instanceof Button) {
                if (so.getPostcode().equalsIgnoreCase(((SearchObject) v.getTag()).getPostcode()))
                {
                    return true;
                }
            }
        }
        return false;
    }
}

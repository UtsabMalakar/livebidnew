package com.official.livebid.activities.viewListingsAgent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.AuctionApplicationModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AuctionApplicationDetail extends AppCompatActivity implements AsyncInterface {
    private ImageView ivUserImg, ivSign;
    private Button btnApproveDetails, btnApproveSign, btnUnApproveDetails, btnUnApproveSign;
    private TextView tvFullName, tvBidderId, tvAddress, tvMobileNumber, tvEmailAddress;
    private Alerts alerts;
    private AuctionApplicationModel application;
    private static int auctionRegStatus = 0;
    private Button btnVerify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_auction_application_detail);
        init();
        getApplicationDetail();

    }

    private void getApplicationDetail() {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", application.userId);
        System.out.println("Auction Applications  detail params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(AuctionApplicationDetail.this, Request.Method.POST, params, UrlHelper.GET_AUCTION_APPLICATION, headers, false, CommonDef.REQ_AUCTION_APPLICATION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void init() {
        alerts = new Alerts(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            application = (AuctionApplicationModel) b.getSerializable(CommonDef.AUCTION_APPLICATION);
        }
        new CustomActionBar.ApplicationDetailsCAB(this, application.uniqueId);
        ivUserImg = (ImageView) findViewById(R.id.iv_profile_img);
        if (application.profileImage != null && application.profileImage.length() > 0) {
            Picasso.with(this)
                    .load(application.profileImage)
                    .resize(320, 240)
                    .error(R.drawable.profile_icon)
                    .into(ivUserImg);
        }
        CommonMethods.setImageviewRatio(this, ivUserImg, 0);
        ivSign = (ImageView) findViewById(R.id.iv_signature);
        btnApproveDetails = (Button) findViewById(R.id.btn_approve_details);
        btnUnApproveDetails = (Button) findViewById(R.id.btn_unapprove_details);
        btnApproveSign = (Button) findViewById(R.id.btn_approve_sign);
        btnUnApproveSign = (Button) findViewById(R.id.btn_unapprove_sign);
        tvFullName = (TextView) findViewById(R.id.tv_full_name);
        tvBidderId = (TextView) findViewById(R.id.tv_bidder_id);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        tvMobileNumber = (TextView) findViewById(R.id.tv_mobile_number);
        tvEmailAddress = (TextView) findViewById(R.id.tv_email_address);
        btnVerify = (Button) findViewById(R.id.btn_bottom);
        btnVerify.setOnClickListener(btnClickListener);
        btnApproveSign.setOnClickListener(btnClickListener);
        btnUnApproveSign.setOnClickListener(btnClickListener);
        btnApproveDetails.setOnClickListener(btnClickListener);
        btnUnApproveDetails.setOnClickListener(btnClickListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_auction_application_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(String result, int requestCode) {

        switch (requestCode) {
            case CommonDef.REQ_AUCTION_APPLICATION:
                System.out.println("Auction Application detail Response:" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    String userImageUrl, signatureUrl;
                    if (code.equals("0001")) {
                        JSONObject joDetail = joResult.getJSONObject("detail");
                        tvFullName.setText(joDetail.getString("first_name") + " " + joDetail.getString("last_name"));
                        tvAddress.setText(joDetail.getString("address"));
                        tvMobileNumber.setText(joDetail.getString("mobile_number"));
                        tvEmailAddress.setText(joDetail.getString("email"));
                        tvBidderId.setText("#" + application.uniqueId);
                        auctionRegStatus = joDetail.getInt("auction_register_status");
                        loadApproveButton(auctionRegStatus);
                        signatureUrl = joDetail.getString("user_signature");
                        loadSign(signatureUrl);


                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.REQ_VERIFY_AUCTION_APPLICATION:
                System.out.println("Auction verify application Response:" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    String userImageUrl, signatureUrl;
                    if (code.equals("0001")) {
                        alerts.showOkMessage(msg);
                        finish();

                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }
    }

    private void loadApproveButton(int auctionRegStatus) {
        if (auctionRegStatus == 1) {
            btnUnApproveDetails.setVisibility(View.VISIBLE);
            btnUnApproveSign.setVisibility(View.VISIBLE);
            btnApproveDetails.setVisibility(View.GONE);
            btnApproveSign.setVisibility(View.GONE);
        } else {
            btnUnApproveDetails.setVisibility(View.GONE);
            btnUnApproveSign.setVisibility(View.GONE);
            btnApproveDetails.setVisibility(View.VISIBLE);
            btnApproveSign.setVisibility(View.VISIBLE);
        }
    }

    private void loadSign(String signatureUrl) {
        if (!signatureUrl.isEmpty()) {
            Picasso.with(this).load(signatureUrl).into(ivSign);
        } else {
//            Picasso.with(this).load(R.drawable.img_coming_soon).into(ivSign);
        }
    }

    private View.OnClickListener btnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_approve_details:
                case R.id.btn_approve_sign:
                case R.id.btn_unapprove_details:
                case R.id.btn_unapprove_sign:
                    toggleButton(auctionRegStatus, view.getId());
                    if (auctionRegStatus != 1)
                        enableVerify();
                    break;
                case R.id.btn_bottom:
                    verifyApplication();
                    break;


            }
        }
    };

    private void verifyApplication() {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", application.userId);
        params.put("property_id", application.propId);
        System.out.println("verify Applications params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(AuctionApplicationDetail.this, Request.Method.POST, params, UrlHelper.VERIFY_AUCTION_APPLICATION, headers, false, CommonDef.REQ_VERIFY_AUCTION_APPLICATION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void toggleButton(int r, int id) {
        if (r != 1) {
            if (btnApproveDetails.getId() == id || btnUnApproveDetails.getId() == id) {
                btnApproveDetails.setVisibility(btnApproveDetails.isShown() ? View.GONE : View.VISIBLE);
                btnUnApproveDetails.setVisibility(btnUnApproveDetails.isShown() ? View.GONE : View.VISIBLE);
                return;
            }
            if (btnApproveSign.getId() == id || btnUnApproveSign.getId() == id) {
                btnApproveSign.setVisibility(btnApproveSign.isShown() ? View.GONE : View.VISIBLE);
                btnUnApproveSign.setVisibility(btnUnApproveSign.isShown() ? View.GONE : View.VISIBLE);
                return;
            }
        }
    }

    public void enableVerify() {
        if (btnUnApproveDetails.isShown() && btnUnApproveSign.isShown()) {
            btnVerify.setEnabled(true);
        } else {
            btnVerify.setEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }
}

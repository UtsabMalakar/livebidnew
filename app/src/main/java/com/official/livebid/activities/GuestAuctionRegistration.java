package com.official.livebid.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.official.livebid.R;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;

public class GuestAuctionRegistration extends MenuActivity implements View.OnClickListener{

    private Button btnSignup;

    @Override
    public int getLayoutId() {
        return R.layout.activity_guest_auction_registration;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_guest_notifications);
        btnSignup= (Button) findViewById(R.id.btn_sign_up);
        btnSignup.setOnClickListener(this);
       new CustomActionBar.TitleBarWithBackAndTitle(this,getResources().getString(R.string.auction_reg));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_sign_up:
//                Opener.Register(GuestAuctionRegistration.this);
                Opener.newRegister(GuestAuctionRegistration.this, UserType.NEW);
                break;
        }
    }


}

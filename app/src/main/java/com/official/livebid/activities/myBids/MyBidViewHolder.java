package com.official.livebid.activities.myBids;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;

/**
 * Created by Ics on 5/12/2016.
 */
public class MyBidViewHolder extends RecyclerView.ViewHolder {
    protected TextView tvAmount, tvTimeEllapsed, tvBidder;
    protected LinearLayout messageContainer;
    protected View seperator;
    protected View rootView;

    public MyBidViewHolder(View itemView) {
        super(itemView);
        rootView = itemView;
        tvAmount = (TextView) itemView.findViewById(R.id.tv_current_bid_amt);
        tvTimeEllapsed = (TextView) itemView.findViewById(R.id.tv_ellasped_time);
        tvBidder = (TextView) itemView.findViewById(R.id.tv_bidder);
        messageContainer = (LinearLayout) itemView.findViewById(R.id.ll_message_container);
        seperator = itemView.findViewById(R.id.sep);
    }
}

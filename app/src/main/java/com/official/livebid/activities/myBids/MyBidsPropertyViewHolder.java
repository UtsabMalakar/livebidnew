package com.official.livebid.activities.myBids;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;

/**
 * Created by Ics on 5/12/2016.
 */
public class MyBidsPropertyViewHolder extends RecyclerView.ViewHolder {
    protected ImageView ivPropertyImg, ivPropertySoldTag;
    protected TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
            tvPropertyStatusIdentifier, tvCurrentBidAmountTitle, tvCurrentBidAmount;
    protected View rootView;
    protected CheckBox chkFav;

    public MyBidsPropertyViewHolder(View view) {
        super(view);
        rootView = view;
        ivPropertyImg = (ImageView) view.findViewById(R.id.iv_property_img);
        ivPropertySoldTag = (ImageView) view.findViewById(R.id.iv_property_sold_tag);
        tvStatusTag = (TextView) view.findViewById(R.id.tv_property_status_tag);
        tvDateTag = (TextView) view.findViewById(R.id.tv_property_date_tag);
        tvPropertySuburbPostcode = (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
        tvPropertyStreet = (TextView) view.findViewById(R.id.tv_property_street);
        tvPropertyStatusIdentifier = (TextView) view.findViewById(R.id.tv_property_status_identifier);
        tvCurrentBidAmountTitle = (TextView) view.findViewById(R.id.tv_current_bid_title);
        tvCurrentBidAmount = (TextView) view.findViewById(R.id.tv_current_bid_amt);
        chkFav = (CheckBox) view.findViewById(R.id.cbox_fav);
    }

}

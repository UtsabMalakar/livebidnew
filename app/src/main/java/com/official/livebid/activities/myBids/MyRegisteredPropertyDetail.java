package com.official.livebid.activities.myBids;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.CustomDateTimePicker;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.activities.liveAuction.CurrentBidModel;
import com.official.livebid.activities.liveAuction.IXMPPRequest;
import com.official.livebid.activities.liveAuction.LiveBidXmlParser;
import com.official.livebid.activities.liveAuction.PropertyAuctionModel;
import com.official.livebid.activities.liveAuction.SoldPropertyModel;
import com.official.livebid.activities.liveAuction.XMPPSettings;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.OfferType;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.RoomDetail;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class MyRegisteredPropertyDetail extends MenuActivity implements AsyncInterface, View.OnClickListener, IXMPPRequest {
    private RecyclerView rvMyBidLive;
    private Alerts alerts;
    private LinearLayoutManager linearLayoutManager;
    private RegisteredPropertyDetailAdapter myBidsLiveAdapter;
    private RegisteredPropertyModel property;
    private ArrayList<CurrentBidModel> myBids;
    private Button btnAction;
    private XMPPSettings xmppSettings;
    private RoomDetail roomDetail;
    private int action;
    private long offerAmount;
    private String settlementDate = "";
    private CustomDateTimePicker custom;
    private SoldPropertyModel soldProp;
    int selector;
    private LiveBidXmlParser xmlParser;

    @Override
    public int getLayoutId() {
        return R.layout.activity_my_bid_live;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
//        setContentView(R.layout.activity_my_bid_live);
        alerts = new Alerts(this);
        linearLayoutManager = new LinearLayoutManager(this);
        rvMyBidLive = (RecyclerView) findViewById(R.id.rc_my_bid_live);
        rvMyBidLive.setLayoutManager(linearLayoutManager);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            property = (RegisteredPropertyModel) bundle.getSerializable(CommonDef.MY_BID_PROP);
            selector = bundle.getInt(CommonDef.MY_BID_SELECTOR);
        }
        new CustomActionBar.MyRegisteredPropertyCAB(this, selector);
        btnAction = (Button) findViewById(R.id.btn_bottom);
        btnAction.setOnClickListener(this);
        /**
         * set property and bids according propertystatus
         * 0 manual calculate/AUCTION DUE TO START
         * 1 live now
         * 2 sold
         * 3 passed in
         * 4 on the marker
         */

        if (selector == CommonDef.MY_BIDS_OFFERS) {
            listMyOffer();
            btnAction.setTag(9);//make an offer
            btnAction.setText("Make An Offer");
        }
        if (selector == CommonDef.MY_BIDS_BIDS) {
            xmppSettings = new XMPPSettings(this);
            connetToLiveAuction();
            getMyBidsForProperty();
            btnAction.setTag(10);//return to auction
            btnAction.setText("Return To Auction");
        }
        if(selector== CommonDef.MY_BIDS_REGISTERED_PROPERTY){
            if (property.propertyStatus == 0) {
                listMyOffer();
                btnAction.setTag(9);//make an offer
                btnAction.setText("Make An Offer");
                btnAction.setVisibility(View.GONE);

            } else if (property.propertyStatus == 1 || property.propertyStatus == 4) {
                xmppSettings = new XMPPSettings(this);
                connetToLiveAuction();
                getMyBidsForProperty();
                btnAction.setTag(10);//return to auction
                btnAction.setText("Return To Auction");

            } else if (property.propertyStatus == 2 || property.propertyStatus == 3) {
                getMyBidsForProperty();
                btnAction.setTag(11);//view summary report//passed in report
                btnAction.setText("View Summary Report");
            } else {

            }

        }



    }

    private void connetToLiveAuction() {
        HashMap<String, String> headers = CommonMethods.getHeaders(MyRegisteredPropertyDetail.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", property.id);
        System.out.println("join room=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(MyRegisteredPropertyDetail.this, Request.Method.POST, params, UrlHelper.JOIN_LIVE_AUCTION, headers, false, CommonDef.RC_CONNECT_LIVE_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (xmppSettings != null)
            xmppSettings.disconnect();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private void getMyBidsForProperty() {
        HashMap<String, String> headers = CommonMethods.getHeaders(MyRegisteredPropertyDetail.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", property.id);
        VolleyRequest volleyRequest = new VolleyRequest(MyRegisteredPropertyDetail.this, Request.Method.POST, params, UrlHelper.MY_BIDS_DETAIL, headers, false, CommonDef.RC_MY_BIDS_DETAIL);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void listMyOffer() {
        HashMap<String, String> headers = CommonMethods.getHeaders(MyRegisteredPropertyDetail.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", property.id);
        params.put("offer_type", OfferType.PRE_OFFER.getOfferType() + "");
        params.put("offer_type", "");// for all
        System.out.println("my offer params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(MyRegisteredPropertyDetail.this, Request.Method.POST, params, UrlHelper.MY_OFFER_DETAILS, headers, false, CommonDef.REQUEST_MY_OFFER);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("My bids: " + result);
        switch (requestCode) {

            case CommonDef.RC_MY_BIDS_DETAIL:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONArray jaList = joResult.getJSONArray("list");
                        if (jaList.length() > 0) {
                            CurrentBidModel bm;
                            JSONObject joBid;
                            myBids = new ArrayList<>();
                            for (int i = 0; i < jaList.length(); i++) {
                                joBid = jaList.getJSONObject(i);
                                bm = new CurrentBidModel();
                                bm.setBidAmount(joBid.getString("bid_amount"));
                                bm.setBidTime(joBid.getLong("bid_date_timestamp"));
                                bm.status = joBid.getString("status");
                                myBids.add(bm);
                            }
                            myBidsLiveAdapter = new RegisteredPropertyDetailAdapter(this, property, myBids,selector);
                            rvMyBidLive.setAdapter(myBidsLiveAdapter);
                        }
                    } else {
                        myBidsLiveAdapter = new RegisteredPropertyDetailAdapter(this, property, new ArrayList<CurrentBidModel>(),selector);
                        rvMyBidLive.setAdapter(myBidsLiveAdapter);
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_JOIN_LIVE_AUCTION:
                System.out.println("join live auction" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONObject jObjRoomDetail = joResult.getJSONObject("room_detail");
                        RoomDetail roomDetail = new RoomDetail();
                        roomDetail.auctionId = jObjRoomDetail.getString("id");
                        roomDetail.propertyId = jObjRoomDetail.getString("property_id");
                        roomDetail.roomName = jObjRoomDetail.getString("room_name");
                        roomDetail.callCount = jObjRoomDetail.getInt("count_call");
                        roomDetail.soldTo = jObjRoomDetail.getString("sold_to");
                        roomDetail.soldAmount = jObjRoomDetail.getString("sold_amount");
                        roomDetail.status = jObjRoomDetail.getString("status");
                        roomDetail.startDateTime = jObjRoomDetail.getString("auction_start_date_time_stamp");

                        PropertyAuctionModel propertyModel = new PropertyAuctionModel();
                        propertyModel.propertyId = property.id;
                        propertyModel.propertyImgUrl = property.propertyImgUrl;
                        propertyModel.street = property.street;
                        propertyModel.regionPostcode = property.suburb + " " + property.postCode;
                        propertyModel.propertyStatus = property.propertyStatus;
                        propertyModel.propertyStatus2 = property.propertyStatus2;
                        propertyModel.agentId=property.agentId;
                        propertyModel.moreImages = property.property_images_more;

                        Opener.PropertyAuction(MyRegisteredPropertyDetail.this, propertyModel, roomDetail);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
            case CommonDef.RC_CONNECT_LIVE_AUCTION:
                System.out.println("join live auction" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONObject jObjRoomDetail = joResult.getJSONObject("room_detail");
                        roomDetail = new RoomDetail();
                        roomDetail.auctionId = jObjRoomDetail.getString("id");
                        roomDetail.propertyId = jObjRoomDetail.getString("property_id");
                        roomDetail.roomName = jObjRoomDetail.getString("room_name");
                        roomDetail.callCount = jObjRoomDetail.getInt("count_call");
                        roomDetail.soldTo = jObjRoomDetail.getString("sold_to");
                        roomDetail.soldAmount = jObjRoomDetail.getString("sold_amount");
                        roomDetail.status = jObjRoomDetail.getString("status");
                        roomDetail.startDateTime = jObjRoomDetail.getString("auction_start_date_time");
                        xmppSettings.connect();
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
            case CommonDef.REQUEST_MY_OFFER:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONArray jaList = joResult.getJSONArray("list");
                        if (jaList.length() > 0) {
                            CurrentBidModel bm;
                            JSONObject joBid;
                            myBids = new ArrayList<>();
                            for (int i = 0; i < jaList.length(); i++) {
                                joBid = jaList.getJSONObject(i);
                                bm = new CurrentBidModel();
                                bm.setBidAmount(joBid.getString("offer_amount"));
                                bm.setBidTime(joBid.getLong("offer_date_timestamp"));
                                bm.startDateTime = joBid.getString("offer_date_time");//user for settlement date in case of preoffer
                                bm.status = joBid.getString("status");
                                myBids.add(bm);
                            }
                            myBidsLiveAdapter = new RegisteredPropertyDetailAdapter(this, property, myBids,selector);
                            rvMyBidLive.setAdapter(myBidsLiveAdapter);
                        }
                    } else {
                        myBidsLiveAdapter = new RegisteredPropertyDetailAdapter(this, property, new ArrayList<CurrentBidModel>(),selector);
                        rvMyBidLive.setAdapter(myBidsLiveAdapter);
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.REQUEST_MAKE_PRE_OFFER:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        listMyOffer();

                    } else {

                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.RC_SUMMARY_REPORT_SOLD:
                System.out.println("sumary report" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONObject jObjBidSummary = joResult.getJSONObject("bid_summary");
                        soldProp = new SoldPropertyModel(property);
                        soldProp.userId = property.agentId;
                        soldProp.propertyId = property.id;
                        soldProp.propertyImgUrl = property.propertyImgUrl;
                        soldProp.street = property.street;
                        soldProp.regionPostcode = property.suburb + " " + property.postCode;
                        soldProp.participations = jObjBidSummary.getString("participation");
                        soldProp.highestBidAmount = soldProp.getHighestBidAmount(jObjBidSummary.getString("highest_bid"));
                        soldProp.highestBidder = jObjBidSummary.getString("highest_bidder");
                        String tempHB = jObjBidSummary.getString("highest_bidder");
                        if (tempHB.equalsIgnoreCase(CommonMethods.getLoggedInUserUniqueId(getApplicationContext())))
                            soldProp.highestBidder = "Y" + "ou";
                        soldProp.soldDate = jObjBidSummary.getString("auction_end_date_time");
                        soldProp.soldDate = soldProp.getSoldDateTime(jObjBidSummary.getString("auction_end_date_time"))[0];
                        soldProp.soldTime = soldProp.getSoldDateTime(jObjBidSummary.getString("auction_end_date_time"))[1];
                        soldProp.bidDuration = soldProp.getDuration(Long.parseLong(jObjBidSummary.getString("duration")));
                        soldProp.setWon(jObjBidSummary.getString("check_myself"));
                        if (property.propertyStatus == 2)
                            Opener.SoldPropertySummary(MyRegisteredPropertyDetail.this, soldProp);
                        else
                            Opener.PassedPropertySummary(MyRegisteredPropertyDetail.this, soldProp);
//                        Opener.SoldPropertySummary(MyRegisteredPropertyDetail.this, soldProp);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    private void joinLiveAuction() {
        HashMap<String, String> headers = CommonMethods.getHeaders(MyRegisteredPropertyDetail.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", property.id);
        System.out.println("join room=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(MyRegisteredPropertyDetail.this, Request.Method.POST, params, UrlHelper.JOIN_LIVE_AUCTION, headers, false, CommonDef.RC_JOIN_LIVE_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_bottom:
                action = (int) view.getTag();
                switch (action) {
                    case 9:// make offer
                        showMakeOfferPopup();
                        break;
                    case 10://join auction
                        joinLiveAuction();
                        break;
                    case 11://view summary sold
                        getSoldPropertySummaryReport();
                        break;
                }
                break;
        }
    }

    public void showMakeOfferPopup() {

        View view = getLayoutInflater().inflate(R.layout.make_offer_dialog, null);
        final Button btnSubmit = (Button) view.findViewById(R.id.btn_submit);
        final EditText etOfferPrice = (EditText) view.findViewById(R.id.et_offer_price);
        final EditText etSettlementDateTime = (EditText) view.findViewById(R.id.et_settlement_date_time);
        etOfferPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {
                    etOfferPrice.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,]", "");


                    String formatted = "";
                    if (cleanString.equalsIgnoreCase("") || cleanString.equalsIgnoreCase("0"))
                        formatted = "";
                    else {
                        offerAmount = Long.parseLong(cleanString);
                        formatted = "$" + NumberFormat.getNumberInstance(Locale.US).format((offerAmount));
                    }
                    current = formatted;
                    etOfferPrice.setText(formatted);
                    etOfferPrice.setSelection(formatted.length());

                    etOfferPrice.addTextChangedListener(this);
                    enableOffer(etOfferPrice.getText().toString(), btnSubmit);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isValidPrice(offerAmount)) {
                    alerts.showOkMessage("Offer price must be greater than 80% of property price.");
                    return;
                }
                if (!isValidSettlementDate(settlementDate)) {
                    alerts.showOkMessage("Settlement date must at least 1 day after today.");
                    return;
                }
                mBottomSheetDialog.dismiss();
                makeOffer();

            }
        });
        etSettlementDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                custom = new CustomDateTimePicker(MyRegisteredPropertyDetail.this,
                        new CustomDateTimePicker.ICustomDateTimeListener() {

                            @Override
                            public void onSet(Dialog dialog, Calendar calendarSelected,
                                              Date dateSelected, int year, String monthFullName,
                                              String monthShortName, int monthNumber, int date,
                                              String weekDayFullName, String weekDayShortName,
                                              int hour24, int hour12, int min, int sec,
                                              String AM_PM) {

                                etSettlementDateTime.setText(calendarSelected
                                        .get(Calendar.DAY_OF_MONTH)
                                        + " " + (monthShortName) + " at "
                                        + hour12 + ":" + min
                                        + " " + AM_PM);
                                String m = (monthNumber + 1) < 10 ? "0" + (monthNumber + 1) : (monthNumber + 1) + "";
                                String d = calendarSelected.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendarSelected.get(Calendar.DAY_OF_MONTH)
                                        : calendarSelected.get(Calendar.DAY_OF_MONTH) + "";
                                settlementDate = year + "-" + m + "-" + d + " "
                                        + hour24 + ":"
                                        + min + ":"
                                        + sec;
                                System.out.println("sDate:" + settlementDate);
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                custom.set24HourFormat(false);
                custom.setDate(Calendar.getInstance());
                custom.showDialog();
            }
        });


    }

    private void enableOffer(String price, Button submit) {
        if (!price.isEmpty()) {
            submit.setEnabled(true);
        } else {
            submit.setEnabled(false);
        }
    }

    private boolean isValidPrice(long price) {
        boolean result = false;
        if (price > Long.parseLong(property.price.toString().replaceAll("[$,]", "")) * .8)
            result = true;
        return result;
    }

    private boolean isValidSettlementDate(String sDate) {
        boolean result = false;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        if (sDate.isEmpty())
            return true;

        Date notifiedDate = null;
        try {
            notifiedDate = dateFormat.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = Calendar.getInstance().getTime();
        long different = notifiedDate.getTime() - (now.getTime() + 24 * 60 * 60 * 1000);
        if (different >= 0)
            result = true;
        return result;
    }

    private void makeOffer() {
        HashMap<String, String> headers = CommonMethods.getHeaders(MyRegisteredPropertyDetail.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", property.id);
        params.put("offer_type", OfferType.PRE_OFFER.getOfferType() + "");
        params.put("offer_amount", offerAmount + ".00");
        params.put("preferred_settlement", settlementDate);
        System.out.println("pre offer Auction params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(MyRegisteredPropertyDetail.this, Request.Method.POST, params, UrlHelper.MAKE_PRE_OFFER, headers, false, CommonDef.REQUEST_MAKE_PRE_OFFER);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void getSoldPropertySummaryReport() {
        HashMap<String, String> headers = CommonMethods.getHeaders(MyRegisteredPropertyDetail.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", property.id);
        System.out.println("SUMMARY REPORT=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(MyRegisteredPropertyDetail.this, Request.Method.POST, params, UrlHelper.GET_SUMMARY_REPORT_SOLD, headers, false, CommonDef.RC_SUMMARY_REPORT_SOLD);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }


    public void setConnection(XMPPTCPConnection connection) {
        Presence presence = new Presence(Presence.Type.available);
        try {
            connection.sendPacket(presence);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
        xmppSettings.joinMultiUserRoom(connection, roomDetail.roomName);
    }


    @Override
    public void XMPPConSuccess(XMPPTCPConnection connection) {
        if (connection != null) {
            System.out.println("Testing testing on success:");
            setConnection(connection);
        } else
            Toast.makeText(getApplicationContext(), "Connection failure", Toast.LENGTH_LONG).show();
    }

    @Override
    public void XMPPConFailure(Exception ex) {

    }

    @Override
    public void onReceiveMessage(Message message) throws IOException, XmlPullParserException {
        System.out.println("My message: " + message.toString().replace("&lt;", "<").replace("&gt;", ">"));
        String xmlString = message.toString().replace("&lt;", "<").replace("&gt;", ">");
        xmlParser = new LiveBidXmlParser(getApplicationContext());
        CurrentBidModel bidModel = xmlParser.parseXML(xmlString);
        property.currentBidAmount = bidModel.bidAmount;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myBidsLiveAdapter.upPropertyHeader(property);
            }
        });

    }
}

package com.official.livebid.activities.profile;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.activities.newRegistration.RegType;
import com.official.livebid.adapters.GooglePlacesAutocompleteAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.Address;
import com.official.livebid.staticClasses.MobileVerification;
import com.official.livebid.staticClasses.RegisterUser;
import com.official.livebid.utils.GPSTracker;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalDetails extends Fragment implements AsyncInterface, CropImageView.OnSetImageUriCompleteListener, CropImageView.OnGetCroppedImageCompleteListener {

    private Alerts alerts;
    private SharedPreference userPrefs;
    private AutoCompleteTextView acAddress;
    private TextView tvProfilePic;
    private ImageView ivProfileImg;
    RelativeLayout cropView, mainView, confirmDialog;
    CropImageView cropImageView;
    Button btnCrop;
    Button btnSave;
    Button btnConfirmOk;
    private static Bitmap croppedImage;
    static boolean isUpdatePhoto = false;
    static boolean isAutoAddress = false;
    GPSTracker gps;

    CustomEditText cvFirstName, cvLastName, cvEmailAddress, cvMobileNumber;
    EditText etFirstName, etLastName, etEmailAddress, etMobileNumber;
    AutoCompleteTextView etAddress;
    String firstName, lastName, address, emailAddress, mobileNumber;
    private String strFormatedAddress;// for tracking for select from autocomplete
    private String autoLat;
    private String autoLng;

    public PersonalDetails() {
        // Required empty public constructor

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        alerts = new Alerts(getActivity());
        userPrefs = new SharedPreference(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_personal_details, container, false);
        gps = new GPSTracker(getActivity());
        cvFirstName = (CustomEditText) view.findViewById(R.id.cv_first_name);
        etFirstName = (EditText) cvFirstName.findViewById(R.id.textEdit);
        cvLastName = (CustomEditText) view.findViewById(R.id.cv_last_name);
        etLastName = (EditText) cvLastName.findViewById(R.id.textEdit);
        cvEmailAddress = (CustomEditText) view.findViewById(R.id.cv_email);
        etEmailAddress = (EditText) cvEmailAddress.findViewById(R.id.textEdit);
        etEmailAddress.setFocusable(false);
        etEmailAddress.setClickable(false);
        cvMobileNumber = (CustomEditText) view.findViewById(R.id.cv_mobile_no);
        etMobileNumber = (EditText) cvMobileNumber.findViewById(R.id.textEdit);
        etAddress = (AutoCompleteTextView) view.findViewById(R.id.et_address);

        cropView = (RelativeLayout) view.findViewById(R.id.rl_crop_view);
        cropImageView = (CropImageView) view.findViewById(R.id.CropImageView);
        cropImageView.setCropShape(CropImageView.CropShape.RECTANGLE);
        cropImageView.setFixedAspectRatio(true);
        cropImageView.setAspectRatio(4, 3);
        btnCrop = (Button) view.findViewById(R.id.btn_crop);
        mainView = (RelativeLayout) view.findViewById(R.id.rl_main_view);
        CommonMethods.setupUI(mainView,getActivity());
        confirmDialog = (RelativeLayout) view.findViewById(R.id.rl_confirmation);
        btnSave = (Button) view.findViewById(R.id.btn_save_changes);
        btnConfirmOk = (Button) view.findViewById(R.id.btn_ok);


        acAddress = (AutoCompleteTextView) view.findViewById(R.id.et_address);
        acAddress.setDropDownWidth(getResources().getDisplayMetrics().widthPixels);
        tvProfilePic = (TextView) view.findViewById(R.id.tv_user_profile_pic);
        ivProfileImg = (ImageView) view.findViewById(R.id.iv_upload_img);
        CommonMethods.setImageviewRatio(getActivity(), ivProfileImg, (int)CommonMethods.pxFromDp(getActivity(),26));
        tvProfilePic.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (tvProfilePic.getRight() - tvProfilePic.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        startActivityForResult(CommonMethods.getPickImageChooserIntent(getActivity()), 111);
                        return true;
                    }
                }
                return false;
            }
        });
        acAddress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (acAddress.getRight() - acAddress.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
//                        Toast.makeText(getActivity(), "address", Toast.LENGTH_SHORT).show();
                        if (gps.canGetLocation()) {
                            autoLat = gps.getLatitude() + "";
                            autoLng = gps.getLongitude() + "";
                            System.out.println("auto address: " + autoLat + "," + autoLng);
                            try {
                                getAddressFromLatLng(autoLat, autoLng);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
//                            isLocationSet = true;
//                            getProperty();
                        } else {
                            gps.showSettingsAlert();
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        etFirstName.addTextChangedListener(tw);
        etLastName.addTextChangedListener(tw);
        etAddress.addTextChangedListener(tw);
        etMobileNumber.addTextChangedListener(tw);
        etAddress.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.place_autocomplete_list_item));
        etAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                strFormatedAddress = (String) adapterView.getItemAtPosition(i);
                etAddress.clearFocus();
                try {
                    getGeocodeFromAddress(strFormatedAddress);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
//                enableUpdate();

//                Toast.makeText(getActivity(), strFormatedAddress, Toast.LENGTH_SHORT).show();
            }
        });
        etAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (strFormatedAddress == null) {
                        etAddress.setText("");
                    }
                }
            }
        });

        btnCrop.setOnClickListener(clickListener);
        btnSave.setOnClickListener(clickListener);
        btnConfirmOk.setOnClickListener(clickListener);
        getUserProfile();

        return view;
    }


    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_USER_PROFILE:
                System.out.println("view profile=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        String imgUrl;
                        JSONObject joUserInfo = joResult.getJSONObject("detail");
                        cvFirstName.setText(com.official.livebid.staticClasses.PersonalDetails.firstName = joUserInfo.getString("first_name"));
                        cvLastName.setText(com.official.livebid.staticClasses.PersonalDetails.lastName = joUserInfo.getString("last_name"));
                        if (joUserInfo.getString("temp_mobile_number").isEmpty()) {
                            cvMobileNumber.setText(com.official.livebid.staticClasses.PersonalDetails.mobileNumber = joUserInfo.getString("mobile_number"));
                            cvMobileNumber.setDrawable(0);
                            etMobileNumber.setOnTouchListener(null);
                        } else {
                            cvMobileNumber.setText(com.official.livebid.staticClasses.PersonalDetails.mobileNumber = joUserInfo.getString("temp_mobile_number"));
                            cvMobileNumber.setDrawable(R.drawable.whati_icon);
                            etMobileNumber.setOnTouchListener(drawableListener);
//                            etMobileNumber.setFocusable(false);
//                            etMobileNumber.setClickable(true);
                        }
                        cvEmailAddress.setText(joUserInfo.getString("email"));
                        etAddress.setText(com.official.livebid.staticClasses.PersonalDetails.address = joUserInfo.getString("address"));
                        Address.lat = joUserInfo.getString("lat");
                        Address.lng = joUserInfo.getString("lng");
                        imgUrl = joUserInfo.getString("profile_image");
                        if(croppedImage==null) {
                            if (imgUrl != null && imgUrl.length() > 0) {
                                Picasso.with(getActivity())
                                        .load(imgUrl)
                                        .resize(320, 240)
                                        .error(R.drawable.profile_icon)
//                                    .placeholder( R.drawable.img_loading)
//                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .into(ivProfileImg);
                            }
                        }
                        else {
                            ivProfileImg.setImageBitmap(croppedImage);

                        }

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_UPDATE_USER_PROFILE:
                System.out.println("UPDATEPROFILE=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001") || code.equals("0032")) {
//                        alerts.showOkMessage(msg);
                        JSONObject joUserInfo = joResult.getJSONObject("detail");
                        userPrefs.setKeyValues(CommonDef.SharedPrefKeys.FIRST_NAME, joUserInfo.getString("first_name"));
                        userPrefs.setKeyValues(CommonDef.SharedPrefKeys.LAST_NAME, joUserInfo.getString("last_name"));
//                        userPrefs.setKeyValues(CommonDef.SharedPrefKeys.USER_EMAIL, joUserInfo.getString("email"));
                        userPrefs.setKeyValues(CommonDef.SharedPrefKeys.PROFILE_IMG, joUserInfo.getString("profile_image"));
                        btnSave.setVisibility(View.GONE);
                        confirmDialog.setVisibility(View.VISIBLE);
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_LAT_LNG:
                System.out.println("LAT-LNG=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("status");
                    if (code.equalsIgnoreCase("ok")) {
//                        alerts.showOkMessage(msg);
                        JSONObject jObjLocation = joResult.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
                        Address.lat = jObjLocation.getString("lat");
                        Address.lng = jObjLocation.getString("lng");
                        System.out.println("lat=" + Address.lat);
                        System.out.println("lng=" + Address.lng);
                        enableUpdate();
                    } else {
//                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
            case CommonDef.REQUEST_ADDRESS_FROM_LAT_LNG:
                System.out.println("LAT-LNG_addr=>" + result.toString());
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("status");
                    if (code.equalsIgnoreCase("ok")) {
//                        alerts.showOkMessage(code);
                        strFormatedAddress = joResult.getJSONArray("results").getJSONObject(0).getString("formatted_address");
                        isAutoAddress = true;
                        etAddress.setText(strFormatedAddress);
                        etAddress.clearFocus();
                        Address.lat = autoLat + "";
                        Address.lng = autoLng + "";
                        System.out.println("formatted address" + strFormatedAddress);
                        enableUpdate();
                    } else {
//                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            cropView.setVisibility(View.VISIBLE);
            btnCrop.setVisibility(View.VISIBLE);
            mainView.setVisibility(View.GONE);
            btnSave.setVisibility(View.GONE);
            Uri imageUri = CommonMethods.getPickImageResultUri(data, getActivity());
            cropImageView.setImageUriAsync(imageUri);

        }
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_crop:
                    cropImageView.getCroppedImageAsync(cropImageView.getCropShape(), 0 ,0 );
                    cropView.setVisibility(View.GONE);
                    btnCrop.setVisibility(View.GONE);
                    mainView.setVisibility(View.VISIBLE);
                    btnSave.setVisibility(View.VISIBLE);
                    isUpdatePhoto = true;
                    enableUpdate();

                    break;
                case R.id.btn_save_changes:
//                    new UpdateProfileAsyncTask().execute(UrlHelper.UPDATE_USER_PROFILE);
                    CommonMethods.hideSoftKeyboard(getActivity());
                    if (isMobileValid(etMobileNumber.getText().toString())) {
                        cvMobileNumber.setValid();
                        UpdateUserProfile();
                    }
                    else {
                        cvMobileNumber.setInvalid();
                    }

                    break;
                case R.id.btn_ok:
//                   //
                    getUserProfile();
                    isUpdatePhoto = false;
                    isAutoAddress = false;
                    enableUpdate();
                    etAddress.clearFocus();
                    confirmDialog.setVisibility(View.GONE);
                    btnSave.setVisibility(View.VISIBLE);
                    break;
            }


        }
    };


    @Override
    public void onGetCroppedImageComplete(CropImageView view, Bitmap bitmap, Exception error) {
        if (error == null) {
            croppedImage =Bitmap.createScaledBitmap(bitmap,320,240,false) ;
            ivProfileImg.setImageBitmap(croppedImage);
        } else {
            Toast.makeText(cropImageView.getContext(), "Image crop failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
//            Toast.makeText(cropImageView.getContext(), "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(cropImageView.getContext(), "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        cropImageView.setOnSetImageUriCompleteListener(this);
        cropImageView.setOnGetCroppedImageCompleteListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        cropImageView.setOnSetImageUriCompleteListener(null);
        cropImageView.setOnGetCroppedImageCompleteListener(null);
        gps.stopUsingGPS();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }

    @Override
    public void onPause() {
        super.onPause();
        gps.stopUsingGPS();
    }


    public void getUserProfile() {
        HashMap<String, String> headers = CommonMethods.getHeaders(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("device_type", "2");
        System.out.println("logout header=>" + headers.toString());
        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.POST, params, UrlHelper.USER_VIEW_PROFILE, headers, false, CommonDef.REQUEST_USER_PROFILE);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    public void UpdateUserProfile() {
        HashMap<String, String> headers =CommonMethods.getHeaders(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        params.put("mobile_number", mobileNumber);
        params.put("address", etAddress.getText().toString());
        params.put("lat", (Address.lat != null && !Address.lat.isEmpty()) ? Address.lat : "0");
        params.put("lng", (Address.lng != null && !Address.lng.isEmpty()) ? Address.lng : "0");
        System.out.println("updateProfile header=>" + headers.toString());
        System.out.println("updateProfile params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.POST, params, UrlHelper.UPDATE_USER_PROFILE, headers, true, CommonDef.REQUEST_UPDATE_USER_PROFILE);
        volleyRequest.asyncInterface = this;
        volleyRequest.profileImg = croppedImage;
        volleyRequest.request();
    }

    private void enableUpdate() {
        if (isUpdatable()) {
            btnSave.setEnabled(true);
        } else {
            btnSave.setEnabled(false);
        }
    }

    public void getDetail() {
        firstName = etFirstName.getText().toString();
        lastName = etLastName.getText().toString();
        address = etAddress.getText().toString();
        emailAddress = etEmailAddress.getText().toString();
        mobileNumber = etMobileNumber.getText().toString();
    }

    public boolean isUpdatable() {
        getDetail();
        return !firstName.equalsIgnoreCase(com.official.livebid.staticClasses.PersonalDetails.firstName)
                || !lastName.equalsIgnoreCase(com.official.livebid.staticClasses.PersonalDetails.lastName)
                || !mobileNumber.equalsIgnoreCase(com.official.livebid.staticClasses.PersonalDetails.mobileNumber)
                || !address.equalsIgnoreCase(com.official.livebid.staticClasses.PersonalDetails.address) && strFormatedAddress != null
                || isUpdatePhoto;
    }

    private TextWatcher tw = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (!isAutoAddress) {
                strFormatedAddress = null;
            }

            enableUpdate();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    public void onResume() {
        super.onResume();
//        Toast.makeText(getActivity(), "userProfile", Toast.LENGTH_SHORT).show();
        getUserProfile();
    }

    View.OnTouchListener drawableListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (motionEvent.getRawX() >= (etMobileNumber.getRight() - etMobileNumber.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here
//                        Toast.makeText(getActivity(), "mobile verification", Toast.LENGTH_SHORT).show();
                    MobileVerification.mode = 3;
                    RegisterUser.mobileNumber = com.official.livebid.staticClasses.PersonalDetails.mobileNumber;
                    Opener.MobileVerification(getActivity(), RegType.USER);

                    return true;
                }
            }
            return false;
        }
    };

    private void getGeocodeFromAddress(String addr) throws UnsupportedEncodingException {

        String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(addr, "utf8");
        System.out.println("GeocodeUrl" + PLACES_API_BASE);
        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();
        params.put("ley", "value");
//        headers.put("Content-Type", "application/json");

        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.GET, params, PLACES_API_BASE, headers, false, CommonDef.REQUEST_LAT_LNG);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();


    }

    private void getAddressFromLatLng(String lat, String lng) throws UnsupportedEncodingException {

        String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng;
        System.out.println("GeocodeUrl" + PLACES_API_BASE);
        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();
        params.put("ley", "value");
//        headers.put("Content-Type", "application/json");

        VolleyRequest volleyRequest = new VolleyRequest(getActivity(), Request.Method.GET, params, PLACES_API_BASE, headers, false, CommonDef.REQUEST_ADDRESS_FROM_LAT_LNG);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();


    }

    private boolean isMobileValid(String mobile) {
//        isMibileOk = mobile.length() >= 9 ? true : false;
//        final Pattern phonePattern = Pattern.compile("^(?:\\+?61|0)4 ?(?:(?:[01] ?[0-9]|2 ?[0-57-9]|3 ?[1-9]|4 ?[7-9]|5 ?[018]) ?[0-9]|3 ?0 ?[0-5])(?: ?[0-9]){5}$");
        final Pattern phonePattern = Pattern.compile("^(?:\\(?(?:\\+?61|0)4\\)?(?:[ -]?[0-9]){7}[0-9]$)");
        return phonePattern.matcher(mobile).matches();

    }
}

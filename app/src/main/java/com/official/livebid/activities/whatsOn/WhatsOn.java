package com.official.livebid.activities.whatsOn;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.activities.MenuActivity;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class WhatsOn extends MenuActivity implements AsyncInterface {

    private RecyclerView rcWhatsOn;
    private ImageButton ibtnSearch;
    private RelativeLayout rvSearchBar;
    private ArrayList<OpenHouseProperty> alPropertyListInfo;
    private com.official.livebid.alerts.Alerts alerts;
    private boolean isLastPage = false;
    private int offset = 0;
    private WhatsOnAdapter whatsOnAdapter;
    private TextView tvSearchSuburb;

    @Override
    public int getLayoutId() {
        return R.layout.activity_whats_on;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
//        setContentView(R.layout.activity_whats_on);
        init();
        loadWhatsOn(false);
        setSearchHistory();

    }


    private void loadWhatsOn(boolean hidePd) {
        HashMap<String, String> params = new HashMap<>();
        params.put("lat", "0");
        params.put("lng", "0");
        params.put("is_trending", "0");
        params.put("discover_home", "0");
        params.put("this_week_auction", "1");
        params.put("this_week_open_house", "0");
        params.put("post_code_ids",prefs.getStringValues("searchPostCodeIds"));
        HashMap headers = CommonMethods.getHeaders(this);
        VolleyRequest volleyRequest = new VolleyRequest(WhatsOn.this, Request.Method.POST, params, UrlHelper.GET_WHATS_ON, headers, false, CommonDef.RC_WHATS_ON);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(hidePd);
    }

    private void init() {
        alerts = new Alerts(this);

        new CustomActionBar.WhatsOnCAB(this);
        rcWhatsOn = (RecyclerView) findViewById(R.id.rc_whats_on);
        tvSearchSuburb = (TextView) findViewById(R.id.tvSearchSuburb);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcWhatsOn.setLayoutManager(linearLayoutManager);
        rcWhatsOn.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (!isLastPage) {
                    loadWhatsOn(true);
                }
            }
        });
        ibtnSearch = (ImageButton) findViewById(R.id.ibtn_search);
        rvSearchBar = (RelativeLayout) findViewById(R.id.rlSearchBar);
        ibtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Opener.Search(WhatsOn.this,CommonDef.CallingActivity.Whats_On);
            }
        });
        rvSearchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Opener.Search(WhatsOn.this,CommonDef.CallingActivity.Whats_On);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_whats_on, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MenuActivity.activeTab = MenuActivity.NOT_SELECTED;
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.RC_WHATS_ON:
                System.out.println("Result " + result);
                initPropertyList(result);
                break;
        }
    }

    private void initPropertyList(String result) {

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            String[] images = new String[2];
            images[0] = joResult.getString("discover_main_image");
            images[1] = joResult.getString("discover_trending_image");
            if (code.equals("0001")) {
                JSONArray jaList = joResult.getJSONArray("list");
                if (jaList.length() > 0) {
                    alPropertyListInfo = new ArrayList<>();
                    OpenHouseProperty propertyListInfo;
                    for (int i = 0; i < jaList.length(); i++) {
                        JSONObject joList = jaList.getJSONObject(i);
                        propertyListInfo = new OpenHouseProperty();
                        propertyListInfo.id = joList.getString("id");
                        propertyListInfo.agentId = joList.getString("agent_id");
                        propertyListInfo.agencyId = joList.getString("agency_id");
                        propertyListInfo.auctionDate = joList.getString("auction_date");
                        propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
                        HashMap<String ,String> agentName = CommonMethods.getName(joList.getString("agent_name").split(" "));
                        propertyListInfo.agentFName = agentName.get("fName");
                        propertyListInfo.agentLName = agentName.get("lName");
                        propertyListInfo.propertyStatus = joList.getInt("property_status");
                        propertyListInfo.statusTag = "";
                        propertyListInfo.dateTag = "Auction due to 40 days";
                        propertyListInfo.setFav(joList.getString("favorite"));
                        propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
                        propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
                        propertyListInfo.nosOfParking = joList.getString("parking");
                        propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(joList.getString("price")));
                        propertyListInfo.propertyStatusIdentifier = "from";
                        propertyListInfo.price_text=joList.getString("price_text");
                        propertyListInfo.suburb = joList.getString("suburb");
                        propertyListInfo.postCode = joList.getString("postcode");
                        propertyListInfo.propertyTitle = joList.getString("property_title");
                        propertyListInfo.propertyDesc = joList.getString("property_description");
                        propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
                        propertyListInfo.street = joList.getString("street");
                        propertyListInfo.propertyState = joList.getString("state");
                        propertyListInfo.propertyImgUrl = joList.getString("property_image");
                        propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
                        propertyListInfo.distance = joList.getString("distance");
                        propertyListInfo.lat = joList.getString("lat");
                        propertyListInfo.lng = joList.getString("lng");
                        propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
                        propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
                        propertyListInfo.auctionDateTimeStamp = joList.getLong("auction_date_timestamp");
                        propertyListInfo.propertyStatus2 = joList.getInt("property_status_2");
                        JSONArray JPropertyImageMore = joList.getJSONArray("property_images_more");
                        if (JPropertyImageMore.length() > 0) {
                            propertyListInfo.property_images_more = new ArrayList<>();
                            for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                   /* moreImage = new PropertyInfo.Property_images_more(
                                            JPropertyImageMore.getJSONObject(j).getString("image_url")
                                    );*/
                                propertyListInfo.property_images_more.add(JPropertyImageMore.getString(j));
                            }
                        }
                        JSONArray jaOpenHouse = null;
                        jaOpenHouse = joList.getJSONArray("open_house");
                        propertyListInfo.setOHP(false);
                        if (jaOpenHouse.length() > 0) {

                            JSONObject joOpenHouse;
                            joOpenHouse = jaOpenHouse.getJSONObject(0);
                            propertyListInfo.setOhpStatus(joOpenHouse.getInt("open_house_status"));
                            propertyListInfo.setHasMore(joOpenHouse.getString("has_more"));
                            propertyListInfo.setOpenHouseTime(joOpenHouse.getString("open_house_time"));
                            if (propertyListInfo.getOhpStatus() == 1 || propertyListInfo.getOhpStatus() == 2)
                                propertyListInfo.setOHP(true);
                        }
                        alPropertyListInfo.add(propertyListInfo);
                    }
                    isLastPage = joResult.getBoolean("last_page");
                    if (whatsOnAdapter == null) {
                        whatsOnAdapter = new WhatsOnAdapter(WhatsOn.this, alPropertyListInfo, images[0], images[1]);
                        rcWhatsOn.setAdapter(whatsOnAdapter);
                    } else {
//                        curSize = myListingAdapter.getItemCount();
//                        myListingAdapter.notifyItemRangeInserted(curSize, alPropertyListInfo.size()-1);
                        whatsOnAdapter.addMoreData(alPropertyListInfo);

                    }
                    offset = joResult.getInt("offset");
                } else {
                    whatsOnAdapter = new WhatsOnAdapter(WhatsOn.this, new ArrayList<OpenHouseProperty>(), images[0], images[1]);
                    rcWhatsOn.setAdapter(whatsOnAdapter);
                }
            } else {
                alerts.showOkMessage(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getSearchedNames(List<String> list)
    {
        int no_of_search=0;
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<list.size();i++)
        {
            if(i==0)
            {
                sb.append(list.get(i));
            }
            else
            {
                no_of_search++;
            }
        }
        if(list.size()==1)
        {
            return sb.toString();
        }
        else {
            sb.append(" and " + no_of_search + " more");
            return sb.toString();
        }
    }

    private void setSearchHistory() {
        prefs = new SharedPreference(this);
        String searched_suburb = prefs.getStringValues(CommonDef.SharedPrefKeys.Search_SuburbNames);
        System.out.print("Searched suburb" + searched_suburb);
        List<String> list = new ArrayList<String>(Arrays.asList(searched_suburb.split(",")));
        tvSearchSuburb.setText(getSearchedNames(list));
    }


    @Override
    protected void onResume() {
        super.onResume();
//        setSearchHistory();
//        isLastPage = false;
//        offset = 0;
//        loadWhatsOn(false);
        imgWhatsOn = (ImageButton) findViewById(R.id.imgCentertMenu);
        imgWhatsOn.setImageResource(R.drawable.ic_whats_on_dark);
    }
}

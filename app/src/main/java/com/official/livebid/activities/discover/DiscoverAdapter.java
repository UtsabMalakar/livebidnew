package com.official.livebid.activities.discover;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SlideShowViewPager;
import com.official.livebid.listeners.FavouriteCompleteListener;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.PropertyLists;
import com.official.livebid.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Robz on 1/6/2016.
 */
public class
DiscoverAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    public static ArrayList<PropertyListInfo> alPropertyListInfo;
    int AUCTION_TRENDING = 0, PROPERTY = 1;


    public DiscoverAdapter(Context context, ArrayList<PropertyListInfo> alPropertyListInfo) {
        this.context = context;
        this.alPropertyListInfo = alPropertyListInfo;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == AUCTION_TRENDING) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.auctions_trending_list_item, parent, false);
            return new AuctionsTrendingViewHolder(view);
        } else if (viewType == PROPERTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_list_item, parent, false);
            return new PropertyViewHolder(view);
        } else {
            throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof PropertyViewHolder) {
            initProperties((PropertyViewHolder) holder, position);
            ((PropertyViewHolder) holder).rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Opener.PropertyDetails((Activity) context, alPropertyListInfo.get(position));
                }
            });
        }
    }


    private void initProperties(PropertyViewHolder holder, final int position) {
        final int finalPos = position;
        final PropertyListInfo propertyListInfo = alPropertyListInfo.get(finalPos);
        final PropertyViewHolder propertyViewHolder = holder;
        propertyViewHolder.tvPrice.setText(propertyListInfo.price);
        String id = propertyListInfo.id;

        if (propertyListInfo.propertyStatus == 0) {
            if ( propertyListInfo.propertyStatus2 == 4) {
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                propertyViewHolder.tvDateTag.setVisibility(View.GONE);

            } else {
                if (propertyListInfo.propertyStatus2 != 0)
                propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvDateTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                propertyViewHolder.tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
            }

        } else {
            propertyViewHolder.tvDateTag.setVisibility(View.GONE);
            propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
            propertyViewHolder.tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
        }
        propertyViewHolder.tvAgentFName.setText(propertyListInfo.agentFName);
        propertyViewHolder.tvAgentLName.setText(propertyListInfo.agentLName);

        propertyViewHolder.cboxFav.setChecked(propertyListInfo.isFav());
        propertyViewHolder.tvPropertyStreet.setText(propertyListInfo.street);
        propertyViewHolder.tvPropertySuburbPostcode.setText(propertyListInfo.suburb + " "+ propertyListInfo.propertyState+ " " + propertyListInfo.postCode);
        if (!propertyListInfo.agentAgencyLogoUrl.trim().isEmpty())
            Picasso.with(context)
                    .load(propertyListInfo.agentAgencyLogoUrl)
                    .resize((int)CommonMethods.pxFromDp(context,96),(int)CommonMethods.pxFromDp(context,60))
                    .into(propertyViewHolder.ivAgentAgencyIcon);
        else {
            Picasso.with(context)
                    .load(R.drawable.black_livebid_logo)
                    .into(propertyViewHolder.ivAgentAgencyIcon);
        }
        if (!propertyListInfo.agentProfileImg.trim().isEmpty()) {
            Picasso.with(context)
                    .load(propertyListInfo.agentProfileImg)
                    .placeholder(R.drawable.profile_icon)
                    .resize((int)CommonMethods.pxFromDp(context,96),(int)CommonMethods.pxFromDp(context,96))
                    .centerCrop()
                    .transform(new CircleTransform((Activity) context))
                    .into(propertyViewHolder.ivAgentImg);
        } else {
            Picasso.with(context)
                    .load(R.drawable.profile_icon)
//                    .transform(new CircleTransform((Activity) context))
                    .into(propertyViewHolder.ivAgentImg);
        }

        propertyViewHolder.tvPropertyImageIndicator.setText(1 + "/" + propertyListInfo.property_images_more.size());
        ImageSlideAdapter mImageSlideAdapter;
        mImageSlideAdapter = new ImageSlideAdapter(context, propertyListInfo.property_images_more,propertyListInfo.propertyStatus);
        propertyViewHolder.mViewPager.setAdapter(mImageSlideAdapter);
        propertyViewHolder.mViewPager.setOnViewPagerClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Opener.PropertyDetails((Activity) context, alPropertyListInfo.get(position));

            }
        });

            if (propertyListInfo.propertyStatus == 2) {
                propertyViewHolder.tvStatusTag.setVisibility(View.INVISIBLE);
                propertyViewHolder.ivSoldPropertyTag.setVisibility(View.VISIBLE);
                propertyViewHolder.tvPropertyImageIndicator.setVisibility(View.GONE);
//                propertyViewHolder.mViewPager.beginFakeDrag();

            }else {
                propertyViewHolder.ivSoldPropertyTag.setVisibility(View.GONE);
                if (propertyListInfo.propertyStatus==5){
                    propertyViewHolder.tvStatusTag.setVisibility(View.INVISIBLE);
                }
                else {
                    propertyViewHolder.tvStatusTag.setVisibility(View.VISIBLE);
                }
                propertyViewHolder.tvPropertyImageIndicator.setVisibility(View.GONE);
                propertyViewHolder.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        propertyViewHolder.tvPropertyImageIndicator.setText(position+1 + "/" + propertyListInfo.property_images_more.size());
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });

            }


        propertyViewHolder.tvNosBathroom.setText(propertyListInfo.nosOfBathroom);
        propertyViewHolder.tvNosBedroom.setText(propertyListInfo.nosOfBedroom);
        propertyViewHolder.tvNosParking.setText(propertyListInfo.nosOfParking);
        propertyViewHolder.tvPriceText.setVisibility(View.VISIBLE);
        propertyViewHolder.tvPriceText.setText(propertyListInfo.price_text);

        propertyViewHolder.cboxFav.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final CheckBox cboxTemp = (CheckBox) view;
                        new FavoriteHelper(context).makeFavorite(alPropertyListInfo.get(finalPos).id, alPropertyListInfo.get(finalPos).getFav(), new FavouriteCompleteListener() {
                            @Override
                            public void onSuccess(boolean b) {
                                alPropertyListInfo.get(finalPos).setFav(b);
                                cboxTemp.setChecked(b);
                            }

                            @Override
                            public void onFailure() {
                                cboxTemp.setChecked(alPropertyListInfo.get(finalPos).getFav());
                            }

                            @Override
                            public void onNotLoggedIn() {
                                cboxTemp.setChecked(false);
                                new Alerts((Activity) context).signIn();
                            }
                        });
                    }
                });

    }

    @Override
    public int getItemCount() {

        return alPropertyListInfo.size();
    }

    @Override
    public int getItemViewType(int position) {
        return PROPERTY;
    }


    public class PropertyViewHolder extends RecyclerView.ViewHolder{

        private final TextView tvPriceText;
        ImageView ivPropertyImg, ivAgentImg, ivAgentAgencyIcon, ivSoldPropertyTag;
        CheckBox cboxFav;
        SlideShowViewPager mViewPager;
        TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
                tvPropertyStatusIdentifier, tvPrice, tvNosBedroom, tvNosBathroom,
                tvNosParking, tvAgentFName, tvAgentLName;

        final TextView tvPropertyImageIndicator;
        View rootView;

        public PropertyViewHolder(View view) {
            super(view);
            rootView = view;
            ivPropertyImg = (ImageView) view.findViewById(R.id.iv_property_img);
            ivSoldPropertyTag = (ImageView) view.findViewById(R.id.iv_property_sold_tag);
            ivAgentImg = (ImageView) view.findViewById(R.id.iv_agent_img);
            ivAgentAgencyIcon = (ImageView) view.findViewById(R.id.iv_agent_agency_icon);
            cboxFav = (CheckBox) view.findViewById(R.id.cbox_fav);

            tvStatusTag = (TextView) view.findViewById(R.id.tv_property_status_tag);
            tvDateTag = (TextView) view.findViewById(R.id.tv_property_date_tag);
            tvPriceText = (TextView) view.findViewById(R.id.tvPriceText);
            tvPropertySuburbPostcode = (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
            tvPropertyStreet = (TextView) view.findViewById(R.id.tv_property_street);
            tvPropertyStatusIdentifier = (TextView) view.findViewById(R.id.tv_property_status_identifier);
            tvAgentFName = (TextView) view.findViewById(R.id.tv_agent_fname);
            tvAgentLName = (TextView) view.findViewById(R.id.tv_agent_lname);
            tvPrice = (TextView) view.findViewById(R.id.tv_property_price);
            tvNosBedroom = (TextView) view.findViewById(R.id.tv_nosBedroom);
            tvNosBathroom = (TextView) view.findViewById(R.id.tv_nosBathroom);
            tvNosParking = (TextView) view.findViewById(R.id.tv_nosParking);
            tvPropertyImageIndicator = (TextView) view.findViewById(R.id.tvPropertyImageIndicator);
            com.official.livebid.enums.AppMode appMode = CommonMethods.getAppMode(context);
            if (appMode == AppMode.guest){
                cboxFav.setVisibility(View.GONE);
            }

            mViewPager = (SlideShowViewPager) view.findViewById(R.id.vp_property_img);

        }
    }


    public class AuctionsTrendingViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        Button btnBrowse;
        ImageView ivBgImage;

        public AuctionsTrendingViewHolder(View view) {
            super(view);

            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            btnBrowse = (Button) view.findViewById(R.id.btn_browse);
            ivBgImage = (ImageView) view.findViewById(R.id.iv_bgImg);
        }

    }

    public void addMoreData(ArrayList<PropertyListInfo> data) {
        int startPosition = alPropertyListInfo.size();
        this.alPropertyListInfo.addAll(data);
        PropertyLists.alPropertyListInfo = alPropertyListInfo;
        this.notifyItemRangeInserted(startPosition, data.size());
    }
}

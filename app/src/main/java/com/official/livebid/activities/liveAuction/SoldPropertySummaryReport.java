package com.official.livebid.activities.liveAuction;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SoldPropertySummaryReport extends AppCompatActivity implements AsyncInterface, BidHistoryAdapter.IBidHistory, View.OnClickListener {
    private RecyclerView rvSoldPropertySummary;
    private LinearLayoutManager linearLayoutManager;
    private Alerts alerts;
    private SharedPreference prefs;
    private SoldPropertyModel soldProperty;
    private BidHistoryAdapter bidHistoryAdapter;
    private ArrayList<BidModel> bids;
    private boolean isLastPage = false;
    private int offset = 0;
    private AppMode appMode;
    private Button btnButtom,btnSendReport;
    private LinearLayout toolbar;
    private ImageButton ibtnBack;
    private boolean isToolbarVisible;
    private String mobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sold_property_summary_report);
        getSupportActionBar().hide();
        new CustomActionBar.SoldPropertySummaryCAB(this);
        ibtnBack = (ImageButton) findViewById(R.id.ibtn_left);
        toolbar = (LinearLayout) findViewById(R.id.ll_cab);
        prefs = new SharedPreference(this);
        appMode = CommonMethods.getAppMode(this);
        btnButtom = (Button) findViewById(R.id.btn_bottom);
        btnSendReport = (Button) findViewById(R.id.btn_send_report);
        btnSendReport.setOnClickListener(this);
        btnButtom.setOnClickListener(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            soldProperty = (SoldPropertyModel) b.getSerializable(CommonDef.SOLD_PROPERTY);
        }
        if (appMode == AppMode.agent) {
            btnButtom.setTag(0);
            btnButtom.setText("Contact Purchaser");
            String userId = CommonMethods.getLoggedInUserId(this);
            if (soldProperty.userId.equalsIgnoreCase(CommonMethods.getLoggedInUserId(this))) {
                btnButtom.setEnabled(true);
                btnSendReport.setEnabled(true);
            }
            else {
                btnButtom.setEnabled(true);
                btnSendReport.setVisibility(View.VISIBLE);
                btnSendReport.setEnabled(true);
            }
        } else {
            btnButtom.setTag(1);
            btnButtom.setText("Contact Agent");
            if (soldProperty.isWon()) {
                btnButtom.setEnabled(true);
            } else {
                btnButtom.setEnabled(false);
            }
            btnSendReport.setVisibility(View.GONE);
        }
        linearLayoutManager = new LinearLayoutManager(this);
        alerts = new Alerts(this);
        rvSoldPropertySummary = (RecyclerView) findViewById(R.id.rv_sold_property_summary);
        rvSoldPropertySummary.setLayoutManager(linearLayoutManager);

        bidHistoryAdapter = new BidHistoryAdapter(this, soldProperty, new ArrayList<BidModel>(), this, BidHistoryAdapter.SummaryReportType.sold, isLastPage);
        rvSoldPropertySummary.setAdapter(bidHistoryAdapter);
        getSoldPropertySummaryReport();


    }



    private void getSoldPropertySummaryReport() {
        HashMap<String, String> headers = CommonMethods.getHeaders(SoldPropertySummaryReport.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", soldProperty.propertyId);
        params.put("offset", offset + "");
        System.out.println("Bid History=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(SoldPropertySummaryReport.this, Request.Method.POST, params, UrlHelper.GET_BID_HISTORY, headers, false, CommonDef.RC_GET_BID_HISTORY);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sold_property_summary_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewMore() {
        if (!isLastPage)
            getSoldPropertySummaryReport();
        else
            bidHistoryAdapter.stopLoading();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);

    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("bid history:" + result);
        switch (requestCode) {
            case CommonDef.RC_GET_BID_HISTORY:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONArray jaList = joResult.getJSONArray("bid_history");
                        if (jaList.length() > 0) {
                            BidModel bm;
                            JSONObject joBid;
                            bids = new ArrayList<>();
                            for (int i = 0; i < jaList.length(); i++) {
                                joBid = jaList.getJSONObject(i);
                                bm = new BidModel();
                                bm.bidId = i + "";
                                bm.bidAmount = bm.getBidAmount(joBid.getString("bid_amount"));
                                bm.bidderId = bm.getBidderId(joBid.getString("unique_id"));
                                bm.setBidType(joBid.getInt("bid_type"));
                                bm.setMyBid(joBid.getString("check_myself"));
                                bm.status = joBid.getString("status");
                                if (bm.bidType == CurrentBidModel.MessageType.onSiteBid || bm.bidType == CurrentBidModel.MessageType.vendorBid || bm.bidType == CurrentBidModel.MessageType.generalBid)
                                    bids.add(bm);
                            }
                            isLastPage = joResult.getBoolean("last_page");
                            offset = joResult.getInt("offset");
                            if(bidHistoryAdapter==null){
                                bidHistoryAdapter = new BidHistoryAdapter(this, soldProperty, bids, this, BidHistoryAdapter.SummaryReportType.passed_in,isLastPage);
                                rvSoldPropertySummary.setAdapter(bidHistoryAdapter);
                            }
                            else {
                                bidHistoryAdapter.addMoreData(bids,isLastPage);
                            }


                        }
                        else {
                            bidHistoryAdapter = new BidHistoryAdapter(this, soldProperty, new ArrayList<BidModel>(), this, BidHistoryAdapter.SummaryReportType.passed_in,isLastPage);
                            rvSoldPropertySummary.setAdapter(bidHistoryAdapter);
                        }
                    } else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CommonDef.REQ_PROPERTY_BID_DLT:
                try {
                    JSONObject joResult = new JSONObject(result);
                    String code, msg;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    alerts.showOkMessage(msg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_bottom:
                int action = (int) view.getTag();
                switch (action) {
                    case 0://contact putchaser
                        showContactBottomSheet("9849150750", "shiv.anga@yahoo.com");

                        break;
                    case 1:// contact agent
                        Opener.contactAgent(SoldPropertySummaryReport.this, soldProperty.agentId);
                        break;
                }
                break;
            case R.id.btn_send_report:
                HashMap<String, String> headers = CommonMethods.getHeaders(this);
                HashMap<String, String> params = new HashMap<>();
                params.put("property_id", soldProperty.propertyId);

                System.out.println("registered bidders params=>" + params.toString());
                VolleyRequest volleyRequest = new VolleyRequest(SoldPropertySummaryReport.this, Request.Method.POST, params, UrlHelper.GET_PROPERTY_BID_DTL, headers, false, CommonDef.REQ_PROPERTY_BID_DLT);
                volleyRequest.asyncInterface = this;
                volleyRequest.request();
                break;

        }

    }

    public void showContactBottomSheet(final String mobileNo, final String emailId) {

        View view = getLayoutInflater().inflate(R.layout.contact_agent_bottom_sheet, null);
        Button btnEmail = (Button) view.findViewById(R.id.btn_email);
        btnEmail.setText("Email");
        Button bntCall = (Button) view.findViewById(R.id.btn_call);
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);

        final Dialog mBottomSheetDialog = new Dialog(SoldPropertySummaryReport.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        bntCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                String mobile = mobileNo;
                if (mobile != null && !mobile.isEmpty())
                    permissionCheckCall(mobile);
            }
        });
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                CommonMethods.sendEmail(SoldPropertySummaryReport.this, emailId);
            }
        });


    }

    private void permissionCheckCall(String offer) {
        mobile = offer;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && this.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.CALL_PHONE},
                    1300);

            return;
        } else {
            CommonMethods.makeCall(SoldPropertySummaryReport.this, offer);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    CommonMethods.makeCall(SoldPropertySummaryReport.this, mobile);
                break;

        }
    }
}

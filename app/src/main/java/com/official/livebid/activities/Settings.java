package com.official.livebid.activities;

import android.support.annotation.BoolRes;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.AppMode;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.LiveBidSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Settings extends AppCompatActivity implements View.OnClickListener, AsyncInterface {
    private LinearLayout settingLoremIpsum;
    private CustomActionBar.TitleBarWithBackAndTitle settingsTitleBar;
    private Switch tbPushNotification, tbSurroundings;
    private boolean isChecked;
    AppMode appMode;
    SharedPreference prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().hide();
        appMode = CommonMethods.getAppMode(this);
        prefs = new SharedPreference(this);
        settingsTitleBar = new CustomActionBar.TitleBarWithBackAndTitle(this, "Settings");
//        settingLoremIpsum = (LinearLayout) findViewById(R.id.ll_lorem_ipsum);
        tbPushNotification = (Switch) findViewById(R.id.tb_pushnotifivation);
        tbSurroundings = (Switch) findViewById(R.id.tb_surroundings);
        setAppMode(appMode);
        tbPushNotification.setOnClickListener(this);
        tbSurroundings.setOnClickListener(this);
//        settingLoremIpsum.setClickable(true);
//        settingLoremIpsum.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (view.getId() == R.id.ll_lorem_ipsum) {
//                    Opener.SettingsLoremIpsum(Settings.this);
//                }
//            }
//        });

    }

    private void setAppMode(AppMode appMode) {
        if (appMode == AppMode.user) {
            tbPushNotification.setEnabled(true);
            tbPushNotification.setChecked(prefs.getIntValues(CommonDef.SharedPrefKeys.PUSH_NOTIFICATION) == 1 ? true : false);
            tbPushNotification.setTag(prefs.getIntValues(CommonDef.SharedPrefKeys.PUSH_NOTIFICATION));
            tbSurroundings.setEnabled(true);
            tbSurroundings.setChecked(prefs.getIntValues(CommonDef.SharedPrefKeys.SHOW_SURROUNDINGS) == 1 ? true : false);
            tbSurroundings.setTag(prefs.getIntValues(CommonDef.SharedPrefKeys.SHOW_SURROUNDINGS));
        } else if (appMode == AppMode.agent) {
            tbPushNotification.setEnabled(true);
            tbPushNotification.setChecked(prefs.getIntValues(CommonDef.SharedPrefKeys.PUSH_NOTIFICATION) == 1 ? true : false);
            tbPushNotification.setTag(prefs.getIntValues(CommonDef.SharedPrefKeys.PUSH_NOTIFICATION));
            tbSurroundings.setEnabled(false);
            tbSurroundings.setChecked(false);
        } else {
            //guest notification
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_pushnotifivation:
                updateSettings(1);//
                break;
            case R.id.tb_surroundings:
                updateSettings(2);
                break;
        }
    }

    public void updateSettings(int settingsId) {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("setting_id", settingsId + "");
        VolleyRequest volleyRequest = new VolleyRequest(this, Request.Method.POST, params, UrlHelper.UPDATE_SETTINGS, headers, false, CommonDef.RC_UPDATE_SETTINGS + settingsId);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void processFinish(String result, int requestCode) {
        System.out.println("Notification listings =>" + result);
        switch (requestCode - CommonDef.RC_UPDATE_SETTINGS) {
            case 1://PUSH NOTIFICATION

                try {
                    JSONObject jObjResult = new JSONObject(result);
                    String msg, code;
                    code = jObjResult.getString("code");
                    msg = jObjResult.getString("msg");

                    if (code.equalsIgnoreCase("0001")) {
                        togglePushNotification();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    setAppMode(appMode);
                }
                break;
            case 2://show Surroundings

                try {
                    JSONObject jObjResult = new JSONObject(result);
                    String msg, code;
                    code = jObjResult.getString("code");
                    msg = jObjResult.getString("msg");

                    if (code.equalsIgnoreCase("0001")) {
                        toggleShowSurroundigs();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    setAppMode(appMode);
                }
                break;
        }


    }

    private void togglePushNotification() {
        int status = prefs.getIntValues(CommonDef.SharedPrefKeys.PUSH_NOTIFICATION);
        if (status == 1) {
            prefs.setKeyValues(CommonDef.SharedPrefKeys.PUSH_NOTIFICATION, 0);
        } else {
            prefs.setKeyValues(CommonDef.SharedPrefKeys.PUSH_NOTIFICATION, 1);
        }
    }

    private void toggleShowSurroundigs() {
        int status = prefs.getIntValues(CommonDef.SharedPrefKeys.SHOW_SURROUNDINGS);
        if (status == 1) {
            prefs.setKeyValues(CommonDef.SharedPrefKeys.SHOW_SURROUNDINGS, 0);
        } else {
            prefs.setKeyValues(CommonDef.SharedPrefKeys.SHOW_SURROUNDINGS, 1);
        }
    }
}

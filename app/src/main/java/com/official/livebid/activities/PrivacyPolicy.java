package com.official.livebid.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;


public class PrivacyPolicy extends ActionBarActivity implements AsyncInterface {

    WebView wv;
    private com.official.livebid.alerts.Alerts alerts;
    private CustomActionBar.TitleBarWithBackAndTitle privacyPolicyTitleBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        getSupportActionBar().hide();
        init();
        VolleyRequest volleyRequest = new VolleyRequest(PrivacyPolicy.this, Request.Method.GET, UrlHelper.GET_PRIVACY_POLICY, CommonDef.REQUEST_PRIVACY_POLICY);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }

    private void init() {
        privacyPolicyTitleBar=new CustomActionBar.TitleBarWithBackAndTitle(this,"Privacy Policy");
        wv = (WebView) findViewById(R.id.wv_privacy_policy);
        alerts = new Alerts(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_terms_of_services, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        if (requestCode == CommonDef.REQUEST_PRIVACY_POLICY) {
            System.out.println("Privacy policy=>" + result);
            wv.loadData(result, "text/html", null);

        }
    }
}

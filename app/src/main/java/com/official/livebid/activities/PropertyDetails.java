package com.official.livebid.activities;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.official.livebid.R;
import com.official.livebid.Views.CustomDateTimePicker;
import com.official.livebid.Views.EndlessListView;
import com.official.livebid.activities.liveAuction.PropertyAuctionModel;
import com.official.livebid.activities.liveAuction.SoldPropertyModel;
import com.official.livebid.adapters.AgentProfilePropertiesListingsAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.enums.AppMode;
import com.official.livebid.enums.OfferType;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.StringHelper;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.listeners.FavouriteCompleteListener;
import com.official.livebid.objects.AgencyInfo;
import com.official.livebid.objects.AgentInfo;
import com.official.livebid.objects.AvailabilityModel;
import com.official.livebid.objects.PropertyInfo;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.objects.RoomDetail;
import com.official.livebid.utils.CircleTransform;
import com.official.livebid.utils.GPSTracker;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class
PropertyDetails extends AppCompatActivity implements OnMapReadyCallback, AsyncInterface, EndlessListView.EndlessListener, View.OnClickListener {

    public static PropertyListInfo propertyListInfo;
    public static boolean needToUpdate = false;
    public static String propertyId;
    private static ArrayList<AgentInfo> alAgentInfo;
    private static AgencyInfo agencyInfo;
    int id;
    ImageView ivPropertyImg, ivAgentAgencyIcon; //ivAgentImg,
    TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
            tvPropertyStatusIdentifier, tvPrice, tvNosBedroom, tvNosBathroom,
            tvNosParking, tvAgentFName, tvAgentLName, tvPropertyTitle, tvPropertyDesc, tvNoOhi;
    LinearLayout llOhiContainer, llOhiWrapper;
    EndlessListView lvPropertyDetails;
    Button btnBottom;
    TextView tvAuctionStatus;
    Button btnMakeAnOffer;
    double lat, lng;
    GPSTracker gps;
    int offset = 0;
    boolean isLastPage = false;
    CustomDateTimePicker custom;
    private boolean isLocationSet = false;
    private ArrayList<PropertyInfo> properties;
    private AgentProfilePropertiesListingsAdapter propertiesListingsAdapter;
    private LinearLayout llSalesAgentContainer;
    private AgentInfo salesAgent;
    private com.official.livebid.helpers.SharedPreference prefs;
    private CheckBox chkFav;
    private ImageButton ibtnBack;
    private SoldPropertyModel soldProp;
    private LinearLayout toolbar;
    private AppMode appMode;
    private RelativeLayout llPropertyStatus;
    private ViewPager mViewPager;
    private TextView tvPropertyImageIndicator;
    private OfferType offerType;
    private String settlementDate = "";
    private Alerts alerts;
    private long longPriveValue = 0;
    private TextView tvPriceText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            propertyListInfo = (PropertyListInfo) bundle.getSerializable(CommonDef.PROPERTY_LIST_INFO);
            propertyId = propertyListInfo.id;

            if (propertyListInfo.propertyStatus == 2) {
                offerType = OfferType.POST_OFFER;
            } else {
                offerType = OfferType.PRE_OFFER;
            }
        }

        setContentView(R.layout.activity_property_details);
        View header = getLayoutInflater().inflate(R.layout.property_details_header, null, false);
        alerts = new Alerts(this);
        gps = new GPSTracker(this);
        prefs = new SharedPreference(this);
        appMode = CommonMethods.getAppMode(this);
        initHeader(header);
        if (propertyListInfo != null) {
            setElements();

            if (prefs != null && prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).equals("1"))
                setGuestMode();
            else if (prefs != null && prefs.getStringValues(CommonDef.SharedPrefKeys.USER_TYPE).equals("2"))
                setUserMode();
            else
                setGuestMode();

            updateRegStatus();


        }
        toolbar = (LinearLayout) findViewById(R.id.ll_cab);
//        toolbar.setVisibility(View.GONE);
        lvPropertyDetails = (EndlessListView) findViewById(R.id.lv_property_listings);

        lvPropertyDetails.addHeaderView(header);
        getPropertyList();
        getContactAgentInfo(propertyListInfo.id);
    }

    /**
     * method to update if property is registered to auction ;
     * <p/>
     * property status:
     * 0 manual calulate
     * 1 live now
     * 2 sold
     * 3 passed in
     * 4 on the market
     * <p/>
     * property status 2
     * 0 none
     * 1 new
     * 2 trending
     * 3 registered
     * 4 auction due start
     */

// update button action based on property status
    private void updateRegStatus() {
        if (propertyListInfo.propertyStatus == 2) { //if property sold hide property status
            llPropertyStatus.setVisibility(View.GONE);
            llOhiWrapper.setVisibility(View.GONE);
        } else {
            llPropertyStatus.setVisibility(View.VISIBLE);
            llOhiWrapper.setVisibility(View.VISIBLE);
        }
        if (propertyListInfo.propertyStatus2 == 5) {
            chkFav.setVisibility(View.INVISIBLE);
            btnBottom.setVisibility(View.GONE);
        }


        if (appMode == AppMode.guest) {
            chkFav.setVisibility(View.INVISIBLE);
//            btnBottom.setVisibility(View.GONE);
            btnMakeAnOffer.setVisibility(View.GONE);
            llPropertyStatus.setVisibility(View.GONE);
            if (propertyListInfo.propertyStatus == 2 || propertyListInfo.propertyStatus == 3) {
                btnBottom.setText("Vew Summary Report");
                btnBottom.setTag(14);
            } else if (propertyListInfo.propertyStatus == 5) {
                tvStatusTag.setVisibility(View.INVISIBLE);

            } else {
                btnBottom.setText("Auction Registration");
                btnBottom.setTag(14);
                btnBottom.setEnabled(true);
            }
            return;
        }
        if (appMode == AppMode.user) {
            if (CommonDef.canPlaceBid) {
                if (propertyListInfo.propertyStatus == 0) {
//                    btnBottom.setVisibility(View.GONE);
//                    btnMakeAnOffer.setVisibility(View.GONE);
                    if (propertyListInfo.registeredForProperty.equalsIgnoreCase("0")) {
                        btnBottom.setText("Auction Registration");
                        tvAuctionStatus.setText(getString(R.string.registration_open));
                        btnBottom.setTag(0);

//                else if (propertyListInfo.registeredForProperty.equalsIgnoreCase("2")) {
//                    btnBottom.setText("Cancel Registration");
//                    tvAuctionStatus.setText(getString(R.string.application_pending));
//                    btnBottom.setTag(2);

                    } else if (propertyListInfo.registeredForProperty.equalsIgnoreCase("1")) {
                        if (prefs.getStringValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS).equalsIgnoreCase("1")) {
                            btnBottom.setText("Cancel Registration");
                            tvAuctionStatus.setText(getString(R.string.application_registered));
                            btnBottom.setTag(2);
//                        btnBottom.setVisibility(View.GONE);
                        } else {
                            btnBottom.setText("Cancel Registration");
                            tvAuctionStatus.setText(getString(R.string.application_pending));
                            btnBottom.setTag(2);
//                        btnBottom.setVisibility(View.GONE);
                        }
                    }
//                } else if (propertyListInfo.propertyStatus == 1 || propertyListInfo.propertyStatus == 4) {
//                    btnMakeAnOffer.setVisibility(View.GONE);
//                    btnBottom.setVisibility(View.VISIBLE);
//                    btnBottom.setEnabled(true);
//                    if (propertyListInfo.registeredForProperty.equalsIgnoreCase("0")) {
//                        btnBottom.setText("Auction Registration");
//                        tvAuctionStatus.setText(getString(R.string.registration_open));
//                        btnBottom.setTag(0);
//
////                } else if (propertyListInfo.registeredForProperty.equalsIgnoreCase("2")) {
////                    btnMakeAnOffer.setVisibility(View.GONE);
////                    btnBottom.setText("Cancel Registration");
////                    tvAuctionStatus.setText(getString(R.string.application_pending));
////                    btnBottom.setTag(2);
////
//                    } else if (propertyListInfo.registeredForProperty.equalsIgnoreCase("1")) {
//                        if (prefs.getStringValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS).equalsIgnoreCase("1")) {
//                            btnBottom.setText("Join Auction");
//                            tvAuctionStatus.setText(getString(R.string.application_registered));
//                            btnBottom.setTag(13);
//                        } else {
////                        btnMakeAnOffer.setVisibility(View.GONE);
//                            btnBottom.setText("Cancel Registration");
//                            tvAuctionStatus.setText(getString(R.string.application_pending));
//                            btnBottom.setTag(2);
////                        btnBottom.setVisibility(View.GONE);
//                        }
//                    } else {
////                    btnMakeAnOffer.setVisibility(View.GONE);
//                        btnBottom.setText("Cancel Registration");
//                        tvAuctionStatus.setText(getString(R.string.application_pending));
//                        btnBottom.setTag(2);
////                    btnBottom.setVisibility(View.GONE);
//                    }
////                btnBottom.setText("Join Auction");
////                tvAuctionStatus.setText(getString(R.string.application_registered));
////                btnBottom.setTag(13);
                }else if (propertyListInfo.propertyStatus == 1 || propertyListInfo.propertyStatus == 4) {
                        btnMakeAnOffer.setVisibility(View.GONE);
                        btnBottom.setVisibility(View.VISIBLE);
                        btnBottom.setEnabled(true);
                        btnBottom.setText("View Property");
                        tvAuctionStatus.setText(getString(R.string.registration_open));
                        btnBottom.setTag(13);


//
                } else if (propertyListInfo.propertyStatus == 2) {
                    btnBottom.setEnabled(true);
                    btnMakeAnOffer.setVisibility(View.GONE);
                    tvAuctionStatus.setText("Property Sold");
                    btnBottom.setText("View Summary Report");
                    btnBottom.setTag(12);
                } else if (propertyListInfo.propertyStatus == 3) {
                    llPropertyStatus.setVisibility(View.GONE);
                    llOhiWrapper.setVisibility(View.GONE);
                    btnBottom.setEnabled(true);
//                btnMakeAnOffer.setVisibility(View.VISIBLE);
                    btnBottom.setText("View Summary Report");
                    btnBottom.setTag(11);
                }
                if (propertyListInfo.propertyStatus == 5) {
                    llPropertyStatus.setVisibility(View.GONE);
                    llOhiWrapper.setVisibility(View.GONE);
                    btnBottom.setVisibility(View.GONE);
//                btnBottom.setEnabled(true);
                    btnMakeAnOffer.setVisibility(View.GONE);
//                btnBottom.setText("View Summary Report");
//                btnBottom.setTag(11);
                }
                return;
            } else {
                if (propertyListInfo.propertyStatus == 1 || propertyListInfo.propertyStatus == 4) {
                    btnMakeAnOffer.setVisibility(View.GONE);
                    btnBottom.setVisibility(View.VISIBLE);
                    btnBottom.setEnabled(true);
                    btnBottom.setText("View Property");
                    tvAuctionStatus.setText(getString(R.string.registration_open));
                    btnBottom.setTag(13);

                } else if (propertyListInfo.propertyStatus == 3 || propertyListInfo.propertyStatus == 2) {
                    llPropertyStatus.setVisibility(View.GONE);
                    llOhiWrapper.setVisibility(View.GONE);
                    btnBottom.setEnabled(true);
//                    btnMakeAnOffer.setVisibility(View.VISIBLE);
                    btnBottom.setText("View Summary Report");
                    btnBottom.setTag(11);
                } else if (propertyListInfo.propertyStatus == 0) {
                    if (propertyListInfo.propertyStatus2 == 4 || propertyListInfo.propertyStatus2 == 0) {
                        llPropertyStatus.setVisibility(View.VISIBLE);
                        btnBottom.setVisibility(View.GONE);
                        btnMakeAnOffer.setVisibility(View.VISIBLE);
                    }

                } else {
                    btnBottom.setVisibility(View.GONE);
                    btnMakeAnOffer.setVisibility(View.GONE);
                }
            }
        }

        if (appMode == AppMode.agent)

        {
            if (propertyListInfo.propertyStatus == 0) {
                if (propertyListInfo.propertyStatus2 == 4 && prefs.getStringValues(CommonDef.SharedPrefKeys.AGENT_ID).equalsIgnoreCase(propertyListInfo.agentId)) {
                    btnBottom.setText("Start Auction");
                    btnBottom.setTag(15);
                    btnBottom.setEnabled(true);
                } else {
                    btnBottom.setText("Start Auction");
                    btnBottom.setTag(15);
                    btnBottom.setEnabled(false);// for test purpose btn is enabled
                }
            } else if (propertyListInfo.propertyStatus == 1 || propertyListInfo.propertyStatus == 4) {
                if (prefs.getStringValues(CommonDef.SharedPrefKeys.AGENT_ID).equalsIgnoreCase(propertyListInfo.agentId)) {
                    btnBottom.setText("Continue To Auction");
                    btnBottom.setTag(16);
                    btnBottom.setTag(13);//join room just like user
                    btnBottom.setEnabled(true);
//                    btnBottom.setVisibility(View.GONE);
                } else {
                    btnBottom.setText("Start Auction");
                    btnBottom.setTag(16);
//                    btnBottom.setTag(13);
                    btnBottom.setEnabled(false);// for test purpose btn is enabled
//                    btnBottom.setVisibility(View.GONE);
                }
                // live now and on the market
            } else if (propertyListInfo.propertyStatus == 2) {
                btnBottom.setText("View Summary Report");
                btnBottom.setTag(12);
            } else if (propertyListInfo.propertyStatus == 3) {
                llPropertyStatus.setVisibility(View.GONE);
                llOhiWrapper.setVisibility(View.GONE);

                btnBottom.setText("View Summary Report");
                btnBottom.setTag(11);
            }
            if (propertyListInfo.propertyStatus == 5) {
                btnBottom.setVisibility(View.GONE);
                llPropertyStatus.setVisibility(View.GONE);
            }

            return;
        }

    }

    private void getContactAgentInfo(String id) {
        HashMap<String, String> headers = CommonMethods.getHeaders(this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", id);
        VolleyRequest volleyRequest = new VolleyRequest(PropertyDetails.this, Request.Method.POST, params, UrlHelper.GET_PROPERTY_AGENTS, headers, false, CommonDef.REQUEST_PROPERTY_AGENT_CONTACT);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void initialiseMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setElements() {
        tvPrice.setText(propertyListInfo.price);
        ImageSlideAdapter mImageSlideAdapter;
        mImageSlideAdapter = new ImageSlideAdapter(this, propertyListInfo.property_images_more, propertyListInfo.propertyStatus);
        mViewPager.setAdapter(mImageSlideAdapter);
        if (propertyListInfo.propertyStatus == 0) {
            if (propertyListInfo.propertyStatus2 == 4) {
                if (propertyListInfo.propertyStatus2 != 0)
                    tvStatusTag.setVisibility(View.VISIBLE);
                tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                tvDateTag.setVisibility(View.GONE);

            } else {
                if (propertyListInfo.propertyStatus2 != 0)
                    tvStatusTag.setVisibility(View.VISIBLE);
                tvDateTag.setVisibility(View.VISIBLE);
                tvStatusTag.setText(CommonMethods.getPropertyStatus2(propertyListInfo.propertyStatus2));
                tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(propertyListInfo.auctionDateTimeStamp)));
                if (propertyListInfo.propertyStatus == 0) {
                    tvStatusTag.setVisibility(View.INVISIBLE);
                }
            }

        } else {
            tvDateTag.setVisibility(View.GONE);
            tvStatusTag.setVisibility(View.VISIBLE);
            tvStatusTag.setText(CommonMethods.getPropertyStatus(propertyListInfo.propertyStatus));
        }
        tvPropertyStreet.setText(propertyListInfo.street);
        tvPropertySuburbPostcode.setText(propertyListInfo.suburb + " " + propertyListInfo.propertyState + " " + propertyListInfo.postCode);
//        if (propertyListInfo.agentProfileImg != null && !propertyListInfo.agentProfileImg.isEmpty())
//            Picasso.with(this).load(propertyListInfo.agentProfileImg).transform(new CircleTransform(PropertyDetails.this)).into(ivAgentImg);
        if (propertyListInfo.propertyStatus == 2) {
            tvStatusTag.setVisibility(View.VISIBLE);
            tvPropertyImageIndicator.setVisibility(View.GONE);
//                propertyViewHolder.mViewPager.beginFakeDrag();

        } else {
            if (propertyListInfo.propertyStatus == 5) {
                tvStatusTag.setVisibility(View.GONE);
            } else {
                tvStatusTag.setVisibility(View.VISIBLE);

            }
            tvPropertyImageIndicator.setVisibility(View.GONE);
            tvPropertyImageIndicator.setText(1 + "/" + propertyListInfo.property_images_more.size());
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    tvPropertyImageIndicator.setText(position + 1 + "/" + propertyListInfo.property_images_more.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


        }

        chkFav.setChecked(propertyListInfo.isFav());
        tvNosBathroom.setText(propertyListInfo.nosOfBathroom);
        tvNosBedroom.setText(propertyListInfo.nosOfBedroom);
        tvNosParking.setText(propertyListInfo.nosOfParking);
        tvPropertyTitle.setText(propertyListInfo.propertyTitle);
        tvPriceText.setVisibility(View.VISIBLE);
        tvPriceText.setText(propertyListInfo.price_text);


        if (propertyListInfo.propertyOtherFeatures != null && !propertyListInfo.propertyOtherFeatures.isEmpty())
            tvPropertyDesc.setText(Html.fromHtml(propertyListInfo.propertyDesc + "<br/><br/><b><font color='black'> Other features:</b> </font><br/><br/>" + propertyListInfo.propertyOtherFeatures));
        else
            tvPropertyDesc.setText(Html.fromHtml(propertyListInfo.propertyDesc));
// tvPropertyDesc.setText(propertyListInfo.propertyDesc + "\n\n<b> Other features <>\n\n" + propertyListInfo.propertyOtherFeatures);

        JSONArray jaInspectionTime;
        try {
            jaInspectionTime = new JSONArray(propertyListInfo.inspectionTimeString);
            if (jaInspectionTime != null && jaInspectionTime.length() > 0) {
                llOhiWrapper.setVisibility(View.VISIBLE);
                for (int j = 0; j < jaInspectionTime.length(); j++) {
                    String day, from, to;
                    JSONObject joInspectionTime = jaInspectionTime.getJSONObject(j);
                    day = joInspectionTime.getString("inspection_date");
                    from = joInspectionTime.getString("inspection_time_from");
                    to = joInspectionTime.getString("inspection_time_to");
                    llOhiContainer.addView(addAvailabilityView(day, from, to));
                    if (j < jaInspectionTime.length() - 1) {
                        llOhiContainer.addView(addDivider());
                    }
                }
            } else {
                tvNoOhi.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private View addDivider() {
        FrameLayout v = new FrameLayout(this);
        v.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 1));
        v.setBackgroundColor(getResources().getColor(R.color.form_divider));
        return v;
    }

    private View addAvailabilityView(String day, String from, String to) {
        View view = getLayoutInflater().inflate(R.layout.ohi_item, null, false);
        TextView avDay = (TextView) view.findViewById(R.id.tv_ohi_day);
        TextView avTime = (TextView) view.findViewById(R.id.tv_ohi_time);
        avDay.setText(day);
        avTime.setText(from + " - " + to);
        return view;
    }

    public void initHeader(View view) {
        mViewPager = (ViewPager) view.findViewById(R.id.vp_property_img);
        tvPropertyImageIndicator = (TextView) view.findViewById(R.id.tvPropertyImageIndicator);

        ivAgentAgencyIcon = (ImageView) view.findViewById(R.id.iv_agent_agency_icon);
        llPropertyStatus = (RelativeLayout) view.findViewById(R.id.ll_property_status);
        llOhiContainer = (LinearLayout) view.findViewById(R.id.ll_ohi_container);
        llOhiWrapper = (LinearLayout) view.findViewById(R.id.ll_ohi_wrapper);
        btnBottom = (Button) findViewById(R.id.btn_bottom);
        btnMakeAnOffer = (Button) view.findViewById(R.id.btn_make_an_offer);
        tvAuctionStatus = (TextView) view.findViewById(R.id.tv_reg_status);
        ibtnBack = (ImageButton) findViewById(R.id.ibtn_left);
        CheckBox cv = (CheckBox) view.findViewById(R.id.cbox_fav);
        cv.setVisibility(View.GONE);
        chkFav = (CheckBox) findViewById(R.id.cbox_fav);
        chkFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CheckBox cboxTemp = (CheckBox) view;
                new FavoriteHelper(PropertyDetails.this).makeFavorite(propertyListInfo.id, propertyListInfo.getFav(), new FavouriteCompleteListener() {
                    @Override
                    public void onSuccess(boolean b) {
                        propertyListInfo.setFav(b);
                        cboxTemp.setChecked(b);
                    }

                    @Override
                    public void onFailure() {
                        cboxTemp.setChecked(propertyListInfo.getFav());
                    }

                    @Override
                    public void onNotLoggedIn() {
                        cboxTemp.setChecked(false);
                        alerts.signIn();
                    }
                });

                // Webservice call for change.

            }
        });
        tvStatusTag = (TextView) view.findViewById(R.id.tv_property_status_tag);
        tvDateTag = (TextView) view.findViewById(R.id.tv_property_date_tag);
        tvPriceText = (TextView) view.findViewById(R.id.tvPriceText);
        tvPropertyStreet = (TextView) view.findViewById(R.id.tv_property_street);
        tvPropertySuburbPostcode = (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
        tvPropertyStatusIdentifier = (TextView) view.findViewById(R.id.tv_property_status_identifier);
        tvAgentFName = (TextView) view.findViewById(R.id.tv_agent_fname);
        tvAgentLName = (TextView) view.findViewById(R.id.tv_agent_lname);
        tvPrice = (TextView) view.findViewById(R.id.tv_property_price);
        tvNosBedroom = (TextView) view.findViewById(R.id.tv_nosBedroom);
        tvNosBathroom = (TextView) view.findViewById(R.id.tv_nosBathroom);
        tvNosParking = (TextView) view.findViewById(R.id.tv_nosParking);
        tvPropertyTitle = (TextView) view.findViewById(R.id.tv_property_title);
        tvPropertyDesc = (TextView) view.findViewById(R.id.tv_property_desc);
        llSalesAgentContainer = (LinearLayout) view.findViewById(R.id.ll_agency_sales_team);
        tvNoOhi = (TextView) view.findViewById(R.id.tv_no_ohi);
        btnBottom.setOnClickListener(this);
        btnMakeAnOffer.setOnClickListener(this);
        ibtnBack.setOnClickListener(this);

        initialiseMap();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng location = new LatLng(Double.parseDouble(propertyListInfo.lat), Double.parseDouble(propertyListInfo.lng));
        googleMap.addMarker(new MarkerOptions()
                .position(location)
                .anchor(0.5f, 0.5f)
                .title(propertyListInfo.propertyTitle)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.select_location)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));
    }

    private void getPropertyList() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyDetails.this);
        HashMap<String, String> params = new HashMap<>();
        if (propertyListInfo.agencyId.equalsIgnoreCase("0")) {
            params.put("user_id", propertyListInfo.agentId);
            params.put("listing_type", "1");
        } else {
            params.put("user_id", propertyListInfo.agencyId);
            params.put("listing_type", "2");
        }
        params.put("offset", offset + "");
        params.put("lat", lat + "");
        params.put("lng", lng + "");
        params.put("property_id", propertyListInfo.id);
        System.out.println("peopertyListings params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyDetails.this, Request.Method.POST, params, UrlHelper.GET_PROPERTY_LISTINGS, headers, false, CommonDef.REQUEST_PROPERTY_LISTINGS);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(true);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_PROPERTY_LISTINGS:
                System.out.println("property Listings =>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        JSONArray propertyList = joResult.getJSONArray("list");
                        JSONObject prop = null;
                        PropertyInfo mProp = null;
                        PropertyInfo.OHI mOhi = null;
                        properties = new ArrayList<>();
                        PropertyInfo.Property_images_more moreImage = null;
                        ArrayList<PropertyInfo.OHI> inspectionTimes = null;
                        ArrayList<PropertyInfo.Property_images_more> propertyMoreImages = null;
                        for (int i = 0; i < propertyList.length(); i++) {
                            inspectionTimes = new ArrayList<>();
                            propertyMoreImages = new ArrayList<>();
                            prop = propertyList.getJSONObject(i);
                            mProp = new PropertyInfo();
                            mProp.setId(prop.getString("id"));
                            mProp.setAgent_id(prop.getString("agent_id"));
                            mProp.setAgency_id(prop.getString("agency_id"));
                            mProp.setAuction_date(prop.getString("auction_date"));
                            mProp.setProperty_image(prop.getString("property_image"));
                            mProp.setProperty_title(prop.getString("property_title"));
                            mProp.setProperty_description(prop.getString("property_description"));
                            mProp.setPrice(prop.getString("price"));
//                            mProp.setPrice("$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(prop.getString("price"))));
                            mProp.setStreet(prop.getString("street"));
                            mProp.setPostcode(prop.getString("postcode"));
                            mProp.setSuburb(prop.getString("suburb"));
                            mProp.setState(prop.getString("state"));
                            mProp.setParking(prop.getString("parking"));
                            mProp.setBathrooms(prop.getString("bathrooms"));
                            mProp.setBedrooms(prop.getString("bedrooms"));
                            mProp.setLat(prop.getString("lat"));
                            mProp.setLng(prop.getString("lng"));
                            mProp.setProperty_status(prop.getInt("property_status"));
                            mProp.setProperty_code(prop.getString("property_code"));
                            mProp.setAuthority(prop.getString("authority"));
                            mProp.setAgent_name(prop.getString("agent_name"));
                            mProp.setUnique_number(prop.getString("unique_number"));
                            mProp.setAgent_profile_image(prop.getString("agent_profile_image"));
                            mProp.setAgency_logo_url(prop.getString("agency_logo_url"));
                            mProp.setFavorite(prop.getString("favorite"));
                            mProp.setDistance(prop.getString("distance"));
                            mProp.setRegisteredForProperty(prop.getString("registered_for_property"));
                            JSONArray jOhi = prop.getJSONArray("inspection_times");
                            if (jOhi.length() > 0) {
                                for (int j = 0; j < jOhi.length(); j++) {
                                    mOhi = new PropertyInfo.OHI(
                                            jOhi.getJSONObject(j).getString("inspection_date"),
                                            jOhi.getJSONObject(j).getString("inspection_time_from"),
                                            jOhi.getJSONObject(j).getString("inspection_time_to")
                                    );
                                    inspectionTimes.add(mOhi);
                                }
                            }
                            mProp.setInspectionTimeString(jOhi.toString());
                            JSONArray JPropertyImageMore = prop.getJSONArray("property_images_more");
                            if (JPropertyImageMore.length() > 0) {
                                for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                   /* moreImage = new PropertyInfo.Property_images_more(
                                            JPropertyImageMore.getJSONObject(j).getString("image_url")
                                    );*/

                                    mProp.property_images_more.add(JPropertyImageMore.getString(j));
                                }
                            }
                            mProp.setInspectionTimes(inspectionTimes);
                            properties.add(mProp);
                        }
                        isLastPage = joResult.getBoolean("last_page");
                        if (offset == 0) {
                            propertiesListingsAdapter = new AgentProfilePropertiesListingsAdapter(this, properties);
                            lvPropertyDetails.setAdapter(propertiesListingsAdapter);
                            lvPropertyDetails.setLoadingView(R.layout.layout_endless_loading);
                            lvPropertyDetails.setListener(this);
                        } else {
                            lvPropertyDetails.addNewData(properties);
                        }
                        offset = joResult.getInt("offset");
                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());
                }

                break;
            case CommonDef.REQUEST_PROPERTY_AGENT_CONTACT:
                populateContact(result);
                break;
            case CommonDef.REQUEST_REGISTER_AUCTION:
                System.out.println("auction registration =>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        prefs.setKeyValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS, joResult.getString("auction_register_status"));
                        propertyListInfo.registeredForProperty = "1";
                        updateRegStatus();
                        getPropertyDetail();
//                        setElements();
                        alerts.confirmAuctionRegistration("Registered to auction.");


                    } else {
                        alerts.confirmAuctionRegistration(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_CANCEL_AUCTION_REGISTER:
                System.out.println("cancel auction registration =>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0041")) {
//                        prefs.setKeyValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS, joResult.getString("auction_register_status"));
                        propertyListInfo.registeredForProperty = "0";
                        updateRegStatus();
                        getPropertyDetail();
                        alerts.confirmAuctionRegistration("Successfully canceled your auction registration.");


                    } else {
                        alerts.confirmAuctionRegistration(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.RC_SUMMARY_REPORT_SOLD:
                System.out.println("sumary report" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONObject jObjBidSummary = joResult.getJSONObject("bid_summary");
                        soldProp = new SoldPropertyModel(propertyListInfo);
                        soldProp.userId = propertyListInfo.agentId;
                        soldProp.propertyId = propertyListInfo.id;
                        soldProp.propertyImgUrl = propertyListInfo.propertyImgUrl;
                        soldProp.street = propertyListInfo.street;
                        soldProp.regionPostcode = propertyListInfo.suburb + " " + propertyListInfo.propertyState + " " + propertyListInfo.postCode;
                        soldProp.participations = jObjBidSummary.getString("participation");
                        soldProp.highestBidAmount = soldProp.getHighestBidAmount(jObjBidSummary.getString("highest_bid"));
                        soldProp.highestBidder = jObjBidSummary.getString("highest_bidder");
                        String tempHB = jObjBidSummary.getString("highest_bidder");
                        if (tempHB.equalsIgnoreCase(CommonMethods.getLoggedInUserUniqueId(getApplicationContext())))
                            soldProp.highestBidder = "You";
                        soldProp.soldDate = jObjBidSummary.getString("auction_end_date_time");
                        soldProp.soldDate = soldProp.getSoldDateTime(jObjBidSummary.getString("auction_end_date_time"))[0];
                        soldProp.soldTime = soldProp.getSoldDateTime(jObjBidSummary.getString("auction_end_date_time"))[1];
                        soldProp.bidDuration = soldProp.getDuration(Long.parseLong(jObjBidSummary.getString("duration")));
                        soldProp.setWon(jObjBidSummary.getString("check_myself"));
                        if (propertyListInfo.propertyStatus == 2)
                            Opener.SoldPropertySummary(PropertyDetails.this, soldProp);
                        else
                            Opener.PassedPropertySummary(PropertyDetails.this, soldProp);


                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.RC_JOIN_LIVE_AUCTION:
                System.out.println("join live auction" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        JSONObject jObjRoomDetail = joResult.getJSONObject("room_detail");
                        RoomDetail roomDetail = new RoomDetail();
                        roomDetail.auctionId = jObjRoomDetail.getString("id");
                        roomDetail.propertyId = jObjRoomDetail.getString("property_id");
                        roomDetail.roomName = jObjRoomDetail.getString("room_name");
                        roomDetail.callCount = jObjRoomDetail.getInt("count_call");
                        roomDetail.soldTo = jObjRoomDetail.getString("sold_to");
                        roomDetail.soldAmount = jObjRoomDetail.getString("sold_amount");
                        roomDetail.status = jObjRoomDetail.getString("status");
                        roomDetail.startDateTime = jObjRoomDetail.getString("auction_start_date_time_stamp");

                        PropertyAuctionModel propertyModel = new PropertyAuctionModel();
                        propertyModel.propertyId = propertyListInfo.id;
                        propertyModel.propertyImgUrl = propertyListInfo.propertyImgUrl;
                        propertyModel.street = propertyListInfo.street;
                        propertyModel.regionPostcode = propertyListInfo.suburb + " " + propertyListInfo.propertyState + " " + propertyListInfo.postCode;
                        propertyModel.propertyStatus = propertyListInfo.propertyStatus;
                        propertyModel.propertyStatus2 = propertyListInfo.propertyStatus2;
                        propertyModel.agentId = propertyListInfo.agentId;
                        propertyModel.propertyPrice = propertyListInfo.price;
                        propertyModel.moreImages = propertyListInfo.property_images_more;
                        propertyModel.setFav(propertyListInfo.isFav());


                        Opener.PropertyAuction(PropertyDetails.this, propertyModel, roomDetail);

                    } else {
                        if (appMode == AppMode.agent) {
                            startAuction();
                        } else
                            alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
            case CommonDef.RC_START_AUCTION:
                System.out.println("join live auction" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        RoomDetail roomDetail = new RoomDetail();
                        roomDetail.auctionId = joResult.getString("property_auction_id");
                        roomDetail.propertyId = propertyListInfo.id;
                        roomDetail.roomName = joResult.getString("chat_room");
                        roomDetail.callCount = 0;
                        roomDetail.soldTo = "";
                        roomDetail.soldAmount = "";
                        roomDetail.status = "";
                        roomDetail.startDateTime = joResult.getString("auction_start_date_time_stamp");

                        PropertyAuctionModel propertyModel = new PropertyAuctionModel();
                        propertyModel.propertyId = propertyListInfo.id;
                        propertyModel.propertyImgUrl = propertyListInfo.propertyImgUrl;
                        propertyModel.street = propertyListInfo.street;
                        propertyModel.regionPostcode = propertyListInfo.suburb + " " + propertyListInfo.propertyState + " " + propertyListInfo.postCode;
                        propertyModel.propertyStatus = propertyListInfo.propertyStatus;
                        propertyModel.propertyStatus2 = propertyListInfo.propertyStatus2;
                        propertyModel.agentId = propertyListInfo.agentId;
                        propertyModel.propertyPrice = propertyListInfo.price;
                        propertyModel.moreImages = propertyListInfo.property_images_more;
                        propertyModel.setFav(propertyListInfo.isFav());


                        Opener.PropertyAuction(PropertyDetails.this, propertyModel, roomDetail);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;
            case CommonDef.REQ_SALE_OF_CONTRACT:
                System.out.println("Privacy policy=>" + result);
                try {
                    String code, msg, contract_of_sale;
                    JSONObject jsonObject = new JSONObject(result);
                    code = jsonObject.getString("code");
                    msg = jsonObject.getString("msg");
                    if (code.equalsIgnoreCase("0001")) {
                        contract_of_sale = jsonObject.getString("contract_of_sale");
                    } else {
                        contract_of_sale = "";
                    }
                    alerts.SaleOfContract(this, code, msg, contract_of_sale, propertyListInfo);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;

            case CommonDef.REQ_SALE_OF_CONTRACT_EMAIL:
                System.out.println("Privacy policy=>" + result);
                try {
                    String code, msg, contract_of_sale;
                    JSONObject jsonObject = new JSONObject(result);
                    code = jsonObject.getString("code");
                    msg = jsonObject.getString("msg");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case CommonDef.RC_GET_PROPERTY_INFO:
                System.out.println("join live auction" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        propertyListInfo = new PropertyListInfo().getPropertyInfo(joResult.getJSONObject("detail"));
                        setElements();

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }
                break;

        }
    }

    private void populateContact(String result) {
        parseJSONContact(result);
        setContactView();
    }

    public void showMakeOfferPopup() {
        View view = getLayoutInflater().inflate(R.layout.make_offer_dialog, null);
        final Button btnSubmit = (Button) view.findViewById(R.id.btn_submit);
        final EditText etOfferPrice = (EditText) view.findViewById(R.id.et_offer_price);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_property_listings);
        if (offerType == OfferType.PRE_OFFER) {
            etOfferPrice.setHint("Pre-Auction Offer");
            tvTitle.setText("Pre-Auction Offer");
        } else {
            etOfferPrice.setHint("Post-Auction Offer");
            tvTitle.setText("Post-Auction Offer");
        }
        final EditText etSettlementDateTime = (EditText) view.findViewById(R.id.et_settlement_date_time);
        etOfferPrice.addTextChangedListener(new TextWatcher() {
            private String current = "";

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {
                    etOfferPrice.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,]", "");


                    String formatted = "";
                    if (cleanString.equalsIgnoreCase("") || cleanString.equalsIgnoreCase("0"))
                        formatted = "";
                    else {
                        longPriveValue = Long.parseLong(cleanString);
                        formatted = "$" + NumberFormat.getNumberInstance(Locale.US).format((longPriveValue));
                    }
                    current = formatted;
                    etOfferPrice.setText(formatted);
                    etOfferPrice.setSelection(formatted.length());

                    etOfferPrice.addTextChangedListener(this);
                    enableOffer(etOfferPrice.getText().toString(), btnSubmit);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isValidPrice(longPriveValue)) {
                    alerts.showOkMessage("Offer price must be greater than 80% of property price.");
                    return;
                }
                if (!isValidSettlementDate(settlementDate)) {
                    alerts.showOkMessage("Please verify settlement date.");
                    return;
                }
                propertyId = propertyListInfo.id;
                Opener.MakeAnOffer(PropertyDetails.this, propertyListInfo, propertyListInfo.propertyStatus == 3 ? OfferType.POST_OFFER : OfferType.PRE_OFFER, longPriveValue, settlementDate);//propertystatus 3=passed
                mBottomSheetDialog.dismiss();

            }
        });
        etSettlementDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                custom = new CustomDateTimePicker(PropertyDetails.this,
                        new CustomDateTimePicker.ICustomDateTimeListener() {

                            @Override
                            public void onSet(Dialog dialog, Calendar calendarSelected,
                                              Date dateSelected, int year, String monthFullName,
                                              String monthShortName, int monthNumber, int date,
                                              String weekDayFullName, String weekDayShortName,
                                              int hour24, int hour12, int min, int sec,
                                              String AM_PM) {

                                etSettlementDateTime.setText(calendarSelected
                                        .get(Calendar.DAY_OF_MONTH)
                                        + " " + (monthShortName) + " at "
                                        + hour12 + ":" + min
                                        + " " + AM_PM);
                                String m = (monthNumber + 1) < 10 ? "0" + (monthNumber + 1) : (monthNumber + 1) + "";
                                String d = calendarSelected.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendarSelected.get(Calendar.DAY_OF_MONTH)
                                        : calendarSelected.get(Calendar.DAY_OF_MONTH) + "";
                                settlementDate = year + "-" + m + "-" + d + " "
                                        + hour24 + ":"
                                        + min + ":"
                                        + sec;
                                System.out.println("sDate:" + settlementDate);
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                custom.set24HourFormat(false);
                custom.setDate(Calendar.getInstance());
                custom.showDialog();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_contact:
                int index = (Integer) v.getTag();
                if (index != -1)
                    alerts.contactAgentBottomSheet(alAgentInfo.get(index));
                else
                    alerts.contactAgencyBottomSheet(agencyInfo);
                break;
            case R.id.rl_agent_details:
                index = (Integer) v.getTag();
                if (index != -1)
                    Opener.AgentPublicProfile(PropertyDetails.this, alAgentInfo.get(index).getUserId());
                else
                    Opener.AgencyProfile(PropertyDetails.this, agencyInfo.getId());
                break;
            case R.id.btn_bottom:
//                prefs.setKeyValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS,"0");
                int action = (int) btnBottom.getTag();
                switch (action) {
                    case 0:
//                        if (prefs.getStringValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS).equalsIgnoreCase("0"))
//                            Opener.AuctionRegistration(PropertyDetails.this, propertyListInfo);
//                        else
//                            registerAuction();
//                        Opener.SaleOfContract(PropertyDetails.this);
                        loadContract();
//                        alerts.SaleOfContract(this);
                        break;
                    case 1:
                    case 2:
                        alerts.cancelAuctionReg();
                        break;
                    case 11:// passed in property
                        getSoldPropertySummaryReport();
//                        Opener.PassedPropertySummary(PropertyDetails.this, soldProp);
                        break;
                    case 12:// sold property summary
                        getSoldPropertySummaryReport();
                        break;
                    case 13:// join auction
                        joinLiveAuction();
//                        loadContract();
                        break;
                    case 14:// guest mode
//                        Opener.GuestAuctionRegistration(PropertyDetails.this);
                        Opener.guestMode(PropertyDetails.this, "Auction Registration");
                        break;
                    case 15:// start auction
//                        Toast.makeText(getApplicationContext(), "start Auction", Toast.LENGTH_SHORT).show();
                        startAuction();
                        break;
                    case 16:// join auction agent
//                        Toast.makeText(getApplicationContext(), "start Auction", Toast.LENGTH_SHORT).show();
                        joinLiveAuction();
                        break;
//
                }

                break;
            case R.id.btn_make_an_offer:
                showMakeOfferPopup();
                break;

            case R.id.ibtn_left:
                onBackPressed();
                break;
        }

    }

    public void loadContract() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyDetails.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        params.put("view_email_flag", "0");
        VolleyRequest volleyRequest = new VolleyRequest(this, Request.Method.POST, params, UrlHelper.GET_SALE_OF_CONTRACT, headers, false, CommonDef.REQ_SALE_OF_CONTRACT);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    public void loadContractWithEmail() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyDetails.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        params.put("view_email_flag", "1");
        VolleyRequest volleyRequest = new VolleyRequest(this, Request.Method.POST, params, UrlHelper.GET_SALE_OF_CONTRACT, headers, false, CommonDef.REQ_SALE_OF_CONTRACT_EMAIL);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private boolean isValidPrice(long price) {
        boolean result = false;
        if (price > Long.parseLong(propertyListInfo.price.toString().replaceAll("[$,]", "")) * .8)
            result = true;
        return result;
    }

    private boolean isValidSettlementDate(String sDate) {
        boolean result = false;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        if (sDate.isEmpty())
            return true;

        Date notifiedDate = null;
        try {
            notifiedDate = dateFormat.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = Calendar.getInstance().getTime();
        long different = notifiedDate.getTime() - (now.getTime() + 24 * 60 * 60 * 1000);
        if (different >= 0)
            result = true;
        return result;
    }

    private void enableOffer(String price, Button submit) {
        if (!price.isEmpty()) {
            submit.setEnabled(true);
        } else {
            submit.setEnabled(false);
        }
    }

    private void startAuction() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyDetails.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("start auction=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyDetails.this, Request.Method.POST, params, UrlHelper.START_AUCTION, headers, false, CommonDef.RC_START_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    public void joinLiveAuction() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyDetails.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("join room=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyDetails.this, Request.Method.POST, params, UrlHelper.JOIN_LIVE_AUCTION, headers, false, CommonDef.RC_JOIN_LIVE_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void getSoldPropertySummaryReport() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyDetails.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("SUMMARY REPORT=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyDetails.this, Request.Method.POST, params, UrlHelper.GET_SUMMARY_REPORT_SOLD, headers, false, CommonDef.RC_SUMMARY_REPORT_SOLD);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    private void setContactView() {
        int length = alAgentInfo.size() > 1 ? 1 : alAgentInfo.size();
        for (int i = 0; i < length; i++) {
            loadContact(alAgentInfo.get(i), i);
        }
        if (agencyInfo != null)
            loadContact(agencyInfo);
    }

    private void loadContact(AgencyInfo agency) {
        ViewHolder holder = new ViewHolder();
        View view = getLayoutInflater().inflate(R.layout.sales_team_item, null, false);
        holder.rlAgentDetails = (RelativeLayout) view.findViewById(R.id.rl_agent_details);
        holder.rlAgentDetails.setTag(-1);
        holder.ivAgentImg = (ImageView) view.findViewById(R.id.iv_agent_img);
        holder.tvAgentName = (TextView) view.findViewById(R.id.tv_agent_name);
        holder.tvSaleAgentNo = (TextView) view.findViewById(R.id.tv_agent_unique_no);
        holder.btnContact = (Button) view.findViewById(R.id.btn_contact);
        holder.btnContact.setTag(-1);
        if (!agency.getName().isEmpty())
            holder.tvAgentName.setText(Html.fromHtml("<b>" + StringHelper.capitalizeInitial(agency.getName()) + "</b>"));
        holder.tvSaleAgentNo.setVisibility(View.GONE);
        if (!agencyInfo.getLogoUrl().isEmpty()) {
            Picasso.with(PropertyDetails.this)
                    .load(agency.getLogoUrl())
                    .resize((int) CommonMethods.pxFromDp(PropertyDetails.this, 100), (int) CommonMethods.pxFromDp(PropertyDetails.this, 100))
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(holder.ivAgentImg);
        } else {
            Picasso.with(PropertyDetails.this)
                    .load(R.drawable.io_white_profile_default)
                    .transform(new CircleTransform())
                    .into(holder.ivAgentImg);
        }
        if (llSalesAgentContainer.getChildCount() > 0) //{
            llSalesAgentContainer.addView(addDivider());
        llSalesAgentContainer.addView(view);


        holder.rlAgentDetails.setOnClickListener(this);
        holder.btnContact.setOnClickListener(this);
//        }
    }

    public void loadContact(AgentInfo agentInfo, int pos) {
        ViewHolder holder = new ViewHolder();
        HashMap<String, String> name = null;
        View view = getLayoutInflater().inflate(R.layout.sales_team_item, null, false);
        holder.rlAgentDetails = (RelativeLayout) view.findViewById(R.id.rl_agent_details);
        holder.rlAgentDetails.setTag(pos);
        holder.ivAgentImg = (ImageView) view.findViewById(R.id.iv_agent_img);
        holder.tvAgentName = (TextView) view.findViewById(R.id.tv_agent_name);
        holder.tvSaleAgentNo = (TextView) view.findViewById(R.id.tv_agent_unique_no);
        holder.btnContact = (Button) view.findViewById(R.id.btn_contact);
        holder.btnContact.setTag(pos);
        name = CommonMethods.getName(agentInfo.getName().split(" "));
        holder.tvAgentName.setText(Html.fromHtml("<b>" + StringHelper.capitalizeInitial(name.get("fName")) + "</b>" + " " + StringHelper.capitalizeInitial(name.get("lName"))));
//        holder.tvAgentName.setText(agentInfo.getName());
        holder.tvSaleAgentNo.setText("Sales Agent- " + agentInfo.getUniqueNumber());
        if (!agentInfo.getProfileImage().isEmpty()) {
            Picasso.with(PropertyDetails.this)
                    .load(agentInfo.getProfileImage())
                    .resize((int) CommonMethods.pxFromDp(PropertyDetails.this, 100), (int) CommonMethods.pxFromDp(PropertyDetails.this, 100))
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(holder.ivAgentImg);
        } else {
            Picasso.with(PropertyDetails.this)
                    .load(R.drawable.io_white_profile_default)
                    .transform(new CircleTransform())
                    .into(holder.ivAgentImg);
        }
        if (llSalesAgentContainer.getChildCount() > 0) //{
            llSalesAgentContainer.addView(addDivider());
        llSalesAgentContainer.addView(view);


        holder.rlAgentDetails.setOnClickListener(this);
        holder.btnContact.setOnClickListener(this);

        //}
    }

    private void parseJSONContact(String result) {

        System.out.println("The result is : " + result);

        try {
            JSONObject joResult = new JSONObject(result);
            String msg, code;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            alAgentInfo = new ArrayList<>();
            if (code.equals("0001")) {
                if (joResult.has("auction_register_status"))
                    prefs.setKeyValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS, joResult.getString("auction_register_status"));
                if (joResult.has("registered_for_property"))
                    propertyListInfo.registeredForProperty = joResult.getString("registered_for_property");
                JSONArray jaAgents = joResult.getJSONArray("agents");

                for (int i = 0; i < jaAgents.length(); i++) {
                    JSONObject joAgent = jaAgents.getJSONObject(i);
                    AgentInfo agentInfo = new AgentInfo();
                    agentInfo.setName(joAgent.getString("agent_name"));
                    agentInfo.setUniqueNumber(joAgent.getString("unique_number"));
                    agentInfo.setEmail(joAgent.getString("agent_email"));
                    agentInfo.setMobile(joAgent.getString("mobile_number"));
                    agentInfo.setProfileImage(joAgent.getString("profile_image"));
                    agentInfo.setUserId(joAgent.getString("agent_id"));

                    ArrayList<AvailabilityModel> alAvailabilityModel = new ArrayList<>();
                    JSONArray jaAvailability = joAgent.getJSONArray("availabilities");
                    for (int j = 0; j < jaAvailability.length(); j++) {
                        JSONObject joAvailability = jaAvailability.getJSONObject(j);
                        int fromDay, toDay;
                        String fromTime, toTime;
                        fromDay = joAvailability.getInt("from_day");
                        fromTime = joAvailability.getString("from_time");
                        toDay = joAvailability.getInt("to_day");
                        toTime = joAvailability.getString("to_time");
                        alAvailabilityModel.add(new AvailabilityModel(fromTime, toTime, toDay, fromDay));
                    }

                    agentInfo.setAvailabilities(alAvailabilityModel);
                    alAgentInfo.add(agentInfo);
                }
                agencyInfo = null;
                JSONObject joAgency = null;
                if (joResult.has("agency")) {
                    joAgency = joResult.getJSONObject("agency");
                    if (joAgency.length() > 0) {
                        agencyInfo = new AgencyInfo();
                        agencyInfo.setId(joAgency.getString("agency_id"));
                        agencyInfo.setState(joAgency.getString("agency_address"));
                        agencyInfo.setLogoUrl(joAgency.getString("agency_logo"));
                        agencyInfo.setName(joAgency.getString("agency_name"));
                        agencyInfo.setMobile(joAgency.getString("mobile_number"));

                        ArrayList<AvailabilityModel> alAvailabilityModel = new ArrayList<>();
                        JSONArray jaAvailability = joAgency.getJSONArray("availabilities");
                        for (int j = 0; j < jaAvailability.length(); j++) {
                            JSONObject joAvailability = jaAvailability.getJSONObject(j);
                            int fromDay, toDay;
                            String fromTime, toTime;
                            fromDay = joAvailability.getInt("from_day");
                            fromTime = joAvailability.getString("from_time");
                            toDay = joAvailability.getInt("to_day");
                            toTime = joAvailability.getString("to_time");
                            alAvailabilityModel.add(new AvailabilityModel(fromTime, toTime, toDay, fromDay));
                        }
                        agencyInfo.setAvailabilities(alAvailabilityModel);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    @Override
    public void loadData() {
        if (!isLastPage)
            getPropertyList();
        else
            lvPropertyDetails.finaliseLoading();
    }

    @Override
    public void showToolbar() {
        ibtnBack.setImageResource(R.drawable.left_arrow);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(toolbar, "backgroundColor", new ArgbEvaluator(), Color.argb(0, 255, 255, 255), Color.argb(255, 255, 255, 255));//getResources().getColor(R.color.theme_yellow)
        colorFade.setDuration(1000);
        colorFade.start();
    }

    @Override
    public void hideToolbar() {
        ibtnBack.setImageResource(R.drawable.back_icon);
        ObjectAnimator colorFade = ObjectAnimator.ofObject(toolbar, "backgroundColor", new ArgbEvaluator(), Color.argb(255, 255, 255, 255), Color.argb(0, 255, 255, 255));
        colorFade.setDuration(1000);
        colorFade.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
            isLocationSet = true;
        } else {
            gps.showSettingsAlert();
        }
        if (needToUpdate)
            updateRegStatus();

    }

    @Override
    protected void onPause() {
        super.onPause();
        gps.stopUsingGPS();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gps.stopUsingGPS();
    }

    public void registerAuction() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyDetails.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("register Auction params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyDetails.this, Request.Method.POST, params, UrlHelper.REGISTER_TO_AUCTION, headers, false, CommonDef.REQUEST_REGISTER_AUCTION);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    public void cancelRegisterAuction() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyDetails.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("register Auction params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyDetails.this, Request.Method.POST, params, UrlHelper.CANCEL_AUCTION_REG, headers, false, CommonDef.REQUEST_CANCEL_AUCTION_REGISTER);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    /**
     * method to set ui for user
     */
    private void setUserMode() {
        if (propertyListInfo.propertyStatus2 == 5) {
            btnBottom.setVisibility(View.GONE);
            btnMakeAnOffer.setVisibility(View.GONE);
        } else {
            btnBottom.setVisibility(View.VISIBLE);
            btnMakeAnOffer.setVisibility(View.VISIBLE);
        }

    }

    /**
     * method to set ui for agent
     */
    private void setAgentMode() {
        btnBottom.setVisibility(View.GONE);
        btnMakeAnOffer.setVisibility(View.GONE);
    }

    /**
     * method to set ui for guest
     */
    private void setGuestMode() {
        btnBottom.setVisibility(View.VISIBLE);
        btnMakeAnOffer.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
    }

    private static class ViewHolder {
        RelativeLayout rlAgentDetails;
        ImageView ivAgentImg;
        TextView tvAgentName,
                tvSaleAgentNo;
        Button btnContact;
    }

    private void getPropertyDetail() {
        HashMap<String, String> headers = CommonMethods.getHeaders(PropertyDetails.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("property_id", propertyListInfo.id);
        System.out.println("register Auction params=>" + params.toString());
        VolleyRequest volleyRequest = new VolleyRequest(PropertyDetails.this, Request.Method.POST, params, UrlHelper.GET_PROPERTY_INFO, headers, false, CommonDef.RC_GET_PROPERTY_INFO);
        volleyRequest.asyncInterface = this;
        volleyRequest.request(true);
    }


}

package com.official.livebid.activities.newRegistration;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import com.official.livebid.R;
import com.official.livebid.helpers.CommonDef;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragSelectUserType.OnSelectRegTypeListener} interface
 * to handle interaction events.
 */
public class FragSelectUserType extends Fragment implements View.OnClickListener {
    private Button btnSelectUser, btnSelectAgent;

    private OnSelectRegTypeListener mListener;
    private UserType userType;

    public FragSelectUserType() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frag_select_user_type, container, false);
        btnSelectUser = (Button) v.findViewById(R.id.btn_user);
        btnSelectUser.setTag(RegType.USER);
        btnSelectAgent = (Button) v.findViewById(R.id.btn_agent);
        btnSelectAgent.setTag(RegType.AGENT);
//        if (getArguments() != null) {
//            userType = (UserType) getArguments().getSerializable(CommonDef.USER_TYPE);
//        }
        btnSelectUser.setOnClickListener(this);
        btnSelectAgent.setOnClickListener(this);
        return v;
    }


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnSelectRegTypeListener) {
            mListener = (OnSelectRegTypeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_user:
            case R.id.btn_agent:
                mListener.onSelectRegType((RegType) view.getTag());
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSelectRegTypeListener {
        // TODO: Update argument type and name
        void onSelectRegType(RegType regType);
    }
}

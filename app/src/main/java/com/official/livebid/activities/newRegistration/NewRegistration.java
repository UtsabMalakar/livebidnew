package com.official.livebid.activities.newRegistration;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.official.livebid.R;
import com.official.livebid.activities.discover.Discover;
import com.official.livebid.objects.LiveBidSettings;
import com.official.livebid.pushNotification.MyReciever;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.pushNotification.RegistrationIntentService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class NewRegistration extends AppCompatActivity implements AsyncInterface, FragSelectUserType.OnSelectRegTypeListener, FragWhoAreYou.OnWhoRUListener, FragSignIn.OnSignInListener, FragSecureAccount.OnPasswordSetListener, FragVerifyMobile.OnMobileVerifyListener {
    RelativeLayout fragContainer;
    private RegType regType;
    private UserType userType;
    private SharedPreference prefs;
    private Alerts alerts;
    public static UserModel userModel;
    private String password;
    //push notification
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private FragSelectUserType frag;
    //


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_new_registraion);
        prefs = new SharedPreference(this);
        alerts = new Alerts(this);
//        new CustomActionBar.NewRegistrationCAB(this);
        fragContainer = (RelativeLayout) findViewById(R.id.frag_container);
        CommonMethods.setupUI(findViewById(R.id.rl_main_view), NewRegistration.this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            userType = (UserType) b.getSerializable(CommonDef.USER_TYPE);
        }
//        getFragmentManager()
//                .beginTransaction()
//                .add(R.id.frag_container, new FragSelectUserType())
//                .addToBackStack(null)
//                .commit();

        /**
         * for desabling registration
         */
        frag = new FragSelectUserType();
//        frag.setArguments(b);
        getFragmentManager()
                .beginTransaction()
//                .setCustomAnimations(R.anim.fragment_slide_left_enter,
//                        R.anim.fragment_slide_left_exit,
//                        R.anim.fragment_slide_right_enter,
//                        R.anim.fragment_slide_right_exit)
                .replace(R.id.frag_container, frag)
                .addToBackStack(null)
                .commit();
        findViewById(R.id.ibtn_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // Registering BroadcastReceiver
//        registerReceiver();


    }


    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 1) {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
            return;
        }
        getFragmentManager().popBackStackImmediate();

    }

    @Override
    public void onSelectRegType(RegType regType) {
        this.userModel = null;
        this.regType = regType;
        Fragment frag;
        Bundle b = new Bundle();
        b.putSerializable(CommonDef.REG_TYPE, regType);
        if (userType == UserType.NEW) {
            frag = new FragWhoAreYou();
            frag.setArguments(b);
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.animator.fragment_slide_left_enter,
                            R.animator.fragment_slide_left_exit,
                            R.animator.fragment_slide_right_enter,
                            R.animator.fragment_slide_right_exit)
                    .replace(R.id.frag_container, frag)
                    .addToBackStack(null)
                    .commit();
        } else {
            frag = new FragSignIn();
            frag.setArguments(b);
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.animator.fragment_slide_left_enter,
                            R.animator.fragment_slide_left_exit,
                            R.animator.fragment_slide_right_enter,
                            R.animator.fragment_slide_right_exit)
                    .replace(R.id.frag_container, frag)
                    .addToBackStack(null)
                    .commit();
        }


    }


    @Override
    public void onSignIn(String email, String password) {
        if (userModel == null)
            userModel = new UserModel();
        userModel.setLoginInfo(email, password);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("device_type", "2");
        headers.put("device_id", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        headers.put("user_type", regType.getRegType() + "");

        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        System.out.println("loginHeader:" + headers.toString());
        System.out.println("loginParam:" + params.toString());

        VolleyRequest volleyRequest = new VolleyRequest(NewRegistration.this, Request.Method.POST, params, UrlHelper.SIGN_IN, headers, false, CommonDef.REQUEST_SIGN_IN);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void onForgetPassword() {
        Opener.ProblemSignIn(NewRegistration.this, regType);
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_SIGN_IN:
                System.out.println("USER DETAILS=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    final String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                       alerts.showOkMessage(msg);
                        JSONObject joUserInfo = joResult.getJSONObject("user_info");
                        setUserInfo(joUserInfo);
                        landUser();


                    } else if (code.equals("0007")) {
                        lunchMobileVerifier(joResult.getString("mobile_number"));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alerts.showOkMessage(msg);
                            }
                        });


                    } else if (code.equals("0070")) {
                        //free lancer
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alerts.showOkMessage(msg);
                            }
                        });

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());
                }

                break;
            case CommonDef.REQUEST_REGISTER_USER:
                try {
                    JSONObject joResult = new JSONObject(result);
                    final String msg, code,isFreelancer;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        lunchMobileVerifier(userModel.getMobile());
                        alerts.showOkMessage(msg);
                    }
                    else if(code.equals("0004"))
                    {
                        new AlertDialog.Builder(this)
                                .setTitle("Livebid")
                                .setMessage(msg)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }
                                }).show();
                    }

                    else {
                        alerts.showOkMessage(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());
                }
                break;

            case CommonDef.RC_REG_AGENT:
                System.out.println("agent reg=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    final String msg, code, isFreelancer;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        isFreelancer = joResult.getString("is_freelancer");
//                        if (userModel.isFreeLancer()) {
                            if (isFreelancer.equalsIgnoreCase("1")) {
                                new AlertDialog.Builder(this)
                                        .setTitle("Livebid")
                                        .setMessage(msg)
                                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // continue with delete
                                                finish();
                                                overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
                                            }
                                        })

                                        .show();

                            } else {
                                lunchMobileVerifier(userModel.getMobile());
                                alerts.showOkMessage(msg);
                            }


//                        } else {
//                            alerts.showOkMessage(msg);
//
//                        }
                    }
                    else if(code.equals("0004"))
                    {
                        new AlertDialog.Builder(this)
                                .setTitle("Livebid")
                                .setMessage(msg)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }
                                })

                                .show();
                    }

                    else {
                        alerts.showOkMessage(msg);

                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());
                }
                break;
            case CommonDef.REQUEST_ACTIVATE_USER:
                System.out.println("agent reg=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    final String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0027")) {
                        //setuser
                        //landuser
                        JSONObject joUserInfo = null;
                        if (joResult.has("user_info"))
                            joUserInfo = joResult.getJSONObject("user_info");
                        if(joUserInfo!=null) {
                            setUserInfo(joUserInfo);
                            landUser();
                        }else {
                            alerts.showOkMessage("userInfo not found");
                        }
//                        lunchSignIn();
//                        alerts.showOkMessage(msg);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());
                }
                break;
            case CommonDef.REQUEST_NEW_CODE:
                System.out.println("agent reg=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    final String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        alerts.showOkMessage(msg);

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());
                }
                break;
        }

    }

    private void lunchSignIn() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragSignIn frag = new FragSignIn();
        Bundle b = new Bundle();
        b.putSerializable(CommonDef.REG_TYPE, regType);
        frag.setArguments(b);
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.fragment_slide_left_enter,
                        R.animator.fragment_slide_left_exit,
                        R.animator.fragment_slide_right_enter,
                        R.animator.fragment_slide_right_exit)
                .replace(R.id.frag_container, frag)
                .addToBackStack(null)
                .commit();
    }

    private void lunchMobileVerifier(String mobile) {
        if (userModel == null)
            userModel = new UserModel();
        userModel.setMobile(mobile);
        Bundle b = new Bundle();
        b.putSerializable(CommonDef.USER_DATA, userModel);
        FragVerifyMobile fragVerifyMobile = new FragVerifyMobile();
        fragVerifyMobile.setArguments(b);
        if (userType == UserType.NEW)
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.fragment_slide_left_enter,
                        R.animator.fragment_slide_left_exit,
                        R.animator.fragment_slide_right_enter,
                        R.animator.fragment_slide_right_exit)
                .replace(R.id.frag_container, fragVerifyMobile)

                .addToBackStack(null)
                .commit();
    }


    @Override
    public void onGetUserData(UserModel userModel) {
        this.userModel = userModel;
        if (userModel != null) {
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.animator.fragment_slide_left_enter,
                            R.animator.fragment_slide_left_exit,
                            R.animator.fragment_slide_right_enter,
                            R.animator.fragment_slide_right_exit)
                    .replace(R.id.frag_container, new FragSecureAccount())
                    .addToBackStack(null)
                    .commit();
        }

    }

    @Override
    public void onSetPassword(String pass) {
        this.password = pass;
        if (regType == RegType.USER)
            registerUser();
        else
            registerAgent();
    }

    private void registerAgent() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("device_type", "2");
        headers.put("device_id", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        headers.put("user_type", regType.getRegType() + "");

        HashMap<String, String> params = new HashMap<>();
        params.put("email", userModel.getEmail());
        params.put("full_name", userModel.getFirstName());
        params.put("mobile_number", userModel.getMobile());
        params.put("password", password);
//        params.put("freelancer", userModel.isFreeLancer() ? "1" : "0");
        params.put("freelancer", "1");
        System.out.println("reg Header:" + headers.toString());
        System.out.println("reg Param:" + params.toString());

        VolleyRequest volleyRequest = new VolleyRequest(NewRegistration.this, Request.Method.POST, params, UrlHelper.REG_AGENT, headers, false, CommonDef.RC_REG_AGENT);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }

    private void registerUser() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("device_type", "2");
        headers.put("device_id", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        headers.put("user_type", regType.getRegType() + "");

        HashMap<String, String> params = new HashMap<>();
        params.put("first_name", userModel.getFirstName());
        params.put("last_name", userModel.getLastName());
        params.put("email", userModel.getEmail());
        params.put("password", password);
        params.put("mobile_number", userModel.getMobile());
        System.out.println("register:" + params.toString());

        VolleyRequest volleyRequest = new VolleyRequest(NewRegistration.this, Request.Method.POST, params, UrlHelper.REGISTER_USER, headers, false, CommonDef.REQUEST_REGISTER_USER);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void onMobileVerify(String mobile, String code) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("device_type", "2");
        headers.put("device_id", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        headers.put("user_type", regType.getRegType() + "");

        HashMap<String, String> params = new HashMap<>();
        params.put("mobile_number", mobile);
        params.put("code", code);
        System.out.println("mobileVerification:" + params.toString());

        VolleyRequest volleyRequest = new VolleyRequest(NewRegistration.this, Request.Method.POST, params,
                UrlHelper.VERIFY_USER,
                headers,
                false,
                CommonDef.REQUEST_ACTIVATE_USER);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();
    }

    @Override
    public void onResendCode(String mobile , String oldMobile) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("device_type", "2");
        headers.put("device_id", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        headers.put("user_type", regType.getRegType() + "");

        HashMap<String, String> params = new HashMap<>();
        params.put("mobile_number", oldMobile);
        params.put("new_mobile_number" , mobile);
        System.out.println("mobileVerification:" + params.toString());

        VolleyRequest volleyRequest = new VolleyRequest(
                NewRegistration.this,
                Request.Method.POST, params,
                UrlHelper.RESEND_CODE,
                headers,
                false,
                CommonDef.REQUEST_NEW_CODE);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }

    private void registerReceiver() {
//        if (!isReceiverRegistered) {
//            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
//            isReceiverRegistered = true;
//        }
        IntentFilter filter = new IntentFilter();
        filter.addAction("SOME_ACTION");
        filter.addAction("SOME_OTHER_ACTION");
        registerReceiver(new MyReciever(), filter);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
//        isReceiverRegistered = false;
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void setUserInfo(JSONObject joUserInfo) throws JSONException {
        prefs.setKeyValues(CommonDef.SharedPrefKeys.USER_ID, joUserInfo.getString("user_id"));
        prefs.setKeyValues(CommonDef.SharedPrefKeys.UNIQUE_NUMBER, joUserInfo.getString("unique_number"));
        prefs.setKeyValues(CommonDef.SharedPrefKeys.USER_TYPE, joUserInfo.getString("user_type"));
        prefs.setKeyValues(CommonDef.SharedPrefKeys.HASH_CODE, joUserInfo.getString("hash_code"));
        prefs.setKeyValues(CommonDef.SharedPrefKeys.LAT, joUserInfo.getString("lat"));
        prefs.setKeyValues(CommonDef.SharedPrefKeys.LNG, joUserInfo.getString("lng"));
        if (regType == RegType.USER) {
            prefs.setKeyValues(CommonDef.SharedPrefKeys.FIRST_NAME, joUserInfo.getString("first_name"));
            prefs.setKeyValues(CommonDef.SharedPrefKeys.LAST_NAME, joUserInfo.getString("last_name"));
            prefs.setKeyValues(CommonDef.SharedPrefKeys.ADDRESS, joUserInfo.getString("address"));
            prefs.setKeyValues(CommonDef.SharedPrefKeys.AUCTION_REGISTRATION_STATUS, joUserInfo.getString("auction_register_status"));
        } else if (regType == RegType.AGENT) {
            prefs.setKeyValues(CommonDef.SharedPrefKeys.AGENT_ID, joUserInfo.getString("agent_id"));
            String name = joUserInfo.getString("name").trim();
            HashMap<String, String> agentName = CommonMethods.getName(name.split(" "));
            prefs.setKeyValues(CommonDef.SharedPrefKeys.FIRST_NAME, agentName.get("fName"));
            prefs.setKeyValues(CommonDef.SharedPrefKeys.LAST_NAME, agentName.get("lName"));
            prefs.setKeyValues(CommonDef.SharedPrefKeys.ADDRESS,
                    joUserInfo.getString("street") + " "
                            + joUserInfo.getString("postcode") + " "
                            + joUserInfo.getString("suburb")
            );

        }
        prefs.setKeyValues(CommonDef.SharedPrefKeys.MOBILE_NUMBER, joUserInfo.getString("mobile_number"));
        prefs.setKeyValues(CommonDef.SharedPrefKeys.USER_EMAIL, joUserInfo.getString("email"));
        prefs.setKeyValues(CommonDef.SharedPrefKeys.PROFILE_IMG, joUserInfo.getString("profile_image"));
        prefs.setKeyValues(CommonDef.SharedPrefKeys.DEVICE_ID, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        prefs.setKeyValues(CommonDef.SharedPrefKeys.IS_LOGGED_IN, true);
        new LiveBidSettings(this).setLiveBidSettings(joUserInfo.getJSONArray("settings"));

    }

    private void landUser() {
        this.finish();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
//                        Opener.Discover(SignIn.this );
        if(!prefs.getStringValues(CommonDef.SharedPrefKeys.OLD_USER_ID).equalsIgnoreCase(prefs.getStringValues(CommonDef.SharedPrefKeys.USER_ID)))
        {
            prefs.setKeyValues(CommonDef.SharedPrefKeys.Search_PostCodeIds,"");
            prefs.setKeyValues(CommonDef.SharedPrefKeys.Search_SuburbNames,"");
        }
        Intent i = new Intent(NewRegistration.this, Discover.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


}

package com.official.livebid.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.Views.EndlessListView;
import com.official.livebid.adapters.SalesAgentListAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.AgentInfo;
import com.official.livebid.objects.AvailabilityModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SalesAgent extends AppCompatActivity implements AsyncInterface, EndlessListView.EndlessListener {
    private EndlessListView lvSalesAgent;
    ArrayList<AgentInfo> salesAgents;
    private SalesAgentListAdapter salesAgentAdapter;

    int salesAgentOffset = 0;//sales agent
    boolean isLastSalesAgent = false;
    private String agencyId = "1";
    private Alerts alerts;
    private AgentInfo agentInfo;
    private ArrayList<AvailabilityModel> agentAvailabilities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_agent);
        getSupportActionBar().hide();
        alerts = new Alerts(this);

        ImageButton ivBack = (ImageButton) findViewById(R.id.ibtn_left);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView tvCenter = (TextView) findViewById(R.id.tv_center);
        tvCenter.setText(getString(R.string.sales_team));

        lvSalesAgent= (EndlessListView) findViewById(R.id.lv_sales_agent);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            agencyId = b.getString("agency_id");
        }
        loadSalesAgent();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sales_agent, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadSalesAgent() {
        if (!isLastSalesAgent) {
            HashMap<String, String> headers = CommonMethods.getHeaders(SalesAgent.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("agency_id", agencyId);
            params.put("offset", salesAgentOffset + "");

            System.out.println("agent Listings params=>" + params.toString());
            VolleyRequest volleyRequest = new VolleyRequest(SalesAgent.this, Request.Method.POST, params, UrlHelper.GET_AGENT_LISTINGS, headers, false, CommonDef.REQUEST_AGENT_LISTINGS);
            volleyRequest.asyncInterface = this;
            volleyRequest.request();
        }
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_AGENT_LISTINGS:
                System.out.println("agent listing=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        ArrayList<AgentInfo> salesAgents = new ArrayList<>();
                        JSONArray jObjAgentList = joResult.getJSONArray("agents");
                        JSONObject agent = null;
                        for (int i = 0; i < jObjAgentList.length(); i++) {
                            agentInfo = new AgentInfo();
                            agentAvailabilities = new ArrayList<AvailabilityModel>();
                            agent = jObjAgentList.getJSONObject(i);
                            agentInfo.setUserId(agent.getString("agent_id"));
                            agentInfo.setUniqueNumber(agent.getString("unique_number"));
                            agentInfo.setName(agent.getString("agent_name"));
                            agentInfo.setEmail(agent.getString("email"));
                            agentInfo.setMobile(agent.getString("mobile_number"));
                            agentInfo.setProfileImage(agent.getString("profile_image"));
                            agentInfo.setStatus(agent.getString("status"));
                            agentAvailabilities.add(new AvailabilityModel(
                                    agent.getString("from_time"),
                                    agent.getString("to_time"),
                                    agent.getInt("to_day"),
                                    agent.getInt("from_day")

                            ));
                            agentInfo.setAvailabilities(agentAvailabilities);
                            salesAgents.add(agentInfo);
                        }
                        isLastSalesAgent = joResult.getBoolean("last_page");
                        if (salesAgentOffset == 0) {
                            salesAgentAdapter = new SalesAgentListAdapter(this, salesAgents);
                            lvSalesAgent.setAdapter(salesAgentAdapter);
                            lvSalesAgent.setLoadingView(R.layout.layout_endless_loading);
                            lvSalesAgent.setListener(this);
                        } else {
                            lvSalesAgent.addNewData(salesAgents);
                        }
                        salesAgentOffset = joResult.getInt("offset");

                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
        }


    }

    @Override
    public void loadData() {
        if (!isLastSalesAgent)
            loadSalesAgent();
        else
            lvSalesAgent.finaliseLoading();
    }

    @Override
    public void showToolbar() {

    }

    @Override
    public void hideToolbar() {

    }
}

package com.official.livebid.activities.myBids;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.official.livebid.R;
import com.official.livebid.activities.newRegistration.UserType;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;

public class MyBidsGuest extends ActionBarActivity {

    private Button btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bids_guest);
        getSupportActionBar().hide();
        new CustomActionBar.MyBidsCAB(this);
        btnSignup= (Button)findViewById(R.id.btn_sign_up);
        btnSignup.setOnClickListener(clicklistener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_bids_guest, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private View.OnClickListener clicklistener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btn_sign_up:
//                    Opener.Register(MyBidsGuest.this);
                    Opener.newRegister(MyBidsGuest.this, UserType.NEW);
                    break;
            }
        }
    };
}

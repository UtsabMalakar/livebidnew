package com.official.livebid.activities.newRegistration;

/**
 * Created by Ics on 5/17/2016.
 */
public interface LoginFinishListener {
   void onSuccess();
   void onFailure();
}

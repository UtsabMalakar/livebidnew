package com.official.livebid.activities.newRegistration;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonMethods;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragSignIn.OnSignInListener} interface
 * to handle interaction events.
 */
public class FragSignIn extends Fragment implements View.OnClickListener {

    private OnSignInListener mListener;
    CustomEditText ctEmail, ctPassword;
    Button btnSignIn, btnForgetPassword;
    private String emailAddress;
    private String password;
    private boolean isPasswordOk;
    private boolean isEmailOk;

    public FragSignIn() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frag_sign_in, container, false);
        ctEmail = (CustomEditText) v.findViewById(R.id.cv_email);
        ctEmail.enableEditMode(true);
        ctEmail.addTextWatcher(tw);
        ctPassword = (CustomEditText) v.findViewById(R.id.cv_password);
        ctPassword.enableEditMode(true);
        ctPassword.addTextWatcher(tw);
        btnSignIn = (Button) v.findViewById(R.id.btn_sign_in);
        btnForgetPassword = (Button) v.findViewById(R.id.btn_forgot_pwd);
        btnSignIn.setOnClickListener(this);
        btnForgetPassword.setOnClickListener(this);
        CommonMethods.setupUI(v.findViewById(R.id.frag_main), getActivity());
        return v;
    }


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnSignInListener) {
            mListener = (OnSignInListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_in:
                if (isValidForm()) {
                    mListener.onSignIn(emailAddress, password);
//                    signIn();
                }

                break;
            case R.id.btn_forgot_pwd:
                mListener.onForgetPassword();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSignInListener {
        // TODO: Update argument type and name
        void onSignIn(String email, String password);

        void onForgetPassword();
    }

    private TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            enableRegistration();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void enableRegistration() {
        getformField();
        if (!emailAddress.isEmpty() || !password.isEmpty()) {
            btnSignIn.setEnabled(true);
        } else
            btnSignIn.setEnabled(false);
    }

    private void getformField() {
        emailAddress = ctEmail.getText().toString();
        password = ctPassword.getText().toString();
    }

    private void validateForm() {
        if (!ctPassword.getText().isEmpty() && !isPasswordValid(ctPassword.getText()))
            ctPassword.setInvalid();
        else {
            ctPassword.setValid();
        }

        if (!ctEmail.getText().isEmpty() && !isEmailValid(ctEmail.getText()))
            ctEmail.setInvalid();
        else {
            ctEmail.setValid();
//            count++;
        }

//        return (count == 4) ? true : false;


    }

    private boolean isValidForm() {
        Alerts alerts = new Alerts(getActivity());
        if (emailAddress.isEmpty()) {
            alerts.showOkMessage("Email address is empty.");
            return false;
        } else if (!isEmailValid(emailAddress)) {
            alerts.showOkMessage("Email address is invalid.");
            return false;
        } else if (password.isEmpty()) {
            alerts.showOkMessage("Password is empty.");
            return false;
        }
        return true;
    }

    private boolean isEmailValid(String emailAddress) {
        isEmailOk = !emailAddress.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress.trim()).matches(); // 2.2 froyo and above
        return isEmailOk;
    }

    private boolean isPasswordValid(String password) {
        isPasswordOk = password.length() >= 8 ? true : false;
        return isPasswordOk;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (NewRegistration.userModel != null) {
            ctEmail.setText(NewRegistration.userModel.getUserName());
            ctPassword.setText(NewRegistration.userModel.getPassword());
        }

    }

}

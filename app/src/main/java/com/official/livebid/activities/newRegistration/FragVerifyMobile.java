package com.official.livebid.activities.newRegistration;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonDef;

import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragVerifyMobile.OnMobileVerifyListener} interface
 * to handle interaction events.
 */
public class FragVerifyMobile extends Fragment implements View.OnClickListener {
    CustomEditText ctMobileNumber, ctVerificationCode;
    Button btnVerifyCode, btnResendCode;

    private OnMobileVerifyListener mListener;
    private UserModel user;

    private Alerts alerts;

    public FragVerifyMobile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frag_verify_mobile, container, false);
        if (getArguments() != null) {
            user = (UserModel) getArguments().getSerializable(CommonDef.USER_DATA);
        }
        ctMobileNumber = (CustomEditText) v.findViewById(R.id.cv_mobile_no);
        ctMobileNumber.setText(user.getMobile());
        ctMobileNumber.setFocusable(true);
        ctVerificationCode = (CustomEditText) v.findViewById(R.id.cv_v_code);
        ctVerificationCode.addTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (ctVerificationCode.getText().toString().length() >= 4) {
                    btnVerifyCode.setEnabled(true);
                } else {
                    btnVerifyCode.setEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        alerts = new Alerts(getActivity());
        btnVerifyCode = (Button) v.findViewById(R.id.btn_verify_code);
        btnResendCode = (Button) v.findViewById(R.id.btn_resend_sms);
        btnVerifyCode.setOnClickListener(this);
        btnResendCode.setOnClickListener(this);
        return v;
    }


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnMobileVerifyListener) {
            mListener = (OnMobileVerifyListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_verify_code:
                if (ctVerificationCode.getText().toString().length() >= 4)
                    mListener.onMobileVerify(ctMobileNumber.getText().toString(),ctVerificationCode.getText().toString());
                break;
            case R.id.btn_resend_sms:
                if(isValid())
                {
                    mListener.onResendCode(ctMobileNumber.getText().toString(),user.getMobile());
                    user.setMobile(ctMobileNumber.getText().toString());
                }
                break;
        }

    }

    private Boolean isValid()
    {
        if (ctMobileNumber.getText().toString().isEmpty()) {
            alerts.showOkMessage("Mobile number is empty.");
            return false;
        } else if (!isMobileValid(ctMobileNumber.getText().toString())) {
            alerts.showOkMessage("Mobile number is invalid.");
            return false;
        }
        return true;
    }

    private boolean isMobileValid(String mobile) {
        final Pattern phonePattern = Pattern.compile("^(?:\\(?(?:\\+?61|0)4\\)?(?:[ -]?[0-9]){7}[0-9]$)");
        return phonePattern.matcher(mobile).matches();

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMobileVerifyListener {
        // TODO: Update argument type and name
        void onMobileVerify(String Mobile,String code);

        void onResendCode(String mobile, String old_Mobile);
    }
}

package com.official.livebid.activities.liveAuction;

import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.objects.PropertyListInfo;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Ics on 4/8/2016.
 */
public class SoldPropertyModel extends PropertyListInfo implements Serializable {
    public String userId;
    public String propertyId;
//    public String street;
    public String regionPostcode;
    public String highestBidAmount;
    public String highestBidder;
    public String soldDate;
    public String soldTime;
    public String bidDuration;
    public String participations;
    private boolean isWon;

    public SoldPropertyModel(PropertyListInfo propertyListInfo) {
       id = propertyListInfo.id;
        property_images_more=propertyListInfo.property_images_more;
       agentId = propertyListInfo.agentId;
       propertyTitle = propertyListInfo.propertyTitle;
       propertyDesc = propertyListInfo.propertyDesc;
       propertyCode = propertyListInfo.propertyCode;
       propertyType = propertyListInfo.propertyType;
       landArea = propertyListInfo.landArea;
       cat = propertyListInfo.cat;
       postCode =propertyListInfo.postCode;
       suburb = propertyListInfo.suburb;
       lat =propertyListInfo.lat;
       lng = propertyListInfo.lng;
       views =propertyListInfo.views;
       agentName = propertyListInfo.agentName;
       agentTradingName = propertyListInfo.agentTradingName;
       agentImportId = propertyListInfo.agentImportId;
       distance = propertyListInfo.distance;
       street = propertyListInfo.street;
       propertyImgUrl = propertyListInfo.propertyImgUrl;
       propertyStatus = propertyListInfo.propertyStatus;
       propertyStatus2 = propertyListInfo.propertyStatus2;
       dateTag = propertyListInfo.dateTag;
       suburb = propertyListInfo.suburb;
       propertyState = propertyListInfo.propertyState;
       propertyStatusIdentifier =propertyListInfo.propertyStatusIdentifier;
       price = propertyListInfo.price;
       nosOfBedroom = propertyListInfo.nosOfBedroom;
       nosOfBathroom = propertyListInfo.nosOfBathroom;
       nosOfParking = propertyListInfo.nosOfParking;
       agentProfileImg = propertyListInfo.agentProfileImg;
       agentFName = propertyListInfo.agentFName;
       agentLName = propertyListInfo.agentLName;
       agentAgencyLogoUrl = propertyListInfo.agentAgencyLogoUrl;
       inspectionTimeString = propertyListInfo.inspectionTimeString;
       registeredForProperty = propertyListInfo.registeredForProperty;
       auctionDate = propertyListInfo.auctionDate;
       setFav(propertyListInfo.isFav());
       auctionDateTimeStamp=propertyListInfo.auctionDateTimeStamp;
    }

    public boolean isWon() {
        return isWon;
    }

    public void setWon(String won) {
        if (won.equalsIgnoreCase("0"))
            isWon = false;
        else
            isWon = true;
    }

    public String getDuration(long timestamp) {
        return DateTimeHelper.getLaggingTimeFormated(timestamp * 1000).replaceAll("ago","");
    }

    public String getHighestBidAmount(String amount) {
        double p = Math.ceil(Double.parseDouble(amount));
        return "$" + NumberFormat.getNumberInstance(Locale.US).format((int) p);

    }

    public String[] getSoldDateTime(String strTimeStamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE d MMM");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
        long timeStamp = Long.parseLong(strTimeStamp);
        Date d = null;
        d = new Date(timeStamp * 1000);
        return new String[]{dateFormat.format(d), timeFormat.format(d)};
    }

}

package com.official.livebid.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.adapters.PropertyListingAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.SharedPreference;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.PropertyListInfo;
import com.official.livebid.staticClasses.PropertyLists;
import com.official.livebid.staticClasses.SearchParams;
import com.official.livebid.utils.GPSTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class PropertyListing extends AppCompatActivity implements AsyncInterface {

    RecyclerView rvPropertyListing;
    ImageButton ibtnSearch;
    String result; // in json format;
    boolean isLocationSet = true;
    double lat, lng;
    GPSTracker gps;
    Alerts alerts;
    private ArrayList<PropertyListInfo> alPropertyListInfo;
    private int propertyType = 0;
    private SharedPreference pref;
    private TextView tvSearchSuburb;
    private RelativeLayout rlSearchBar;
    private String callingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_property_listing);
        init();
    }

    private void getProperty() {
        if (isLocationSet) {
            HashMap<String, String> params = new HashMap<>();
            params.put("lat", lat + "");
            params.put("lng", lng + "");
            params.put("is_trending", propertyType + "");
//            params.put("is_logged_in", prefs.getBoolValues(CommonDef.SharedPrefKeys.IS_LOGGED_IN) ? "1" : "0");
            HashMap headers = CommonMethods.getHeaders(this);
            VolleyRequest volleyRequest = new VolleyRequest(PropertyListing.this, Request.Method.POST, params, UrlHelper.DISCOVER_PROPERTY_LIST, headers, false, CommonDef.REQUEST_DISCOVER_PROPERTYLIST);
            volleyRequest.asyncInterface = this;
            volleyRequest.request();
        }
    }

    private void init() {
        alerts = new Alerts(PropertyListing.this);
        gps = new GPSTracker(PropertyListing.this);
        pref = new SharedPreference(this);
        rvPropertyListing = (RecyclerView) findViewById(R.id.rc_propertyListing);
        rlSearchBar = (RelativeLayout) findViewById(R.id.rlSearchBar);
        tvSearchSuburb = (TextView) findViewById(R.id.tvSearchSuburb);
        rvPropertyListing.setLayoutManager(new LinearLayoutManager(this));
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            result = bundle.getString("result");
            callingActivity = bundle.getString(CommonDef.CallingActivity.callingActivity);
            if (!result.isEmpty()) {
                initPropertyList();
            } else {
                Toast.makeText(getApplicationContext(), "No property found", Toast.LENGTH_LONG).show();
            }
        } else {
//            initFakePropertyList();
//            rvPropertyListing.setAdapter(new PropertyListingAdapter(this, PropertyLists.alPropertyListInfo, false));
        }

        ibtnSearch = (ImageButton) findViewById(R.id.ibtn_search);
        rlSearchBar.setOnClickListener(clickListener);
        ibtnSearch.setOnClickListener(clickListener);
        rlSearchBar.setOnClickListener(clickListener);
        new CustomActionBar.PropertyListingMenu(PropertyListing.this,callingActivity);
        setSearchHistory();
//        getProperty();
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ibtn_search:
                    Opener.Search(PropertyListing.this,callingActivity);
                    break;
                case R.id.rlSearchBar:
                    Opener.Search(PropertyListing.this,callingActivity);
                    break;
            }
        }
    };

    private void initPropertyList() {
        if (PropertyLists.alPropertyListInfo != null)
//            PropertyLists.alPropertyListInfo.clear();
            try {
                JSONObject joResult = new JSONObject(result);
                String code, msg;
                code = joResult.getString("code");
                msg = joResult.getString("msg");
                if (code.equals("0001")) {
                    JSONArray jaList = joResult.getJSONArray("list");
                    if (jaList.length() > 0) {
                        pref.setKeyValues("searchPostCodeIds", SearchParams.postCodeId);
                        pref.setKeyValues("searchSuburbNames", SearchParams.suburbNames);
                        System.out.print("Testing search==" + SearchParams.postCodeId + "----------" + SearchParams.suburbNames);
                        PropertyLists.alPropertyListInfo = new ArrayList<>();
                        for (int i = 0; i < jaList.length(); i++) {
                            JSONObject joList = jaList.getJSONObject(i);
                            PropertyListInfo propertyListInfo = new PropertyListInfo();
                            propertyListInfo.id = joList.getString("id");
                            propertyListInfo.agentId = joList.getString("agent_id");
                            propertyListInfo.agencyId = joList.getString("agency_id");
                            propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
                            propertyListInfo.agentFName = joList.getString("agent_name");
                            propertyListInfo.agentLName = joList.getString("agent_name");
                            propertyListInfo.propertyStatus = joList.getInt("property_status");
                            propertyListInfo.statusTag = "";
                            propertyListInfo.dateTag = "Auction due to 40 days";
                            propertyListInfo.setFav(joList.getString("favorite"));
                            propertyListInfo.auctionDate = joList.getString("auction_date");
                            propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
                            propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
                            propertyListInfo.nosOfParking = joList.getString("parking");
                            propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(joList.getString("price")));
                            propertyListInfo.propertyStatusIdentifier = "from";
                            propertyListInfo.price_text=joList.getString("price_text");
                            propertyListInfo.suburb = joList.getString("suburb");
                            propertyListInfo.postCode = joList.getString("postcode");
                            propertyListInfo.propertyTitle = joList.getString("property_title");
                            propertyListInfo.propertyDesc = joList.getString("property_description");
                            if (joList.has("other_features"))
                                propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
                            propertyListInfo.street = joList.getString("street");
                            propertyListInfo.propertyState = joList.getString("state");
                            propertyListInfo.propertyImgUrl = joList.getString("property_image");
                            propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
                            propertyListInfo.distance = joList.getString("distance");
                            propertyListInfo.lat = joList.getString("lat");
                            propertyListInfo.lng = joList.getString("lng");
                            propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
                            propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
                            propertyListInfo.property_images_more = new ArrayList<>();
                            JSONArray JPropertyImageMore = joList.getJSONArray("property_images_more");
                            if (JPropertyImageMore.length() > 0) {
                                for (int j = 0; j < JPropertyImageMore.length(); j++) {
                                    propertyListInfo.property_images_more.add(JPropertyImageMore.getString(j));
                                }
                            }

                            PropertyLists.alPropertyListInfo.add(propertyListInfo);
                        }
                        rvPropertyListing.setAdapter(new PropertyListingAdapter(this, PropertyLists.alPropertyListInfo));
                        rvPropertyListing.setAdapter(new PropertyListingAdapter(this, PropertyLists.alPropertyListInfo));
                    }
                } else {
                    alerts.showOkMessage(msg);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    private void initFakePropertyList() {
        PropertyLists.alPropertyListInfo = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            PropertyListInfo propertyListInfo = new PropertyListInfo();
            propertyListInfo.agentAgencyLogoUrl = "";
            propertyListInfo.agentFName = "Rabin";
            propertyListInfo.agentLName = "Shrestha";
            propertyListInfo.statusTag = "New";
            propertyListInfo.dateTag = "Auction start due date 40 days.";
            propertyListInfo.setFav("1");
            propertyListInfo.nosOfBathroom = "4";
            propertyListInfo.nosOfBedroom = "4";
            propertyListInfo.nosOfParking = "2";
            propertyListInfo.price = "$142,345,678";
            propertyListInfo.propertyStatusIdentifier = "from";
            propertyListInfo.suburb = "Kingsford";
            propertyListInfo.propertyState = "North Sydney";
            propertyListInfo.propertyImgUrl = "";
            propertyListInfo.lat = "35.3080";
            propertyListInfo.lng = "149.1245";
            propertyListInfo.id = "1";
            propertyListInfo.agentId = "23";
            propertyListInfo.postCode = "1542";
            propertyListInfo.propertyTitle = "This is my property title";
            propertyListInfo.propertyDesc = "This is my property description and it is very long. This is just a test and very very long.";
            propertyListInfo.street = "Property test";
            propertyListInfo.propertyState = "State";
            propertyListInfo.agentProfileImg = "";
            propertyListInfo.distance = "152";
            propertyListInfo.inspectionTimeString = "[\n" +
                    "                                    {\n" +
                    "                                        \"id\": \"4\",\n" +
                    "                                        \"property_id\": \"3026\",\n" +
                    "                                        \"inspection_date\": \"2016-02-13\",\n" +
                    "                                        \"inspection_time_from\": \"12:00:00\",\n" +
                    "                                        \"inspection_time_to\": \"12:45:00\"\n" +
                    "                                    },\n" +
                    "                                    {\n" +
                    "                                        \"id\": \"5\",\n" +
                    "                                        \"property_id\": \"3026\",\n" +
                    "                                        \"inspection_date\": \"2016-02-14\",\n" +
                    "                                        \"inspection_time_from\": \"12:00:00\",\n" +
                    "                                        \"inspection_time_to\": \"12:45:00\"\n" +
                    "                                    }\n" +
                    "                                ]";


            PropertyLists.alPropertyListInfo.add(propertyListInfo);

        }

    }

    private void initPropertyList(String result) {

        try {
            JSONObject joResult = new JSONObject(result);
            String code, msg;
            code = joResult.getString("code");
            msg = joResult.getString("msg");
            String[] images = new String[2];
            images[0] = joResult.getString("discover_main_image");
            images[1] = joResult.getString("discover_trending_image");
            if (code.equals("0001")) {
                JSONArray jaList = joResult.getJSONArray("list");
                if (jaList.length() > 0) {
                    alPropertyListInfo = new ArrayList<>();
                    for (int i = 0; i < jaList.length(); i++) {
                        JSONObject joList = jaList.getJSONObject(i);
                        PropertyListInfo propertyListInfo = new PropertyListInfo();
                        propertyListInfo.id = joList.getString("id");
                        propertyListInfo.agentId = joList.getString("agent_id");
                        propertyListInfo.agencyId = joList.getString("agency_id");
                        propertyListInfo.auctionDate = joList.getString("auction_date");
                        propertyListInfo.agentAgencyLogoUrl = joList.getString("agency_logo_url");
                        propertyListInfo.agentFName = joList.getString("agent_name");
                        propertyListInfo.agentLName = joList.getString("agent_name");
                        propertyListInfo.propertyStatus = joList.getInt("property_status");
                        propertyListInfo.statusTag = "";
                        propertyListInfo.dateTag = "Auction due to 40 days";
                        propertyListInfo.setFav(joList.getString("favorite"));
                        propertyListInfo.nosOfBathroom = joList.getString("bathrooms");
                        propertyListInfo.nosOfBedroom = joList.getString("bedrooms");
                        propertyListInfo.nosOfParking = joList.getString("parking");
                        propertyListInfo.price = "$" + NumberFormat.getNumberInstance(Locale.US).format((int) Double.parseDouble(joList.getString("price")));
                        propertyListInfo.propertyStatusIdentifier = "from";
                        propertyListInfo.suburb = joList.getString("suburb");
                        propertyListInfo.postCode = joList.getString("postcode");
                        propertyListInfo.propertyTitle = joList.getString("property_title");
                        propertyListInfo.propertyDesc = joList.getString("property_description");
                        propertyListInfo.propertyOtherFeatures = joList.getString("other_features");
                        propertyListInfo.street = joList.getString("street");
                        propertyListInfo.propertyState = joList.getString("state");
                        propertyListInfo.propertyImgUrl = joList.getString("property_image");
                        propertyListInfo.agentProfileImg = joList.getString("agent_profile_image");
                        propertyListInfo.distance = joList.getString("distance");
                        propertyListInfo.lat = joList.getString("lat");
                        propertyListInfo.lng = joList.getString("lng");
                        propertyListInfo.inspectionTimeString = joList.getString("inspection_times");
                        propertyListInfo.registeredForProperty = joList.getString("registered_for_property");
                        alPropertyListInfo.add(propertyListInfo);
                    }
                    PropertyLists.alPropertyListInfo = alPropertyListInfo;
                    rvPropertyListing.setAdapter(new PropertyListingAdapter(this, PropertyLists.alPropertyListInfo));
                }
            } else {
                alerts.showOkMessage(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setSearchHistory() {
        pref = new SharedPreference(this);
        String searched_suburb = pref.getStringValues(CommonDef.SharedPrefKeys.Search_SuburbNames);
        System.out.print("Searched suburb" + searched_suburb);
        List<String> list = new ArrayList<String>(Arrays.asList(searched_suburb.split(",")));
        tvSearchSuburb.setText(getSearchedNames(list));
    }

    private String getSearchedNames(List<String> list)
    {
        int no_of_search=0;
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<list.size();i++)
        {
            if(i==0)
            {
                sb.append(list.get(i));
            }
            else
            {
                no_of_search++;
            }
        }
        if(list.size()==1)
        {
            return sb.toString();
        }
        else {
            sb.append(" and " + no_of_search + " more");
            return sb.toString();
        }
    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_DISCOVER_PROPERTYLIST:
                System.out.println("Result " + result);
                initPropertyList(result);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        MenuActivity.selectedPos=1;
        if (gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
            isLocationSet = true;
        } else {
            gps.showSettingsAlert();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gps.stopUsingGPS();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gps.stopUsingGPS();
    }

}

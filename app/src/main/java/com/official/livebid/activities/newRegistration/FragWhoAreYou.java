package com.official.livebid.activities.newRegistration;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.official.livebid.R;
import com.official.livebid.Views.CustomEditText;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonDef;

import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragWhoAreYou.OnWhoRUListener} interface
 * to handle interaction events.
 */
public class FragWhoAreYou extends Fragment implements View.OnClickListener {
    CustomEditText ctFirstName, ctLastName, ctEmail, ctMobile;
    Button btnNext;
    UserModel user;
    private OnWhoRUListener mListener;
    RegType regType;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String mobileNo;
    private CheckBox chkFreeLancer;
    private boolean isFreelancer = false;
    Alerts alerts;


    public FragWhoAreYou() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frag_who_are_you, container, false);
        ctFirstName = (CustomEditText) v.findViewById(R.id.cv_first_name);
        ctLastName = (CustomEditText) v.findViewById(R.id.cv_last_name);
        ctEmail = (CustomEditText) v.findViewById(R.id.cv_email);
        ctMobile = (CustomEditText) v.findViewById(R.id.cv_mobile_no);
        chkFreeLancer = (CheckBox) v.findViewById(R.id.chk_freelancer);
        btnNext = (Button) v.findViewById(R.id.btn_next_who);
        alerts = new Alerts(getActivity());
        btnNext.setOnClickListener(this);
        if (getArguments() != null) {
            regType = (RegType) getArguments().getSerializable(CommonDef.REG_TYPE);
        }
        if (regType == RegType.AGENT) {
            ctFirstName.setLable("Name");
            ctLastName.setVisibility(View.GONE);
            chkFreeLancer.setVisibility(View.GONE);
        }
        ctFirstName.addTextWatcher(tw);
        ctLastName.addTextWatcher(tw);
        ctMobile.addTextWatcher(tw);
        ctEmail.addTextWatcher(tw);


        return v;
    }


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnWhoRUListener) {
            mListener = (OnWhoRUListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next_who:
                if (isValidFormField(regType)) {
                    mListener.onGetUserData(new UserModel(firstName, lastName, emailAddress, mobileNo, chkFreeLancer.isChecked()));
                }

                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnWhoRUListener {
        // TODO: Update argument type and name
        void onGetUserData(UserModel userModel);
    }

    private TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            enableRegistration();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void enableRegistration() {
        getformField();
        if (isValidForm()) {
            btnNext.setEnabled(true);
        } else {
            btnNext.setEnabled(false);
        }
    }

    private boolean isValidForm() {
        if (regType == RegType.USER) {
            return !emailAddress.isEmpty()
                    || !mobileNo.isEmpty()
                    || !firstName.isEmpty()
                    || !lastName.isEmpty();
        } else {
            return !emailAddress.isEmpty()
                    || !mobileNo.isEmpty()
                    || !firstName.isEmpty();
        }
    }

    private void validateForm() {

        if (!ctMobile.getText().isEmpty() && !isMobileValid(ctMobile.getText()))
            ctMobile.setInvalid();
        else {
            ctMobile.setValid();
        }

        if (!ctEmail.getText().isEmpty() && !isEmailValid(ctEmail.getText()))
            ctEmail.setInvalid();
        else {
            ctEmail.setValid();
        }
    }

    private boolean isMobileValid(String mobile) {
        final Pattern phonePattern = Pattern.compile("^(?:\\(?(?:\\+?61|0)4\\)?(?:[ -]?[0-9]){7}[0-9]$)");
        return phonePattern.matcher(mobile).matches();

    }

    private boolean isEmailValid(String emailAddress) {
        return !emailAddress.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress.trim()).matches(); // 2.2 froyo and above
    }

    private void getformField() {

        firstName = ctFirstName.getText().toString().trim();
        lastName = ctLastName.getText().toString().trim();
        emailAddress = ctEmail.getText().toString().trim();
        mobileNo = ctMobile.getText().toString().trim();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save the fragment's state here
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (NewRegistration.userModel != null) {
            ctFirstName.setText(NewRegistration.userModel.getFirstName());
            ctLastName.setText(NewRegistration.userModel.getLastName());
            ctEmail.setText(NewRegistration.userModel.getEmail());
            ctMobile.setText(NewRegistration.userModel.getMobile());
//            chkFreeLancer.setChecked(NewRegistration.userModel.isFreeLancer()
//            );
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Restore the fragment's state here

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onStop() {
        super.onPause();
    }

    private boolean isValidFormField(RegType regType) {
        getformField();
        if (regType == RegType.USER) {
            if (firstName.isEmpty()) {
                alerts.showOkMessage("First name is empty.");
                return false;
            } else if (lastName.isEmpty()) {
                alerts.showOkMessage("Last name is empty.");
                return false;
            } else if (emailAddress.isEmpty()) {
                alerts.showOkMessage("Email address is empty.");
                return false;
            } else if (!isEmailValid(emailAddress)) {
                alerts.showOkMessage("Email address is invalid.");
                return false;
            } else if (mobileNo.isEmpty()) {
                alerts.showOkMessage("Mobile number is empty.");
                return false;
            } else if (!isMobileValid(mobileNo)) {
                alerts.showOkMessage("Mobile number is invalid.");
                return false;
            }
        } else {
            if (firstName.isEmpty()) {
                alerts.showOkMessage("Name is empty.");
                return false;
            } else if (emailAddress.isEmpty()) {
                alerts.showOkMessage("Email address is empty.");
                return false;
            } else if (!isEmailValid(emailAddress)) {
                alerts.showOkMessage("Email address is invalid.");
                return false;
            } else if (mobileNo.isEmpty()) {
                alerts.showOkMessage("Mobile number is empty.");
                return false;
            } else if (!isMobileValid(mobileNo)) {
                alerts.showOkMessage("Mobile number is invalid.");
                return false;
            }
        }
        return true;
    }
}

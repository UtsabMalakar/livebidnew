package com.official.livebid.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.official.livebid.R;
import com.official.livebid.adapters.CommonSpinnerAdapter;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.asyncTasks.VolleyRequest;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.CustomActionBar;
import com.official.livebid.helpers.Opener;
import com.official.livebid.helpers.UrlHelper;
import com.official.livebid.interfaces.AsyncInterface;
import com.official.livebid.objects.EnquiryTypeModel;
import com.official.livebid.staticClasses.ContactAgencyModal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class ContactAgency extends AppCompatActivity implements View.OnClickListener, AsyncInterface,AdapterView.OnItemSelectedListener {
    Spinner spEnquiryType;
    private CommonSpinnerAdapter enquiryTypeAdapter;
    private ArrayList<EnquiryTypeModel> enquiryTypes;
    LinearLayout llSelectAgent;
    LinearLayout rl_main_view;

    private String agencyId = "1";
    private String agentId = "0";
    private String agentName = "";
    EditText etSelectAgent;
    LinearLayout llMessage;
    LinearLayout llEnquiryType;
    EditText etMessage;
    TextView tvMessageCount;
    EditText cvFullName, cvMobileNumber, cvEmailAddress;
    private RadioGroup rgContactType;
    private RadioButton rbTypeEmail, rbTypeCall;
    Button btnSubmit;
    private Alerts alerts;
    private CustomActionBar.AgentPublicProfileTitleBar titlebar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_agency);
        getSupportActionBar().hide();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            agencyId = b.getString("agency_id");
        }
        alerts = new Alerts(this);
        titlebar = new CustomActionBar.AgentPublicProfileTitleBar(this, "Contact Agent");
        rl_main_view = (LinearLayout) findViewById(R.id.rl_main_view);
        CommonMethods.setupUI(rl_main_view, this);
        enquiryTypes = new ArrayList<>();
        llEnquiryType = (LinearLayout) findViewById(R.id.ll_enquiryType);
        spEnquiryType = (Spinner) findViewById(R.id.sp_enquiry_type);
        etSelectAgent = (EditText) findViewById(R.id.et_select_agent);
        llMessage = (LinearLayout) findViewById(R.id.ll_message);
        etMessage = (EditText) findViewById(R.id.et_message);
        tvMessageCount = (TextView) findViewById(R.id.tv_message_count);
        cvFullName = (EditText) findViewById(R.id.cv_full_name);
        cvMobileNumber = (EditText) findViewById(R.id.cv_mobile_no);
        cvEmailAddress = (EditText) findViewById(R.id.cv_email);
        rgContactType = (RadioGroup) findViewById(R.id.rg_contact_type);
        rbTypeEmail = (RadioButton) findViewById(R.id.rb_type_email);
        rbTypeCall = (RadioButton) findViewById(R.id.rb_type_call);
        btnSubmit = (Button) findViewById(R.id.btn_submit_enquiry);

        etSelectAgent.addTextChangedListener(tw);
        etMessage.addTextChangedListener(tw);
        cvFullName.addTextChangedListener(tw);
        cvMobileNumber.addTextChangedListener(tw);
        cvEmailAddress.addTextChangedListener(tw);

        llSelectAgent = (LinearLayout) findViewById(R.id.ll_select_agent);
        llSelectAgent.setOnClickListener(this);
        etSelectAgent.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                String comments = etMessage.getText().toString();
                tvMessageCount.setText(comments.length() + "/500");
                if (comments.length() > 500) {
                    etMessage.setText(comments.substring(0, 500));

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                enableSubmit();
            }
        });
        getEnquiryType();
        spEnquiryType.setOnItemSelectedListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        setFormData();
        enableSubmit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_agency, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_select_agent:
            case R.id.et_select_agent:
//                getFormData();
                Opener.SelectAgent(ContactAgency.this, agencyId);
                break;
            case R.id.btn_submit_enquiry:
                sendEnquiry();
//                if (isValid()) {
//                    sendEnquiry();
//                }
                break;
        }
    }

    private Boolean isValid() {
//        int count = 0;
//        if (spEnquiryType.getSelectedItemPosition() == 0)
//            llEnquiryType.setBackground(getResources().getDrawable(R.drawable.layout_rounded_error));
//        else {
//            llEnquiryType.setBackground(getResources().getDrawable(R.drawable.layout_rounded));
//            count++;
//        }
//        if (cvFullName.getText().isEmpty())
//            cvFullName.setInvalid();
//        else {
//            cvFullName.setValid();
//            count++;
//        }

        if (spEnquiryType.getSelectedItemPosition() == 0)
        {
            alerts.showOkMessage(getResources().getString(R.string.err_enquiry_type));
            return false;
        }
//            llEnquiryType.setBackground(getResources().getDrawable(R.drawable.layout_rounded_error));

        if(cvFullName.getText().toString().isEmpty())
        {
            alerts.showOkMessage(getResources().getString(R.string.err_fullname_empty));
            return false;
        }

        if (!cvMobileNumber.getText().toString().isEmpty())
        {
            if(!isMobileValid(cvMobileNumber.getText().toString()))
            {
                alerts.showOkMessage(getResources().getString(R.string.err_valid_phone));
                return false;
            }
        }
//            cvMobileNumber.setInvalid();
        else
        {
            alerts.showOkMessage(getResources().getString(R.string.err_phoneno_empty));
            return false;
        }

        if (!cvEmailAddress.getText().toString().isEmpty())
        {
            if(!isEmailValid(cvEmailAddress.getText().toString()))
            {
                alerts.showOkMessage(getResources().getString(R.string.err_valid_email));
                return false;
            }
        }

        else {
            alerts.showOkMessage(getResources().getString(R.string.err_email_empty));
            return false;
        }
        return true;
    }

    private boolean isEmailValid(String emailAddress) {

        return !emailAddress.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress.trim()).matches(); // 2.2 froyo and above

    }


    private boolean isMobileValid(String mobile) {
//        isMibileOk = mobile.length() >= 9 ? true : false;
//        final Pattern phonePattern = Pattern.compile("^(?:\\+?61|0)4 ?(?:(?:[01] ?[0-9]|2 ?[0-57-9]|3 ?[1-9]|4 ?[7-9]|5 ?[018]) ?[0-9]|3 ?0 ?[0-5])(?: ?[0-9]){5}$");
        final Pattern phonePattern = Pattern.compile("^(?:\\(?(?:\\+?61|0)4\\)?(?:[ -]?[0-9]){7}[0-9]$)");
        return phonePattern.matcher(mobile).matches();

    }

    private void getFormData() {
        ContactAgencyModal.enquiryTypeId = spEnquiryType.getSelectedItemPosition();
        ContactAgencyModal.agentId = agentId;
        ContactAgencyModal.agentName = etSelectAgent.getText().toString();
        ContactAgencyModal.message = etMessage.getText().toString();
        ContactAgencyModal.fullName = cvFullName.getText().toString();
        ContactAgencyModal.mobileNumber = cvMobileNumber.getText().toString();
        ContactAgencyModal.emailAddress = cvEmailAddress.getText().toString();
        ContactAgencyModal.contactType = rbTypeEmail.isChecked() ? 1 : 2; //email=1,phoneCall=2
    }

    private void setFormData() {
        spEnquiryType.setSelection(ContactAgencyModal.enquiryTypeId);
        agentId = ContactAgencyModal.agentId;
        etSelectAgent.setText(ContactAgencyModal.agentName);
        etMessage.setText(ContactAgencyModal.message);
        cvFullName.setText(ContactAgencyModal.fullName);
        cvMobileNumber.setText(ContactAgencyModal.mobileNumber);
        cvEmailAddress.setText(ContactAgencyModal.emailAddress);
        if (ContactAgencyModal.contactType == 1)
            rbTypeEmail.setChecked(true);
        else
            rbTypeCall.setChecked(true);
    }

    public void getEnquiryType() {
        VolleyRequest volleyRequest = new VolleyRequest(ContactAgency.this, Request.Method.GET, UrlHelper.GET_ENQUIRY_TYPE, CommonDef.REQUEST_ENQUIRY_TYPE);
        volleyRequest.asyncInterface = this;
        volleyRequest.request();

    }

    @Override
    public void processFinish(String result, int requestCode) {
        switch (requestCode) {
            case CommonDef.REQUEST_ENQUIRY_TYPE:
                System.out.println("Enquiry type=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
//                        alerts.showOkMessage(msg);
                        JSONArray list = joResult.getJSONArray("list");
                        enquiryTypes.add(new EnquiryTypeModel("0", "Select an option"));
                        for (int i = 0; i < list.length(); i++) {
                            enquiryTypes.add(new EnquiryTypeModel(
                                    list.getJSONObject(i).getString("id"),
                                    list.getJSONObject(i).getString("title")
                            ));
                        }
                        enquiryTypeAdapter = new CommonSpinnerAdapter(this, enquiryTypes);
                        spEnquiryType.setAdapter(enquiryTypeAdapter);


                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;
            case CommonDef.REQUEST_SEND_ENQUIRY:
                System.out.println("Enquiry result=>" + result);
                try {
                    JSONObject joResult = new JSONObject(result);
                    String msg, code;
                    code = joResult.getString("code");
                    msg = joResult.getString("msg");
                    if (code.equals("0001")) {
                        ContactAgencyModal.clearContactAgencyModal();
                        alerts.OkSendEmail(msg);


                    } else {
                        alerts.showOkMessage(msg);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    alerts.showOkMessage(e.getMessage());

                }

                break;

        }
    }

    private void enableSubmit() {
//        isValid();
        if (spEnquiryType.getSelectedItemPosition() > 0
//                || !etSelectAgent.getText().toString().isEmpty()
                || !etMessage.getText().toString().isEmpty()
                || !cvFullName.getText().toString().isEmpty()
                || !cvMobileNumber.getText().toString().isEmpty()
                || !cvEmailAddress.getText().toString().isEmpty()
                ) {
            btnSubmit.setEnabled(true);

        } else {
            btnSubmit.setEnabled(false);
        }

    }

    private TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            enableSubmit();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void sendEnquiry() {
        if(isValid()) {
            HashMap<String, String> headers = CommonMethods.getHeaders(ContactAgency.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("listing_type", "2");
            params.put("user_id", agencyId);
            params.put("enquiry_type", enquiryTypeAdapter.getId(spEnquiryType.getSelectedItemPosition()));
            params.put("agent_id", agentId);
            params.put("message", etMessage.getText().toString());
            params.put("full_name", cvFullName.getText().toString());
            params.put("mobile_number", cvMobileNumber.getText().toString());
            params.put("email_address", cvEmailAddress.getText().toString());
            params.put("preferred_contact_method", rbTypeEmail.isChecked() ? "1" : "2");
            System.out.println("agency enquiry=>" + params.toString());
            VolleyRequest volleyRequest = new VolleyRequest(ContactAgency.this, Request.Method.POST, params, UrlHelper.SEND_ENQUIRY, headers, false, CommonDef.REQUEST_SEND_ENQUIRY);
            volleyRequest.asyncInterface = this;
            volleyRequest.request();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        enableSubmit();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

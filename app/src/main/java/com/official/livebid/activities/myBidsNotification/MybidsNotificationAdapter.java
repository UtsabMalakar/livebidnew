package com.official.livebid.activities.myBidsNotification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.official.livebid.R;
import com.official.livebid.activities.myBids.MyBidsPropertyViewHolder;
import com.official.livebid.utils.GrayscaleTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ics on 6/27/2016.
 */
public class MybidsNotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<MyBidsPropertyModel> myBids;
    private Context context;

    public MybidsNotificationAdapter(Context context, ArrayList<MyBidsPropertyModel> myBids) {
        this.context = context;
        this.myBids = myBids;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_list_item, parent, false);
        return new MyBidItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyBidsPropertyModel myBid=myBids.get(position);
        setBids(holder, myBid);

    }

    private void setBids(RecyclerView.ViewHolder holder, MyBidsPropertyModel myBid) {
        MyBidItemViewHolder h= (MyBidItemViewHolder) holder;
        h.tvNotifiedDate.setVisibility(View.GONE);
        if(!myBid.getPropertyImage().trim().isEmpty()){
            Picasso.with(context)
                    .load(myBid.getPropertyImage())
                    .transform(new GrayscaleTransformation())
                    .placeholder(R.mipmap.img_property_placeholder)
                    .into(h.ivNotificationImage);
        }
        else {
            Picasso.with(context)
                    .load(R.drawable.img_coming_soon)
                    .transform(new GrayscaleTransformation())
                    .placeholder(R.mipmap.img_property_placeholder)
                    .into(((MyBidItemViewHolder) holder).ivNotificationImage);
        }
        h.tvNotificationDesc.setText(myBid.getMessage());



    }

    @Override
    public int getItemCount() {
        return myBids.size();
    }

    public static class MyBidItemViewHolder extends RecyclerView.ViewHolder {
        View rootView;
        ImageView ivNotificationImage;
        TextView tvNotificationDesc, tvNotifiedDate;

        public MyBidItemViewHolder(View itemView) {
            super(itemView);
            rootView=itemView;
            ivNotificationImage= (ImageView) itemView.findViewById(R.id.iv_notification_img);
            tvNotificationDesc= (TextView) itemView.findViewById(R.id.tv_notification);
            tvNotifiedDate= (TextView) itemView.findViewById(R.id.tv_notification_time);
        }
    }

}

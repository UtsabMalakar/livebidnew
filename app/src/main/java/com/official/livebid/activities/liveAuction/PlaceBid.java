package com.official.livebid.activities.liveAuction;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.official.livebid.R;
import com.official.livebid.helpers.CommonDef;
import com.official.livebid.helpers.CustomActionBar;

import java.text.NumberFormat;
import java.util.Locale;

public class PlaceBid extends AppCompatActivity implements View.OnClickListener{
    EditText etBidAmount;
    Button btnPlaceBid1k;
    Button btnPlaceBid5k;
    Button btnPlaceBid10k;
    Button btnPlaceBid15k;
    Button btnPlaceBid25k;
    Button btnPlaceBid50k;
    private Button btnConfirmBidBid;
    private long bidAmount=0;
    private long latestBidAmount=0;

    private int addedBidAmount=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_bid);
        init();
    }

    private void init() {
        getSupportActionBar().hide();
        new CustomActionBar.PlaceBidCAB(PlaceBid.this);
        Bundle b=getIntent().getExtras();
        if(b!=null){
            latestBidAmount=b.getLong(CommonDef.LATEST_BID);
        }
        etBidAmount= (EditText) findViewById(R.id.et_bid_amount);
        btnConfirmBidBid= (Button) findViewById(R.id.btn_confirm_bid);
        btnConfirmBidBid.setEnabled(false);
        btnPlaceBid1k= (Button) findViewById(R.id.btn_price_tag_1k);
        btnPlaceBid1k.setTag(1000);
        btnPlaceBid5k= (Button) findViewById(R.id.btn_price_tag_5k);
        btnPlaceBid5k.setTag(5000);
        btnPlaceBid10k= (Button) findViewById(R.id.btn_price_tag_10k);
        btnPlaceBid10k.setTag(10000);
        btnPlaceBid15k= (Button) findViewById(R.id.btn_price_tag_15k);
        btnPlaceBid15k.setTag(15000);
        btnPlaceBid25k= (Button) findViewById(R.id.btn_price_tag_25k);
        btnPlaceBid25k.setTag(25000);
        btnPlaceBid50k= (Button) findViewById(R.id.btn_price_tag_50k);
        btnPlaceBid50k.setTag(50000);
        etBidAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {
                    etBidAmount.removeTextChangedListener(this);
                    String cleanString = s.toString().replaceAll("[$,]", "");


                    String formatted = "";
                    if (cleanString.equalsIgnoreCase("") || cleanString.equalsIgnoreCase("0"))
                        formatted = "";
                    else {
                        bidAmount = Long.parseLong(cleanString);
                        formatted = "$" + NumberFormat.getNumberInstance(Locale.US).format((bidAmount));
                    }
                    current = formatted;
                    etBidAmount.setText(formatted);
                    etBidAmount.setSelection(formatted.length());

                    etBidAmount.addTextChangedListener(this);
                    resetPriceTag();
                    enableConfirmBid();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnConfirmBidBid.setOnClickListener(this);
        btnPlaceBid1k.setOnClickListener(this);
        btnPlaceBid5k.setOnClickListener(this);
        btnPlaceBid10k.setOnClickListener(this);
        btnPlaceBid15k.setOnClickListener(this);
        btnPlaceBid25k.setOnClickListener(this);
        btnPlaceBid50k.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right);
        Intent intent=new Intent();
        setResult(RESULT_CANCELED,intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_price_tag_1k:
            case R.id.btn_price_tag_5k:
            case R.id.btn_price_tag_10k:
            case R.id.btn_price_tag_15k:
            case R.id.btn_price_tag_25k:
            case R.id.btn_price_tag_50k:
                resetPriceTag();
                setPriceTage(view);
                break;

        }

    }

    private void resetTextField() {
        etBidAmount.setText("");
        bidAmount=0;
    }

    private void setPriceTage(View view) {
        addedBidAmount=(int)view.getTag();
        etBidAmount.setText(latestBidAmount+addedBidAmount+"");
        view.setBackground(getResources().getDrawable(R.drawable.rounded_button_active));
    }

    private void resetPriceTag() {
        btnPlaceBid1k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid5k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid10k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid15k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid25k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
        btnPlaceBid50k.setBackground(getResources().getDrawable(R.drawable.rounded_button_inactive));
    }

    public void enableConfirmBid(){
        if(!etBidAmount.getText().toString().isEmpty()  )
            btnConfirmBidBid.setEnabled(true);
        else
            btnConfirmBidBid.setEnabled(false);
    }
}

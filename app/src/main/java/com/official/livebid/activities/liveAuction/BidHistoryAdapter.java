package com.official.livebid.activities.liveAuction;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.official.livebid.R;
import com.official.livebid.alerts.Alerts;
import com.official.livebid.helpers.CommonMethods;
import com.official.livebid.helpers.DateTimeHelper;
import com.official.livebid.helpers.FavoriteHelper;
import com.official.livebid.helpers.ImageSlideAdapter;
import com.official.livebid.listeners.FavouriteCompleteListener;

import java.util.ArrayList;

/**
 * Created by Ics on 4/8/2016.
 */
public class BidHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private SoldPropertyModel property;
    private ArrayList<BidModel> bids;
    final static int ITEM_PROPERTY = 0;
    final static int ITEM_SUMMARY = 1;
    final static int ITEM_BID_HISTORY = 2;
    final static int ITEM_BID = 3;
    final static int ITEM_FOOTER = 4;
    private IBidHistory delegate;
    private SummaryReportType summaryReport;
    BidHistoryFooterHolder bf;
    boolean isLastPage;


    public BidHistoryAdapter(Context context, SoldPropertyModel property, ArrayList<BidModel> bids, IBidHistory delegate, SummaryReportType summaryReport, boolean isLastPage) {
        this.context = context;
        this.property = property;
        this.bids = bids;
        this.delegate = delegate;
        this.summaryReport = summaryReport;
        this.isLastPage = isLastPage;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return ITEM_PROPERTY;
        else if (position == 1)
            return ITEM_SUMMARY;
        else if (position == 2)
            return ITEM_BID_HISTORY;
        else if (position == bids.size()+3)
            return ITEM_FOOTER;
        else
            return ITEM_BID;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == ITEM_PROPERTY) {
            view = LayoutInflater.from(context).inflate(R.layout.property_header, parent, false);
            return new PropertyHolder(view);
        } else if (viewType == ITEM_SUMMARY) {
            view = LayoutInflater.from(context).inflate(R.layout.layout_sold_property_summary, parent, false);
            return new SoldSummaryHolder(view);
        } else if (viewType == ITEM_BID_HISTORY) {
            view = LayoutInflater.from(context).inflate(R.layout.layout_bid_history_header, parent, false);
            return new BidHistoryHeaderHolder(view);
        } else if (viewType == ITEM_FOOTER) {
            view = LayoutInflater.from(context).inflate(R.layout.layout_bid_history_footer, parent, false);
            return new BidHistoryFooterHolder(view);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.layout_bid_history_list_item, parent, false);
            return new BidItemHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PropertyHolder) {
            setProperty(holder);
            return;
        }
        if (holder instanceof SoldSummaryHolder) {
            setSummary(holder);
            return;
        }
        if (holder instanceof BidHistoryHeaderHolder) {
            setBidHistoryHeader(holder);
            return;
        }
        if (holder instanceof BidHistoryFooterHolder) {
            setBidHistoryFooter(holder);
            return;
        }
        if (holder instanceof BidItemHolder) {
            setBidHistoryList(holder, position - 3);
        }

    }

    private void setBidHistoryList(RecyclerView.ViewHolder holder, int pos) {
        BidItemHolder bl = (BidItemHolder) holder;
        BidModel bid = bids.get(pos);
        bl.tvBidAmount.setText(bid.bidAmount);
        if (bid.bidType == CurrentBidModel.MessageType.vendorBid) {
            bl.tvBidderId.setText(Html.fromHtml("Vendor Bid"));
            bl.messageContainer.setBackgroundColor(Color.WHITE);
        }
        else if (bid.bidType == CurrentBidModel.MessageType.onSiteBid) {
//            bl.tvBidderId.setText(Html.fromHtml("<b>On-Site Bidder</b>"));
            bl.tvBidderId.setText(bid.bidderId);
            bl.messageContainer.setBackgroundColor(Color.WHITE);
        }
        else {
            if (bid.isMyBid()) {
                bl.tvBidderId.setText(Html.fromHtml("<b>You</b>"));
                bl.messageContainer.setBackgroundColor(context.getResources().getColor(R.color.self_highlight));
            } else {
                bl.tvBidderId.setText(bid.bidderId);
                bl.messageContainer.setBackgroundColor(Color.WHITE);
            }
        }
    }


    private void setBidHistoryFooter(RecyclerView.ViewHolder holder) {
        bf = (BidHistoryFooterHolder) holder;
        if(isLastPage){
            bf.tvAddMore.setVisibility(View.GONE);
            bf.loading.setVisibility(View.GONE);
            bf.rootView.setVisibility(View.GONE);
        }
        bf.rootView.setOnClickListener(addMoreClickListener);
//        if(bids.size()==0)
//            startLoading();
    }

    private View.OnClickListener addMoreClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startLoading();

        }
    };

    private void setBidHistoryHeader(RecyclerView.ViewHolder holder) {
        BidHistoryHeaderHolder bh = (BidHistoryHeaderHolder) holder;
        bh.tvReportTitle.setText(summaryReport == SummaryReportType.passed_in ? "Auction Passes In" : "Property Sold");
        bh.tvSoldDate.setText(property.soldDate);
        bh.tvSoldTime.setText(property.soldTime);
    }


    private void setSummary(RecyclerView.ViewHolder holder) {
        SoldSummaryHolder sh = (SoldSummaryHolder) holder;
        sh.tvDuration.setText(property.bidDuration);
        sh.tvParticipation.setText(property.participations);
//        sh.tvloremIpsum.setText("Lorem imsum dolor");
    }

    private void setProperty(RecyclerView.ViewHolder holder) {
        final PropertyHolder header = (PropertyHolder) holder;
        header.tvHighestBidTitle.setText("Highest Bid");
        header.tvHighestBidAmount.setText(property.highestBidAmount);
        header.tvHighestBidderTitle.setText("Highest Bidder");
        header.tvHighestBidder.setText(property.highestBidder);
        header.tvPropertyStreet.setText(property.street);
        header.tvPropertySuburbPostcode.setText(property.regionPostcode);
       //viewpager adapter
        ImageSlideAdapter mImageSlideAdapter;
        mImageSlideAdapter = new ImageSlideAdapter(context, property.property_images_more,property.propertyStatus);
        header.mViewPager.setAdapter(mImageSlideAdapter);
        if (property.propertyStatus == 0) {
            if (property.propertyStatus2 == 4) {
                header.tvStatusTag.setVisibility(View.VISIBLE);
                header.tvStatusTag.setText(CommonMethods.getPropertyStatus2(property.propertyStatus2));
                header.tvDateTag.setVisibility(View.GONE);

            } else {
                if (property.propertyStatus2 != 0)
               header.tvStatusTag.setVisibility(View.VISIBLE);
               header.tvDateTag.setVisibility(View.VISIBLE);
               header.tvStatusTag.setText(CommonMethods.getPropertyStatus2(property.propertyStatus2));
               header.tvDateTag.setText(Html.fromHtml(DateTimeHelper.getAuctionDueDateFromTimeStamp(property.auctionDateTimeStamp)));
            }

        } else {
           header.tvDateTag.setVisibility(View.GONE);
           header.tvStatusTag.setVisibility(View.VISIBLE);
           header.tvStatusTag.setText(CommonMethods.getPropertyStatus(property.propertyStatus));
        }

        if (property.propertyStatus == 2) {
            header.tvStatusTag.setVisibility(View.VISIBLE);
            header.tvPropertyImageIndicator.setVisibility(View.GONE);
//                propertyViewHolder.mViewPager.beginFakeDrag();

        }else {
            header.tvStatusTag.setVisibility(View.VISIBLE);
            header.tvPropertyImageIndicator.setVisibility(View.VISIBLE);
            header.tvPropertyImageIndicator.setText(1 + "/" + property.property_images_more.size());
            header.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    header.tvPropertyImageIndicator.setText(position+1 + "/" + property.property_images_more.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


        }
        header.cboxFav.setChecked(property.isFav());
        header.cboxFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CheckBox cboxTemp = (CheckBox) view;
                new FavoriteHelper(context).makeFavorite(property.id, property.getFav(), new FavouriteCompleteListener() {
                    @Override
                    public void onSuccess(boolean b) {
                        property.setFav(b);
                        cboxTemp.setChecked(b);
                    }

                    @Override
                    public void onFailure() {
                        cboxTemp.setChecked(property.getFav());
                    }

                    @Override
                    public void onNotLoggedIn() {
                        cboxTemp.setChecked(false);
                        new Alerts(context).signIn();
                    }
                });

                // Webservice call for change.

            }
        });

        header.tvDateTag.setBackgroundColor(context.getResources().getColor(R.color.theme_red));
        header.tvDateTag.setText(summaryReport == SummaryReportType.sold ? "PROPERTY SOLD" : "PROPERTY PASSED IN");



    }

    @Override
    public int getItemCount() {
        if (bids.size() == 0)
            return 3;
        else
            return bids.size() + 4;
    }

    public void addMoreData(ArrayList<BidModel> newBids, boolean isLastPage) {
        this.isLastPage = isLastPage;
        int startPosition = bids.size() + 4;
        this.bids.addAll(newBids);
        this.notifyItemRangeInserted(startPosition, newBids.size());
        stopLoading();
    }


    public static class PropertyHolder extends RecyclerView.ViewHolder {
        private final TextView tvPropertyImageIndicator;
        private final ViewPager mViewPager;
        private final CheckBox cboxFav;
        protected ImageView ivPropertyImg;

        protected TextView tvStatusTag, tvDateTag, tvPropertyStreet, tvPropertySuburbPostcode,
                tvPropertyStatusIdentifier, tvHighestBidAmount, tvHighestBidder, tvHighestBidderTitle, tvHighestBidTitle;
        protected View rootView;

        public PropertyHolder(View view) {
            super(view);
            rootView = view;
            mViewPager = (ViewPager) view.findViewById(R.id.vp_property_img);
            cboxFav = (CheckBox) view.findViewById(R.id.cbox_fav);
            tvPropertyImageIndicator = (TextView) view.findViewById(R.id.tvPropertyImageIndicator);
            tvStatusTag = (TextView) view.findViewById(R.id.tv_property_status_tag);
            tvDateTag = (TextView) view.findViewById(R.id.tv_property_date_tag);
            tvPropertySuburbPostcode = (TextView) view.findViewById(R.id.tv_property_suburb_postcode);
            tvPropertyStreet = (TextView) view.findViewById(R.id.tv_property_street);
            tvPropertyStatusIdentifier = (TextView) view.findViewById(R.id.tv_property_status_identifier);
            //change acc to need
            tvHighestBidTitle = (TextView) view.findViewById(R.id.tv_reserve_price_title);
            tvHighestBidAmount = (TextView) view.findViewById(R.id.tv_reserve_price);
            tvHighestBidder = (TextView) view.findViewById(R.id.tv_property_no);
            tvHighestBidderTitle = (TextView) view.findViewById(R.id.tv_property_no_title);
        }
    }

    public static class SoldSummaryHolder extends RecyclerView.ViewHolder {
        protected TextView tvDuration;
        protected TextView tvParticipation;
        protected TextView tvloremIpsum;
        protected View rootView;

        public SoldSummaryHolder(View view) {
            super(view);
            rootView = view;
            tvDuration = (TextView) view.findViewById(R.id.tv_duration);
            tvParticipation = (TextView) view.findViewById(R.id.tv_participation);
        }
    }

    public static class BidHistoryHeaderHolder extends RecyclerView.ViewHolder {
        protected View rootView;
        protected TextView tvSoldDate;
        protected TextView tvSoldTime;
        protected TextView tvReportTitle;

        public BidHistoryHeaderHolder(View view) {
            super(view);
            rootView = view;
            tvSoldDate = (TextView) view.findViewById(R.id.tv_date);
            tvSoldTime = (TextView) view.findViewById(R.id.tv_time);
            tvReportTitle = (TextView) view.findViewById(R.id.tv_report_title);
        }
    }

    public static class BidHistoryFooterHolder extends RecyclerView.ViewHolder {
        protected View rootView;
        protected TextView tvAddMore;
        protected ProgressBar loading;

        public BidHistoryFooterHolder(View itemView) {
            super(itemView);
            tvAddMore = (TextView) itemView.findViewById(R.id.tv_add_more);
            loading = (ProgressBar) itemView.findViewById(R.id.progressBar);
            rootView = itemView;
        }
    }

    public static class BidItemHolder extends RecyclerView.ViewHolder {
        protected View rootView;
        protected TextView tvBidAmount;
        protected TextView tvBidderId;
        protected LinearLayout messageContainer;

        public BidItemHolder(View itemView) {
            super(itemView);
            tvBidAmount = (TextView) itemView.findViewById(R.id.tv_bid_amount);
            tvBidderId = (TextView) itemView.findViewById(R.id.tv_bidder_id);
            messageContainer=(LinearLayout)itemView.findViewById(R.id.ll_message_container);
        }

    }


    public interface IBidHistory {
        void onViewMore();

    }

    public void addMore(ArrayList<BidModel> morebids) {
        //  update recycler view
        Toast.makeText(context, "add more", Toast.LENGTH_SHORT).show();
        int startPos = bids.size();
        bids.addAll(morebids);
        notifyItemRangeChanged(startPos, morebids.size());
    }

    public enum SummaryReportType {
        passed_in,
        sold;
    }

    public void startLoading() {
        bf.tvAddMore.setVisibility(View.GONE);
        bf.loading.setVisibility(View.VISIBLE);
        delegate.onViewMore();
        bf.rootView.setOnClickListener(null);
    }

    public void stopLoading() {
        if (bf != null) {
            bf.tvAddMore.setVisibility(View.VISIBLE);
            bf.loading.setVisibility(View.GONE);
            bf.rootView.setOnClickListener(addMoreClickListener);
        }
    }

}

package com.official.livebid.activities.liveAuction;

import android.content.Context;

import com.official.livebid.objects.RoomDetail;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by Ics on 5/12/2016.
 */
public class LiveBidXmlParser {
    private Context context;
    private RoomDetail roomDetail;
    private String text;

    public LiveBidXmlParser(Context context, RoomDetail roomDetail) {
        this.context = context;
        this.roomDetail = roomDetail;
    }

    public LiveBidXmlParser(Context context) {
        this.context = context;
    }

    public CurrentBidModel parseXML(String data) throws XmlPullParserException, IOException {
        CurrentBidModel cb = null;
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();

        xpp.setInput(new StringReader(data));
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String tagName = xpp.getName();
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (tagName.equalsIgnoreCase("body")) {
                        // create a new instance of employee
                        cb = new CurrentBidModel();
                        if (roomDetail != null)
                            cb.startDateTime = roomDetail.startDateTime;

                    }
                    break;
                case XmlPullParser.TEXT:
                    text = xpp.getText();
                    break;
                case XmlPullParser.END_TAG:


                    if (tagName.equalsIgnoreCase("body")) {
                        // add employee object to list
//                        employees.add(employee);
                    } else if (tagName.equalsIgnoreCase("id")) {
                        cb.msgId = text;
                    } else if (tagName.equalsIgnoreCase("property_id")) {
//                        note.from = text;
                    } else if (tagName.equalsIgnoreCase("bid_type")) {
                        cb.setBidType(Integer.parseInt(text));
                    } else if (tagName.equalsIgnoreCase("user_id")) {
                        cb.userId = text;
                    } else if (tagName.equalsIgnoreCase("user_unique_id")) {
                        cb.setUserUniqueId(text);
                    } else if (tagName.equalsIgnoreCase("bid_timestamp")) {
                        cb.setBidTime(Long.parseLong(text));
                    } else if (tagName.equalsIgnoreCase("bid_amount")) {
                        cb.setBidAmount(text);
                    } else if (tagName.equalsIgnoreCase("temp_id")) {
//                        note.msg = text;
                        ;
                    }

                    break;

                default:
                    break;
            }
            eventType = xpp.next();
        }
        return cb;
    }
}
